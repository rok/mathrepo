# Initial Setup

You need to create a local copy of the standard repository for "mathrepo".
Furthermore, you need a recent version of Python, especially not version 2.x. 
You can check your version of Python with
```sh
python --version
```
or
```sh
python3 --version
```

On Linux, you should use the default package manager of your Linux distribution to 
install or update Python to version 3.x, if necessary, e.g.
```sh
apt install python3     # maybe as superuser, i.e. sudo apt install python3
```
on Debian or Ubuntu distributions.

On MacOS, you could use [HomeBrew](https://brew.sh), which simplifies package installation.
```sh
brew install python                                         # install python via homebrew
```

For Windows see the alternative commands below.

Furthermore, you need additional python packages (Sphinx) to be able to build the web
pages from the source files.
Lastly, you might need to install [pandoc](https://pandoc.org/installing.html).

A recommended way is to set up a *virtual environment* for python, which holds
your copy and all needed packages:

```sh
mkdir mathrepo_website                                         # create top directory
cd mathrepo_website                                            # switch to the top directory
pip3 install virtualenv                                        # install virtual environment package
virtualenv -p python3 venv                                     # set up virtual environment for python
```

Then activate the environment via
```sh
source venv/bin/activate
```

or **on Windows**: 

```sh
source venv/Scripts/activate
```

Afterwards install sphinx, theme and needed extensions
```sh
pip3 install sphinx sphinx-rtd-theme nbsphinx                  # install Sphinx and needed theme package and notebook extension
pip3 install gitpython                                         # install gitpython
```
Finally clone the template_branch of the mathrepo repository
```sh
git clone -b template_branch --single-branch https://gitlab.mis.mpg.de/rok/mathrepo.git           # create local of template branch
cd mathrepo                                                    # switch to the repository
git checkout -b newtopic                                       # create and switch to a new branch, 'newtopic' should be the name your project
git push --set-upstream origin newtopic                  # push your new branch to Mathrepo (this does not yet publish anything)
mkdir source/newtopic                                          # make new directory with the same name 

```


If your page was created before January 2025, you have to clone the whole repo to modify it
```sh
git clone https://gitlab.mis.mpg.de/rok/mathrepo.git           # create local copy of repository
cd mathrepo                                                    # switch to the repository
git checkout develop                                           # switch to develop branch
```

# Create a Contribution to the Repository

If you created your page before January 2025, then you will just read and write to the developer branch. Otherwise, you will work on the page branch you have created following the instructions in [Initial Setup](#initial-setup). After your first version of your project is finished, and you made your first merge request, the mathrepo team will do a final check
and add your project to the website. **Please do a merge request of your project only when it is ready to be published.** Don't leave unfinished projects for a longer time.

Please refer to the [template](template.rst) in this repository for the syntax and examples. The project pages in
mathrepo are .rst files - please see the .rst documentation for details. For quick reference check
[here](https://docutils.sourceforge.io/docs/user/rst/quickref.html) or
[here](https://thomas-cokelaer.info/tutorials/sphinx/rest_syntax.html) or refer to the other projects in this repository
as examples.

**Step by step instruction:**
All files of your page will be located in the folder you have created inside the folder "source", say "newtopic".

In folder "newtopic" add a file called "index.rst". You can use the 'template.rst' file for creating it. See also the other
project pages for reference. It is recommended to create the index.rst as landing page including all information
describing the project and all necessary metadata, and then define and create subpages.

Define any subpage in your "index.rst" file by including the name, e. g. "Y". For each subpage that you have defined,
make a new "Y.rst" file in the folder "newtopic". The name "Y" has to be the same as you have set it in your "index.rst".

Add the images that you have in the folder "newtopic" in png format.

Include any downloadable files in the folder "newtopic". It is recommended to include short blocks of code or data directly in
the project pages and everything more extensive as a downloadable file (e.g. libraries, packages, computations). Every
serious computation should be downloadable for reproducibility. If you include a jupyter notebook - please link to the
notebook-file, so people can download it directly from the project page.

To see the result, refer to [Initial Setup](#initial-setup) and [Generate Web pages](#generate-web-pages) in this
README. If you just want to take a quick look at your own project page without simulating the whole mathrepo, you can just
run the command "open build/html/index.html" in your terminal after the make/build operation.

# Necessary Metadata to Include in your Contribution

When creating a new project, please indicate the following information on the top of the index page:

Page content type (e. g. supplementary code to a publication); please include a short summary of the project and the
most important questions you considered; if applicable, indicate the related publication: authors, title, arXiv-ID and
DOI (if applicable); if you like to - as a ..  comment: state of publication: submitted, to be submitted etc. + where
and when);

At the bottom of the index page: Creation date of project page, Project contributors/collaborators; Authors of code (if
differing, also with date if differing from the publication's date); Responsible person for project page on
https://mathrepo.mis.mpg.de (optionally with mail address).

If applicable, please include all relevant information regarding your code: Software / Language used and its version;
Version and system setup you used for computing your results in the corresponding paper.

In addition, please make a statement under what license your research data may be reused. We recommend to use an open,
permissive license. However, each situation is unique. Funders may require specific licenses and journals have different
policies, so it is possible to choose a different license, which we ask you to clearly indicate on your individual
page. You find a list of licences here: https://spdx.org/licenses/ Check with all project contributors if they agree to
the license.

# Generate Web Pages

It is assumed that your python has access to all needed packages. 
You can ensure this by using the created virtual environment:

You can start it in the "mathrepo_website" directory using the following command.

**This is not necessary when you just completed the steps above.**

```sh
source venv/bin/activate
```

**on Windows**: 

```sh
source venv/Scripts/activate
```

To generate the web pages run the following command within the "mathrepo" directory.

```sh
make html
```
**on Windows**:
```sh
sphinx-build -b html source build
```

# Show Web Pages

If just want to take a quick look at your own project page, you can just run the following command after the make/build operation:

```sh
open build/html/<yourprojectname>/index.html
```

or **on Windows**: 

```sh
open build/<yourprojectname>/index.html
```

You may also simulate your local copy of mathrepo by starting a local web server. Since this command will continuously run in
your terminal we recommend executing these commands in a separate teminal or tab.

```sh
cd build/html/ **on Windows**: cd build
python3 -m http.server --cgi 8000
```
or **on Windows**
``` sh
python3 -m http.server --cgi 8000 
```
(possibly also `python -m http.server --cgi 8000`).


Finally connect your web browser to 

  <http://localhost:8000>


# Integrate Jupyter Notebooks

Place the notebook file into the sub directory it should belong to:

```sh
cp mynotebook.ipynb source/newtopic/
```

Furthermore you need to create a local `index.rst` file in the `newtopic` directory. You can use the
[template](template.rst) in this repository and edit it according to your needs.

You need to add the name of the notebook to the "toctree" of the local *index.rst* file:

```rst
.. toctree::
    :maxdepth: 1
    :glob:

    mynotebook
```

After running `make html` or `sphinx-build -b html source build` the notebook was translated into a standard web page.

# Adding your Edits to the Repository

After you are done editing your page, you can add all the changes by using the following standard git commands:

```sh
git pull                                  # Pull the latest changes from the repository before you push your changes.
git add source/newtopic/index.rst         # You need to include all files belonging to your site here. 
                                          # One option for this is git add source/newtopic/*
git status                                # Before commiting you can check using this command that that exactly those files you want are staged for the commit.
git commit -m "A commit message"          # Please write some meaningful commit message here. E.g. Added Project XYZ or Edited XYZ.
git push                                  # This command pushes your changes into the repository. 
                                          # You will need to enter your gitlab username and password the first time you run this command.                                         
```

In order to publish your page with the new changes on <mathrepo.mis.mpg.de> you have to create a merge request.

To create a merge request visit the link, that will be shown in the terminal once you push.
This only works if you already have a GitLab account with access to this repository. If this is not the case just send us an email and we will sort this out. You have to set up Two-Factor Authentication (2FA) for your login, too, otherwise the Gitlab system will not permit you to finish the login process.
Alternatively, you can create a merge request from the GitLab webpage using your browser.

**Get 2FA to work via SSH**

1. Upload an SSH key to GitLab. Find a detailed description under https://docs.gitlab.com/ee/user/ssh.html .

2. Run "git remote set-url origin git@gitlab.mis.mpg.de:rok/mathrepo.git ".

**Get 2FA to work via personal access token**

1. Produce a personal access token following https://docs.mis.mpg.de/gitlab/.

2. Run "git remote set-url origin https://oauth2:<my token>@gitlab.mis.mpg.de/rok/mathrepo ".

Personal access tokens expire in four weeks, therefore we recommend using SSH. 

# Making your Notebook Executable through Binder

[Binder](mybinder.org) is a webservice that allows to run Jupyter notebooks online with installing any software.
This is a convenient way of offering people the chance to actually verify one's computations.
You can find a nice illustration of binder and its role in the reproducability of scientific computations in this
[cartoon](https://twitter.com/mybinderteam/status/1082556317842264064).

For an example of binder click the binder link at the bottom of this page on our
[MathRepo](https://mathrepo.mis.mpg.de/TangentQuadricsInThreeSpace/index.html).

If you want to add a binder link that runs a Jupyter notebook based on **SageMath** you can follow the instructions
below. Otherwise, just send us an email and we will happily provide technical support in creating a binder link that
runs your software setup. The following instructions assume that the notebook 'my_notebook.ipynb' is in the directory
'NewTopic' in 'develop' branch of the repository.

1. Open [mybinder.org](mybinder.org) and choose the option *Git repository* in the first drop down menu.

2. Add the link to our repository (https://gitlab.mis.mpg.de/rok/mathrepo) in the field *Arbitrary git repository URL*.

3. Write 'develop' the *Git ref* field to tell binder to look for your notebook in the develop branch.

4. Add the path to your notebook in the field *Path to a notebook file*, e.g. 'source/NewTopic/my_notebook.ipynb'.

5. Click *Launch* and try if everything works. This step might take a couple of minutes.

6. If it works you can return to [mybinder.org](mybinder.org), repeat steps 1.-3. and copy the link that is displayed at the bootom.

7. Add the link to your *local* 'index.rst' file by adding the lines

```rst
.. image:: ../binder_badge.svg
 :target: <your binder link>
```

Feel free to contact us if you encounter problems at any step.
