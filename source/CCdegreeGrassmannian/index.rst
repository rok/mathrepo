=============================
CC degree of the Grassmannian
=============================


| This page contains auxiliary files to the paper:
| Viktoriia Borovik, Bernd Sturmfels and Svala Sverrisdóttir: Coupled cluster degree of the Grassmannian
| In: Journal of symbolic computation, 128 (2025), 102396
| DOI: `10.1016/j.jsc.2024.102396 <https://dx.doi.org/10.1016/j.jsc.2024.102396>`_ ARXIV: https://arxiv.org/abs/2310.15474 CODE: https://mathrepo.mis.mpg.de/CCdegreeGrassmannian/

   
ABSTRACT:   
We determine the number of complex solutions to a nonlinear eigenvalue problem on the Grassmannian  in its Plücker embedding. 
This is motivated by quantum chemistry, where it represents the truncation to single electrons in coupled cluster theory. 
We prove the formula for the Grassmannian of lines which was conjectured in earlier work with Fabian Faulstich. 
This rests on the geometry of the graph of a birational parametrization of the Grassmannian. 
We present  a squarefree Gröbner basis for this graph, and we develop connections to toric degenerations from representation theory.

* Macaulay2 source code :download:`verifyTheorem14functions.m2 <verifyTheorem14functions.m2>`, :download:`Verify (3,9) <verify39.m2>`
* Jupyter Notebook :download:`Theorem 14 Verifications <Theorem14verifications.ipynb>`, :download:`CC degree of the Grassmannian <CCdegOfGrassmannian.ipynb>`
* Julia source code :download:`9 real solutions <9real_sols.jl>`

Verifications of Theorem 14
---------------------------

We used `Macaulay2 <https://macaulay2.com/>`_ (version 1.22) to verify Theorem 14 in the cases of

.. math::
  (d,n) = (3,7), (3,8), (3,9), (4,8).

.. The folder :download:`Verifications for d,n <Gdnverifications.zip>` includes Macaulay2 files with the ideals for :math:`\mathcal{G}(d,n)`
.. and :math:`\mathcal{T}(d,n)`.
   
To verify the first statement of Theorem 14 for a specific case you need the function KhovanskiiCheck from the file :download:`verifyTheorem14functions.m2 <verifyTheorem14functions.m2>`. To test a specific case of :math:`{\rm Gr}(d,n)`, e.g. :math:`{\rm Gr}(d,n) = {\rm Gr}(3,7)`, run

.. code-block:: macaulay2

  load "verifyTheorem14functions.m2";
  (d,n) = (3,7);
  KhovanskiiCheck(d,n)

This function compares the Hilbert series for the graph of the parametrization of :math:`{\rm Gr}(d,n)` and the graph of the corresponding monomial parametrization. Knowing the PBW poset for the Grassmannian speeds up the computation of the ideal :math:`{\mathcal G}(d,n)`. The usage is as follows:

.. code-block:: macaulay2

  KhovanskiiCheckWithPoset(3,7, poset37)

Here, poset37 should be a list of lists of indices ordered consistently to the PBW order.

To verify the second statement of Theorem 14 you need to use the function SquarefreeCheck from :download:`verifyTheorem14functions.m2 <verifyTheorem14functions.m2>`:

.. code-block:: macaulay2

  load "verifyTheorem14functions.m2";
  (d,n) = (3,7);
  SquarefreeCheck(d,n, poset37)

.. We have verified all cases from the Remark 15: 

.. .. math::
..   \begin{matrix}
..     (d,n) && (3,7) & (3,8) & (3,9) & (4,8) \\
..     {\rm CCdeg}_{3,7} &&  2883 & 38610 & 574507 & 154441\\
..     {\rm KhovanskiiCheckWithPoset (sec)} && 19 & 388 &  & 1520  \\
..     {\rm SquarefreeCheck (sec)} && 7 & 276 &  & 1235  \\
..   \end{matrix}

Cases :math:`(3,6)`, :math:`(3,7)` and :math:`(4,8)` are verified in the 
Jupyter Notebook :download:`Theorem 14 Verifications <Theorem14verifications.ipynb>`.
The PBW posets for all the 4 cases in Remark 15 can also be found in the notebook. 
The case :math:`(4,9)` was verified using the code :download:`Verify (3,9) <verify39.m2>`.
There we calculate that the CC degree is 574507, while computing the ideals :math:`G` and :math:`T` takes 9484 seconds
and verifying the first and second statement in Theorem 14 takes 68 seconds.

The CC degree of the Grassmannian calculated numerically
--------------------------------------------------------

The code :download:`CC degree of the Grassmannian <CCdegOfGrassmannian.ipynb>` uses `HomotopyContinuation.jl <https://www.juliahomotopycontinuation.org/>`_ (version 2.9.2)
to calculate the CC degree of the Grassmannian :math:`\operatorname{Gr}(d,n)`. By Theorem 1 we can use this code to find the degree of
the graph :math:`\mathcal{G}(d,n)` and numerically verify Example 4. Also by Theorem 14 we can use this code to calculate the normalized volume of :math:`\operatorname{CFFLV}(d,n)`.

Hamiltonians providing real and non-degenerate solutions
--------------------------------------------------------

.. Remark 6 in our paper provides an algorithm to find a Hamiltonian matrix, not necessarily symmetric,
.. providing real and non-degenerate solutions to the CC equations. Now we want to show this is also true for symmetric Hamiltonians.
.. The code :download:`9 real solutions <9real_sols.jl>` proves this for :math:`d = 2` and :math:`n = 4`, providing an example of a
.. symmetric :math:`6 \times 6` Hamiltonian matrix for which all 9 solutions are real and non-degenerate. 

We illustrate Remark 6 by a small example of CC degree for :math:`\mathrm{Gr}(2,4)`. We found a symmetric :math:`6 \times 6` Hamiltonian for which CC equations have all 9 solutions real and nondegenerate.

.. math:: H = \begin{bmatrix} 33   &  10  &  -89  &  -25  &  -42  &  -68\\ 10  &    7 &    65  &  -96   &   7  &  -60\\ -89  &   65  &   12  &   50  &  -89   &  16\\ -25  &  -96   &  50  &  -60  &  -70   &  52\\ -42  &    7  &  -89  &  -70  &   34  &  -20\\ -68  &  -60  &   16  &   52  &  -20  &   -4 \end{bmatrix}.

Solutions can be found with the code :download:`9 real solutions <9real_sols.jl>` using HomotopyContinuation.jl. 


------------------------------------------------------------------------------------------------------------------------------

Project page created: 24/10/2023

Project contributors: Viktoriia Borovik, Bernd Sturmfels and Svala Sverrisdóttir

Corresponding author of this page: Svala Sverrrisdóttir, svala@math.berkeley.edu
 
Macaulay2 code written by: Viktoriia Borovik

Julia code written by: Bernd Sturmfels

Jupyter notebook written by: Svala Sverrisdottir and Viktoriia Borovik

Software used: Julia (Version 1.9.2), Macaulay2 (Version 1.22)

System setup used: MacBook Pro with macOS Venture 13.5.1, Processor 2,6 GHz 6-Core Intel Core i7, Memory 16 GB 2400 MHz DDR4.

License for code of this project page: MIT License (https://spdx.org/licenses/MIT.html)

License for all other content of this project page: CC BY 4.0 (https://creativecommons.org/licenses/by/4.0/)


Last updated 20/10/2023.
