%Anton Ullrich
%MCF
function MCFScheme
%fcn = @(x,y) x.^2 + y.^2;
set(gcf,'Position',[100 100 800 800])
%fig = uifigure('Position',[100 200 350 275]);
%fig = uifigure('Name', 'MCF','Colormap', hot);
%fig.Position(3)=750;%1500
%fig.Position(4)=768;
global W;
global X;
global Y;
%.005
[W,X,Y] = meshgrid(-1:.05:1);%.02
[Xplain, Yplain] = meshgrid(-1:.05:1);%.02
%heatKernel=1/(4*pi*t)*exp(-|x-y|^2/(4t));
Z = startfct(W,X,Y);%X .* exp(-X.^2 - Y.^2);
Z= discretise(Z);
%U=Z;
%Z=fcn(X,Y);
%hAxes= axes('Parent',fig);
%hAxes2= axes('Parent',fig);
%set(hAxes,'Position',[.05 .25 .9 .7]);
%set(hAxes2,'Position',[.55 .25 .4 .7]);
%colorbar('Parent',fig);
global h;
%200
h=25;%200 weiter als 100
%500
n=100;%150 or 100
cm = colormap(parula(n));%A,1
Out=reshape(1-Z(W==0), size(Xplain))';
v = VideoWriter('mcf.avi');
v.FrameRate=15;
open(v);
%set(hAxes,'nextplot','replacechildren'); 

%contourf(hAxes,Xplain,Yplain,Out,1);
%frame = getframe(hAxes);
%writeVideo(v,frame);
%for k=1:1:n
while any(Z(:)>0)
    Zdiff=convolve(Z);
    Z=discretise(Zdiff);
    if any(Z(:)>0)    
        Out=reshape(1-Z(W==0), size(Xplain))';
        %contourf(hAxes,Xplain,Yplain,Out,1);
        
        %hold(hAxes,'on');
        %cla(hAxes);
        %p=patch(hAxes,isosurface(W,X,Y,Z,0.5));
        %isonormals(W,X,Y,Z,p);
        %5
        %set(p,'FaceColor','red','EdgeColor','none');
        %daspect(hAxes,[1 1 1])
        %view(hAxes,3);
        %hold(hAxes,'off');
        %camlight 
        %lighting gouraud
        %cla(gcf,'reset');
        %isosurface(W,X,Y,Zdiff,0.2);
        %view(3);
        cla;
        p = patch(isosurface(W,X,Y,Zdiff,0.5));
        isonormals(W,X,Y,Zdiff,p)
        set(p,'FaceColor','red','EdgeColor','none');
        daspect([1 1 1])
        view(3); axis tight
        camlight 
        lighting gouraud
        
        
        view([-65,45]);
        xlim([-1,1]);
        ylim([-1,1]);
        zlim([-1,1]);
        %patch('Faces',faces,'Vertices',verts,'Color', color);
        %patch(hAxes, s);
        %hold(hAxes,'on');
        %contour(hAxes,X,Y,Z,1,'LineColor', cm(k,:));
        %hold(hAxes,'off');
        frame = getframe(gcf);%(hAxes)
        writeVideo(v,frame);
    end
    %U=U+Z;
end
%hPlot=surf(hAxes2,X,Y,U);
close(v);
end

function Z=fcn(X,Y)
    Z=peaks(X,Y);
end

function Z=startfct(W,X,Y)
    %Z=1-X.^2-Y.^2;%1-4*((X-0.5).^2+(Y-0.5).^2);
    %Z=1.2-(X.^2+Y.^2+Y.^3.*(X.^2-0.3));
    Z=min(max(max(0.125-(X-0.65).^2-Y.^2-W.^2,0.125-(X+0.65).^2-Y.^2-W.^2),0.06-Y.^2-W.^2), 0.7-X.^2-Y.^2-W.^2)+0.45;
end

function Z=convolve(Zold)
    global h;
    global X;
    global Y;
    global W;
    Z = abs(ifftn(fftshift(exp((-(X).^2-(Y).^2-(W).^2).*h/2).*fftshift(fftn(Zold)))));%./(2*pi)
    %real or abs nicht notwendig aber um ggf. translationsfehler zu
    %beheben, da nichtnegativ
end

function Z=discretise(Zold)
%(sign(-0.5)+1)/2
    Z=ones(size(Zold));
    zeroArray=zeros(size(Zold));
    Z(Zold<0.5)=zeroArray(Zold<0.5);
%if x <=0
%y=x.^3
%else
%y= x.^3
%y(x>0)=x(x>0).^2
%end
end