.. _Algorithm:

=======================
Median filter algorithm
=======================

Back to :ref:`main page <index>`

As described in :ref:`Thresholding scheme & level set method <LevelMBO>`, our median filter algorithm is an extension of the MBO scheme to the level set setting. Specifically, we choose a discretization of our domain :math:`D` (and given measure :math:`\mu`) by a point process :math:`P_N`. Additionally, we choose a kernel. In practice, this will often be a (normalized) characteristic function of a neighborhood domain (stencil). For now, we will consider the ball. Next, given an initial datum :math:`g:D\to\mathbb{R}`, we get the evolution by iteratively taking the median in the local stencil aroung each point simultaneously.

.. container:: float
   :name: fig:Schematic

   |image1|

   Figure 7. Visualization of the algorithm.
   

A visualization of this for a single point is shown in :ref:`Figure 7 <fig:Schematic>`. 

Algorithm 1: The median filter scheme
	| **Data:** :math:`g\in \text{BUC}(D;\mathbb{R})`
	| **Result:** Evolution :math:`u^n_h`
	| :math:`u^0_h(x):= g(x)\ \forall x\in P_N`;
	| :math:`n\gets 0`;
	| **while** :math:`\frac{nh}{c_A}<T` **do**
	|  :math:`u^{n+1}_h(x):= \text{med}_N(u^n_h;A_r(x))\ \forall x\in P`;
	|  :math:`n\gets n+1`;
	| **end**

The pseudo code is also shown in Algorithm 1. Here, :math:`h` is the time step size (related to :math:`r` by the parabolic scaling :math:`h=r^2`) and :math:`\text{med}_N(u^n_h;A_r(x))` denotes the local median of :math:`u_h^n` in the neighborhood :math:`A_r`. This neighborhood is usually the ball of size :math:`r`. As a reminder, the median is the value in the middle in the sense that there are an equal number of values above and below it.

.. container:: float
   :name: fig:SimplifiedAlgorithm

   |image2|

   Figure 8. A simplified example of the algorithm.

To visualize one step of the value update, one can look at :ref:`Figure 8 <fig:SimplifiedAlgorithm>`. 

Intuitively this converges to MCF since the median is related to the level set Laplacian in the same way that the mean is related to the ordinary Laplacian.

An efficient implementation of our algorithm in rust can be found in :download:`MedianFilter.zip <Codes/medianfilter.zip>`.

One could also use other kernels and domains for the algorithm. Partial results are also given for these cases.

   
.. |image1| image:: Algorithm-Images/Tikz1.png
   :width: 50.0%
.. |image2| image:: Algorithm-Images/SimplifiedAlgorithm.png
   :width: 80.0%