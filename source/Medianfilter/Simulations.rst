.. _Simulations:

===========
Simulations
===========

Back to :ref:`main page <index>`

.. container:: float
   :name: fig:Ellipse

   |image8| |image9| |image10|

   Figure 9. The evolution of a non-symmetric function.

.. |image8| image:: MedianFilterImages/Ellipse-0.png
   :width: 32.0%
.. |image9| image:: MedianFilterImages/Ellipse-1.png
   :width: 32.0%
.. |image10| image:: MedianFilterImages/Ellipse-2.png
   :width: 32.0%

:ref:`Figure 9 <fig:Ellipse>` shows the evolution of a function with ellipses as level sets. The white circle in the upper left corner shows which kernel was used in the simulation. Additionally, the red curve at the orange level set is a comparison to show the similarity to the (almost) exact evolution of a level set. This comparison was computed by a small step direct Euler method for a parameterization in tangential polar coordinates.

Classifier
----------

Due to its nature as an area minimizing flow, the stationary sets are local minimizers, also called minimal surfaces. Thus, one can use the mean curvature flow algorithm to find classifiers.

+-----------+-----------+-----------+-----------+
| |image11| | |image12| | |image13| | |image14| |
+-----------+-----------+-----------+-----------+
| |image15| | |image16| | |image17| | |image18| |
+-----------+-----------+-----------+-----------+
| |image19| | |image20| | |image21| | |image22| |
+-----------+-----------+-----------+-----------+

.. |image11| image:: MedianFilterImages/Ring-0.png
.. |image12| image:: MedianFilterImages/Ring-1.png
.. |image13| image:: MedianFilterImages/Ring-2.png
.. |image14| image:: MedianFilterImages/Ring-3.png

.. |image15| image:: MedianFilterImages/Heavy-0.png
.. |image16| image:: MedianFilterImages/Heavy-1.png
.. |image17| image:: MedianFilterImages/Heavy-2.png
.. |image18| image:: MedianFilterImages/Heavy-3.png

.. |image19| image:: MedianFilterImages/RandDumb-0.png
.. |image20| image:: MedianFilterImages/RandDumb-1.png
.. |image21| image:: MedianFilterImages/RandDumb-2.png
.. |image22| image:: MedianFilterImages/RandDumb-3.png

Some examples of this are shown above, where we assume that the objects shaped like dumbbells are manifolds representing our data. If they symbolize two classes of loosely connected sets, the optimal separation would be at the neck.

Young angle conditions
----------------------

So far, all of our considerations neglected the boundary and the conditions we imposed there. This resulted in homogeneous Neumann conditions. That is, the level sets meet the boundary orthogonally and the gradient is perpendicular to the normal of the domain.

This can be generalized to Young angle conditions, i.e. we impose a fixed contact angle for all level sets. This can be achieved by changing the threshold depending on the position relative to the domain boundary. In terms of our level set function, this results in changing the median to a k-select. We don't choose the value that lies in the middle but a different value depending on the desired percentage of values that should lie above our k-select value.

.. container:: float
   :name: fig:Neumann

   |image23| |image24|

   Figure 10. Evolution for different Young angle conditions.

.. |image23| image:: MedianFilterImages/Neumann-Ur.png
   :width: 45.0%
.. |image24| image:: MedianFilterImages/Combined.png
   :width: 45.0%

:ref:`Figure 11 <fig:Neumann>` shows the evolution of a quadratic radially symmetric function under different Neumann conditions. On the right is a zoom in of the upper right corner for four conditions is shown. The prescribed contact angles are :math:`0^\circ, 45^\circ, 90^\circ` and :math:`180^\circ`.

The formulation of this scheme and the Young angle conditions results from an energy penalization of the common boundary with the domain.