.. _index:

==============================================
A median filter scheme for mean curvature flow
==============================================

| This page contains the explanations and source code as well as introductory material related to the results in the paper:
| Anton Ullrich and Tim Laux. *Median filter method for mean curvature flow using a random Jacobi algorithm*
| ARXIV: https://arxiv.org/abs/2410.07776
| **Keywords:** Mean curvature flow, manifold learning, level set method, thresholding scheme, median filter, viscosity solution, Young angle condition, continuum limit

ABSTRACT:
We present an efficient scheme for level set mean curvature flow using a domain discretization and median filters. 
For this scheme, we show convergence in :math:`L^\infty`-norm under mild assumptions on the number of points in the discretization. 
In addition, we strengthen the weak convergence result for the MBO thresholding scheme applied to data clustering of Laux and Lelmi. 
This is done through a strong convergence of the discretized heat flow in the optimal regime. Different boundary conditions are also discussed.

.. container:: float
   :name: fig:EyeCatcher

   |image| 
   |image1| |image2|

   Figure 1. Sample pictures demonstrating the content.

.. |image| image:: CatchyImages/Voronoi.png
   :width: 81.0%
.. |image1| image:: CatchyImages/dumbbell2.png
   :width: 40.0%
.. |image2| image:: CatchyImages/Ur.png
   :width: 40.0%

The idea of this page is to be a mostly self-contained repository for the code of the associated paper. To this end, we give a brief introduction to the main topics, including mean curvature flow, the thresholding (MBO) scheme, and our algorithm. Then we present the applications for which our code is used and give example simulations.

.. toctree::
	:maxdepth: 2

  MCF.rst
  LevelMBO.rst
  Algorithm.rst
  Simulations.rst

Structure
---------

In the first section, :ref:`Mean curvature flow <MCF>`, we give an introduction to the evolution known as mean curvature flow, some of its properties and the main applications. Some examples and simulations are also shown to give an intuition of how the flow behaves.

Then, in :ref:`Thresholding scheme & level set method <LevelMBO>`, we present one of the most important approximation algorithm, the MBO scheme, which is also called the thresholding scheme. Our algorithm is also an extension of this scheme. After an introduction, we explain what the level set method is and why we will use it.

After a discretization, this leads to the formulation of our algorithm, which is done in :ref:`Median filter algorithm <Algorithm>`. Here we give the algorithm, an explanation and simplified examples for it. In addition, the structure of our program is explained and we sketch how the other programs work as well.

We conclude in :ref:`Simulations <Simulations>` with simulations and applications of our algorithm.

------------------------------------------------------------------------------------------------------------------------------

Project page created: 30/07/2024


Project contributors: Anton Ullrich, Tim Laux


Corresponding author of this page: Anton Ullrich, `anton.ullrich@mis.mpg.de <mailto:anton.ullrich@mis.mpg.de>`_


Code written by: Anton Ullrich


Software used: Matlab (Version R2022b), Python (Version 3.9.2), Rust (Version 1.75.0)


| System setup used: 
|  - 2x 32-Core AMD Epyc 7601 at 2.2 GHz (max. 3.2 GHz)
|  - 1024 GB RAM


License for code of this project page: MIT License (https://spdx.org/licenses/MIT.html)


License for all other content of this project page (text, images, …): CC BY 4.0 (https://creativecommons.org/licenses/by/4.0/)

.. Optional additional data:

Last updated 17/10/2024.

.. meta::
   :description: A median filter scheme for mean curvature flow
   :keywords: Mean curvature flow, Manifold learning, Level set method, median filter, viscosity solution, Young angle condition


.. Kommentare https://pandoc.org/try/
.. Aber muesste in _static rein oder so (ggf. ausklappbar), EPGP
.. jupyther testen ob es klappt und vorcompilliert ist und man Sachen allgemeiner hat
..    .. raw:: html  <video width="45%" controls autoplay loop src="./_static/EPGP/wave_nice.mp4"></video>