// for each of the six triplet of lines, 
// the first pluecker coordinate p_12 has been normalized to one,
// ones_and_zeros,  lists in addition the coordinates which are zero

ones_and_zeros:=[
[ 1, 2, 5 ],
[ 1, 3, 4 ],
[ 1, 2 ],
[ 1, 3 ],
[ 1, 4 ],
[ 1, 5 ]
];

// minpolys consists of six lists of lengths (3,3,4,4,4,4)
// each entry in each of these lists represents a univariate cubic polynomial
// this cubic is represented as a list of size 4 with [c0,c1,c2,c3] representing
// c0+c1*t+c2*t^2+c3*t^3

minpolys:=[
[
[
a*d*f - f^2*g,
a*b*d + a*f*h - 2*b*f*g + c*d*f - d^2*e,
a*b*h - b^2*g + b*c*d + c*f*h - 2*d*e*h,
b*c*h - e*h^2
],
[
-b*c*e + e^2*h,
a*b*c - 2*a*e*h + b*e*g - c^2*f + c*d*e,
a^2*h - a*b*g - a*c*d + 2*c*f*g - d*e*g,
a*d*g - f*g^2
],
[
-e*f,
a*b - c*f - d*e,
a*h + b*g - c*d,
g*h
]
],
[
[
b*d*f - f^2*h,
a*b*d - 2*a*f*h + b*f*g + c*d*f - d^2*e,
-a^2*h + a*b*g + a*c*d + c*f*g - 2*d*e*g,
a*c*g - e*g^2
],
[
-a*c*e + e^2*g,
a*b*c + a*e*h - 2*b*e*g - c^2*f + c*d*e,
-a*b*h + b^2*g - b*c*d + 2*c*f*h - d*e*h,
b*d*h - f*h^2
],
[
e*f,
a*b - c*f - d*e,
-a*h - b*g + c*d,
g*h
]
],
[
[
-f^2*g,
-a*f*h - b*f*g + c*d*f,
-a*b*h + c*f*h + d*e*h,
e*h^2
],
[
-b*c*e + e^2*h,
a*b*c - 2*a*e*h + b*e*g - c^2*f + c*d*e,
a^2*h - a*b*g - a*c*d + 2*c*f*g - d*e*g,
a*d*g - f*g^2
],
[
-a^2*c^2 + 2*a*c*e*g - e^2*g^2,
2*a^2*c*h - a*b*c*g + a*c^2*d - 2*a*e*g*h + b*e*g^2 - c*d*e*g,
-a^2*h^2 + a*b*g*h - 2*a*c*d*h + c*f*g*h + d*e*g*h,
a*d*h^2 - f*g*h^2
],
[
b*c*f^2 - e*f^2*h,
a*b*f*h - b^2*f*g + b*c*d*f + c*f^2*h - 2*d*e*f*h,
a*b*d*h + a*f*h^2 - 2*b*f*g*h + c*d*f*h - d^2*e*h,
a*d*h^2 - f*g*h^2
]
],
[
[
-f^2*h,
-a*f*h - b*f*g + c*d*f,
-a*b*g + c*f*g + d*e*g,
e*g^2
],
[
-b^2*c^2 + 2*b*c*e*h - e^2*h^2,
-a*b*c*h + a*e*h^2 + 2*b^2*c*g + b*c^2*d - 2*b*e*g*h - c*d*e*h,
a*b*g*h - b^2*g^2 - 2*b*c*d*g + c*f*g*h + d*e*g*h,
b*d*g^2 - f*g^2*h
],
[
-a*c*e + e^2*g,
a*b*c + a*e*h - 2*b*e*g - c^2*f + c*d*e,
-a*b*h + b^2*g - b*c*d + 2*c*f*h - d*e*h,
b*d*h - f*h^2
],
[
-a*c*f^2 + e*f^2*g,
-a^2*f*h + a*b*f*g + a*c*d*f + c*f^2*g - 2*d*e*f*g,
-a*b*d*g + 2*a*f*g*h - b*f*g^2 - c*d*f*g + d^2*e*g,
b*d*g^2 - f*g^2*h
]
],
[
[
b*d*f - f^2*h,
a*b*d - 2*a*f*h + b*f*g + c*d*f - d^2*e,
-a^2*h + a*b*g + a*c*d + c*f*g - 2*d*e*g,
a*c*g - e*g^2
],
[
a^2*d^2 - 2*a*d*f*g + f^2*g^2,
2*a^2*d*h - a*b*d*g + a*c*d^2 - 2*a*f*g*h + b*f*g^2 - c*d*f*g,
a^2*h^2 - a*b*g*h + 2*a*c*d*h - c*f*g*h - d*e*g*h,
a*c*h^2 - e*g*h^2
],
[
e^2*g,
-a*e*h - b*e*g + c*d*e,
a*b*h - c*f*h - d*e*h,
f*h^2
],
[
-b*d*e^2 + e^2*f*h,
a*b*e*h - b^2*e*g + b*c*d*e - 2*c*e*f*h + d*e^2*h,
-a*b*c*h - a*e*h^2 + 2*b*e*g*h + c^2*f*h - c*d*e*h,
a*c*h^2 - e*g*h^2
]
],
[
[
b^2*d^2 - 2*b*d*f*h + f^2*h^2,
-a*b*d*h + a*f*h^2 + 2*b^2*d*g + b*c*d^2 - 2*b*f*g*h - c*d*f*h,
-a*b*g*h + b^2*g^2 + 2*b*c*d*g - c*f*g*h - d*e*g*h,
b*c*g^2 - e*g^2*h
],
[
a*d*f - f^2*g,
a*b*d + a*f*h - 2*b*f*g + c*d*f - d^2*e,
a*b*h - b^2*g + b*c*d + c*f*h - 2*d*e*h,
b*c*h - e*h^2
],
[
e^2*h,
-a*e*h - b*e*g + c*d*e,
a*b*g - c*f*g - d*e*g,
f*g^2
],
[
a*d*e^2 - e^2*f*g,
-a^2*e*h + a*b*e*g + a*c*d*e - 2*c*e*f*g + d*e^2*g,
a*b*c*g - 2*a*e*g*h + b*e*g^2 - c^2*f*g + c*d*e*g,
b*c*g^2 - e*g^2*h
]
]
];
