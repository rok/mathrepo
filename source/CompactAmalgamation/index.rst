=======================================================================
Some thoughts and experiments on Bergman’s compact amalgamation problem
=======================================================================

| This page contains the source code and explanations related to the
  computational results presented in the paper:
| Michael Joswig, Mario Kummer, Andreas Thom, and Claudia He Yun : Some thoughts and experiments on Bergman’s compact amalgamation
  problem
| In: Beiträge zur Algebra und Geometrie, 65 (2024) 4, p. 789-807
| DOI: `10.1007/s13366-023-00709-8 <https://dx.doi.org/10.1007/s13366-023-00709-8>`_ ARXIV: https://arxiv.org/abs/2304.08365 CODE: https://mathrepo.mis.mpg.de/CompactAmalgamation/

Abstract
~~~~~~~~

We study the question whether copies of :math:`S^1` in
:math:`\mathrm{SU}(3)` can be amalgamated in a compact group. This is
the simplest instance of a fundamental open problem in the theory of
compact groups raised by George Bergman in 1987. Considerable
computational experiments suggest that the answer is positive in this
case. We obtain a positive answer for a relaxed problem using
theoretical considerations.

Amalgamating representations of the circle group
------------------------------------------------

Representations of the circle group
:math:`S^1 = \{z \in \mathbb{C} : \|z\|=1\}` in :math:`\mathrm{SU}(3)`
are given by :math:`(a,b,c)\in \mathbb{Z}^3` with :math:`a+b+c=0` and
:math:`\mathrm{gcd}(a,b,c)=1`, up to conjugation. Given two such
representations :math:`\psi_{a,b,c}` and :math:`\psi_{a',b',c'}`, we
look for injective representations
:math:`f,g: \mathrm{SU}(3) \to \mathrm{U}(n)` such that
:math:`f \circ \psi_{a,b,c} = g \circ \psi_{a',b',c'}`. Unitary
representations of :math:`\mathrm{SU}(3)` are semi-simple. The
irreducible representations have characters Schur polynomials
:math:`s_\lambda(x_1,x_2,x_3)`, where :math:`\lambda` is either the
partition :math:`(1,1,1)` or a partition with at most two parts.
Therefore, our goal is to find positive linear combinations :math:`P,Q`
of suitable Schur polynomials such that
:math:`P(z^a,z^b,z^c) = Q(z^{a'},z^{b'},z^{c'})`. By Proposition 3.1, we
can accomplish this by solving a particular integer linear program
:math:`(\mathrm{ILP}_k)` on page 5. We set up our linear program using
the computer algebra system ``OSCAR``. We solve this linear program in
``OSCAR`` and ``SCIP``.

In Table 2 Column 4, we only record the dimension of the amalgamated
representations. For explicit descriptions of these representations, see
`this page <Table2.html>`__.

Setting up computations
-----------------------

See the following for a walkthrough of Section 3. We give a detailed explanation of Example 3.3 and show how we produced Tables 1 and 2.

.. toctree::
   :maxdepth: 1
   :glob:

   amalgamation

To reprodice these computations please do the following.

-  Download the Jupyter notebook :download:`amalgamation.ipynb <amalgamation.ipynb>` and :download:`source code <amalgamation.jl>`.
-  Download and install `Julia <https://julialang.org/>`__.
-  Install the Julia package
   `OSCAR <https://docs.oscar-system.org/stable/>`__.
-  Download and install `SCIP <https://www.scipopt.org/>`__ and
   configure it as follows.

Configure ``SCIP``
------------------

Install ``SCIP`` and record the path of the binary file. In the file
amalgamation.jl, find the following function and replace
``/Users/yun/Documents/scipoptsuite-8.0.2/scip/bin/scip`` by your path.

::

   function scip(filename,outputname)
       run(`/Users/yun/Documents/scipoptsuite-8.0.2/scip/bin/scip -f $filename -l $outputname -q`);
   end

Checking Conjecture 4.9
-----------------------

Let

.. math:: F_{a,b} = (1+z+\dots+z^{b-1})^a\cdot (1+z^{-1}+\dots+z^{-(b-1)})^a.

\ Let :math:`v=(v_1,v_2,v_3) \in \mathbb{Z}^3` be a vector such that
:math:`v_1+v_2+v_3=0` and :math:`\mathrm{gcd}(v_1,v_2,v_3)=1`.

Conjecture 4.9. There are natural numbers :math:`a_0,b_0 > 0` such that
for all :math:`a \geq a_0`, all :math:`b` divisible by :math:`b_0` there
is :math:`N\in \mathbb{N}` and a Schur positive symmetric polynomial
:math:`P` in three variables such that

.. math:: N\cdot F_{a,b}=P_v.

To verify this conjecture, we pick values for :math:`a,b` and :math:`N`
and search for appropriate :math:`P`. We can set up a similar integer
linear program by writing

.. math:: P=\lambda_1S_1+\dots+\lambda_kS_k,

\ where :math:`\{S_1,\dots,S_k\}` are Schur polynomials in some
ordering. Then the linear program is given by matching coefficients of
:math:`P_v` with :math:`F_{a,b}` while requiring
:math:`\lambda_i \geq 0`.

Note that once :math:`a` and :math:`b` are chosen, they define a finite
set of Schur polynomials that can appear in :math:`P` in the following
sense. Let :math:`(r,t)` be a partition. Let :math:`v=(v_1,v_2,v_3)` as
before, assuming :math:`v_1\geq v_2 > 0`. Then :math:`(s_{r,t})_v(z)`
has leading deg :math:`rv_1+tv_2` and trailing deg
:math:`-rv_1+(t-r)v_2`. In the meantime, the polynomial :math:`F_{a,b}`
has leading degree :math:`(b-1)a` and trailing degree :math:`(1-b)a`.
Therefore, all Schur polynomials :math:`s_{r,t}` that appear in
:math:`P` must satisfy

.. math:: rv_1+tv_2 \leq (b-1)a,

\ 

.. math:: -rv_1+(t-r)v_2 \geq (1-b)a.

There are only finitely many such partitions. In our program, we use
this finite set of partitions and order them lexicographically.

All computations are done in ``Julia`` and ``OSCAR``. In Table 3 Column
4, only dimensions of the representations given by :math:`P` are
recorded. For explicit descriptions, see `this page <Table3.html>`__.

See the following page for more details.

.. toctree::
   :maxdepth: 1
   :glob:

   conjecture_section4


This notebook is also available for download
:download:`conjecture_section4.ipynb <conjecture_section4.ipynb>` and :download:`source code <conjecture_section4.jl>`.

------------------------------------------------

Project page created: 19/05/2023

Project contributors: Michael Joswig, Mario Kummer, Andreas Thom, Claudia He Yun

Corresponding author of this page: Claudia Yun, clyun@mis.mpg.de

Software used: Julia (version 1.8.5), OSCAR (version 0.12.0), SCIP (version 8.0.2)

System setup used:  

* Hydra at the MPI MiS: 4x16-core Intel Xeon E7-8867 v3 CPU (3300 MHz) on Debian GNU/Linux 5.10.149-2 (2022-10-21) x86_64  
* MacBook Pro: 8-core Intel i7-6700HQ CPU (2600 MHz) on Darwin Kernel 21.6.0 (2022-09-29) x86_64  

License for code of this project page: MIT License (https://spdx.org/licenses/MIT.html)

License for all other content of this project page (text, images, …): CC BY 4.0 (https://creativecommons.org/licenses/by/4.0/)

Last updated 21/08/2023.
