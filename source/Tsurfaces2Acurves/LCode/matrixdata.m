function [row, col, data, rc] = matrixdata(indices, lam, n) %input: indices = dictionary which converts variable to its index, lam = side length of L, n= level of iteration
    [N, D] = rat(lam);
    step = 2*D; %size of boxes is 1/ LCM(lam.denominator, 2)
    %holomorphicity equations
    datalength = (2*3^n*N)^2 - (2*3^n*(N-D))^2; %the number of squares
    dat = [1i -1i -1 1];
    datahol = repmat(dat, 1, datalength); %create data vector which repeats coefficients
    rowhol = repmat(1:datalength, [4 1]); %create row vector which repeats eq number
    rowhol = rowhol(:)'; %previous line made a 4 by datalength matrix, we now make it into one vector
    %the following commands make a column vector which records the indices
    %of the variables used in the equations
    colj = repmat(0:step*3^n-1, [2*N*3^n 1]);
    colj = colj(:)';
    coli = repmat(0:2*N * 3^n-1, 1, step * 3^n);
    colhol = arrayfun(@(x,y) colindshol(indices, x, y), coli, colj, 'un',0);
    colj2 = repmat(step * 3^n:2*N * 3^n-1, [step*3^n 1]);
    colj2 = colj2(:)';
    coli2 = repmat(0:step * 3^n-1, 1, (N-D)*2*3^n);
    colhol2 = arrayfun(@(x,y) colindshol(indices, x, y), coli2, colj2, 'un',0);
    
    %periodicity equations
    %A-periods
    datapera = repmat([1, -1, 1], 1, step*3^n+1);
    rowpera = repmat(datalength+1:datalength+step*3^n+1, [3 1]);
    rowpera = rowpera(:)';
    colpera = arrayfun(@(j) colindsper(lam, step, n, indices, "a", j), 0:step*3^n, 'un', 0);

    %differences of the A-periods
    dataperdiffa = repmat([1, -1, -1, 1], 1, (N-D)*2*3^n);
    rowperdiffa = repmat(datalength+step*3^n + 2:datalength+2*N * 3^n+1, [4 1]);
    rowperdiffa = rowperdiffa(:)';
    colperdiffa = arrayfun(@(j) colindsper(lam, step, n, indices, "diffa", j), step * 3^n+1:2*N * 3^n, 'un', 0);

    %B-periods
    %dataperb is the same as datapera so we are not creating it
    rowperb = repmat(datalength+2*N*3^n+2:datalength+(N+D)*2*3^n+2, [3 1]);
    rowperb = rowperb(:)';
    colperb = arrayfun(@(j) colindsper(lam, step, n, indices, "b", j), 0:step*3^n, 'un', 0);

    %differences of the B-periods
    dataperdiffb = repmat([1, 1, -1, 1], 1, (N-D)*2*3^n);
    rowperdiffb = repmat(datalength+(N+D)*2*3^n+2 : datalength+2*N*2*3^n+1, [4 1]);
    rowperdiffb = rowperdiffb(:)';
    colperdiffb = arrayfun(@(j) colindsper(lam, step, n, indices, "diffb", j), step * 3^n+1 : N*2 * 3^n, 'un', 0);

    %these are the hard-coded extra equations in the end
    rc = datalength+2*N*2*3^n +2;
    extrarow = [rc, rc, rc, rc+1, rc+1, rc+1, rc+2, rc+2, rc+2, rc+3, rc+3, rc+3, rc+4, rc+5, rc+6, rc+6, rc+7, rc+7];
    extracol = [indices("A2b"), indices(2*N * 3^n+", "+0), indices(step * 3^n+", "+0), indices("A2b"), indices(2*N * 3^n+", "+step * 3^n), indices(step * 3^n+", "+step * 3^n), indices("B2b"), indices(0+", "+step * 3^n), indices(0+", "+2*N * 3^n), indices("B2b"), indices(step * 3^n+", "+step * 3^n), indices(step * 3^n+", "+2*N * 3^n), indices("0, 0"), indices("1, 0"), indices("A1w"), indices("A1b"), indices("A2w"), indices("A2b")];
    extradat = [1, -1, 1, 1, -1, 1, 1, -1, 1, 1, -1, 1, 1, 1, 1, -1, 1, -1];
    rc = rc+8;
    row = [rowhol, rowpera, rowperdiffa, rowperb, rowperdiffb, extrarow];
    col = [colhol, colhol2, colpera, colperdiffa, colperb, colperdiffb, extracol];
    col = cat(2,col{:});
    data = [datahol, datapera, dataperdiffa, datapera, dataperdiffb, extradat];
end