
%%%Find discrete Riemann matrix associated to the genus 2 JS representative
%%%with varying side lengths.
tic %start time
g = 2; %Genus of JS representative given by unit squares
%%% Nlam, Dlam are numerator and denominator of lambda
%%%% Nlam and Dlam must both be odd
Nlam = 1393;
Dlam =985;
lam = Nlam/Dlam;

%%% Nmu and Dmu are num. and den. of mu
%%% Nmu and Dmu must also both be odd
Nmu = 1;
Dmu = 1;
mu = Nmu/ Dmu;
n = 0; %n = level of approximation


%make sparse matrix of coefficients
M = matrix2(g, n, Nlam, Dlam, Nmu, Dmu);
%make matrix of b vectors: ith column = ith vector
nonormstotal =2+ (3 + (Nmu/Dmu))*(Dlam*Dmu)*3^n * (Nlam * Dmu)*3^n+  ((Dlam*Dmu)*3^n+1) + ((Dlam*Dmu)*3^n+1) + (Dlam * Nmu)*3^n+1 + (Dlam * Dmu)* 3^n + 1 +(Nlam * Dmu)*3^n+1 + 2 +2;
brow = nonormstotal+1:nonormstotal+g;
bcol = 1:g;
bdat = ones(1, g);
B = sparse(brow, bcol, bdat, nonormstotal+g, g);

%numvars = the total number  of variables
numvars =((3 + (Nmu/Dmu))*(Dlam*Dmu)*3^n+1)*((Nlam * Dmu)*3^n+1) + 4*2 + 2 ;

%make solution matrix by 
sols = zeros(g);
for i = 1:g
    b = B(:, i);
    x = M\b;
    %if i>g k = i-g; else k=i; end
    for j = 1:g
        sols(i, j) = sols(i, j) + x(numvars-g+j);
        %i, j, sols %Un comment for outputting each i,j, and solutions
    end
end


n %print n
sols %print the nth level discrete Riemann matrix

toc %end time


writematrix(sols,'lam1393_985mu1.txt','Delimiter',';')   %write the solutions matrix to a .txt file