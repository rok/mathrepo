function index = varindex2(g, n, str,i, j, Nlam, Dlam, Nmu, Dmu)
%VARINDEX gives the index of the variable. 
% For the x_(i, j), str = "NA" and we just use i, j
%Use "A or B" if the variable is one of the periods. then i = the index of
%the period and j = 1 if white, 0 if black
% and use "sum" for the sum of the Bi's. then the i = which Bis we are summing
% and j doesn't matter
if str== "NA"
    index = ((Nlam * Dmu)*3^n+1)*i + j + 1;
elseif str == "A"
    index = ((3 + (Nmu/Dmu))*(Dlam*Dmu)*3^n+1)*((Nlam * Dmu)*3^n+1) + 2*i +j -1;
elseif str == "B"
    index = ((3 + (Nmu/Dmu))*(Dlam*Dmu)*3^n+1)*((Nlam * Dmu)*3^n+1) + 2*g + 2*i +j -1;
elseif str == "sum"
    index = ((3 + (Nmu/Dmu))*(Dlam*Dmu)*3^n+1)*((Nlam * Dmu)*3^n+1) + 4*g + i;
end 

 