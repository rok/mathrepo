using Oscar
using IterTools


# Computes the matrix A with columns given by e_i+e_j where [i,j] is an edge of a bipartite graph G 
# ----------------- Input:
# G = [V1,V2,E]           a bipartite graph
#                         V1 = [0,1,...k]
#                         V2 = [k+1,...,n]
#                         E = [[i,j],...] for ij an edge in the graph
# ----------------- Output:
# A                       the matrix A 
function AfromG(G)
    n=length(G[1])+length(G[2]);
    k=length(G[1])-1;
    cols = [[Int.(zeros(G[3][i][1])); Int.(1)
    ;Int.(zeros(k-G[3][i][1]));Int.(zeros(G[3][i][2]-k-1)); Int.(1) ;Int.(zeros(n-G[3][i][2]-1))] for
    i=1:length(G[3])];
    return hcat(cols...)
end;


# Computes the subgraph H = G_{I,J} of G induced by the subsets I,J of V1 and V2, respectively
# ----------------- Input:
# G                        a bipartite graph as defined above
# I                        a vector representing a subset of V1 = [0,1,...k]
# J                        a subset representing a subset of V2 = [k+1,...,n]
# ----------------- Output:
# H = [VH1,VH2,EH]         the subgraph H of G
function subgraph(G,I,J)
    E=filter(x->x[1] in I,filter(x->x[2] in J,G[3]))
    return [I,J,E]
    end;

    
# Computes the matrix A_{G/H} from Subsection 3.4
# ----------------- Input:
# G                        a bipartite graph as defined above
# H                        a subgraph of G constructed with the function subgraph(G,I,J)
# ----------------- Output:
# A                        the matrix A_{G/H}
function AfromGH(G,H)
    A = AfromG(G);
    d = Dict{Array}{Int}();
    for i=1:length(G[3])
        d[G[3][i]] = i
    end
    remaining_rows = setdiff(0:(size(A, 1)-1), union(H[1],H[2])).+ 1
    remaining_cols = [d[i] for i in setdiff(G[3],H[3])]  
    return A[remaining_rows, remaining_cols]
end;


# Compute the subdiagram volume u(S(A)/Q) assuming that the polytopes
# Conv(A_{G/H}) and Conv(A_{G/H}U{0}) have the same dimension, (see Proposition 3.11)
# ----------------- Input:
# A                       the matrix A_{G/H}
# ----------------- Output:
# integer                 the subdiagram volume
function subdiagram_volume(A)
    null_column = Int.(zeros(size(A, 1)));
    N1=convex_hull((hcat(A,null_column))');
    N2=convex_hull(A');
    if dim(N1)==dim(N2)
       return lattice_volume(N1)-lattice_volume(N2)
    else
       return lattice_volume(N1)
    end
end;


# Checks if H = G_{I,J} is connected
# my_is_connected may return true even if there is an isolated vertex, however this situation cannot arise
# ----------------- Input:
# G                        a bipartite graph as defined above
# I                        a vector representing a subset of V1 = [0,1,...k]
# J                        a subset representing a subset of V2 = [k+1,...,n]
# ----------------- Output:
# true                     if H is connected 
# false                    if H is disconnected
function
    my_is_connected(G,I,J)
         H=subgraph(G,I,J)
         d=Dict{Int}{Int}()
         for i=1:length(H[1])
             d[H[1][i]]=i
         end
         for i=1:length(H[2])
             d[H[2][i]]=i+length(H[1])
         end
    return is_connected(graph_from_edges([[d[v[1]],d[v[2]]] for v in H[3]]))
         end;
         
         
         
       
# Checks if H = G_{I,J} verifies condition (*) from Subsection 3.3
# ----------------- Input:
# G                        a bipartite graph as defined above
# I                        a vector representing a subset of V1 = [0,1,...k]
# J                        a subset representing a subset of V2 = [k+1,...,n]
# ----------------- Output:
# true                     if H verfies (*)
# false                    if H does not verify (*)
function condition_star(G,I,J)
    H=subgraph(G,I,J)
    if length(H[3])==0
        return false
    else
    d=Dict{Int}{Int}()
    for i=1:length(H[1])
        d[H[1][i]]=i
    end
    for i=1:length(H[2])
        d[H[2][i]]=i+length(H[1])
    end
    H2=graph_from_edges([[d[v[1]],d[v[2]]] for v in H[3]])
    P=[]
    for i=1:length(H[1])-1
        Q=collect(IterTools.subsets([d[v] for v in H[1]],i))
        P=append!(P,Q)
    end
    c=1
    while c<=length(P) &&
        length(unique!(collect(Iterators.flatten([all_neighbors(H2,v) for v in P[c]]))))>length(P[c])
        c+=1
    end
    return c==length(P)+1
end
end



# produces the list of pairs [I,J] that determine a subgraph corresponding
# to a non-defective face of P_G=Conv(A)
# ----------------- Input:
# G                        a bipartite graph as defined above
# ----------------- Output:
# non_defective_faces      the list of non-defective faces of G 
function non_defective_faces(G)
    P=[];
    for i=1:min(length(G[1]),length(G[2]))
        P1=collect(IterTools.subsets(G[1],i))
        P2=collect(IterTools.subsets(G[2],i))
        P=append!(P,[[P1[i],P2[j]] for i in 1:length(P1) for j in 1:length(P2)])
    end
return P=filter(x->length(subgraph(G,x[1],x[2])[3])>0 &&
        my_is_connected(G,x[1],x[2]) &&
        condition_star(G,x[1],x[2]),P)
end;


# produces the list of subgraphs corresponding to non-defective faces 
# and the corresponding multiplicities of the discriminants
# ----------------- Input:
# G                        a bipartite graph as defined above
# ----------------- Output:
# subdiagram,multiplicities     a list of pairs [[I,J],m] where I,J determines a subgraph and m is the subdiagram volume  
function bipartite_A_determinant(G)
    L  = non_defective_faces(G)
return [[K,subdiagram_volume(AfromGH(G,subgraph(G,K[1],K[2])))] for K in L]
end;

# checks the degree of the principal A-determinant accordingly with Proposition 2.4
# and the corresponding multiplicities of the discriminants
# ----------------- Input:
#G                        a bipartite graph as defined above    
# ----------------- Output:
# deg                           if the value of the degree if it agrees with the expected one 
# error message                 otherwise 
function check_degree(G)
    PAD = bipartite_A_determinant(G);
    d1 = sum([length(PAD[i][1][1])*PAD[i][2] for i=1:length(PAD)]);
    A = AfromG(G);
    P = convex_hull(A');
    d2 = (Oscar.dim(P)+1)*lattice_volume(P);
    if d1 == d2
       return d1 
    else 
        printstyled("The degree does not match the expectation from Proposition 2.4\n"; color = :magenta)    
    end
end;






    
