restart
restart
needsPackage "Quasidegrees"


----------------------
-- the function lowestDegreePart computes the monomials
-- of minimum degree in each generator of an ideal
lowestDegreePart=i->(
S=terms i_0;
d=flatten apply(S,j->degree(j));
d=min d;
L=0_(i_1);
while length(S)>0 do
(
if (degree(S_0))_0==d then L=L+S_0;
S=drop(S,1);
);
return L
)


---------------------
-- A is the matrix from Example 3.5 
A=transpose matrix{{1,0,0,1,0,0,0},{1,0,0,0,1,0,0},{0,1,0,1,0,0,0},{0,1,0,0,0,1,0},{0,1,0,0,0,0,1},{0,0,1,0,1,0,0},{0,0,1,0,0,1,0},{0,0,1,0,0,0,1}}
-- A03 is the matrix A/H for H induced by I={0},J={3}
A03=submatrix(A,{1,2,4,5,6},toList(1..7))
R03=QQ[x_0..x_6]
I03=toricIdeal(A03,R03)


---computing the local multiplicity based on Eisenbud 15.10.3
S03=QQ[join({t},toList(x_0..x_6)),MonomialOrder=>{Weights=>{1,0,0,0,0,0,0,0}}]
L=apply((entries gens I03)_0,i->sub(i,S03))
L=apply(L,i->homogenize(i,t))
G=(entries gens gb ideal L)_0
G=apply(G,i->substitute(i,{t=>1}))
G=apply(G,i->sub(i,R03))
J03=ideal apply(G,i->lowestDegreePart({i,R03}))
hilbertPolynomial(J03,Projective=>false)
--correct number 3 is found

---------------------
-- A15 is the matrix A/H for H induced by I={1},J={5}
A15=submatrix(A,{0,2,3,4,6},{0,1,2,4,5,6,7})
R15=QQ[x_0..x_6]
I15=toricIdeal(A15,R15)


---computing the local multiplicity based on Eisenbud 15.10.3
S15=QQ[join({t},toList(x_0..x_6)),MonomialOrder=>{Weights=>{1,0,0,0,0,0,0,0}}]
L=apply((entries gens I15)_0,i->sub(i,S15))
L=apply(L,i->homogenize(i,t))
G=(entries gens gb ideal L)_0
G=apply(G,i->substitute(i,{t=>1}))
G=apply(G,i->sub(i,R15))
J15=ideal apply(G,i->lowestDegreePart({i,R15}))
hilbertPolynomial(J15,Projective=>false)
--correct number 2 is found
