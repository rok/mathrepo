================================================
Euler Discriminant of Complements of Hyperplanes
================================================

| This page contains auxiliary files to the paper: 
| Claudia Fevola, and Saiei-Jaeyeong Matsubara-Heo: Euler Discriminant of Complements of Hyperplanes
| ARXIV: https://arxiv.org/abs/2411.19696 


ABSTRACT: The Euler discriminant of a family of very affine varieties is defined as the locus where the Euler characteristic of the family drops. In this work, we study the Euler discriminant of families of complements of hyperplanes. We prove that the Euler discriminant is a hypersurface in the space of coefficients, and provide its defining equation in two cases: (1) when the coefficients are generic, and (2) when they are constrained to a proper subspace. In the generic case, we show that the multiplicities of the components can be recovered combinatorially. This analysis also recovers the singularities of an Euler integral. In the appendix, we discuss a relation to cosmological correlators.


	.. image:: all.png
  	  :width: 900
	  :align: center
           

The following Jupyter notebook, which may be downloaded here :download:`PAD_notebook.ipynb <PAD_notebook.ipynb>`, illustrates how to use the main features provided by the code :math:`\verb|PAD_Hyperplanes.jl|`.

.. toctree::
	:maxdepth: 1
	:glob:

	PAD_notebook

In the Macaulay2 code :download:`multiplicities.m2 <multiplicities.m2>`, we show in an example an alternative method to compute multiplicities of components of the principal A-determinant of a complement of hyperplanes. 
This is discussed at the end of Subsection 3.4. 

Project page created: 28/11/2024

Project contributors: Claudia Fevola, Saiei-Jaeyeong Matsubara-Heo

Corresponding author of this page: Claudia Fevola, claudia.fevola@inria.fr

Software used: Julia (Version 1.11), Oscar (Version 1.2.0)

System setup used: MacBook Air with macOS Sonoma 14.6.1, Processor Apple M2, Memory 16 GB 

License for code of this project page: MIT License (https://spdx.org/licenses/MIT.html)

License for all other content of this project page: CC BY 4.0 (https://creativecommons.org/licenses/by/4.0/)

Last updated 28/11/2024.


