using Oscar
### Functions necessary for code

#Finds the first time a monodromy representation appears in a list of monodromy representations
function findfirst_monodromy(A::Vector{Perm{Int64}},B::Vector{Vector{Perm{Int64}}})
    for i in 1:length(B)
        if A == B[i]
            return i
        end
    end
    return 0
end

# returns the "canonical representative" of an element in the equivalence class of monodromy representations
# of degree 3 under automorphism.  This is the lexicographically increasing representative of the class.
function canonical_monodromy_representation_degree_3(monod::Vector{Perm{Int64}})
    a = Perm([2,1,3])
    b = Perm([3,2,1])
    c = Perm([1,3,2])
    transpos_order=[a,b,c]
    return canonical_monodromy_representation_internal(monod,transpos_order)
end



# returns the "canonical representative" of an element in the equivalence class of monodromy representations
# of degree 4 under automorphism.  This is the lexicographically increasing representative of the class.
function canonical_monodromy_representation_degree_4(monod::Vector{Perm{Int64}})
    identity_vector = [1,2,3,4]
    V = convert.(Vector{Int64},(monod))
    difference = identity_vector .!= V[1,1]
    first_perm_independent = independent_permutation_degree_4(monod[1])
    first_merge = 0
    for i in 1:length(monod)
        if (monod[i] != monod[1]) && monod[i]!=first_perm_independent
            first_merge=i
            break
        end
    end
    second_difference = identity_vector .!= V[first_merge]
    alpha = findfirst(second_difference .* difference)
    beta = findfirst(difference.* .!(second_difference .* difference))
    gamma = findfirst(second_difference .* .!(second_difference .* difference))
    delta = findfirst(.!(difference) .* .!(second_difference))
    d = Dict(alpha=>1, beta=>2, gamma=>3, delta=>4)

    monod_out = Vector{Perm{Int64}}([])
    for j in 1:length(V)
        is_changed = (V[j] .!= identity_vector)
        cycle = [findfirst(is_changed),findlast(is_changed)]
        perm_cycle = [d[cycle[k]] for k in 1:2]
        monod_out = push!(monod_out, cycle_to_perm_deg_4(perm_cycle))
    end
    return monod_out
end

# internal for canonical_monodromy_representation_degree_3
function canonical_monodromy_representation_internal(monod::Vector{Perm{Int64}},transpos_order::Vector{Perm{Int64}})
    current_index = 1
    m = deepcopy(monod)
    for i in 1:length(m)
        if order_number(m[i],transpos_order)>current_index
            m = swap(m,transpos_order[current_index],m[i])
            current_index+=1
        elseif order_number(m[i],transpos_order)==current_index
            current_index+=1
        end
    end
    return m
end

# Input is a cycle in vector notation, given as [1,2]. Returns the corresponding permutation in S_4
function cycle_to_perm_deg_4(cycle::Vector{Int64})
    identity_vector = [1,2,3,4]
    for i in 1:length(identity_vector)
        if identity_vector[i] == cycle[1]
            identity_vector[i] = cycle[2]
        elseif identity_vector[i] == cycle[2]
            identity_vector[i] = cycle[1]
        end
    end
    return Perm(identity_vector)
end

#for a transposition, returns the independent transposition of degree 4 (i.e. for (12), returns (34))
function independent_permutation_degree_4(perm::Perm{Int64})
    a = Perm([2,1,3,4])
    b = Perm([3,2,1,4])
    c = Perm([4,2,3,1])
    d = Perm([1,3,2,4])
    e = Perm([1,4,3,2])
    f = Perm([1,2,4,3])
    if perm ==a
        return f
    elseif perm == b
        return e
    elseif perm == c
        return d
    elseif perm == d
        return c
    elseif perm == e
        return b
    elseif perm == f
        return a
    end
end


# computes the different states of the monodromy representation after applying each transposition to the original configuration. 
function cycles_of_monodromy(monod::Vector{Vector{Perm{Int64}}})
    cycle_list=Vector{Vector{Perm{Int64}}}([])
    for i in 1:length(monod)
        c = (accumulate(*,monod[i]))
        cycle_list = push!(cycle_list,c)
    end
    return cycle_list
end


# computes the different states of the monodromy representation after applying each transposition to the original configuration. Additionally, saves them in a file.
function cycles_of_monodromy_to_file(monod::Vector{Vector{Perm{Int64}}})
    cycle_list=Vector{Vector{Perm{Int64}}}([])
    for i in 1:length(monod)
        c = (accumulate(*,monod[i]))
        cycle_list = push!(cycle_list,c)
    end
    open("cycles_of_monodromy_test.txt","w") do io
        write(io,string(cycle_list))
    end;
    return cycle_list
end


# returns the index of a in  order_list
function order_number(a::Perm{Int64}, order_list::Vector{Perm{Int64}})
    count = 1
    for i in 1:length(order_list)
        if order_list[i] == a
            return count
        end
        count+=1
    end
end

#swaps two permutations in an array of permutations
function swap(monod::Vector{Perm{Int64}},a::Perm{Int64},b::Perm{Int64})
    for i in 1:length(monod)        
        if monod[i] == a
            monod[i] = b
        elseif monod[i] == b
            monod[i] = a
        end
    end    
    return monod
end



#### Functions for convenience and readability ####

# maps a list of monodromy representations to their current name in the paper
function map_to_current_notation_cubics(monod::Vector{Vector{Perm{Int64}}})
    a = Perm([2,1,3])
    b = Perm([3,2,1])
    c = Perm([1,3,2])
    for i in 1:length(monod)
        monod[i] = canonical_monodromy_representation_degree_3(monod[i])
    end

    h = [
    [a,a,a,a,b,b],
    
    [a,a,a,b,a,c],
    
    [a,a,a,b,b,a],
    
    [a,a,a,b,c,b],
    
    [a,a,b,a,a,b],
    
    [a,a,b,a,b,c],
    
    [a,a,b,a,c,a],
    
    [a,a,b,b,a,a],
    
    [a,a,b,b,b,b],
    
    [a,a,b,b,c,c],
    
    [a,a,b,c,a,c],
    
    [a,a,b,c,b,a],
    
    [a,a,b,c,c,b],
    
    [a,b,a,a,a,c],
    
    [a,b,a,a,b,a],
    
    [a,b,a,a,c,b],
    
    [a,b,a,b,a,b],
    
    [a,b,a,b,b,c],
    
    [a,b,a,b,c,a],
    
    [a,b,a,c,a,a],
    
    [a,b,a,c,b,b],
    
    [a,b,a,c,c,c],
    
    [a,b,b,a,a,a],
    
    [a,b,b,a,b,b],
    
    [a,b,b,a,c,c],
    
    [a,b,b,b,a,c],
    
    [a,b,b,b,b,a],
    
    [a,b,b,b,c,b],
    
    [a,b,b,c,a,b],
    
    [a,b,b,c,b,c],
    
    [a,b,b,c,c,a],
    
    [a,b,c,a,a,b],
    
    [a,b,c,a,b,c],
    
    [a,b,c,a,c,a],
    
    [a,b,c,b,a,a],
    
    [a,b,c,b,b,b],
    
    [a,b,c,b,c,c],
    
    [a,b,c,c,a,c],
    
    [a,b,c,c,b,a],
    
    [a,b,c,c,c,b]]
    ;
    V = Vector{Int64}(zeros(length(monod)))
    #find numbers in monodromy representation of Bernds file
    for i in 1:length(monod)
        V[i]= findfirst_monodromy(monod[i], h )
    end
    # Now swap to current notation
    W = swap_our_notation(V)
    return apply_readable_form(W)
end

# Swaps the ordering of monodromy representations in Bernd's file for degree 3 to the notation used in the document, in Machine readable form
function swap_our_notation(V::Vector{Int64})
    d = Dict(1 => 27, 2=> 23, 3=> 21, 4=> 22, 5=>32, 6=>34, 7=> 30, 8=>19, 9=> 28, 10 => 20, 11=>33, 12=> 29, 13=>31, 14=>10, 15=>8,16=>9,17=>17, 18=>18,19=>16,20=>36,21=>38,22=>26,23=>24,24=>37,
    25=>39,26=>4,27=>1,28=>3,29=>11,30=>12,31=>2,32=>14,33=>15,34=>13,35=>35,36=>25,37=>40,38=>7,39=>5,40=>6)
    return getindex.(Ref(d),V)
end

# Swaps the ordering of monodromy representations in our paper to their actual name.
function apply_readable_form(V::Vector{Int64})
    d = Dict(1=>"A_1",2=>"A_2", 3=>"A_3",4=>"A_4", 5=>"A_5", 6=>"A_6", 7=>"A_7", 8=>"A_8", 9=>"A_9", 10=> "A_10", 11=>"A_11", 12=> "A_12", 13=>"A_13", 14=>"A_14", 15=>"A_15",
    16=>"A_16", 17=>"A_17", 18=>"A_18", 19=>"B_1", 20=>"B_2", 21 =>"C^l_1", 22=>"C^l_2", 23 =>"C^l_3", 24 =>"C^r_1", 25 =>"C^r_2", 26 =>"C^r_3", 27 =>"D^l_1", 28 =>"D^r_1", 29=> "E^l_1",
    30=> "E^l_2", 31=> "E^l_3",32=> "E^l_4", 33=> "E^l_5", 34=> "E^l_6", 35=> "E^r_1", 36=> "E^r_2", 37=> "E^r_3", 38=>"E^r_4", 39 => "E^r_5", 40=>"E^r_6" )
    return getindex.(Ref(d),V)
end

# applies readable form to a list of momodromy representations
function apply_readable_form_to_all(V::Vector{Vector{Int64}})
    W = Vector{Vector{String}}([])
    for i in 1:length(V)
      W = append!(W, [apply_readable_form(swap_our_notation(V[i]))])
    end
    return W
end