===========================================
Recovery of Plane Curves from Branch Points
===========================================

| This page contains auxiliary files to the paper: 
| Daniele Agostini, Hannah Markwig, Clemens Nollau, Victoria Schleis, Javier Sendra–Arranz, Bernd Sturmfels: Recovery of plane curves from branch points
| In: Discrete and computational geometry, 72 (2024) 2, p. 451-475
| DOI: `10.1007/s00454-023-00538-5 <https://dx.doi.org/10.1007/s00454-023-00538-5>`_ ARXIV: https://arxiv.org/abs/2205.11287    CODE: https://mathrepo.mis.mpg.de/BranchPoints

*Abstract of the paper*: We seek to recover plane curves from their branch points under projection onto a line. Our focus lies on cubics and quartics. These have 6 and 12 branch points respectively. The plane Hurwitz numbers 40 and 120 count the orbits of solutions. We determine the numbers of real solutions, and we present exact algorithms for recovery. Our tools rely on 150 years of beautiful algebraic geometry, from Clebsch to Vakil and beyond.

This repository includes the code in  `OSCAR.jl <https://oscar.computeralgebra.de>`_ to compute the monodromy representations of genus 1 branched coverings of degree 3, genus 3 branched coverings of degree 4,  and the corresponding real monodromy representations. This repository also contains a code to recover the 120 quartics from the branch locus as a Jupyter notebook in  `julia <https://julialang.org/>`_.

The :math:`\verb|OSCAR|`: code can be viewed here:

.. toctree::
	:maxdepth: 1
        
        monodromy.rst



The Jupyter notebook can be viewed here:

.. toctree::
        :maxdepth: 1
        :glob:
   
        120quartics

The Jupyter notebook can be downloaded here: :download:`120quartics.ipynb <120quartics.ipynb>`. 


Project page created: 19/05/2022

Project contributors: Daniele Agostini, Hannah Markwig, Clemens Nollau, Victoria Schleis, Javier Sendra–Arranz, and Bernd Sturmfels

Software used: Julia 1.7.1. and OSCAR 0.8.3-dev

Corresponding author of this page: Javier Sendra-Arranz sendra@mis.mpg.de
