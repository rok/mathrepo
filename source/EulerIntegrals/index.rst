============================================
Vector Spaces of Generalized Euler Integrals 
============================================

| This page contains auxiliary files to the paper:
| Daniele Agostini, Claudia Fevola, Anna-Laura Sattelberger, and Simon Telen: Vector spaces of generalized Euler integrals
| In: Communications in number theory and physics, 18 (2024) 2, p. 327-370
| DOI: `10.4310/CNTP.2024.v18.n2.a2 <https://dx.doi.org/10.4310/CNTP.2024.v18.n2.a2>`_ ARXIV: https://arxiv.org/abs/2208.08967 CODE: https://mathrepo.mis.mpg.de/EulerIntegrals

ABSTRACT: We study vector spaces associated to a family of generalized Euler integrals. Their dimension is given by the Euler characteristic of a very affine variety. Motivated by Feynman integrals from particle physics, this has been investigated using tools from homological algebra and the theory of :math:`D`-modules.
We present an overview and discuss relations between these approaches. We also provide new algorithmic tools.

Example 3.7 in the article illustrates how to compute the :math:`s`-parametric annihilator of :math:`f^s` for a univariate polynomial :math:`f \in \mathbb{C}[x]`. This is done using the library  `dmod\_lib <https://www.singular.uni-kl.de/Manual/4-0-3/sing_588.htm>`_ in `Singular <https://www.singular.uni-kl.de/index.php.html>`_. The code can be downloaded here :download:`singular_notebook.ipynb.zip <singular_notebook.ipynb.zip>`. It is illustrated in the following notebook: 

.. toctree::
	:maxdepth: 1
	:glob:

	Annihilator

In Example 4.2 we compute the GKZ system associated to the polynomial :math:`f = -c_1 xy^2 + c_2 xy^3 + c_3 x^2y - c_4 x^2y^3 - c_5 x^3y + c_6 x^3y^2.` The code in the subpage linked below provides a way to perform this computation in `Macaulay2 <http://www2.macaulay2.com/Macaulay2/>`_:

.. toctree::
   :maxdepth: 2

   GKZsystems.rst


Section 5 presents ideas for the use of numerical methods for computing the dimension of the vector spaces of generalized Euler integrals we are interested in. Moreover, it provides a method for obtaining :math:`\mathbb{C}`-linear relations among the generators of the vector space 

.. math:: V_\Gamma \, :=  \, \hbox{Span}_{\mathbb{C}} \left \{ [\Gamma] \longmapsto \int_\Gamma \,f^{s + a} \,x^{\nu + b} \, \frac{\mathrm{d} x}{x} \right \}_{(a,b) \, \in \, \mathbb{Z}^\ell \! \times \!\, \mathbb{Z}^n}, 

presented in the introduction, when :math:`n=1`. 
Our algorithm is implemented in `julia <https://julialang.org/>`_. The code can be downloaded here :download:`julia_notebook.ipynb.zip <julia_notebook.ipynb.zip>`. It is illustrated in the following notebook:  

.. toctree::
	:maxdepth: 1
	:glob:

	EulerIntegralsNumerics


Project page created: 18/08/2022

Project contributors: Daniele Agostini, Claudia Fevola, Anna-Laura Sattelberger, Simon Telen

Corresponding author of this page: Claudia Fevola, claudia.fevola@mis.mpg.de

Software used: Julia (Version 1.7.1), Macaulay2 (Version 1.13), Singular (Version 4.1.2)

System setup used: MacBook Pro with macOS Monterey 12.0.1, Processor 2,7 GHz Intel Core i7, Memory 16 GB 2133 MHz LPDDR3, Graphics Intel Iris Graphics 550 1536 MB.
