=======================================
Proudfoot-Speyer degenerations of scattering equations
=======================================

| This page contains auxiliary code, data and visualizations to the paper:
| Barbara Betti, Viktoriia Borovik and Simon Telen: Proudfoot-Speyer degenerations of scattering equations
| ARXIV: https://arxiv.org/abs/2410.03614   CODE: https://mathrepo.mis.mpg.de/ProudfootSpeyerDegeneration 


Overview
--------

**Abstract.** We study scattering equations of hyperplane arrangements from the perspective of combinatorial commutative algebra and numerical algebraic geometry. We formulate the problem as linear equations on a reciprocal linear space and develop a degeneration-based homotopy algorithm for solving them. We investigate the Hilbert regularity of the corresponding homogeneous ideal and apply our methods to CHY scattering equations.

The picture below on the left represents the arrangement of four lines in :math:`\mathbb{R}^{2}` of Example 2.3. The corresponding reciprocal linear space, on the right, is a cubic surface in :math:`\mathbb{P}^{3}`.
The scattering equations have three solutions represented by the three intersection points of the line with the cubic surface.


.. image:: 4linesandReciprocalsurface.png
  :width: 650
  :align: center	


* `Julia <https://docs.julialang.org/en/v1/>`_ source code :download:`ProudfootSpeyerHomotopy.zip`
* `Macaulay2 <https://macaulay2.com/>`_ source code  :download:`ScatteringEquationsM2.zip <ScatteringEquationsM2.zip>` 

The folder :download:`ScatteringEquationsM2.zip <ScatteringEquationsM2.zip>`  contains the following `Macaulay2 <https://macaulay2.com/>`_ files:

* :math:`\texttt{ScatteringEquationsUtils.m2}`: auxiliary functions
* :math:`\texttt{TestSolvingScatEqs.m2}`: it solves scattering equations on :math:`M_{0,5}` over :math:`\mathbb{Q}`
* :math:`\texttt{MacaulayMatrixU34.m2}`: computes the Macaulay matrix in Example 6.3
* :math:`\texttt{CHYamplitude.m2}`: evaluates the CHY amplitude by choosing :math:`h_1` and :math:`h_2` to be the numerator and denominator of the toric Hessian determinant as described in Example 6.3


The  `Julia <https://docs.julialang.org/en/v1/>`_ source code is explained in the following Jupyter Notebook tutorial:

.. toctree::
    :maxdepth: 1
    :glob:

    tutorial

The Notebook can be downloaded here :download:`tutorial.ipynb <Tutorial.ipynb>`


| Project page created: 20/9/2024
| Project contributors: Barbara Betti, Viktoriia Borovik and Simon Telen
| Corresponding author of this page: Barbara Betti, betti@mis.mpg.de

| Software used: Julia v. 1.10.5 , OSCAR v.1.0.4 , HomotopyContinuation v. 2.9.5, Macaulay2 v. 1.24.05 

| License for code of this project page: MIT License (https://spdx.org/licenses/MIT.html)
| License for all other content of this project page (text, images, …): CC BY 4.0 (https://creativecommons.org/licenses/by/4.0/)
| Last updated 07/10/2024
