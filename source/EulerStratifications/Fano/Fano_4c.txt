Euler strata of the GKZ family corresponding to: 
A = [1 0 1 2 1; 0 2 1 2 2] 
 
 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 2 
string: (3,1,2) 
 ideal generators: 
z1
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 2 
string: (3,1,2) 
 ideal generators: 
z2
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 4 
euler characteristic: 3 
string: (3,4,3) 
 ideal generators: 
64*z1^2*z2*z4 - 16*z1^2*z5^2 + 8*z1*z3^2*z5 - z3^4
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 2 
string: (3,1,2) 
 ideal generators: 
z4
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 2 
euler characteristic: 3 
string: (3,2,3) 
 ideal generators: 
4*z2*z4 - z5^2
------------------------------------------------- 
dimension stratum: 2 
degree stratum: 1 
euler characteristic: 0 
string: (2,1,0) 
 ideal generators: 
z3
z1
------------------------------------------------- 
dimension stratum: 2 
degree stratum: 1 
euler characteristic: 1 
string: (2,1,1) 
 ideal generators: 
z2
z1
------------------------------------------------- 
dimension stratum: 2 
degree stratum: 1 
euler characteristic: 1 
string: (2,1,1) 
 ideal generators: 
z4
z1
------------------------------------------------- 
dimension stratum: 2 
degree stratum: 2 
euler characteristic: 1 
string: (2,2,1) 
 ideal generators: 
4*z2*z4 - z5^2
z1
------------------------------------------------- 
dimension stratum: 2 
degree stratum: 2 
euler characteristic: 1 
string: (2,2,1) 
 ideal generators: 
4*z1*z5 - z3^2
z2
------------------------------------------------- 
dimension stratum: 2 
degree stratum: 1 
euler characteristic: 0 
string: (2,1,0) 
 ideal generators: 
z4
z2
------------------------------------------------- 
dimension stratum: 2 
degree stratum: 1 
euler characteristic: 1 
string: (2,1,1) 
 ideal generators: 
z5
z2
------------------------------------------------- 
dimension stratum: 2 
degree stratum: 2 
euler characteristic: 1 
string: (2,2,1) 
 ideal generators: 
4*z1*z5 - z3^2
z4
------------------------------------------------- 
dimension stratum: 2 
degree stratum: 2 
euler characteristic: 2 
string: (2,2,2) 
 ideal generators: 
4*z2*z4 - z5^2
z3
------------------------------------------------- 
dimension stratum: 2 
degree stratum: 4 
euler characteristic: 2 
string: (2,4,2) 
 ideal generators: 
4*z2*z4 - z5^2
8*z1*z5 - z3^2
------------------------------------------------- 
dimension stratum: 2 
degree stratum: 1 
euler characteristic: 1 
string: (2,1,1) 
 ideal generators: 
z5
z4
------------------------------------------------- 
dimension stratum: 1 
degree stratum: 1 
euler characteristic: 0 
string: (1,1,0) 
 ideal generators: 
z3
z2
z1
------------------------------------------------- 
dimension stratum: 1 
degree stratum: 1 
euler characteristic: 0 
string: (1,1,0) 
 ideal generators: 
z4
z2
z1
------------------------------------------------- 
dimension stratum: 1 
degree stratum: 1 
euler characteristic: 0 
string: (1,1,0) 
 ideal generators: 
z5
z2
z1
------------------------------------------------- 
dimension stratum: 1 
degree stratum: 1 
euler characteristic: 0 
string: (1,1,0) 
 ideal generators: 
z4
z3
z1
------------------------------------------------- 
dimension stratum: 1 
degree stratum: 1 
euler characteristic: 0 
string: (1,1,0) 
 ideal generators: 
z5
z4
z1
------------------------------------------------- 
dimension stratum: 1 
degree stratum: 2 
euler characteristic: 0 
string: (1,2,0) 
 ideal generators: 
4*z2*z4 - z5^2
z3
z1
------------------------------------------------- 
dimension stratum: 1 
degree stratum: 2 
euler characteristic: 0 
string: (1,2,0) 
 ideal generators: 
4*z1*z5 - z3^2
z4
z2
------------------------------------------------- 
dimension stratum: 1 
degree stratum: 1 
euler characteristic: 0 
string: (1,1,0) 
 ideal generators: 
z5
z3
z2
------------------------------------------------- 
dimension stratum: 0 
degree stratum: 1 
euler characteristic: 0 
string: (0,1,0) 
 ideal generators: 
z5
z3
z2
z1
------------------------------------------------- 
dimension stratum: 1 
degree stratum: 1 
euler characteristic: 0 
string: (1,1,0) 
 ideal generators: 
z5
z4
z2
------------------------------------------------- 
dimension stratum: 1 
degree stratum: 1 
euler characteristic: 0 
string: (1,1,0) 
 ideal generators: 
z5
z4
z3
------------------------------------------------- 
