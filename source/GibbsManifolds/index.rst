===============
Gibbs Manifolds
===============

| This page contains auxiliary files to the paper:
| Dmitrii Pavlov, Bernd Sturmfels, and Simon Telen: Gibbs manifolds
| In: Information geometry, 7 (2024) 2 supplement, p. 691-717
| DOI: `10.1007/s41884-023-00111-2 <https://dx.doi.org/10.1007/s41884-023-00111-2>`_ ARXIV: http://arxiv.org/abs/2211.15490 CODE: https://mathrepo.mis.mpg.de/GibbsManifolds

| ABSTRACT: Gibbs manifolds are images of affine spaces of symmetric matrices  under the exponential map. They arise in applications such as optimization statistics and quantum physics, where they extend the ubiquitous role of toric geometry. The Gibbs variety is the zero locus of all polynomials that vanish on the Gibbs manifold. We compute these polynomials and show that the Gibbs variety is low-dimensional. Our theory is applied to a wide range of scenarios, including matrix pencils and quantum optimal transport.

Section 3 features a symbolic implicitization algorithm for the Gibbs variety of a given LSSM defined over :math:`\mathbb{Q}`. We implemented this algorithm in `Julia  <https://julialang.org>`_ using the `Oscar <https://oscar.computeralgebra.de>`_ computer algebra package. Our code can be downloaded here: :download:`GibbsCode.zip <GibbsCode.zip>`. An implementation of the algorithm can be found in :download:`symbolic_implicitization.jl <symbolic_implicitization.jl>` inside this archive.

Example 3.4 illustrates how numerical methods can be used to implicitize Gibbs varieties that are infeasible for symbolic computation: the defining equation is found numerically for the Gibbs variety of the following LSSM of Hankel matrices:

.. math:: {\cal L} \, = \, \left\{ \begin{bmatrix} 0 & y_2 & y_3 & y_4 \\ y_2 & y_3 & y_4 & y_5\\ y_3 & y_4 & y_5 & y_6\\ y_4 & y_5 & y_6 & y_7 \end{bmatrix}  \, : \, (y_2, \ldots, y_7) \in \mathbb{R}^6 \right\}.

Julia code for this example can be downloaded here: :download:`numerical_implicitization.jl <numerical_implicitization.jl>` (this file is also part of :download:`GibbsCode.zip <GibbsCode.zip>`). The resulting equation has 853 terms and can be found here: :download:`hankel4.txt<hankel4.txt>`.

Project page created: 25/11/2022.

Project contributors: Dmitrii Pavlov, Bernd Sturmfels, Simon Telen.

Corresponding author of this page: Dmitrii Pavlov, `pavlov@mis.mpg.de <mailto:pavlov@mis.mpg.de>`_.
 
Software used: Julia (Version 1.8.3), Oscar (Version 0.11.0).

System setup used: MacBook Pro with macOS Monterey 12.6, Processor 2,8 GHz Intel Core i7, Memory 16 GB 2133 MHz LPDDR3, Graphics Intel HD Graphics 630 1536 MB.

Last updated 29/11/22.
