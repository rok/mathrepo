=============================
Principal Landau Determinants
=============================

| This page contains auxiliary files to the papers: 
| Claudia Fevola, Sebastian Mizera, and Simon Telen: Landau singularities revisited : computational algebraic geometry for Feynman integrals
| In: Physical Review Letters Vol. 132, Iss. 10, 101601 (2024)
| DOI: `10.1103/PhysRevLett.132.101601 <https://doi.org/10.1103/PhysRevLett.132.101601>`_ ARXIV: https://arxiv.org/abs/2311.14669
|
| AND
|
| Claudia Fevola, Sebastian Mizera, and Simon Telen: Principal Landau Determinants
| In: Computer physics communications, 303 (2024) , 109278
| DOI: `10.1016/j.cpc.2024.109278 <https://doi.org/10.1016/j.cpc.2024.109278>`_ ARXIV: https://arxiv.org/abs/2311.16219


ABSTRACT: We reformulate the Landau analysis of Feynman integrals with the aim of advancing the state of the art in modern particle-physics computations. We contribute new algorithms for computing Landau singularities, using tools from polyhedral geometry and symbolic/numerical elimination. Inspired by the work of Gelfand, Kapranov, and Zelevinsky (GKZ) on generalized Euler integrals, we define the principal Landau determinant of a Feynman diagram. We illustrate with a number of examples that this algebraic formalism allows to compute many components of the Landau singular locus. We adapt the GKZ framework by carefully specializing Euler integrals to Feynman integrals. For instance, ultraviolet and infrared singularities are detected as irreducible components of an incidence variety, which project dominantly to the kinematic space. We compute principal Landau determinants for the infinite families of one-loop and banana diagrams with different mass configurations, and for a range of cutting-edge Standard Model processes. Our algorithms build on the :math:`\verb|Julia|` package `Landau.jl <https://mathrepo.mis.mpg.de/Landau/>`_ and are implemented in the new open-source package :math:`\verb|PLD.jl|`.


As an illustration, the following figure shows the real part of the incidence variety introduced in Example 5.1 in the article. Our algorithm can compute the projection of each codimension one primary component to the parameter space symbolically or numerically. Section 5 of the paper explains in detail how the algorithm works.

	.. image:: project_codim1.jpg
  	  :width: 400
	  :align: center
           
As mentioned above, our symbolic and numerical methods are implemented in `julia <https://julialang.org/>`_. Our package :math:`\verb|PLD.jl|` can be download here :download:`PLD_code.zip <PLD_code.zip>`. It makes use of `HomotopyContinuation.jl <https://www.juliahomotopycontinuation.org/>`_ (v2.6.4) and `Oscar.jl <https://www.oscar-system.org/>`_ (v0.10.0). 

The following Jupyter notebook, which may be downloaded here :download:`PLD_notebook.zip <PLD_notebook.zip>`, illustrates how to use the main features provided by the package :math:`\verb|PLD.jl|`.

.. toctree::
	:maxdepth: 1
	:glob:

	PLDTutorial


A Python wrapper for PLD.jl was developed by Tristan Jacquel and can be found at https://github.com/Tracque/PLD-Wrapper. 


Database
--------

The following database gives the components of the principal Landau determinant for 23 Feynman diagrams, together with different choices of linear subspaces of the kinematic space, for a total of 114 example diagrams.

For each diagram, we provide: Symanzik polynomials, value of the generic signed Euler characteristic, f-vector of the Newton polytope of the graph polynomial.
An additional purpose of this database is to compare the components of the principal Landau determinant with the output given by the :math:`\verb|cgReduction|` from `HyperInt <https://arxiv.org/abs/1403.3385>`_.

In particular, for each component we indicate:

 * the method used to compute it, namely, :math:`\verb|PLD.jl|` (with symbolic "PLD_sym" or numerical "PLD_num" method) or HyperInt.

 * the Euler characteristic for generic choices of parameters on the prescribed component.

You can download the complete database by clicking here :download:`PLD_database.zip <PLD_database.zip>`. The notation of the files is as follows :math:`\verb|NameOfDiagram_internalMasses_externalMasses.txt|`. 
If :math:`\verb|*|` appears at the end of the file-name, it means that the :math:`\verb|cgReduction|` in HyperInt did not terminate on that specific choice of kinematic space.
For further details, we refer to Section 5 and Appendix A in the paper.


+-------------------------------------------------------------------------------+-----------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
|                                                                               |                                                                             |                                                                                        +
|   .. figure:: figures_png/A4.png                                              | .. figure:: figures_png/B4.png                                              | .. figure:: figures_png/parachute.png                                                  +
|      :width: 60%                                                              |    :width: 85%                                                              |    :width: 60%                                                                         +
|      :align: center                                                           |    :align: center                                                           |    :align: center                                                                      +
|                                                                               |                                                                             |                                                                                        +
||  :math:`G\,=\,\texttt{A}_4`                                                  || :math:`G\,=\,\texttt{B}_4`                                                 ||  :math:`G\,=\,\texttt{par}`                                                           +
||  :download:`A4_zero_zero.txt <database/A4_zero_zero.txt>`                    || :download:`B4_zero_zero.txt <database/B4_zero_zero.txt>`                   ||  :download:`par_zero_zero.txt <database/par_zero_zero.txt>`                           +
||  :download:`A4_zero_equal.txt <database/A4_zero_equal.txt>`                  || :download:`B4_zero_equal.txt <database/B4_zero_equal.txt>`                 ||  :download:`par_zero_equal.txt <database/par_zero_equal.txt>`                         +
||  :download:`A4_equal_zero.txt <database/A4_equal_zero.txt>`                  || :download:`B4_equal_zero.txt <database/B4_equal_zero.txt>`                 ||  :download:`par_equal_zero.txt <database/par_equal_zero.txt>`                         +
||  :download:`A4_zero_generic.txt <database/A4_zero_generic.txt>`              || :download:`B4_zero_generic.txt <database/B4_zero_generic.txt>`             ||  :download:`par_zero_generic.txt <database/par_zero_generic.txt>`                     +
||  :download:`A4_generic_zero.txt <database/A4_generic_zero.txt>`              || :download:`B4_generic_zero.txt <database/B4_generic_zero.txt>`             ||  :download:`par_generic_zero.txt <database/par_generic_zero.txt>`                     +
||  :download:`A4_equal_equal.txt <database/A4_equal_equal.txt>`                || :download:`B4_equal_equal.txt <database/B4_equal_equal.txt>`               ||  :download:`par_equal_equal.txt <database/par_equal_equal.txt>`                       +
||  :download:`A4_equal_generic.txt <database/A4_equal_generic.txt>`            || :download:`B4_equal_generic.txt <database/B4_equal_generic.txt>`           ||  :download:`par_equal_generic.txt <database/par_equal_generic.txt>`                   +
||  :download:`A4_generic_equal.txt <database/A4_generic_equal.txt>`            || :download:`B4_generic_equal.txt <database/B4_generic_equal.txt>`           ||  :download:`par_generic_equal.txt <database/par_generic_equal.txt>`                   +
||  :download:`A4_generic_generic.txt <database/A4_generic_generic.txt>`        || :download:`B4_generic_generic.txt <database/B4_generic_generic.txt>`       ||                                                                                       +
+-------------------------------------------------------------------------------+-----------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
|                                                                               |                                                                             |                                                                                        +
|   .. figure:: figures_png/acnode.png                                          | .. figure:: figures_png/envelope.png                                        | .. figure:: figures_png/nptrb.png                                                      +
|      :width: 60%                                                              |    :width: 60%                                                              |    :width: 80%                                                                         +
|      :align: center                                                           |    :align: center                                                           |    :align: center                                                                      +
|                                                                               |                                                                             |                                                                                        +
||  :math:`G\,=\,\texttt{acn}`                                                  || :math:`G\,=\,\texttt{env}`                                                 ||  :math:`G\,=\,\texttt{npltrb}`                                                        +
||  :download:`acn_zero_zero.txt <database/acn_zero_zero.txt>`                  || :download:`env_zero_zero.txt <database/env_zero_zero.txt>`                 ||  :download:`npltrb_zero_zero.txt <database/npltrb_zero_zero.txt>`                     +
||  :download:`acn_zero_equal.txt <database/acn_zero_equal.txt>`                || :download:`env_zero_equal.txt <database/env_zero_equal.txt>`               ||  :download:`npltrb_zero_equal.txt <database/npltrb_zero_equal.txt>`                   +
||  :download:`acn_equal_zero.txt <database/acn_equal_zero.txt>`                || :download:`env_equal_zero*.txt <database/env_equal_zero.txt>`              ||  :download:`npltrb_equal_zero.txt <database/npltrb_equal_zero.txt>`                   +
||  :download:`acn_zero_generic.txt <database/acn_zero_generic.txt>`            || :download:`env_zero_generic.txt <database/env_zero_generic.txt>`           ||  :download:`npltrb_zero_generic*.txt <database/npltrb_zero_generic.txt>`              +
||  :download:`acn_generic_zero.txt <database/acn_generic_zero.txt>`            || :download:`env_generic_zero*.txt <database/env_generic_zero.txt>`          ||  :download:`npltrb_generic_zero.txt <database/npltrb_generic_zero.txt>`               +
||  :download:`acn_equal_equal.txt <database/acn_equal_equal.txt>`              || :download:`env_equal_equal*.txt <database/env_equal_equal.txt>`            ||  :download:`npltrb_equal_equal*.txt <database/npltrb_equal_equal.txt>`                +
||  :download:`acn_equal_generic*.txt <database/acn_equal_generic.txt>`         || :download:`env_equal_generic*.txt <database/env_equal_generic.txt>`        ||  :download:`npltrb_equal_generic*.txt <database/npltrb_equal_generic.txt>`            +
||  :download:`acn_generic_equal*.txt <database/acn_generic_equal.txt>`         || :download:`env_generic_equal*.txt <database/env_generic_equal.txt>`        ||  :download:`npltrb_generic_equal*.txt <database/npltrb_generic_equal.txt>`            +
||  :download:`acn_generic_generic*.txt <database/acn_generic_generic.txt>`     || :download:`env_generic_generic*.txt <database/env_generic_generic.txt>`    ||  :download:`npltrb_generic_generic*.txt <database/npltrb_generic_generic.txt>`        +
+-------------------------------------------------------------------------------+-----------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
|                                                                               |                                                                             |                                                                                        +
|   .. figure:: figures_png/detri.png                                           | .. figure:: figures_png/debox.png                                           | .. figure:: figures_png/tdebox.png                                                     +
|      :width: 60%                                                              |    :width: 60%                                                              |    :width: 60%                                                                         +
|      :align: center                                                           |    :align: center                                                           |    :align: center                                                                      +
|                                                                               |                                                                             |                                                                                        +
|| :math:`G\,=\,\texttt{tdetri}`                                                || :math:`G\,=\,\texttt{debox}`                                               ||  :math:`G\,=\,\texttt{tdebox}`                                                        +
|| :download:`tdetri_zero_zero.txt <database/tdetri_zero_zero.txt>`             || :download:`debox_zero_zero.txt <database/debox_zero_zero.txt>`             ||  :download:`tdebox_zero_zero.txt <database/tdebox_zero_zero.txt>`                     +
|| :download:`tdetri_zero_equal.txt <database/tdetri_zero_equal.txt>`           || :download:`debox_zero_equal.txt <database/debox_zero_equal.txt>`           ||  :download:`tdebox_zero_equal.txt <database/tdebox_zero_equal.txt>`                   +
|| :download:`tdetri_equal_zero.txt <database/tdetri_equal_zero.txt>`           || :download:`debox_equal_zero.txt <database/debox_equal_zero.txt>`           ||  :download:`tdebox_equal_zero.txt <database/tdebox_equal_zero.txt>`                   +
|| :download:`tdetri_zero_generic.txt <database/tdetri_zero_generic.txt>`       || :download:`debox_zero_generic.txt <database/debox_zero_generic.txt>`       ||  :download:`tdebox_zero_generic.txt <database/tdebox_zero_generic.txt>`               +
|| :download:`tdetri_generic_zero*.txt <database/tdetri_generic_zero.txt>`      || :download:`debox_generic_zero*.txt <database/debox_generic_zero.txt>`      ||  :download:`tdebox_generic_zero*.txt <database/tdebox_generic_zero.txt>`              +
|| :download:`tdetri_equal_equal.txt <database/tdetri_equal_equal.txt>`         || :download:`debox_equal_equal.txt <database/debox_equal_equal.txt>`         ||  :download:`tdebox_equal_equal.txt <database/tdebox_equal_equal.txt>`                 +
|| :download:`tdetri_equal_generic.txt <database/tdetri_equal_generic.txt>`     || :download:`debox_equal_generic.txt <database/debox_equal_generic.txt>`     ||  :download:`tdebox_equal_generic*.txt <database/tdebox_equal_generic.txt>`            +
|| :download:`tdetri_generic_equal*.txt <database/tdetri_generic_equal.txt>`    || :download:`debox_generic_equal*.txt <database/debox_generic_equal.txt>`    ||  :download:`tdebox_generic_equal*.txt <database/tdebox_generic_equal.txt>`            +
|| :download:`tdetri_generic_generic*.txt <database/tdetri_generic_generic.txt>`|| :download:`debox_generic_generic*.txt <database/debox_generic_generic.txt>`||  :download:`tdebox_generic_generic*.txt <database/tdebox_generic_generic.txt>`        +
+-------------------------------------------------------------------------------+-----------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
|                                                                               |                                                                             |                                                                                        +
|   .. figure:: figures_png/ptrb.png                                            | .. figure:: figures_png/dbox.png                                            | .. figure:: figures_png/pentb.png                                                      +
|      :width: 75%                                                              |    :width: 75%                                                              |    :width: 75%                                                                         +
|      :align: center                                                           |    :align: center                                                           |    :align: center                                                                      +
|                                                                               |                                                                             |                                                                                        +
||  :math:`G\,=\,\texttt{pltrb}`                                                || :math:`G\,=\,\texttt{dbox}`                                                ||  :math:`G\,=\,\texttt{pentb}`                                                         +
||  :download:`pltrb_zero_zero.txt <database/pltrb_zero_zero.txt>`              || :download:`dbox_zero_zero.txt <database/dbox_zero_zero.txt>`               ||  :download:`pentb_zero_zero*.txt <database/pentb_zero_zero.txt>`                      +
||  :download:`pltrb_zero_equal.txt <database/pltrb_zero_equal.txt>`            || :download:`dbox_zero_equal.txt <database/dbox_zero_equal.txt>`             ||  :download:`pentb_zero_equal*.txt <database/pentb_zero_equal.txt>`                    +
||  :download:`pltrb_equal_zero.txt <database/pltrb_equal_zero.txt>`            || :download:`dbox_equal_zero*.txt <database/dbox_equal_zero.txt>`            ||  :download:`pentb_equal_zero*.txt <database/pentb_equal_zero.txt>`                    +
||  :download:`pltrb_zero_generic.txt <database/pltrb_zero_generic.txt>`        || :download:`dbox_zero_generic*.txt <database/dbox_zero_generic.txt>`        ||  :download:`pentb_zero_generic*.txt <database/pentb_zero_generic.txt>`                +
||  :download:`pltrb_generic_zero*.txt <database/pltrb_generic_zero.txt>`       || :download:`dbox_generic_zero*.txt <database/dbox_generic_zero.txt>`        ||                                                                                       +
||  :download:`pltrb_equal_equal.txt <database/pltrb_equal_equal.txt>`          || :download:`dbox_equal_equal*.txt <database/dbox_equal_equal.txt>`          ||                                                                                       +
||  :download:`pltrb_equal_generic.txt <database/pltrb_equal_generic.txt>`      || :download:`dbox_equal_generic*.txt <database/dbox_equal_generic.txt>`      ||                                                                                       +
||  :download:`pltrb_generic_equal.txt <database/pltrb_generic_equal.txt>`      || :download:`dbox_generic_equal*.txt <database/dbox_generic_equal.txt>`      ||                                                                                       +
||  :download:`pltrb_generic_generic*.txt <database/pltrb_generic_generic.txt>` || :download:`dbox_generic_generic*.txt <database/dbox_generic_generic.txt>`  ||                                                                                       +
+-------------------------------------------------------------------------------+-----------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
|                                                                               |                                                                             |                                                                                        +
|   .. figure:: figures_png/inner-dbox.png                                      | .. figure:: figures_png/outer-dbox.png                                      | .. figure:: figures_png/Hj.png                                                         +
|      :width: 75%                                                              |    :width: 75%                                                              |    :width: 75%                                                                         +
|      :align: center                                                           |    :align: center                                                           |    :align: center                                                                      +
|                                                                               |                                                                             |                                                                                        +
||  :math:`G\,=\,\texttt{inner-dbox}`                                           || :math:`G\,=\,\texttt{outer-dbox}`                                          ||  :math:`G\,=\,\texttt{Hj-npl-dbox}`                                                   +
||  :download:`inner-dbox_custom.txt <database/inner-dbox_custom.txt>`          || :download:`outer-dbox_custom.txt <database/outer-dbox_custom.txt>`         ||  :download:`Hj-npl-dbox_custom.txt <database/Hj-npl-dbox_custom.txt>`                 +
+-------------------------------------------------------------------------------+-----------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
|                                                                               |                                                                             |                                                                                        +
|   .. figure:: figures_png/Bhabha-dbox.png                                     | .. figure:: figures_png/Bhabha2-dbox.png                                    | .. figure:: figures_png/Bhabha-npl-dbox.png                                            +
|      :width: 75%                                                              |    :width: 75%                                                              |    :width: 75%                                                                         +
|      :align: center                                                           |    :align: center                                                           |    :align: center                                                                      +
|                                                                               |                                                                             |                                                                                        +
||  :math:`G\,=\,\texttt{Bhabha-dbox}`                                          || :math:`G\,=\,\texttt{Bhabha2-dbox}`                                        ||  :math:`G\,=\,\texttt{Bhabha-npl-dbox}`                                               +
||  :download:`Bhabha-dbox_custom.txt <database/Bhabha-dbox_custom.txt>`        || :download:`Bhabha2-dbox_custom.txt <database/Bhabha2-dbox_custom.txt>`     ||  :download:`Bhabha-npl-dbox_custom*.txt <database/Bhabha-npl-dbox_custom.txt>`        +
+-------------------------------------------------------------------------------+-----------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
|                                                                               |                                                                             |                                                                                        +
|   .. figure:: figures_png/kite.png                                            | .. figure:: figures_png/par2.png                                            | .. figure:: figures_png/Hj-npl-pentb.png                                               +
|      :width: 75%                                                              |    :width: 75%                                                              |    :width: 75%                                                                         +
|      :align: center                                                           |    :align: center                                                           |    :align: center                                                                      +
|                                                                               |                                                                             |                                                                                        +
||  :math:`G\,=\,\texttt{kite}`                                                 || :math:`G\,=\,\texttt{par}`                                                 ||  :math:`G\,=\,\texttt{Hj-npl-pentb}`                                                  +
||  :download:`kite_generic_generic.txt <database/kite_generic_generic.txt>`    || :download:`par_generic_generic.txt <database/par_generic_generic.txt>`     ||  :download:`Hj-npl-pentb_custom*.txt <database/Hj-npl-pentb_custom.txt>`              +
+-------------------------------------------------------------------------------+-----------------------------------------------------------------------------+----------------------------------------------------------------------------------------+
|                                                                               |                                                                             |                                                                                        +
|   .. figure:: figures_png/dpent.png                                           | .. figure:: figures_png/npl-dpent.png                                       | .. figure:: figures_png/npl-dpent2.png                                                 +
|      :width: 75%                                                              |    :width: 75%                                                              |    :width: 75%                                                                         +
|      :align: center                                                           |    :align: center                                                           |    :align: center                                                                      +
|                                                                               |                                                                             |                                                                                        +
||  :math:`G\,=\,\texttt{dpent}`                                                || :math:`G\,=\,\texttt{npl-dpent}`                                           ||  :math:`G\,=\,\texttt{npl-dpent2}`                                                    +
||  :download:`dpent_zero_zero*.txt <database/dpent_zero_zero.txt>`             || :download:`npl-dpent_zero_zero*.txt <database/npl-dpent_zero_zero.txt>`    ||  :download:`npl-dpent2_zero_zero*.txt <database/npl-dpent2_zero_zero.txt>`            +
+-------------------------------------------------------------------------------+-----------------------------------------------------------------------------+----------------------------------------------------------------------------------------+



Project page created: 24/11/2023

Project contributors: Claudia Fevola, Sebastian Mizera, Simon Telen

Corresponding author of this page: Claudia Fevola, claudia.fevola@inria.fr

Software used: HomotopyContinuation.jl (Version 2.6.4), Julia (Version 1.8), Oscar (Version 0.10.0)

System setup used: MacBook Pro with macOS Ventura 13.5.1, Chip Apple M2, Memory 16 GB (for running the package on a diagram with no high complexity). For the database: the full computation for one of the simplest diagrams, :math:`G\,=\,\texttt{inner-dbox}`, took 9.3 minutes, while the most difficult diagram, :math:`G\,=\,\texttt{npl-dpentb}`, took 63.7 hours on two Intel Xeon E5-2695 v4 CPUs with 18 cores each.


License for code of this project page: MIT License (https://spdx.org/licenses/MIT.html)

License for all other content of this project page: CC BY 4.0 (https://creativecommons.org/licenses/by/4.0/)

Last updated 24/11/2023.


