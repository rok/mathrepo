################################
# Diagram information
################################

name = "env"

edges = [[1, 4], [1, 2], [2, 3], [3, 4], [1, 3], [2, 4]]
nodes = [1, 2, 3, 4]
internal_masses = [0, 0, 0, 0, 0, 0]
external_masses = [0, 0, 0, 0]

U = x[1]*x[2]*x[3] + x[1]*x[2]*x[4] + x[1]*x[2]*x[6] + x[1]*x[3]*x[4] + x[1]*x[3]*x[5] + x[1]*x[3]*x[6] + x[1]*x[4]*x[5] + x[1]*x[5]*x[6] + x[2]*x[3]*x[4] + x[2]*x[3]*x[5] + x[2]*x[4]*x[5] + x[2]*x[4]*x[6] + x[2]*x[5]*x[6] + x[3]*x[4]*x[6] + x[3]*x[5]*x[6] + x[4]*x[5]*x[6]
F = (-s - t)*x[1]*x[2]*x[3]*x[4] + s*x[1]*x[3]*x[5]*x[6] + t*x[2]*x[4]*x[5]*x[6]
parameters = [s, t]
variables = [x[1], x[2], x[3], x[4], x[5], x[6]]

χ_generic = 10
f_vector = [19, 93, 193, 190, 88, 17]

################################
# Component 1
################################

D[1] = s
χ[1] = 4
weights[1] = [[-3, -1, -3, -1, -3, -3], [-2, -1, 0, 1, -1, 0], [-1, -2, -1, 1, -1, -1], [0, -1, -2, 1, 0, -1], [0, -1, -1, 1, 0, -2], [0, 1, -2, -1, -1, 0], [-2, 1, 0, -1, 0, -1], [-1, 1, -1, -2, -1, -1], [0, 1, -1, -1, -2, 0], [-1, -1, 0, 1, -2, 0], [-1, 1, 0, -1, 0, -2], [0, -1, 0, -1, 0, 0], [-1, -1, 0, 1, -1, 0], [0, -1, -1, 1, 0, -1], [0, 1, -1, -1, -1, 0], [-1, 1, 0, -1, 0, -1], [0, -1, 0, 0, 0, 0], [0, 0, 0, -1, 0, 0], [0, 0, 0, 0, 0, 0]]
computed_with[1] = ["PLD_sym"]

################################
# Component 2
################################

D[2] = s + t
χ[2] = 4
weights[2] = [[-3, -3, -3, -3, -1, -1], [-2, -1, 0, 0, -1, 1], [-1, -2, 0, 0, -1, 1], [0, -2, -1, 0, 1, -1], [0, -1, -2, 0, 1, -1], [-2, 0, 0, -1, 1, -1], [-1, 0, 0, -2, 1, -1], [-1, -1, -1, -1, 1, -2], [0, 0, -2, -1, -1, 1], [0, 0, -1, -2, -1, 1], [-1, -1, -1, -1, -2, 1], [-1, -1, 0, 0, -1, 1], [0, -1, -1, 0, 1, -1], [-1, 0, 0, -1, 1, -1], [0, 0, -1, -1, -1, 1], [0, 0, 0, 0, -1, -1], [0, 0, 0, 0, 0, -1], [0, 0, 0, 0, -1, 0], [0, 0, 0, 0, 0, 0]]
computed_with[2] = ["PLD_sym", "HyperInt"]

################################
# Component 3
################################

D[3] = t
χ[3] = 4
weights[3] = [[-1, -3, -1, -3, -3, -3], [-2, -1, 1, -1, -1, -1], [-1, -2, 1, 0, -1, 0], [-1, 0, 1, -2, 0, -1], [-1, 0, 1, -1, 0, -2], [-1, -1, 1, 0, -2, 0], [1, -2, -1, 0, 0, -1], [1, -1, -2, -1, -1, -1], [1, 0, -1, -2, -1, 0], [1, 0, -1, -1, -2, 0], [1, -1, -1, 0, 0, -2], [-1, 0, -1, 0, 0, 0], [-1, -1, 1, 0, -1, 0], [-1, 0, 1, -1, 0, -1], [1, 0, -1, -1, -1, 0], [1, -1, -1, 0, 0, -1], [-1, 0, 0, 0, 0, 0], [0, 0, -1, 0, 0, 0], [0, 0, 0, 0, 0, 0]]
computed_with[3] = ["PLD_sym"]

