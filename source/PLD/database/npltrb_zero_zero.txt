################################
# Diagram information
################################

name = "npltrb"

edges = [[1, 2], [2, 3], [3, 5], [4, 5], [1, 5], [2, 4]]
nodes = [1, 1, 3, 4]
internal_masses = [0, 0, 0, 0, 0, 0]
external_masses = [0, 0, 0, 0]

U = x[1]*x[2] + x[1]*x[3] + x[1]*x[4] + x[1]*x[6] + x[2]*x[4] + x[2]*x[5] + x[2]*x[6] + x[3]*x[4] + x[3]*x[5] + x[3]*x[6] + x[4]*x[5] + x[5]*x[6]
F = s*x[1]*x[2]*x[5] + s*x[1]*x[3]*x[4] + s*x[1]*x[3]*x[5] + s*x[1]*x[4]*x[5] + s*x[1]*x[5]*x[6] + s*x[2]*x[5]*x[6]
parameters = [s]
variables = [x[1], x[2], x[3], x[4], x[5], x[6]]

χ_generic = 5
f_vector = [18, 79, 153, 151, 76, 17]

################################
# Component 1
################################

D[1] = s
χ[1] = 0
weights[1] = [[-4, -4, -2, -1, -3, -1], [-4, -4, -5, -5, -2, -4], [-3, -2, -4, -1, -4, -1], [-3, -1, -1, -4, -4, -2], [-4, -1, -1, -2, -3, -4], [-2, -5, -4, -4, -4, -5], [-4, -5, -4, -3, -2, -2], [-3, -3, -3, 0, -3, 0], [-3, -2, 0, -2, -3, 0], [-4, -2, 0, 0, -2, -2], [-3, -5, -3, -2, -3, -3], [-3, -3, -5, -3, -3, -2], [-3, -2, -3, -5, -3, -3], [-4, -2, -3, -4, -2, -5], [-3, -5, -5, -5, -3, -5], [-2, 0, -2, -2, -4, 0], [-3, 0, -2, 0, -3, -2], [-2, -4, -5, -2, -4, -3], [-3, 0, 0, -3, -3, -3], [-2, -3, -2, -5, -4, -4], [-3, -3, -2, -3, -3, -5], [-3, -4, -4, -2, -2, -1], [-3, -3, -2, -3, -2, -1], [-4, -3, -2, -2, -1, -3], [-3, -5, -4, -3, -2, -3], [-2, -1, -1, -1, -3, 1], [-3, -1, -1, 1, -2, -1], [-2, -4, -4, -1, -3, -2], [-3, -1, 1, -1, -2, -1], [-2, -3, -1, -3, -3, -2], [-3, -3, -1, -1, -2, -3], [-2, -1, -3, -3, -3, -1], [-3, -1, -3, -2, -2, -3], [-2, -4, -5, -3, -3, -3], [-3, -1, -2, -4, -2, -4], [-2, -3, -3, -5, -3, -4], [-3, -3, -3, -4, -2, -5], [-2, 1, -1, -1, -3, -1], [-1, -2, -3, -3, -4, -2], [-2, -2, -3, -1, -3, -3], [-2, -2, -1, -4, -3, -4], [-1, -2, -2, 1, 0, -1], [-2, -2, -2, -2, -2, 0], [-3, -2, -2, -1, -1, -2], [0, -2, -2, -1, -1, 1], [-2, -4, -4, -2, -2, -2], [-3, -2, -1, -2, -1, -2], [-2, -3, -2, -3, -2, -2], [-3, -3, -2, -2, -1, -3], [-2, 0, 0, 0, -2, 0], [-1, -2, -2, -2, -3, -1], [-2, -2, -2, 0, -2, -2], [-2, -2, 0, -2, -2, -2], [0, -1, -1, 0, 1, 1], [-1, -2, -2, 0, 1, -1], [-2, 0, -2, -2, -2, -2], [-1, -2, -3, -3, -3, -2], [-2, -2, -3, -2, -2, -3], [-2, -2, -2, -4, -2, -4], [0, 1, -1, -2, -1, -2], [-1, -1, 0, -2, 1, -2], [0, 1, 0, -1, 1, -1], [0, -1, -1, -1, 1, -1], [-1, -1, -2, -2, -3, -2], [-1, -1, 1, -2, 0, -2], [1, -2, -2, -1, -1, 0], [1, -1, -1, -1, 0, -1], [1, 0, 1, -1, 0, -1], [1, -1, -1, 1, 0, 0], [1, 0, -1, -2, -1, -2], [0, -1, -1, 0, 0, 1], [-1, -2, -2, 0, 0, -1], [0, -1, -1, 1, 0, 0], [-2, -1, -1, -1, -1, -1], [-1, -2, -2, -2, -2, -1], [-2, -2, -2, -1, -1, -2], [0, -2, -2, -1, -1, 0], [-2, -2, -1, -2, -1, -2], [-1, -1, -1, -1, -2, -1], [0, -1, -1, 0, 1, 0], [-1, -1, -2, -2, -2, -2], [-1, -1, 0, -2, 0, -2], [0, 1, 0, -1, 0, -1], [0, 0, -1, -2, -1, -2], [0, 0, 0, -1, 1, -1], [0, -1, -1, -1, 0, -1], [0, 0, 1, -1, 0, -1], [1, -1, -1, 0, 0, 0], [1, 0, 0, -1, 0, -1], [0, -1, -1, 0, 0, 0], [-1, -1, -1, -1, -1, -1], [0, 0, 0, -1, 0, -1], [0, 0, 0, 0, 0, 0]]
computed_with[1] = ["PLD_sym"]

