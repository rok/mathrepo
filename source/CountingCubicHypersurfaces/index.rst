============================
Counting Cubic Hypersurfaces
============================

| This page contains auxiliary files to the paper:
| Mara Belotti, Alessandro Danelon, Claudia Fevola, and Andreas Kretschmer: The enumerative geometry of cubic hypersurfaces : point and line conditions
| In: Collectanea mathematica, 75 (2024) 2, p. 593-627
| DOI: `10.1007/s13348-023-00401-z <https://dx.doi.org/10.1007/s13348-023-00401-z>`_ ARXIV: https://arxiv.org/abs/2201.05424 CODE: https://mathrepo.mis.mpg.de/CountingCubicHypersurfaces

In this work we are interested in counting the number of smooth cubic hypersurfaces tangent to a prescribed number of lines and passing through a given number of points. With this goal we construct a compactification of the moduli space of smooth cubic hypersurfaces that we call a :math:`1`-:math:`\textit{complete variety of cubic hypersurfaces}` in analogy to the space of complete quadrics. Paolo Aluffi explored the case of plane cubic curves, see [Alu90]. Starting from his work, we construct such a space in arbitrary dimension by a sequence of five blow-ups. The counting problem is then reduced to the computation of five Chern classes by climbing the sequence of blow-ups.

In Section 3 in the article we describe the Chow rings of the blow-ups' centres and compute the intersection numbers at each of these steps. 
This repository includes our code to compute such intersection numbers, together with the Chern classes of some vector bundles needed to compute the characteristic numbers we aim to compute. 
The code is presented as a Jupyter notebook in :math:`\verb|Sage|`  which can be downloaded here :download:`characteristic_numbers.ipynb <characteristic_numbers.ipynb>`. 

These links point to the main sections of the code:


.. toctree::
        :maxdepth: 1
        :glob:
   
        ComputingCharacteristicNumbers

References
----------

.. role:: raw-html(raw)
    :format: html

[Alu90] Aluffi, Paolo. "The enumerative geometry of plane cubics. I. Smooth cubics." Transactions of the American Mathematical Society 317.2 (1990): 501-539. :raw-html:`<br />`
[Ful16] Fulton, William. Intersection theory. Princeton University Press, 2016. :raw-html:`<br />`




Project page created: 13/01/2022 :raw-html:`<br />`
Project contributors: Mara Belotti, Alessandro Danelon, Claudia Fevola, and Andreas Kretschmer :raw-html:`<br />`

Software used: Sage (Version 8.7) :raw-html:`<br />`
System setup used: MacBook Pro with macOS Monterey 12.0.1, Processor 2,7 GHz Intel Core i7, Memory 16 GB 2133 MHz LPDDR3, Graphics Intel Iris Graphics 550 1536 MB. :raw-html:`<br />`

Corresponding author of this page: Claudia Fevola, claudia.fevola@mis.mpg.de
