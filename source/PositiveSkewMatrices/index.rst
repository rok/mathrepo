========================================
Totally positive skew-symmetric matrices
========================================


| This page contains auxiliary files to the paper:
| Jonathan Boretsky, Veronica Calvo Cortes, and Yassine El Maazouz: Totally positive skew-symmetric matrices
| ARXIV: https://arxiv.org/abs/2412.17233 CODE: https://mathrepo.mis.mpg.de/PositiveSkewMatrices 


| ABSTRACT: A matrix is totally positive if all of its minors are positive. This notion of positivity coincides with the type A version of Lusztig's more general total positivity in reductive real-split algebraic groups. Since skew-symmetric matrices always have nonpositive entries, they are not totally positive in this sense. The space of skew-symmetric matrices is an affine chart of the orthogonal Grassmannian :math:`{\rm OGr}(n,2n)`. Thus, we define a skew-symmetric matrix as totally positive if it lies in the totally positive orthogonal Grassmannian. We provide a positivity criterion for these matrices in terms of a fixed collection of minors, and show that their Pfaffians have a remarkable sign pattern. The totally positive orthogonal Grassmannian is a CW cell complex and is subdivided into Richardson cells. We introduce a method to determine which cell a given point belongs to in terms of its associated matroid.


In this page, we provide ``Macaulay2`` code to compute the objects appearing in the main results of the paper. We also explain how to use the functions and recreate some of the examples from the article.


In order to run our code you should download the file :download:`PositiveSkewSymmetricMatrices.m2 <PositiveSkewSymmetricMatrices.m2>`. After loading this file you will have access to the following main functions.

* ``positivityTest`` returns the minors of a skew-symmetric matrix as in our positivity test from Definition 1.2.

* ``isTotallyPositive`` returns whether a skew-symmetric matrix with rational entries is totally positive.

* ``totallyPositiveSkewMatrix`` given :math:`n` , returns a generic totally positive skew-symmetric :math:`n \times n` matrix.

* ``nonNegativityTest`` given a skew-symmetric matrix, it returns the leading terms in the nonnegativity test from Section 4.1.

* ``isTotallyNonNegative`` returns whether a skew-symmetric matrix with rational entries is totally nonnegative.

* ``totallyNonNegativeSkewMatrix`` given an integer :math:`n` and :math:`v,w` two words in the Weyl group of type :math:`D_n`, it returns a generic totally nonnegative skew-symmetric matrix in :math:`\mathcal{R}_{v,w}`.

* ``nonNegativeRichardsonCell`` given a totally nonnegative skew-symmetric matrix :math:`A` , it returns the permutations :math:`v,w` such that :math:`A \in \mathcal{R}_{v,w}`.

* ``Pfaffians`` returns all the Pfaffians of a skew-symmetric matrix. 

We now illustrate how to use some of these functions to recreate the examples in the paper.

Minors in positivity test
~~~~~~~~~~~~~~~~~~~~~~~~~

To compute the minors in our positivity test for :math:`n=4` as in Example 1.3 you can use the following code.

.. code-block:: macaulay2

	R = QQ[a12,a13,a14,a23,a24,a34]
	M = genericSkewMatrix(R,4)
	positivityTest(M)

The output is a list where each minor is given together with its corresponding rows and columns. For example, the first element of the list is outputed :math:`(\{1,2,4\},\{1,2,3\}),a_{12}a_{14}a_{23}-a_{12}a_{13}a_{24}+a_{12}^2a_{34}`.

Generic TP skew matrix
~~~~~~~~~~~~~~~~~~~~~~

We can recreate Example 2.3 to obtain a generic totally positive skew-symmetric matrix for :math:`n=4` and its corresponding :math:`M_{j,k}` minors.

.. code-block:: macaulay2
	
	A = totallyPositiveSkewMatrix(4)
	positivityTest(A)



Matrices from the pinning
~~~~~~~~~~~~~~~~~~~~~~~~~

The code file provided here also includes a function to compute the pinning of :math:`{\rm SO}(2n)` from Section 2 of the paper. The following code computes the matrices is Example 2.5

.. code-block:: macaulay2
	
	R = QQ[t]
	x1 = phi(4,1,1,t,0,1)
	x2 = phi(4,2,1,t,0,1)
	x3 = phi(4,3,1,t,0,1)
	x4 = phi(4,4,1,t,0,1)


Nonnegative skew matrices and Richardson cells
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To obtain the results in Examples 4.2 and 5.6 we ran the following snippet of code.

.. code-block:: macaulay2

		M1=matrix{{0,0,0,2},{0,0,0,0},{0,0,0,-2},{-2,0,2,0}}
		isTotallyNonNegative(M1)

		M2=matrix{{0,0,0,2},{0,0,1,0},{0,-1,0,2},{-2,0,-2,0}}
		isTotallyNonNegative(M2)

In these cases as the nonnegativity test fails the output is the boolean value "false" together with the leading term of the minor for which the test failed with its corresponding rows and columns. For example, for :math:`M_1` it outputs (false, :math:`(\{3,4\},\{1,2\}),-16`).

---

Now, a small modification of the first matrix will help us illustrate how to compute the Richardson cell of a nonnegative skew-symmetric matrix as mentioned in Example 4.26.

.. code-block:: macaulay2

		M=matrix{{0,0,0,2},{0,0,0,0},{0,0,0,2},{-2,0,-2,0}}
		isTotallyNonNegative(M)
		nonNegativeRichardsonCell(M)

The output of this computation is the pair of permutations :math:`v=2134, w=2385` given in one line notation such that :math:`M \in \mathcal{R}_{v,w}`.





------------------------------------------------------------------------------------------------------------------------------

Project page created: 18/12/2024

Project contributors: Jonathan Boretsky, Veronica Calvo Cortes, and Yassine El Maazouz

Corresponding author of this page: Veronica Calvo Cortes, `veronica.calvo@mis.mpg.de <mailto:veronica.calvo@mis.mpg.de>`_.

Software used: Macaulay2 (v1.24.11)

System setup used: MacBook Pro with macOS Sequoia 15.1.1, Processor Apple M3 Pro, Memory 18GB.

License for code of this project page: MIT License (https://spdx.org/licenses/MIT.html)

Last updated: 18/12/2024.