======================================
Identifiability of Homoscedastic Linear Structural Equation Models using Algebraic Matroids
======================================

| This page contains auxiliary files to the article:
| Mathias Drton, Benjamin Hollering, and Jun Wu:  Identifiability of homoscedastic linear structural equation models using algebraic matroids
| In: Advances in applied mathematics, 163 (2025) B, 102794
| DOI: `10.1016/j.aam.2024.102794 <https://dx.doi.org/10.1016/j.aam.2024.102794>`_ ARXIV: https://arxiv.org/abs/2308.01821 CODE: https://mathrepo.mis.mpg.de/cyclic-sem-identifiability



**Abstract:** We consider structural equation models (SEMs), in which every variable is a function of a subset of the other variables and a stochastic error. Each such SEM is naturally associated with a directed graph describing the relationships between variables. When the errors are homoscedastic, recent work has proposed methods for inferring the graph from observational data under the assumption that the graph is acyclic (i.e., the SEM is recursive). In this work we study the setting of homoscedastic errors but allow the graph to be cyclic (i.e., the SEM to be non-recursive). Using an algebraic approach that compares matroids derived from the parameterizations of the models, we derive sufficient conditions for when two simple directed graphs generate different distributions generically. Based on these conditions, we exhibit subclasses of graphs that allow for directed cycles, yet are generically identifiable. We also conjecture a strengthening of our graphical criterion which can be used to distinguish many more non-complete graphs.  

|



Supplementary Material
----------------------

This page contains supplementary material for the computations performed in the article. All files with ending ``.ipynb`` are Jupyter notebooks executed with a :math:`\verb|SageMath|`. Files endig with ``.sobj`` are sage objects which should also be run in :math:`\verb|SageMath|` and files endig with ``.m2`` are run in :math:`\verb|Macaulay2|`. 


|

Identifiability of Graphs with Distinct Strongly Connected Components
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In the paper, Conjecture 5.1 states that if :math:`G_1` and :math:`G_2` are simple, non-complete graphs with the same out-degree sequence then
there exists a node :math:`i` in :math:`G_1` and a parentally closed set :math:`L` such that :math:`|ch_1(i) \cap L| > |ch_2(i) \cap L|`. The following Jupyter notebooks contain our supporting evidence for this conjecture for graphs with 4, 5, and 6 nodes. For 4 and 5 node graphs we verify this conjecture for every pair of graphs. For 6 node graphs, if there are less than 100,000 pairs to consider, then we verify the conjecture for all pairs. Otherwise, we randomly sample 100,000 pairs of graphs for each distinct out-degree sequence and reject pairs which have the same strongly connected components. The folder :math:`\verb|Certificates.zip|` contains the results for each out-degree sequence stored as a SageMath objects. There is a block of code in the file :math:`\verb|CyclicSEM6.ipynb|` which will quickly read in and verify that in each case, we were able to find a parentally closed set which can be used to distinguish the graphs. To check this yourself, extract the certificates folder, and place the folder in the same path as :math:`\verb|CyclicSEM6.ipynb|` then simply run the block of code which verifies all of the certificates.  

- :download:`CyclicSEM4.ipynb <CyclicSEM4.ipynb>`

- :download:`CyclicSEM5.ipynb <CyclicSEM5.ipynb>`

- :download:`CyclicSEM6.ipynb <CyclicSEM6.ipynb>`

- :download:`Certificates.zip <Certificates.zip>`


Complete graphs with the same Jacobian matroid
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Our other main conjecture (Conjecture 5.4) states that if :math:`G_1` and :math:`G_2` are complete digraphs which are identical except the direction of the last edge between node :math:`p-1` and node :math:`p` is reversed, then the associated Jacobian matroid is the same. The following code computes every such pair of complete digraphs and uses :math:`\verb|Macaulay2|` to verify that their Jacobian matroids are the same. The code below runs quickly for 4 and 5 node graphs and takes approximately 30 minutes for 6 node graphs. 


- :download:`CompleteCyclicSEMs.m2 <CompleteCyclicSEMs.m2>`


|

| **Credits**
| Project page created: 04/08/2023
| Project contributors: Mathias Drton, Benjamin Hollering, and Jun Wu
| Software used: SageMath (Version 10.0),  Macaulay2 (Version 1.22)
| System setup used: MacBook Pro with macOS Ventura 13.2.1, Apple M2 Processor, Memory 16 GB
| Corresponding authors of this page: Benjamin Hollering (benhollering@gmail.com)
