===============================
Introduction to Toric Geometry
===============================

by Chiara Meroni and Simon Telen
--------------------------------

| This page contains auxiliary files to the text:
| Simon Telen: Introduction to Toric Geometry
| ARXIV: http://arxiv.org/abs/2203.01690 CODE: https://mathrepo.mis.mpg.de/ToricGeometry

This webpage contains code accompanying a series of lectures given by Simon Telen at the Max Planck Institute for Mathematics in the Sciences in Leipzig.
Addressed topics include affine and projective toric varieties, abstract normal toric varieties from fans, divisors on toric varieties and Cox's construction of a toric variety as a GIT quotient.
We emphasize the role of toric varieties in solving systems of polynomial equations and provide many computational examples using the Julia package Oscar.jl.
For more details on the course, click `here <https://simontelen.webnode.com/l/introduction-to-toric-geometry/>`_.

.. image:: toric_surface.png

For an explanation we refer to Example 3.54 in the text.

The code is presented as a Jupyter notebook in Julia which can be downloaded here: :download:`ExamplesToricGeometry.ipynb <ExamplesToricGeometry.ipynb>`.

.. toctree::
	:maxdepth: 1
	:glob:

        ExamplesToricGeometry.ipynb

You can also run our jupyter notebook online:

.. image:: ../binder_badge.svg
	:target: https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.mis.mpg.de%2Fkuhne%2Fbinderenvironments/ToricGeometry?urlpath=git-pull%3Frepo%3Dhttps%253A%252F%252Fgitlab.mis.mpg.de%252Frok%252Fmathrepo%26urlpath%3Dtree%252Fmathrepo%252Fsource%252FToricGeometry%252FExamplesToricGeometry.ipynb%26branch%3Ddevelop




| Project page created: 04/03/2022
| Project contributors: Chiara Meroni, Simon Telen
| Software used: Julia (Version 1.7.1), Oscar (Version 0.8.1)
| System setup used: MacBook Pro with macOS Monterey 12.0.1, Processor 2,8 GHz Quad-Core Intel Core i7, Memory 16 GB 2133 MHz LPDDR3, Graphics Intel HD Graphics 630 1536 MB.
| Corresponding author of this page: Chiara Meroni, chiara.meroni@mis.mpg.de
