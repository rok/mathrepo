--- ML degree computation for all reflexive polygons

needsPackage "AlgebraicOptimization"

-- type 3
A = matrix {{2,1,0,1}, {1,2,0,1}, {1,1,1,1}}
c = {1,1,1,1};
toricMLDegree(A, c)

-- type 4a
A = matrix {{2,1,1,1,0}, {1,2,1,0,1}, {1,1,1,1,1}}
c = {1,1,1,1,1};
toricMLDegree(A, c)

-- type 4b
A = matrix {{0,1,1,2,1}, {2,2,1,1,0}, {1,1,1,1,1}}
c = {1,1,1,1,1};
toricMLDegree(A, c)

-- type 4c
A = matrix {{0,1,2,1,1}, {2,2,2,1,0}, {1,1,1,1,1}}
c = {1,1,1,1,1};
toricMLDegree(A, c)

-- type 5a
A = matrix {{0,1,0,1,2,1}, {2,2,1,1,1,0}, {1,1,1,1,1,1}}
c = {1,1,1,1,1,1};
toricMLDegree(A, c)

-- type 5b
A = matrix {{0,1,2,0,1,1}, {2,2,2,1,1,0}, {1,1,1,1,1,1}}
c = {1,1,1,1,1,1};
toricMLDegree(A, c)

-- type 6a
A = matrix {{0,1,0,1,2,1,2}, {2,2,1,1,1,0,0}, {1,1,1,1,1,1,1}}
c = {1,1,1,1,1,1,1};
toricMLDegree(A, c)

-- type 6b
A = matrix {{0,1,2,0,1,2,1}, {2,2,2,1,1,1,0}, {1,1,1,1,1,1,1}}
c = {1,1,1,1,1,1,1};
toricMLDegree(A, c)

-- type 6c
A = matrix {{0,1,2,0,1,0,1}, {2,2,2,1,1,0,0}, {1,1,1,1,1,1,1}}
c = {1,1,1,1,1,1,1};
toricMLDegree(A, c)

-- type6d
A = matrix {{0,1,2,0,1,0,0}, {3,3,3,2,2,1,0}, {1,1,1,1,1,1,1}}
c = {1,1,1,1,1,1,1};
toricMLDegree(A, c)

-- type 7a
A = matrix {{0,1,2,0,1,2,0,1}, {2,2,2,1,1,1,0,0}, {1,1,1,1,1,1,1,1}}
c = {1,1,1,1,1,1,1,1};
toricMLDegree(A, c)

-- type 7b
A = matrix {{0,1,2,0,1,0,1,0}, {3,3,3,2,2,1,1,0}, {1,1,1,1,1,1,1,1}}
c = {1,1,1,1,1,1,1,1};
toricMLDegree(A, c)

-- type 8a
A = matrix {{0,1,2,0,1,2,0,1,2}, {2,2,2,1,1,1,0,0,0}, {1,1,1,1,1,1,1,1,1}}
c = {1,1,1,1,1,1,1,1,1};
toricMLDegree(A, c)

-- type 8b
A = matrix {{0,1,2,0,1,2,0,1,0}, {3,3,3,2,2,2,1,1,0}, {1,1,1,1,1,1,1,1,1}}
c = {1,1,1,1,1,1,1,1,1};
toricMLDegree(A, c)

-- type 8c
A = matrix {{0,1,2,0,1,0,1,0,0}, {4,4,4,3,3,2,2,1,0}, {1,1,1,1,1,1,1,1,1}}
c = {1,1,1,1,1,1,1,1,1};
toricMLDegree(A, c)

-- type 9
A = matrix {{0,1,2,3,0,1,2,0,1,0}, {3,3,3,3,2,2,2,1,1,0}, {1,1,1,1,1,1,1,1,1,1}}
c = {1,1,1,1,1,1,1,1,1,1};
toricMLDegree(A, c)

