## ML degree of scaled 2-cross polytope

using HomotopyContinuation
##
@var x1 x2 x3 u[1:5]
##
F = System(
    [
        (u[1]+u[2]+u[3]+u[4]+u[5])*(2*x1*x2*x3+4*x1*x2^2*x3+25*x1*x2*x3^2+4*x1*x3+1*x1*x2)-(u[1]+u[2]+u[3]+u[4]+u[5]),
        (u[1]+u[2]+u[3]+u[4]+u[5])*(2*x1*x2*x3+2*4*x1*x2^2*x3+25*x1*x2*x3^2+1*x1*x2)-(u[1]+2*u[2]+u[3]+u[5]),
        (u[1]+u[2]+u[3]+u[4]+u[5])*(2*x1*x2*x3+4*x1*x2^2*x3+2*25*x1*x2*x3^2+4*x1*x3)-(u[1]+u[2]+2*u[3]+u[4])
    ];
    parameters = u
)
##

## Solve generically:
u0 = randn(ComplexF64,5)
result_u0 = solve(F, target_parameters = u0)
##
## Solve for random parameter values
data = [rand((0:100000), 5)]

data_points = solve(
    F,
    solutions(result_u0),
    start_parameters =  u0,
    target_parameters = data,
)
##