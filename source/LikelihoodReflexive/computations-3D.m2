--- computations for all reflexive polytopes in three dimensions

needsPackage "ReflexivePolytopesDB"
needsPackage "Polyhedra"
needsPackage "FourTiTwo"

polytopes = kreuzerSkarkeDim3();

-- determine matrix A with all-1-vector and nonnegative entries

matrixA = i -> (
    P = polytopes_i;
    P = matrix P;
    P = convexHull P;
    L = latticePoints P;
    L = matrix{L};
    n = numgens source L;
    d = numgens target L;
    seqn = 0..n-1;
    seqd = 0..d-1;
    listn = toList seqn;
    listd = toList seqd;
    listd = apply(listd, i -> min apply(listn, j -> L_(i,j)));
    T = matrix table(d,n,(i,j) -> -listd_i);
    L = L + T;
    E = transpose matrix vector(apply(listn, i -> 1));
    L = E||L
)


-- degree of toric ideals

deg = i -> (
    A = matrixA i;
    R = QQ[x_1..x_39];
    I = toricMarkov(A,R);
    degree(I)
)

for i from 0 to 4318 do(
    print(deg i)
)


-- number of generators of toric ideals

ngen = i -> (
    A = matrixA i;
    R = QQ[x_1..x_39];
    I = toricMarkov(A,R);
    numgens(I)
)

for i from 0 to 4318 do(
    print(ngen i)
)


-- number of lattice points contained in the polytope

npoints = i -> (
    P = polytopes_i;
    P = matrix P;
    P = convexHull P;
    latticePoints P
)
for i from 0 to 4318 do(
    print(length npoints i)
)


-- f-vector

fvector = i -> (
    P = polytopes_i;
    P = matrix P;
    P = convexHull P;
    fVector P
)

for i from 0 to 4318 do(
    print(fvector i)
)



