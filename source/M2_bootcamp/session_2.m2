-- Best practices
restart

f = i -> i+1

f 7
f(5)

g = (a,b,c) -> (a+2, b+3, c+7)
g(1,7,-3)

R = QQ[x,y]
J = 17

complicated = I -> (
  J := radical I; -- J is local to the function scope, := 
  if J == ideal(x,y) then return ideal(0_R);
  return (J + ideal(x,y))
)

complicated(ideal(x^2+7))
complicated(ideal(x,y))

I = ideal(x,y)
if I == ideal(x,y) then (
  print("Hello!");
  I
)

for i from 0 to 7 do ( print(i) )
for i from 0 to 7 list ( i^2 )

-- Don't
l = {}
for i from 0 to 7 do (
  l = l | {i^2}
)
l
---

-- kind of OK, but
for word in {"my", "name", "is"} do print word

viewHelp apply
apply(toList(0..7), i -> i^2)
toList(0..7) / (i -> i^2) -- same as above

R = QQ[x,y,z,w]
I = ideal(x^2+y, y^2-x*z, w*x)
prim_dec = primaryDecomposition I
prim_dec / dim

primaryDecomposition I / radical // unique -- equivalent to
unique(apply(primaryDecomposition I, radical))

unique({1,2,3,2,2})
S = QQ[ subsets(4,2) / (i -> p_i) ]
describe S

gens gb I

viewHelp forceGB

viewHelp

R = QQ[x,y, MonomialOrder => ]

matrix{{1,2}}

load "my_functions.m2"
f(1)
g(4)