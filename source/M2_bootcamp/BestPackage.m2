newPackage(
  "BestPackage",
  Headline => "an amazing package",
  Version => "1.0"
)

export {"bestMethod"}

bestMethod = method()
bestMethod ZZ := String => i -> if i == 0 then "hello" else "bye"

TEST ///
assert(bestMethod(0) == "hello")
///

beginDocumentation()

doc ///
Key
  BestPackage
Headline
  a great package
///


doc ///
Key
  bestMethod
  (bestMethod, ZZ)
Headline
  a great method, for integers
Usage
  greeting = bestMethod(i)
Inputs
  i:ZZ
    either 1 or something else
Outputs
  greeting:String
    a nice greeting
Description
  Text
    This method outputs a nice greeting based on input. The integer 0 gives a hello
  Example
    bestMethod(0)
  Text
    Any other input says goodbye
  Example
    bestMethod(123)
///

end


restart
uninstallPackage "BestPackage"
installPackage "BestPackage"
viewHelp BestPackage

needsPackage "BestPackage" -- load package
check BestPackage -- run all tests

bestMethod(1)
bestMethod(0)

viewHelp BestPackage

-- for inspiration for package documentation
needsPackage "SimpleDoc"
viewHelp SimpleDoc




needsPackage "Graphs"
viewHelp Graphs

R = QQ[x,y,z]
I = ideal(x^2)
amult I

needsPackage "SimpleDoc"
viewHelp SimpleDoc


-- HashTable
ht = hashTable { h => "Hello", b => "Bye" }

h
b
ht.h
ht.b
ht.akjhsdas

ht

ancestors class I
peek I
I.ring
peek I.cache

f = i -> i+1
f(7)
f(7/2)
f(I)

g = method()
g(7)

g ZZ := ZZ => i -> i^2    -- <method name> <input type> := <output type> => i -> i^2
g QQ := QQ => i -> i^2    -- <method name> <input type> := <output type> => i -> i^2
g(7)
g(1/2)
g(I)

g (QQ, QQ) := QQ => (i,j) -> i^2 + j^2   -- <method name> <input type> := <output type> => i -> i^2
g(1/2, 3/4)


h = method(Options => { Shift => 0 })
h ZZ := ZZ => opts -> i -> i + 1 + opts.Shift

h(7)
h(7, Shift => 27)

viewHelp newRing
options newRing
options h