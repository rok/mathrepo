using HomotopyContinuation
using LinearAlgebra
using Combinatorics

function squared_grassmannian_mldegree(n, d)
    @var a[1:d, 1:n - d]
    @var u[1:binomial(n, d)]
    
    A = [UniformScaling(1) a];
    a = vec(a);
    
    minors = [det(A[:,s]) for s in combinations(1:n, d)]
    denominator = sum([minor^2 for minor in minors])
    likelihood = sum(u[i] * log(minors[i]^2) for i in 1:binomial(n,d)) - sum(u[i] for i in 1:binomial(n,d))*log(denominator)

    F = System(differentiate(likelihood, a); parameters = u)
    start_pair = find_start_pair(F; max_tries = 10000, atol = 0.0, rtol = 1e-12)

    solns = monodromy_solve(F, start_pair[1], start_pair[2], check_startsolutions = false, show_progress=false, threading=true)
    return nresults(solns) / 2^(n-1)
end

n = parse(Int64, ARGS[1])
d = parse(Int64, ARGS[2])

println("The ML degree of the squared Grassmannian for n = ", n, " and d = ", d, " is:")
println(squared_grassmannian_mldegree(n, d))