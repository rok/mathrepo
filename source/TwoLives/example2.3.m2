R = QQ[x12, x13, x23, x14, x24, x34, x15, x25, x35, x45,
       a11, a12, a13, a21, a22, a23];

A = matrix{
    {1, 0, a11, a12, a13},
    {0, 1, a21, a22, a21}
    }

-- Compute P from Pluecker coordinates

X = matrix{
    {  0 ,  x12,  x13,  x14,  x15},
    {-x12,  0  ,  x23,  x24,  x25},
    {-x13, -x23,  0  ,  x34,  x35},
    {-x14, -x24, -x34,  0  ,  x45},
    {-x15, -x25, -x35, -x45, 0   }}


P1 = 2 * (X * transpose(X)) -- delay including the trace factor to avoid division

--- Computing P directly from A

AAT = A*transpose(A)

AATinv = inverse(promote(AAT, frac R))

P2 = transpose(A) * AATinv * A


-- Check Equality

maxMinors =  gens minors(2, A)
-- we use this to write the Pluecker coordinates in terms of the minors of A,
-- leaving the entries of A unchanged
subs = maxMinors|matrix{{a11, a12, a13, a21, a22, a23}}


P2 * sub(trace(transpose(X) * X), subs) - sub(P1, subs)  == 0


