==============================================================
Enumerating Chambers of Hyperplane Arrangements with Symmetry
==============================================================

| This page contains auxiliary material to the paper:
| Taylor Brysiewicz, Holger Eble, and Lukas Kühne: Computing characteristic polynomials of hyperplane arrangements with symmetries
| In: Discrete and computational geometry, 70 (2023) 4, p. 1356-1377
| DOI: `10.1007/s00454-023-00557-2 <https://dx.doi.org/10.1007/s00454-023-00557-2>`_ ARXIV: https://arxiv.org/abs/2105.14542 CODE: https://mathrepo.mis.mpg.de/CountingChambers



.. image:: running_example_Single.png
  :width: 500


The above picture shows an example of a hyperplane arrangement in the plane consisting of four hyperplanes. The complement consists of ten chambers.

One way of counting these chambers is through a deletion-restriction tree depicted below.

.. image:: running_example.png
  :width: 700

Our paper outlines a novel algorithm to count chambers of hyperplane arrangments taking advantage their symmetry. This yields the following reduced algorithmic structure. 

.. image:: running_example_Alg2Groups.png
  :width: 700

           
Our chamber counting algorithm is implemented in `julia <https://julialang.org/>`_ using `OSCAR.jl <https://oscar.computeralgebra.de>`_. It can perform this task for arrangements with over a quadrillion chambers. Our implementation is publicly available here: `CountingChambers.jl <https://github.com/tbrysiewicz/CountingChambers>`_.

The following Jupyter notebooks illustrate how to use our software.

.. toctree::
	:maxdepth: 1
	:glob:

	CountingChambers

You may also run this file online yourself by clicking the link below. 

.. image:: ../binder_badge.svg
 :target: https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.mis.mpg.de%2Fkuhne%2Fbinderenvironments/CountingChambers?urlpath=git-pull%3Frepo%3Dhttps%253A%252F%252Fgitlab.mis.mpg.de%252Frok%252Fmathrepo%26urlpath%3Dtree%252Fmathrepo%252Fsource%252FCountingChambers%252FCountingChambers.ipynb%26branch%3Dmaster


Project page created: 25/05/2021

Project contributors: Taylor Brysiewicz, Holger Eble, and Lukas Kühne

Corresponding author of this page: Lukas Kühne, lukas.kuhne@mis.mpg.de

Software used: Julia (Version 1.6.0), GAP
