needsPackage "Matroids";
needsPackage "Graphs";
-- them main function works only for simple, trivalent graphs
-- one would need to make multigraphs a M2 object and see how to make the functions work for them additionally to the simple graphs 

-- Input: a list of vertices A and a graph G
-- Output: a list of all edges from edges(G) that are incident to some vertex in A
incidentEdges = method()
incidentEdges (List,Graph) := List => (A,G) -> (
    if not isSubset(A,vertices G) then error "the given list must be a subset of the vertices of the graph";
    B = set{};
    for a in A do
    {
      for i from 0 to #edges(G)-1 do
      {
        e = toList((edges(G))#i);
        if e#0 == a or e#1 == a then B = B + set{i};
      };
    };
    return B;
)

-- Input: a simple graph G
-- Output: A list CheckList of boolean values
-- CheckList#i is true if the subset SubS#i in the list SubS of all subsets of the vertices of G satisfies rank(dual(matroid(G)),incidentEdges(SubS#i,G)) <= #(SubS#i) and is false otherwise
rankCheck = method()
rankCheck Graph := List => G ->(
    SubS = subsets vertices G;
    N = dual(matroid(G));
    CheckList = {};
    for i from 0 to (#SubS - 1) do
    {
        B = incidentEdges(SubS#i,G);
        CheckList = append(CheckList, rank(N,B) <= #(SubS#i));
  };
  return CheckList;
)

-- Input: a simple, trivalent graph G
-- Output: the graph curve matroid of G
graphCurveMatroid = method()
graphCurveMatroid Graph := Matroid => G -> (
    SubS := subsets vertices G;
    N := dual(matroid(G));
    rankCheckList := rankCheck(G);
    listCircuits = {};
    for i from 0 to (#SubS - 1) do {
        ok = rankCheckList#i;
        -- ok is a Boolean variable. If ok is true at the end, then A is a circuit.
        -- We start by setting ok to the Boolean value (rank(N, B) <= #A); this has to be true for A to be a circuit.
        for j from 0 to (#SubS - 1) do {
            if ok then {
                -- If we find a subset SubS#j of SubS#i which also has true as value in rankCheckList, then SubS#i is not a circuit, so we set ok to false.
                if isSubset(SubS#j,SubS#i) and SubS#j != SubS#i and SubS#j != {} and rankCheckList#j then ok = false;
            };
        };
        if ok and SubS#i != {} then listCircuits = append(listCircuits, SubS#i);
    };
    P = matroid(vertices G, listCircuits, EntryMode => "circuits");
    return P;
<<endl;)





