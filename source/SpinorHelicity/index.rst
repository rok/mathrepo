
=========================
Spinor-Helicity Varieties
=========================


| This page contains auxiliary files to the paper:
| Yassine El Maazouz, Anaëlle Pfister, Bernd Sturmfels: Spinor-Helicity Varieties
| MIS-Preprint: https://arxiv.org/pdf/2406.17331

Abstract
The spinor-helicity formalism in particle physics gives rise to natural subvarieties in the product of two Grassmannians. These include two-step flag varieties for subspaces of complementary dimension. Taking Hadamard products leads to Mandelstam varieties. We study these varieties through the lens of combinatorics and commutative algebra, and we explore their tropicalization, positive geometry, and scattering correspondence.

:download:`SpinorHelicity.m2 <SpinorHelicity.m2>`

:download:`ScatteringCorrespondence.m2 <ScatteringCorrespondence.m2>`

------------------------------------------------------------------------------------------------------------------------------

Project page created: 24/06/2024

Project contributors: Yassine El Maazouz, Anaëlle Pfister and Bernd Sturmfels

Corresponding author of this page: Anaëlle Pfister, anaelle.pfister@mis.mpg.de

Software used: Julia (Version 1.5.2), Macaulay2 (v1.20)

License for code of this project page: MIT License (https://spdx.org/licenses/MIT.html)

License for all other content of this project page (text, images, …): CC BY 4.0 (https://creativecommons.org/licenses/by/4.0/)

Last updated 24/06/2024.