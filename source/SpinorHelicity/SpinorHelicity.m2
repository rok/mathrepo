loadPackage "SpechtModule";

signOfList = (L) -> (
    Lindexed = {};
    for i from 0 to #L -1 do (Lindexed = append(Lindexed, {L_i, i}));
    Lindexed = sort(Lindexed);
    perm = {};
    for i from 0 to #Lindexed -1 do (perm = append(perm, (Lindexed_i)_1) );
    return permutationSign(perm);
);


SpinorHelicity = (k,n)-> (
   
    N = binomial(n,k);

    L = (0..n-1); 
    V =  subsets(L, k);


    Indices = apply( V , toSequence);
    
    p := symbol p;
    q := symbol q;

    pVars = {};
    qVars = {};

    for i from 0 to #Indices-1 do (
        I = Indices_i;
        pVars = append(pVars, p_I);
        qVars = append(qVars, q_I);          
    );

    V =  subsets(L, k-1);

    dp = N : {1,0};
    dq = N : {0,1};

    multiDegs = toList(join(dp,dq));

    R = QQ[pVars,qVars,Degrees => multiDegs];

    Pmatrix = {};
    Qmatrix = {};

    for i from 0 to #V - 1 do (

        rowP = {};
        rowQ = {};

        I = set(V_i);

        for l from 0 to n-1 do(

            if  (I#?l) then ( 
                rowP = append(rowP,0);
                rowQ = append(rowQ,0);
            );
                
            if  (not I#?l) then(

                Si = join(toList(I), {l});
                e = signOfList(Si);
                Si = toSequence(sort(Si));
                rowP = append(rowP,e*p_Si);
                rowQ = append(rowQ,e*q_Si);
            );
        );

        Pmatrix = append(Pmatrix, rowP);
        Qmatrix = append(Qmatrix, rowQ);

    );

    Pmatrix = matrix(Pmatrix);
    Qmatrix = matrix(Qmatrix);


    OrthoIdeal = ideal(flatten entries( Pmatrix * transpose(Qmatrix)  ));

    pVars = {};
    qVars = {};

    for i from 0 to #Indices-1 do (
        I = Indices_i;
        pVars = append(pVars, p_I);
        qVars = append(qVars, q_I);          
    );

    Gr = Grassmannian(k-1,n-1,CoefficientRing=>QQ);

    S = ring Gr;

    f = map(R,S,pVars);
    g = map(R,S,qVars);

    doubleGr = f(Gr) + g(Gr);

    return  doubleGr + OrthoIdeal;
);



############################################################
#################### ORTHOGONAL ############################
############################################################

k=3;
n=7;

J = SpinorHelicity(k,n);
time multiDegJ = multidegree JJ

I = ideal(gens gb(J));
time multiDegI = multidegree I;

flatten(entries (coefficients(multiDegJ))_1)



degSum = sum flatten(entries (coefficients(multiDegJ))_1 );

R = ring J;




print(isPrime J);
print(betti mingens J);
print(dim(J) - 2);
print(degree J);

InitIdeal = ideal(leadTerm J);

print(InitIdeal == radical InitIdeal);







############################################################
#################### Bernd's computation ###################
############################################################





R = QQ[x,y,a,b,c,d,e,f,g,h,i,
       p123,p124,p125,p126,p134,p135,p136,p145,p146,p156,p234,p235,p236,p245,p246,p256,p345,p346,p356,p456,
       q123,q124,q125,q126,q134,q135,q136,q145,q146,q156,q234,q235,q236,q245,q246,q256,q345,q346,q356,q456,
       MonomialOrder=>Eliminate 11];



I = ideal(  x-p123, 
            -y-q456, 
            x*(a*e-b*d)-p345, 
            x*(-a*h+b*g)-p245, 
            x*(a*f-c*d)-p346, 
            x*(-a*i+c*g)-p246, 
            x*(b*f-c*e)-p356,
            x*(-b*i+c*h)-p256, 
            x*(d*h-e*g)-p145, 
            x*(d*i-f*g)-p146, 
            x*(e*i-f*h)-p156, 
            x*(a*e*i-a*f*h-b*d*i+b*f*g+c*d*h-c*e*g)-p456, 
            y*(-a*e+b*d)-q126, 
            y*(-a*h+b*g)-q136, 
            y*(a*f-c*d)-q125, 
            y*(a*i-c*g)-q135, 
            y*(-b*f+c*e)-q124, 
            y*(-b*i+c*h)-q134,
            y*(-d*h+e*g)-q236, 
            y*(d*i-f*g)-q235, 
            y*(-e*i+f*h)-q234, 
            y*(a*e*i-a*f*h-b*d*i+b*f*g+c*d*h-c*e*g)-q123,
            a*x-p234, 
            a*y-q156, 
            b*x-p235, 
            -b*y-q146, 
            c*x-p236, 
            c*y-q145, 
            -d*x-p134, 
            d*y-q256, 
            -e*x-p135, 
            -e*y-q246,
            -f*x-p136,
            f*y-q245, 
            g*x-p124, 
            g*y-q356, 
            h*x-p125, 
            -h*y-q346, 
            i*x-p126, 
            i*y-q345);

I = ideal selectInSubring(1,gens gb(I));

S = QQ[p123,p124,p125,p126,p134,p135,p136,p145,p146,p156,p234,p235,p236,p245,p246,p256,p345,p346,p356,p456,
       q123,q124,q125,q126,q134,q135,q136,q145,q146,q156,q234,q235,q236,q245,q246,q256,q345,q346,q356,q456,
       Degrees=>{{1,0},{1,0},{1,0},{1,0},{1,0},{1,0},{1,0},{1,0},{1,0},{1,0},{1,0},{1,0},{1,0},{1,0},{1,0},
                 {1,0},{1,0},{1,0},{1,0},{1,0},{0,1},{0,1},{0,1},{0,1},{0,1},{0,1},{0,1},{0,1},
                 {0,1},{0,1},{0,1},{0,1},{0,1},{0,1},{0,1},{0,1},{0,1},{0,1},{0,1},{0,1}}];

F = map(S,R,{0,0,0,0,0,0,0,0,0,0,0,p123,p124,p125,p126,p134,p135,p136,p145,p146,p156,p234,
             p235,p236,p245,p246,p256,p345,p346,p356,p456,q123,q124,q125,q126,q134,
             q135,q136,q145,q146,q156,q234,q235,q236,q245,q246,q256,q345,q346,q356,q456});
I = F(I);