using HomotopyContinuation, LinearAlgebra, Combinatorics, BenchmarkTools

# Given a 2 x n matrix X, return the list of its squared maximal minors
squared_minors(X::Matrix) = [det(X[:, cols])^2 for cols in combinations(1:size(X)[2], size(X)[1])]
# Given a 2 x n matrix X and a length n-choose-2 vector u, return the log-likelihood function 
Lu(X::Matrix, u::Vector) = sum(log.(squared_minors(X)) .* u) - sum(u) * log(sum(squared_minors(X)));

"""
Compute a maximizer of the log-likelihood function for a given data vector (target)
"""
function compute_MLE(n::Int, target::Vector{Int})
    d = 2
    @var X[1:d, d+1:n]
    @var u[1:binomial(n,d)]
    x = vec(X)
    X = [I X];

    # System of rational equations we want to solve: partial derivative of the Lu
    F = System(differentiate(Lu(X, u), x), parameters = u);
    start_pair = find_start_pair(F; max_tries = 10000, atol = 0.0, rtol = 1e-12);
    # Find critical points of Lu for random complex parameters
    # Note that specifying the number of solutions to expect significantly speeds up computation
    m = monodromy_solve(F, start_pair[1], start_pair[2]; target_solutions_count = 2^(n-2)*factorial(n-1), threading=true, show_progress=false);

    # Track solutions to target parameters
    solns = HomotopyContinuation.solve(F, solutions(m); start_parameters=parameters(m), target_parameters = target, show_progress=false)
    real_solns = [[I reshape(real.(s), (d, n-d))] for s in solutions(solns)];

    # Return one of the matrices which maximizes Lu
    return real_solns[argmax([Lu(s, target) for s in real_solns])]
end

# Example: test the runtime for computing the MLE for small values of n
n = 6 
println("n = ", n)
# Compute the runtime for random data
@benchmark compute_MLE(n, data) setup=(data=rand(1:1000, binomial(n, 2)))