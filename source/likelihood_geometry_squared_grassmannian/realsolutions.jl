using HomotopyContinuation, LinearAlgebra, Combinatorics

# Given a 2 x n matrix X, return the list of its squared maximal minors
squared_minors(X::Matrix) = [det(X[:, cols])^2 for cols in combinations(1:size(X)[2], size(X)[1])]
# Given a 2 x n matrix X and a length n-choose-2 vector u, return the log-likelihood function 
Lu(X::Matrix, u::Vector) = sum(log.(squared_minors(X)) .* u) - sum(u) * log(sum(squared_minors(X)));

"""
Compute start solutions for a vector of random complex parameters.
For a fixed d, n, this function only needs to be run once. 
The solutions can then be tracked to the desired targets.
"""
function generate_start_solutions(n::Int, d::Int)
    @var X[1:d, d+1:n]
    @var u[1:binomial(n,d)]
    x = vec(X)
    X = [I X];

    # System of rational equations we want to solve: partial derivative of the Lu
    F = System(differentiate(Lu(X, u), x), parameters = u);
    start_pair = find_start_pair(F; max_tries = 10000, atol = 0.0, rtol = 1e-12);
    # Find critical points of Lu for random complex parameters
    return F, monodromy_solve(F, start_pair[1], start_pair[2]; threading=true, show_progress=false);
end

"""
Given start solutions and parameters for the system F in the form 
of a MonodromyResult, track the solutions to the desired parameter 
vector (target). For the real solutions, use the second derivative 
test to check whether each solution is a local maximum or not. 
"""
function nreal_nmax_solutions(F::System, m::MonodromyResult{Vector{ComplexF64}, Vector{ComplexF64}}, target::Vector{Int})
    # Track solutions to real target parameters
    sols = HomotopyContinuation.solve(F, solutions(m); start_parameters=parameters(m), target_parameters = target, show_progress=false)

    # Construct the hessian of the log-likelihood function from the partial derivatives
    x = variables(F)    
    u = parameters(F)
    hessian = hcat([differentiate(f, x) for f in F]...)

    # For each solution check whether it is a local max or not
    nlocal_max = 0
    for sol in solutions(sols; only_real=true)
        is_local_max = true
        h = evaluate(hessian, x => sol, u => target)
        eigenvalues = real.(eigvals(h))
        # A solution is a local max exactly when the hessian has only negative eigenvalues
        for eigenvalue in eigenvalues
            if abs(eigenvalue) < 1e-15 || eigenvalue > 0.0
                is_local_max = false
                break
            end
        end
        nlocal_max += Int(is_local_max)
    end

    return nreal(sols), nlocal_max
end 


# Example: test a few random real vectors and see 
# how many real critical points and local maxima 
# the log-likelihood function has
d = 3
n = 6
println("d = ", d, ", n = ", n)
F, m = generate_start_solutions(n, d);
println("Number of complex critical points: ", nsolutions(m))
for _ in 1:5
    nreal_sols, nlocal_max = nreal_nmax_solutions(F, m, rand(1:1000, binomial(n, d)))
    println("Number of real critical points: ", nreal_sols, 
            ", Number of local maxima: ", nlocal_max);
end