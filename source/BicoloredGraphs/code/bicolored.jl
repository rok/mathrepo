using HomotopyContinuation
using LinearAlgebra
using Plots
using LaTeXStrings
using AMRVW


# need higher precision to compute accurate asymptotics
setprecision(BigFloat, 1000; base=10)


# double factorial
function dfactorial(n)
    if n <= 0
        return 1
    elseif n % 2 == 0
        return 0
    else
        return BigInt(n*dfactorial(n-2))    # need BigInt to prevent overflow
    end
end


# variables used throughout the code
# a and b serve as parameters, u is an auxiliary variable 
@var x,y,a,b,u 


# polynomial for 2d Ising model example
V = x^4//24 + a^2*y^4//24 + a*x^2*y^2//4


# if V contains parameters, need to specify the variables, e.g. for 
# V = x^4//24 + a^2*y^4//24 + a*x^2*y^2//4, use get_n_coeff(n, V, vars=[x,y])
function get_n_coeff(n, V; vars=variables(V))
    Vu = 1/u^2 * subs(V, vars => u*vars)

    d = degree(Vu, [u])
    Fs = deleteat!(reverse(coeffs_as_dense_poly(u*differentiate(Vu, u), [u], d)), 1)    # only start at u^1
    Qs = Vector{Any}([Rational{BigInt}(1)])   # Q_0 = 1 is at 1st position
    for i in 1:2*n
        push!(Qs, expand(1//i * sum(Fs[j]*Qs[length(Qs) - j + 1] for j in 1:min(length(Fs), length(Qs)))))
    end

    (M, c) = exponents_coefficients(Qs[2*n + 1], vars)
    nth_coeff = Rational{BigInt}(0)
    for i in 1:size(M,2)
        m = M[:, i]
        nth_coeff += prod(dfactorial(l - 1) for l in m) * c[i]
    end
    return nth_coeff
end


# compute weighted sum of 4-regular graphs with Euler characteristic -2
# where each vertex has an even number of edges of each color
get_n_coeff(2, V, vars=[x,y])


# evaluate with BigFloats
function big_evaluate(c, paramvals; params=[a])
    (expos, coeffs) = exponents_coefficients(c, params)
    return sum(coeffs[i] * (prod(map((i,j) -> big(i)^j, paramvals, expos[i]))) for i in 1:length(expos))
end


# returns a tuple (α, β, c) where A_n ~ c*α^n*n^β*Γ(n)
function get_asymp_expansion_params_inhom(V, aval; r=10, n=150, digits=10)
    ag = [log(big_evaluate(get_n_coeff(n-i, V, vars=[x,y]), [aval])/factorial(big(n-i)-1)) for i in 0:r]
    M = transpose(hcat([vcat([big(n-i), log(big(n-i)), big(1)], [1/(big(n-i))^(j) for j in 1:r-2]) for i in 0:r]...))
    X = Float64.(round.(M \ ag, digits=digits))
    return [exp(X[1]), X[2], exp(X[3])]
end


# returns a tuple (α, β, c) where A_n ~ c*α^n*n^β*Γ(n)
# chooses n such that A_n ≠ 0
function get_asymp_expansion_params_hom(V, aval; r=10, n=150, digits=10)
    k = degree(V, [x,y])
    n = Int(floor((n-r)/(k-2)))
    ag = [log(big_evaluate(get_n_coeff((k-2)*(n-i), V, vars=[x,y]), [aval])/factorial((k-2)*big(n-i)-1)) for i in 0:r]
    M = transpose(hcat([vcat([(k-2)*big(n-i), log((k-2)*big(n-i)), big(1)], [1/((k-2)*big(n-i))^(j) for j in 1:r-2]) for i in 0:r]...))
    X = Float64.(round.(M \ ag, digits=digits))
    return [exp(X[1]), X[2], exp(X[3])]
end


# create plot comparing true with numerical value for c(λ)
avalues = vcat([i/60 for i in 0:20])
V = x^4//24 + a^2*y^4//24 + a*x^2*y^2//4
params = [get_asymp_expansion_params_hom(V, av, n=100) for av in avalues]
plot(avalues, [param[3] for param in params], linewidth=4, linecolor=colorant"rgb(68,170,153)", label="numerical", dpi=800, xticks=([0,0.1,0.2,0.3], [L"0", L"0.1", L"0.2", L"0.3"]), yticks=([0.2,0.4,0.6,0.8,1.0], [L"0.2", L"0.4", L"0.6", L"0.8", L"1.0"]))
plot!(avalues, [1/pi * sqrt(1/(2-6*av)) for av in avalues], linewidth=4, linecolor=colorant"rgba(187,85,102,0.5)", label="true", dpi=800)
xlabel!(L"\lambda", dpi=800)
ylabel!(L"c(\lambda)", dpi=800)
savefig("plot_c_Ising.png")


# create plot of zeros of coefficients
# confer Example 21
V = x^4//24 + a^2*y^4//24 + a*x^2*y^2//4
cfs = [coefficients(get_n_coeff(i, V, vars=[x,y]), a) for i in [10, 25, 200]]
roots = [AMRVW.roots(float.(cf)) for cf in cfs]
scatter(roots[1], xlims=(-1.5, 3.5), ylims=(-2,2), label=L"n=10", markershape=:diamond, markercolor=colorant"rgb(0,153,136)", markeralpha=0.9, markerstrokewidth=0, dpi=800)
scatter!(roots[2], label=L"n=25", markershape=:square, markercolor=colorant"rgb(187,85,102)", markeralpha=0.9, markerstrokewidth=0, dpi=800)
scatter!(roots[3], label=L"n=200", markershape=:circle, markercolor=colorant"rgb(221,170,51)", markeralpha=0.5, markerstrokewidth=0, dpi=800)
xlabel!(L"\mathrm{Re}\,(\lambda)")
ylabel!(L"\mathrm{Im}\,(\lambda)")
xticks!([-1,0,1,2,3], [L"-1", L"0", L"1", L"2", L"3"])
yticks!([-2,-1,0,1,2], [L"-2", L"-1", L"0", L"1", L"2"])
savefig("plot_zeros.png")


# example inhomogeneous polynomial
V = x^3//6 + a^2*y^4//24 + a*x*y^2//2
avalues = vcat([i/20 for i in 0:10])
params = [get_asymp_expansion_params_inhom(V, av, n=50) for av in avalues]
plot(avalues, [param[3] for param in params], linewidth=4, linecolor=colorant"rgb(68,170,153)", label="numerical", dpi=800, ylims=(0.1,0.7), xticks=([0,0.1,0.2,0.3,0.4,0.5], [L"0", L"0.1", L"0.2", L"0.3", L"0.4", L"0.5"]), yticks=([0.1,0.2,0.3,0.4,0.5,0.6,0.7], [L"0.1", L"0.2", L"0.3", L"0.4", L"0.5", L"0.6", L"0.7"]))
plot!(vcat(avalues[1:10], [0.49]), [1/(2*pi*sqrt(1-2*av)) for av in vcat(avalues[1:10], [0.49])], linewidth=4, linecolor=colorant"rgba(187,85,102,0.5)", label="true", dpi=800)
xlabel!(L"\lambda", dpi=800)
ylabel!(L"c(\lambda)", dpi=800)
savefig("plot_c_inhom.png")