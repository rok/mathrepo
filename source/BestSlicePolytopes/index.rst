=================================
The Best Ways to Slice a Polytope
=================================

| This page contains auxiliary files to the paper:
| Marie-Charlotte Brandenburg, Jesús A. De Loera, and Chiara Meroni: The best ways to slice a polytope
| In: Mathematics of computation, 94 (2025), p. 1003-1042
| DOI: `10.1090/mcom/4006 <https://dx.doi.org/10.1090/mcom/4006>`_ ARXIV: https://arxiv.org/abs/2304.14239 CODE: https://mathrepo.mis.mpg.de/BestSlicePolytopes

.. figure:: comb_types_cross4.png
   :width: 60%
   :align: center

|

We study the structure of the set of all possible affine hyperplane sections of a convex polytope. We present two different cell decompositions of this set, induced by hyperplane arrangements. Using our decomposition, we bound the number of possible combinatorial types of sections and craft algorithms that compute optimal sections of the polytope according to various combinatorial and metric criteria, including sections that maximize the number of k-dimensional faces, maximize the volume, and maximize the integral of a polynomial. Our optimization algorithms run in polynomial time in fixed dimension, but the same problems show hardness otherwise. Our tools can be extended to intersection with halfspaces and projections onto hyperplanes. Finally, we present several experiments illustrating our theorems and algorithms on famous polytopes.

The objective of this page is to present an implementation of the algorithms mentioned above. The underlying structure is given by hyperplane arrangements; the optimization question can be then both combinatorial or metric. For instance, the figure above shows all combinatorial types of hyperplane sections of the 4-dimensional cross-polytope. This can be computed using the following notebook:\

- `Combinatorial types and maximum number of k-faces of all affine slices <combinatorial-affine-sections.html#Input>`_

A variant with focus on central sections through the origin can be found here:

- `Combinatorial types and maximum number of k-faces of central slices <combinatorial-central-sections.html#Input>`_

From a metric point of view, an interesting question is to find the slice of a polytope with the largest volume, or more in general the largest integral of a certain polynomial. As it is explained in the paper, these quantities are rational functions. We compute these functions, following the approach in Section 3.2, using the following notebook:

- `Integral and volume of all affine slices <integral-volume-affine-sections.html#Input>`_

We can again specify this computation to central section. This is intimately connected to previous results on intersection bodies of polytopes, see `this page <https://mathrepo.mis.mpg.de/intersection-bodies/index.html>`_ for more details and implementation. 

The jupyter notebooks can be downloaded here: 

- :download:`combinatorial-affine-sections.ipynb <combinatorial-affine-sections.ipynb>`

- :download:`combinatorial-central-sections.ipynb <combinatorial-central-sections.ipynb>`

- :download:`integral-volume-affine-sections.ipynb <integral-volume-affine-sections.ipynb>`

------------------------------------------------------------------------------------------------------------------------------

| **Credits**
| Project page created: 13/06/2023
| Project contributors: Jesús A. De Loera, Marie-Charlotte Brandenburg, and Chiara Meroni
| Software used: SageMath (Version 9.2)
| System setup used: MacBook Pro with macOS Monterey 12.0.1, Processor 2,8 GHz Quad-Core Intel Core i7, Memory 16 GB 2133 MHz LPDDR3, Graphics Intel HD Graphics 630 1536 MB.
| Corresponding author of this page: Marie-Charlotte Brandenburg, marie.brandenburg@mis.mpg.de
| License for code of this project page: MIT License (https://spdx.org/licenses/MIT.html)
| License for all other content of this project page (text, images, …): CC BY 4.0 (https://creativecommons.org/licenses/by/4.0/)

Last updated 13/06/2023.