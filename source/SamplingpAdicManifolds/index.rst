=======================================================
Sampling from a :math:`p`-adic manifold
=======================================================

| This page contains auxiliary files to the paper:
| Yassine El Maazouz and Enis Kaya: Sampling from :math:`p`-adic manifold
| ARXIV: https://arxiv.org/abs/2207.05911 CODE: https://mathrepo.mis.mpg.de/SamplingpAdicManifolds/index.html

   
Abstract:
We give a method for sampling points from an algebraic manifold (affine or projective) over a local field with a prescribed probability distribution. In the spirit of the previous work by Breiding and Marigliano on real algebraic manifolds, our method is based on slicing the given variety with random linear spaces of complementary dimension. We also provide an implementation of our sampling method and discuss a few applications. For example, we sample from algebraic :math:`p`-adic matrix groups and modular curves.






.. image:: 1.png
  :width: 300



This repository includes an implementation of the sampling method in the special case where we sample from :math:`\mathcal{O}`-valued points of the manifold :math:`X`.


.. toctree::
  :maxdepth: 1
  :glob:
  
  code  




Project page created: 15/06/2022.

Project contributors: Yassine El Maazouz and Enis Kaya.

Corresponding author of this page: Yassine El Maazouz, yassine.el-maazouz@berkeley.edu

Software used: Sage (version 9.5), Magma (version 2.26-10).
