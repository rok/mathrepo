-- This is an example of a 2x2x3 game whose Nash equilibrium scheme is non-reduced of dimension 0.
-- This scheme is supported at the point ((2/3,1/3),(2/3,1/3),(3/5,1/5,1/5)).
restart
KK = QQ;
d = {2,2,3}; -- game format
R = KK[flatten apply(#d, i-> apply(d#i, j-> p_(i+1,j+1)))];
for i in 1..#d do vecp_i = matrix{apply(d#(i-1), j-> p_(i,j+1))};

-- list of multi-indices
I = toList((#d:1)..toSequence(d))

-- payoff tensors;
X1 = {matrix {{2,2}, {1,3}}, matrix {{1,2}, {2,3}}, matrix {{3,0}, {2,2}}}
X2 = {matrix {{3,2}, {1,4}}, matrix {{2,1}, {1,3}}, matrix {{4,2}, {2,3}}}
X3 = {matrix {{3,1}, {2,4}}, matrix {{2,2}, {3,4}}, matrix {{1,2}, {3,8}}}


-- the lines below are for a 3-player game with payoff tensors written as above
T1 = sum(I, j-> (X1#(j#2-1))_(j#0-1,j#1-1)*p_(1,j#0)*p_(2,j#1)*p_(3,j#2))
T2 = sum(I, j-> (X2#(j#2-1))_(j#0-1,j#1-1)*p_(1,j#0)*p_(2,j#1)*p_(3,j#2))
T3 = sum(I, j-> (X3#(j#2-1))_(j#0-1,j#1-1)*p_(1,j#0)*p_(2,j#1)*p_(3,j#2))

A1 = contract(vecp_1,T1) || matrix{toList(d#0:1)}
A2 = contract(vecp_2,T2) || matrix{toList(d#1:1)}
A3 = contract(vecp_3,T3) || matrix{toList(d#2:1)}

J1 = minors(2, A1) + ideal(-1 + sum first entries vecp_1);
J2 = minors(2, A2) + ideal(-1 + sum first entries vecp_2);
J3 = minors(2, A3) + ideal(-1 + sum first entries vecp_3);

J = trim(J1+J2+J3);
codim J
degree J

decJ = decompose J;

trim sub(J, {p_(1,1) => 2/3, p_(1,2) => 1/3,
	     p_(2,1) => 2/3, p_(2,2) => 1/3,
	     p_(3,1) => 3/5, p_(3,2) => 1/5, p_(3,3) => 1/5}) -- 0
