===========================================
A vector bundle approach to Nash equilibria
===========================================

| This page contains auxiliary files to the article:
| Hirotachi Abo, Irem Portakal, Luca Sodomaco: A vector bundle approach to Nash equilibria
| ARXIV: https://arxiv.org/abs/25XX.XXXX CODE: https://mathrepo.mis.mpg.de/vector-bundle-nash-equilibria/


Supplementary Material
----------------------

The scripts are ending with ``.m2`` and are run in :math:`\verb|Macaulay2|`. 

We provide the following code:

* :download:`example_333_real_root_isolation.m2 <example_333_real_root_isolation.m2>` provides a numerical computation of the Nash equilibrium scheme of a :math:`(3,3,3)` game (Example 2.5). It also certifies that four of the isolated points of the Nash equilibrium scheme are real.
* :download:`singular_strata_222.m2 <singular_strata_222.m2>` computes the singular strata of the Nash discriminant variety for :math:`(2,2,2)` games (Section 3.2).
* :download:`variety_three_non_concurrent_lines.m2 <variety_three_non_concurrent_lines.m2>` computes the Chern-Mather volumes of the singular projective toric variety parametrizing global sections whose Nash equilibrium scheme consists of space connected curve, union of three non concurrent lines (`Remark C <https://mathrepo.mis.mpg.de/vector-bundle-nash-equilibria/#remark-c>`_).
* :download:`variety_three_concurrent_lines.m2 <variety_three_concurrent_lines.m2>` computes the Chern-Mather volumes of the singular projective toric variety parametrizing global sections whose Nash equilibrium scheme consists of space connected curve, union of three concurrent lines (`Remark D <https://mathrepo.mis.mpg.de/vector-bundle-nash-equilibria/#remark-d>`_).
* :download:`case_223.m2 <case_223.m2>` computes the two irreducible components of the Nash discriminant variety for :math:`(2,2,3)` games (Example 3.28).
* :download:`game223_one_double_tmNe.m2 <game223_one_double_tmNe.m2>` computes the Nash equilibrium scheme of a :math:`(2,2,3)` game (Example 3.29). It verifies that the Nash equilibrium scheme consists of a double point whose support is the unique totally mixed Nash equilibrium.

.. list-table:: Singular strata of :math:`\Delta(E)`. The identities in the first column were verified using :math:`\verb|Macaulay2|`.
   :widths: 10 30 20 20
   :header-rows: 1

   * - :math:`i`
     - :math:`\text{Sing}^{(i)}(\Delta(E))`
     - :math:`\dim(\text{Sing}^{(i)}(\Delta(E)))`
     - Number of irreducible components
   * - :math:`1`
     - :math:`\Delta^{\text{cub}}(E) \cup \Delta^{\text{con}}(E) \cup \Delta^{\text{lin}}(E)`
     - :math:`8`
     - :math:`7 = 1 + 3 + 3`
   * - :math:`2`
     - :math:`\Delta^{\text{ll}}(E) \cup \Delta^{\text{cl}}(E)`
     - :math:`7`
     - :math:`6 = 3 + 3`
   * - :math:`3`
     - :math:`\Delta^{\text{lll}}(E) \cup \Delta^{\text{scr}}(E)`
     - :math:`6`
     - :math:`6 = 3 + 3`
   * - :math:`4`
     - :math:`\Delta^{\text{3l}}(E)`
     - :math:`5`
     - :math:`1`
   * - :math:`5`
     - :math:`\Delta^{\text{qq}}(E)`
     - :math:`2`
     - :math:`3`


Singular strata of the Nash discriminant variety for :math:`(2,2,2)` games
--------------------------------------------------------------------------

The singular locus :math:`\text{Sing}^{(1)}(\Delta(E))` of the Nash discriminant variety :math:`\Delta(E)` of  :math:`(2,2,2)` games is studied in Section 3.2 of the mentioned paper. We present the full computation of the singular strata of :math:`\Delta(E)` here as appendix with accompanied code in :math:`\verb|Macaulay2|`. 

Proposition 1.
============== 
The locus :math:`\Delta^{\mathrm{ll}}(E)` consists of three irreducible components, each having dimension :math:`7` and degree :math:`8`.

   *Proof.* Given :math:`f\in H^0(\mathbb{P}^{\boldsymbol{d}},E)`, if :math:`\mathcal Z_f` is a
   singular plane conic, then all three coordinate functions of
   :math:`f` are reducible, and exactly two of them share a linear
   factor. Thus, three different components exist, each corresponding to
   which coordinate function does not share a linear factor with the
   other two. If this component is :math:`f^{(i)}`, then the
   corresponding component of :math:`\Delta^{\mathrm{ll}}(E)` is denoted
   by :math:`\Delta_i^{\mathrm{ll}}(E)`. We only prove that
   :math:`\Delta_1^{\mathrm{ll}}(E)` is irreducible of dimension
   :math:`7`.

   Let :math:`\mathbb{P} :=  \mathbb{P} (V_2^*) \times  \mathbb{P}(V_3^*)`,
   :math:`\mathbb{P}' :=  \mathbb{P}(V_1^*) \times  \mathbb{P}(V_2^* \oplus V_3^*)`, and
   let

   .. math:: U := \{([M'],[N'],[L],[(M,N)]) \in  \mathbb{P}\times \mathbb{P}' \mid \text{$\{M,M'\}$ and $\{N,N'\}$ are linearly independent}\}.

   Define the map
   :math:`\Psi\colon  \mathbb{P}\times \mathbb{P}' \to  \mathbb{P} H^0(\mathbb{P}^{\boldsymbol{d}}, E)` by

   .. math:: \Psi([M'],[N'],[L],[(M,N)]) := [(M'N', LM, LN)]\,.

   The image of :math:`U` under :math:`\Psi` forms an open subset of the
   image of :math:`\Psi`. It also lies in
   :math:`\Delta_1^{\mathrm{ll}}(E)`, and hence so is its Zariski
   closure (which coincides with the image of :math:`\Psi)`.

   On the one hand, the map from :math:`\mathbb P` to
   :math:`\mathbb P(V_2^* \otimes V_3^*)` defined by sending
   :math:`([M'],[N'])` to :math:`[M'N']` is the Segre embedding of
   :math:`\mathbb P(V_2^*)\times \mathbb P(V_3^*)`. On the other hand, the map from
   :math:`\mathbb P'` to
   :math:`\mathbb P((V_1^* \otimes V_2^*) \oplus (V_1^* \otimes V_3^*))`
   defined by sending :math:`([L], [(M,N)])` to :math:`[(LM,LN)]` is the
   Segre embedding of :math:`\mathbb P(V_1^*)\times \mathbb P(V_2^* \oplus V_3^*)`.
   Hence the image of :math:`\Psi` is the join over these two
   irreducible varieties living in two complementary linear subspaces of
   :math:`\mathbb P H^0(\mathbb{P}^{\boldsymbol{d}}, E)`. Using Example 18.17 from 
   `[Har92] <https://link.springer.com/book/10.1007/978-1-4757-2189-8>`_, 
   we conclude that the image of :math:`\Psi` is irreducible of dimension :math:`1`
   plus the dimensions of :math:`\mathbb P` and :math:`\mathbb P'`, that equals
   :math:`1+2+4=7`. Furthermore, its degree is equal to the product of
   the degrees of :math:`\mathbb P` and :math:`\mathbb P'` in their Segre
   embeddings, that equals :math:`2\times 4=8`. ◻

Remark A.
=========  
This remark concerns a determinantal expression for the equations for :math:`\Delta_i^{\mathrm{ll}}(E)`. We only discuss the case when :math:`i=1` as the remaining cases can be treated in a similar manner. As was shown in the proof of `Proposition 1 <https://mathrepo.mis.mpg.de/vector-bundle-nash-equilibria/#proposition-1>`_, the locus :math:`\Delta_1^{\mathrm{ll}}(E)` is the join of two disjoint varieties, namely the images of the two Segre embeddings :math:`\mathbb P(V_2^*) \times \mathbb P(V_3^*)\to\mathbb{P}(V_2^* \otimes V_3^*)` and :math:`\mathbb P(V_1^*)\times \mathbb P(V_2^* \oplus V_3^*)\to\mathbb P((V_1^* \otimes V_3^*) \oplus (V_1^* \otimes V_2^*))`. Thus, the equations for :math:`\Delta_1^{\mathrm{ll}}(E)` are the union of the equations coming from the two Segre embeddings. The first Segre embedding gives the condition

   .. math::

      \begin{vmatrix}
      a^{(1)}_{11} & a^{(1)}_{12} \\[2pt]
      a^{(1)}_{22} & a^{(1)}_{22}
      \end{vmatrix}
      = a^{(1)}_{11}a^{(1)}_{22}-a^{(1)}_{12}a^{(1)}_{21} = 0\,,

which is the condition for :math:`f^{(1)}` to be a product of linear forms. Instead, the equations for the second Segre embedding are discussed in `[Remark 3.22, APS25] <https://arxiv.org/abs/25XX.XXXX>`_.


Proposition 2.
==============  
The locus :math:`\Delta^{\mathrm{cl}}(E)` consists of three irreducible components, each having dimension :math:`7`.

   *Proof.* Given :math:`f\in H^0(\mathbb P^{\boldsymbol{d}},E)`, if :math:`\mathcal Z_f` is a
   space connected cubic, union of a non-singular conic and a line, then
   one of the coordinate functions of :math:`f` is irreducible, and the
   others are reducible and share a linear factor. Thus, three different
   components exist, each corresponding to which coordinate function is
   irreducible. If it is :math:`f^{(i)}`, then the corresponding
   component of :math:`\Delta^{\mathrm{cl}}(E)` is denoted by
   :math:`\Delta_i^{\mathrm{cl}}(E)`. We only prove that
   :math:`\Delta_1^{\mathrm{cl}}(E)` is irreducible of dimension
   :math:`7`.

   Let :math:`U` be the open subset of points of
   :math:`\Delta_1^{\mathrm{cl}}(E)` of the form
   :math:`[(MN'+M'N, LN, LM)]` with
   :math:`L \in V_1^*\setminus \{{\boldsymbol{0}}\}, M, M' \in V_2^* \setminus \{{\boldsymbol{0}}\}`,
   and :math:`N, N' \in V_3^*\setminus \{{\boldsymbol{0}}\}`, where both
   :math:`\{M,M'\}` and :math:`\{N,N'\}` are linearly independent so
   that :math:`MN'+M'N` is irreducible. Define the map from :math:`U` to
   :math:`\mathbb P((V_1^* \otimes V_3^*) \oplus (V_1^* \otimes V_2^*))` by
   :math:`\Psi([(MN'+M'N, LN, LM)]) := [(LN,LM)]`. The image of
   :math:`\Psi` is the Segre embedding of
   :math:`\mathbb P(V_1^*) \times \mathbb{P}(V_2^*\oplus V_3^*)` in
   :math:`\mathbb P((V_1^* \otimes V_3^*) \oplus (V_1^* \otimes V_2^*))` given
   by sending :math:`([L],[(M,N)])` to :math:`[LN,LM]`. So, it is
   irreducible of dimension :math:`4`. The fiber of :math:`\Psi` over
   :math:`[(LN,LM)]` is

   .. math:: \{[(MN'+M'N,LN,LM)] \mid M' \in V_2^* \setminus \{{\boldsymbol{0}}\}, N' \in V_3^* \setminus \{{\boldsymbol{0}}\}\},

   and thus, it can be identified with an open set of the
   :math:`3`-plane
   :math:`\{[(M',N')] \mid M' \in V_2^*, N' \in V_3^*\}`, which implies
   that :math:`U`, and hence its closure, is irreducible and has
   dimension :math:`7`. ◻

Remark B.
=========
Following a similar argument as in `[Remark 3.22, APS25] <https://arxiv.org/abs/25XX.XXXX>`_ and `[Remark 3.24, APS25] <https://arxiv.org/abs/25XX.XXXX>`_, one verifies that the six :math:`2\times 2` minors of the :math:`2\times 4` matrix in `[Equation (3.28), APS25] <https://arxiv.org/abs/25XX.XXXX>`_ and the four polynomials in `[Remark 3.24, APS25] <https://arxiv.org/abs/25XX.XXXX>`_ cut out the variety :math:`\Delta_1^{\mathrm{cl}}(E)` set-theoretically. We verified in :math:`\verb|Macaulay2|` that :math:`\Delta_1^{\mathrm{cl}}(E)` has degree :math:`10`, and similarly for the other two components of :math:`\Delta^{\mathrm{cl}}(E)`.

Proposition 3.
==============  
The locus :math:`\Delta^{\mathrm{lll}}(E)` consists of three irreducible components, each having dimension :math:`6`.

   *Proof.* Suppose that the zero scheme of
   :math:`f:=(f^{(1)},f^{(2)},f^{(3)}) \in H^0(\mathbb P^{\boldsymbol{d}},E)` is a
   space connected cubic, union of three non concurrent lines. Then
   there exists one line meeting the other two. Furthermore, all
   coordinate functions of :math:`f` are reducible, and for exactly one
   :math:`i\in[3]`, the linear factors in :math:`f^{(i)}` divide one of
   the other two components of :math:`f`. Thus, three different
   components exist, each corresponding to which index :math:`i\in[3]`
   is chosen, and the corresponding component of
   :math:`\Delta^{\mathrm{lll}}(E)` is denoted by
   :math:`\Delta_i^{\mathrm{lll}}(E)`. We only prove that
   :math:`\Delta_1^{\mathrm{lll}}(E)` is irreducible of dimension
   :math:`6`.

   Define the map
   :math:`\Psi\colon\mathbb P((V_1^*)^{\oplus 2}\oplus V_2^*\oplus V_3^*)\cong \mathbb P^7 \to \mathbb P H^0(\mathbb P^{\boldsymbol{d}}, E)`
   by

   .. math:: \Psi([(L,L',M,N)]):= [(MN, LN, L'M)]\,.

   The Zariski closure of the image of :math:`\Psi` is
   :math:`\Delta_1^{\mathrm{lll}}(E)`, hence
   :math:`\Delta_1^{\mathrm{lll}}(E)` is an irreducible toric variety.
   The fiber of :math:`\Psi` over :math:`[(MN, LN, L'M)]` is
   :math:`\{[(\lambda L,\mu L',\lambda M,\mu N)] \mid [\lambda,\mu]\in\mathbb{P}^1\}`,
   and thus, it has dimension :math:`1`. Therefore
   :math:`\Delta_1^{\mathrm{lll}}(E)` has dimension :math:`6`. ◻

Remark C.
========= 
Similarly as in `[Remark 3.22, APS25] <https://arxiv.org/abs/25XX.XXXX>`_, and choosing suitable coordinates for the vector spaces involved, one verifies that :math:`\Delta_1^{\mathrm{lll}}(E)` is cut out set-theoretically by the :math:`2\times 2` minors of the two matrices

   .. math::

      \label{eq: matrices defining conditions three lines}
      \begin{pmatrix}
      a^{(3)}_{11} & a^{(3)}_{21} & a^{(1)}_{11} & a^{(1)}_{12} \\[2pt]
      a^{(3)}_{12} & a^{(3)}_{22} & a^{(1)}_{21} & a^{(1)}_{22} 
      \end{pmatrix}\,,\quad
      \begin{pmatrix}
      a^{(2)}_{11} & a^{(2)}_{12} & a^{(1)}_{11} & a^{(1)}_{12} \\[2pt]
      a^{(2)}_{21} & a^{(2)}_{22} & a^{(1)}_{21} & a^{(1)}_{22} 
      \end{pmatrix}\,.

We verified in :math:`\verb|Macaulay2|` with the command ``cmModules`` of the package ``ToricInvariants`` that the list of Chern-Mather volumes of the singular projective toric variety :math:`\Delta_1^{\mathrm{lll}}(E)`, ordered from :math:`0` to :math:`6=\dim(\Delta_1^{\mathrm{lll}}(E))`, is :math:`\{20, 52, 90, 108, 88, 44, 10\}`. In particular :math:`\Delta_1^{\mathrm{lll}}(E)` has degree :math:`10`, and similarly for the other two components of :math:`\Delta^{\mathrm{lll}}(E)`.

Proposition 4.
==============  
The locus :math:`\Delta^{\mathrm{3l}}(E)` is irreducible of dimension :math:`5`.

   *Proof.* Suppose that the zero scheme of
   :math:`f:=(f^{(1)},f^{(2)},f^{(3)}) \in H^0(\mathbb P^{\boldsymbol{d}},E)` is a
   degenerate twisted cubic, union of three concurrent lines. Then all
   coordinate functions of :math:`f` are reducible, and for each
   :math:`i\in[3]`, the linear factors in :math:`f^{(i)}` divide one of
   the other two components of :math:`f`. Thus, defining the map
   :math:`\Psi\colon\mathbb P(V_1^*\oplus V_2^*\oplus V_3^*)\cong \mathbb P^5 \to \mathbb P H^0(\mathbb P^{\boldsymbol{d}}, E)`
   by

   .. math:: \Psi([(L,M,N)]):= [(MN, LN, LM)]\,,

   then the Zariski closure of the image of :math:`\Psi` is
   :math:`\Delta^{\mathrm{3l}}(E)`, hence
   :math:`\Delta^{\mathrm{3l}}(E)` is an irreducible toric variety.
   Since :math:`\Psi` is birational over its image, then
   :math:`\Delta^{\mathrm{3l}}(E)` has dimension :math:`5`. ◻

Remark D.
========= 
Following up on `Remark C <https://mathrepo.mis.mpg.de/vector-bundle-nash-equilibria/#remark-c>`_, one verifies that :math:`\Delta^{\mathrm{3l}}(E)` is cut out set-theoretically by the :math:`2\times 2` minors of the three matrices

   .. math::

      \label{eq: matrices defining conditions three concurrent lines}
      \begin{pmatrix}
      a^{(3)}_{11} & a^{(3)}_{12} & a^{(2)}_{11} & a^{(2)}_{12} \\[2pt]
      a^{(3)}_{21} & a^{(3)}_{22} & a^{(2)}_{21} & a^{(2)}_{22} 
      \end{pmatrix}\,,\quad
      \begin{pmatrix}
      a^{(3)}_{11} & a^{(3)}_{21} & a^{(1)}_{11} & a^{(1)}_{12} \\[2pt]
      a^{(3)}_{12} & a^{(3)}_{22} & a^{(1)}_{21} & a^{(1)}_{22} 
      \end{pmatrix}\,,\quad
      \begin{pmatrix}
      a^{(2)}_{11} & a^{(2)}_{12} & a^{(1)}_{11} & a^{(1)}_{12} \\[2pt]
      a^{(2)}_{21} & a^{(2)}_{22} & a^{(1)}_{21} & a^{(1)}_{22} 
      \end{pmatrix}\,.

Similarly as in Remark `Remark C <https://mathrepo.mis.mpg.de/vector-bundle-nash-equilibria/#remark-c>`_, we verified in :math:`\verb|Macaulay2|` that the list of Chern-Mather volumes of the singular projective toric variety :math:`\Delta^{\mathrm{3l}}(E)`, ordered from :math:`0` to :math:`5=\dim(\Delta^{\mathrm{3l}}(E))`, is :math:`\{24, 48, 68, 66, 42, 14\}`. In particular :math:`\Delta^{\mathrm{3l}}(E)` has degree :math:`14`.

Proposition 5.
==============  
The locus :math:`\Delta^{\mathrm{ql}}(E)` consists of three irreducible components, each having dimension :math:`4` and degree :math:`4`.

   *Proof.* Suppose that the zero scheme of
   :math:`f:=(f^{(1)},f^{(2)},f^{(3)}) \in H^0(\mathbb P^{\boldsymbol{d}},E)` is the
   union of a quadric and a line. Then exactly one coordinate function
   of :math:`f` is zero, and the other two are reducible and share a
   linear factor. Thus, three different components exist, each
   corresponding to which coordinate function :math:`f^{(i)}` is zero,
   and the corresponding component of :math:`\Delta^{\mathrm{ql}}(E)` is
   denoted by :math:`\Delta_i^{\mathrm{ql}}(E)`. The proof of the
   statement follows by a similar argument used in `[Proposition 3.21, APS25] <https://arxiv.org/abs/25XX.XXXX>`_. ◻
	       
Remark E.
========= 
Similarly as in `[Remark 3.22, APS25] <https://arxiv.org/abs/25XX.XXXX>`_, the locus :math:`\Delta_1^{\mathrm{ql}}(E)` is cut out by the :math:`2\times 2` minors of the matrix `[Equation (3.28), APS25] <https://arxiv.org/abs/25XX.XXXX>`_, and by the linear conditions :math:`a_{11}^{(1)}=a_{12}^{(1)}=a_{21}^{(1)}=a_{22}^{(1)}=0`. The cases :math:`i=2` and :math:`i=3` are similar.

Proposition 6.
==============  
The locus :math:`\Delta^{\mathrm{qq}}(E)` consists of three pairwise disjoint non-singular irreducible components, each having dimension :math:`2` and degree :math:`2`.

   *Proof.* Suppose that the zero scheme of
   :math:`f:=(f^{(1)},f^{(2)},f^{(3)}) \in H^0(\mathbb P^{\boldsymbol{d}},E)` is the
   union of two quadric surfaces. Then only one coordinate functions of
   :math:`f`, say :math:`f^{(i)}`, is nonzero, and :math:`f^{(i)}` is
   reducible. Thus, three different components exist, each corresponding
   to which index :math:`i\in[3]` is chosen, and the corresponding
   component of :math:`\Delta^{\mathrm{qq}}(E)` is denoted by
   :math:`\Delta_i^{\mathrm{qq}}(E)`. Choose :math:`i=1` for simplicity.
   Then :math:`\Delta_1^{\mathrm{qq}}(E)` is the transversal
   intersection between the :math:`3`-plane
   :math:`\Delta_1^{\mathrm{scr}}(E)` and the cone over the Segre
   embedding of :math:`\mathbb P(V_2^*)\times\mathbb P(V_3^*)` in
   :math:`\mathbb P(V_2^*\otimes V_3^*)`. In particular it is an irreducible
   quadric surface in :math:`\mathbb{P} H^0(\mathbb{P}^{\boldsymbol{d}},E)`. If :math:`i\neq j`,
   then :math:`\Delta_i^{\mathrm{qq}}(E)` and
   :math:`\Delta_j^{\mathrm{qq}}(E)` are disjoint because the
   corresponding :math:`3`-planes :math:`\Delta_i^{\mathrm{scr}}(E)` and
   :math:`\Delta_j^{\mathrm{scr}}(E)` are. ◻

| **Credits**
| Project page created: 3/3/2025
| Project contributors: Hirotachi Abo, Irem Portakal, Luca Sodomaco
| Software used: Macaulay2 (Version 1.20)
| Corresponding authors of this page: Luca Sodomaco (luca.sodomaco@mis.mpg.de), Irem Portakal (irem.portakal@mis.mpg.de).
