-- We consider the vector bundle E = O(0,1,1)+O(1,0,1)+O(1,1,0) on X = P^1 x P^1 x P^1,
-- and a global section F = (F1,F2,F3) of E
restart
KK = QQ;
Rx = KK[x_1,x_2]; -- variables of the first factor P^1
Ry = KK[y_1,y_2]; -- variables of the second factor P^1
Rz = KK[z_1,z_2]; -- variables of the third factor P^1
Ra = KK[a_1..a_4]; -- coefficients of F1
Rb = KK[b_1..b_4]; -- coefficients of F2
Rc = KK[c_1..c_4]; -- coefficients of F3
Rabc = Ra**Rb**Rc;
R = Rx**Ry**Rz**Ra**Rb**Rc;
vx = matrix{{x_1,x_2}};
vy = matrix{{y_1,y_2}};
vz = matrix{{z_1,z_2}};
F1 = (vy**vz)*transpose matrix{{a_1..a_4}};
F2 = (vx**vz)*transpose matrix{{b_1..b_4}};
F3 = (vx**vy)*transpose matrix{{c_1..c_4}};

-- F = (F1,F2,F3) defines a global section of E
I = ideal (F1,F2,F3);

-- We compute the discriminant locus Δ(E) of E
aI = sub(I,{x_1=>1,y_1=>1,z_1=>1});
dI = minors(3,diff(transpose matrix{{x_2,y_2,z_2}},gens aI));
J = aI+dI;
D = sub(eliminate({x_2,y_2,z_2},J),Rabc);

-- Now we compute Sing(Δ(E))
time singD = ideal singularLocus D;
time compSingD = primaryDecomposition singD;
-- Sing(Δ(E)) has 7 irreducible components
-- one component corresponds to global sections F such that Z_F contains a twisted cubic
-- three components correspond to global sections F such that Z_F contains a conic
-- three components correspond to global sections F such that Z_F contains a line

linex = compSingD#1; -- Z_F is a line of the form P^1 x {y} x {z} (see the generators, the conditions impose that F2 and F3 are products of two linear factors, such that there are only one solution y and one solution z, while x is free)
liney = compSingD#2; -- Z_F is a line of the form {x} x P^1 x {z}
linez = compSingD#5; -- Z_F is a line of the form {x} x {y} x P^1

conicxy = compSingD#4; -- Z_F is a conic in P^1 x P^1 x {z} (see the generators, the conditions impose that F1 and F2 are products of two linear factors, with a common solution z, while x and y satisfy the bilinear equation F3 = 0 defining the conic)
conicxz = compSingD#3; -- Z_F is a conic in P^1 x {y} x P^1 
conicyz = compSingD#0; -- Z_F is a conic in {x} x P^1 x P^1

cubic = compSingD#6; -- twisted cubic component
codim cubic -- codimension 3
degree cubic -- degree 11

-- Now we study Sing^2(Δ(E)).
-- The command "ideal singularLocus singD" would be computationally heavy.
-- Instead, first we compute the singular loci of the components of Sing(Δ(E)).

time singconicxy = ideal singularLocus(conicxy);
decsingconicxy = decompose singconicxy;
scrollxy = decsingconicxy#0 -- Z_F is a rational normal scroll of degree 4 with respect to xy (codim=8, deg=1)
scrollxz = (decompose ideal singularLocus(conicxz))#0 -- Z_F is a rational normal scroll of degree 4 with respect to xz (codim=8, deg=1)
scrollyz = (decompose ideal singularLocus(conicyz))#0 -- Z_F is a rational normal scroll of degree 4 with respect to yz (codim=8, deg=1)

time singlinex = ideal singularLocus(linex);
decsinglinex = decompose singlinex; -- union of 3 components:
-- c_1=...=c_4=0, (a_1,a_2)~(a_3,a_4), (b_1,b_2)~(b_3,b_4), (a_1,a_2)~(b_1,b_2): Z_f is the union of a line_z and a quadric_xy (codim=7, deg=4)
-- b_1=...=b_4=0, (a_1,a_2)~(a_3,a_4), (c_1,c_2)~(c_3,c_4), (a_1,a_3)~(c_1,c_2): Z_f is the union of a line_y and a quadric_xz (codim=7, deg=4)
-- scrollyz
linexquadricyz = (decompose ideal singularLocus(liney))#1
-- a_1=...=a_4=0, (b_1,b_2)~(b_3,b_4), (c_1,c_2)~(c_3,c_4), (b_1,b_3)~(c_1,c_3): Z_f is the union of a line_x and a quadric_yz (codim=7, deg=4)
lineyquadricxz = decsinglinex#1
linezquadricxy = decsinglinex#0

time singcubic = ideal singularLocus(cubic);
decsingcubic = decompose singcubic; -- 6 components: scrollxy, scrollxz, scrollyz, linexquadricyz, lineyquadricxz, linezquadricxy


-- Secondly, we study the pairwise intersections among the 7 irreducible components of Sing(Δ(E))

conicxyandxz = decompose(conicxy+conicxz); -- 1 component
conicxyandyz = decompose(conicxy+conicyz); -- 1 component
conicxzandyz = decompose(conicxz+conicyz); -- 1 component

linexy = decompose(linex+liney); -- 2 components
linexz = decompose(linex+linez); -- 2 components
lineyz = decompose(liney+linez); -- 2 components

linexandconicyz = decompose(linex+conicyz); -- 1 component
linexandconicxy = decompose(linex+conicxy); -- 1 component
linexandconicxz = decompose(linex+conicxz); -- 1 component

lineyandconicyz = decompose(liney+conicyz); -- 1 component
lineyandconicxy = decompose(liney+conicxy); -- 1 component
lineyandconicxz = decompose(liney+conicxz); -- 1 component

linezandconicyz = decompose(linez+conicyz); -- 1 component
linezandconicxy = decompose(linez+conicxy); -- 1 component
linezandconicxz = decompose(linez+conicxz); -- 1 component

cubicandlinex = decompose(cubic+linex); -- 2 components
cubicandliney = decompose(cubic+liney); -- 2 components
cubicandlinez = decompose(cubic+linez); -- 2 components

cubicandconicxy = decompose(cubic+conicxy); -- 1 component
cubicandconicxz = decompose(cubic+conicxz); -- 1 component
cubicandconicyz = decompose(cubic+conicyz); -- 1 component

Linexy = linexy#0; -- Z_F is a singular conic, union of two lines in the plane xy
Linexz = linexz#0; -- Z_F is a singular conic, union of two lines in the plane xz
Lineyz = lineyz#0; -- Z_F is a singular conic, union of two lines in the plane yz
codim(Linexy), degree(Linexy) -- (4,8)

LinexConicyz = linexandconicyz#0; -- Z_F is a space connected cubic, union of a non-singular conic in the plane yz and a line in x
LineyConicxz = lineyandconicxz#0; -- Z_F is a space connected cubic, union of a non-singular conic in the plane xz and a line in y
LinezConicxy = linezandconicxy#0; -- Z_F is a space connected cubic, union of a non-singular conic in the plane xy and a line in z
codim(LinexConicyz), degree(LinexConicyz) -- (4,10)

LinexMeetingLineyz = lineyz#1; -- Z_F is space connected cubic, union of three non concurrent lines; the line in x meets the other two lines in y and z
LineyMeetingLinexz = linexz#1; -- Z_F is space connected cubic, union of three non concurrent lines; the line in y meets the other two lines in x and z
LinezMeetingLinexy = linexy#1; -- Z_F is space connected cubic, union of three non concurrent lines; the line in z meets the other two lines in x and y
codim(LinexMeetingLineyz), degree(LinexMeetingLineyz) -- (5,10)

-- this tells us that the three loci LinexMeetingLineyz, LineyMeetingLinexz, LinezMeetingLinexy above (of codimension 5) are actually in a lower singular stratum
LinexMeetingLineyz+Linexy == LinexMeetingLineyz
LinexMeetingLineyz+Linexz == LinexMeetingLineyz
LineyMeetingLinexz+Linexy == LineyMeetingLinexz
LineyMeetingLinexz+Lineyz == LineyMeetingLinexz
LinezMeetingLinexy+Linexz == LinezMeetingLinexy
LinezMeetingLinexy+Lineyz == LinezMeetingLinexy


codim scrollxy -- 8
codim(Linexy+scrollxy) -- 9
codim(Linexz+scrollxy) -- 9
codim(Lineyz+scrollxy) -- 9
codim(LinexConicyz+scrollxy) -- 9
codim(LineyConicxz+scrollxy) -- 9
codim(LinezConicxy+scrollxy) -- 8

codim linexquadricyz -- 7
codim(Linexy+linexquadricyz) -- 7
codim(Linexz+linexquadricyz) -- 7
codim(Lineyz+linexquadricyz) -- 7

codim(LinexConicyz+linexquadricyz) -- 7
codim(LineyConicxz+linexquadricyz) -- 7
codim(LinezConicxy+linexquadricyz) -- 7

-- Therefore we have the following inclusions (important to check that the singular loci of linex,liney,linez,conicxy,conicxz,conicyz,cubic, are contained in some of the pairwise intersections of these varieties):
-- scrollxy is contained in LinezConicxy
-- scrollxz is contained in LineyConicxz
-- scrollyz is contained in LinexConicyz
-- linexquadricyz,lineyquadricxz,linezquadricxy are contained in Linexy,Linexz,Lineyz,LinexConicyz,LineyConicxz,LinezConicxy


-- Summing up, Sing^2(Δ(E)) consists of six components:
-- three components of codim 4 and degree 8: Linexy, Linexz, Lineyz
-- three components of codim 4 and degree 10: LinexConicyz, LineyConicxz, LinezConicxy


-- Now we consider Sing^3(Δ(E))
time decsingLinexy = decompose ideal singularLocus(Linexy) -- two components:
-- 1) c_i are 0, (a_1,a_2)~(a_3,a_4), (b_1,b_2)~(b_3,b_4), (a_1,a_2)~(b_1,b_2), (codim, deg) = (7,4)
-- 2) a_i and b_i are 0, (c_1,c_2)~(c_3,c_4), (codim, deg) = (9,2)
twoquadricsxy = scrollxy+sub(minors(2,matrix{{c_1,c_2},{c_3,c_4}}),Rabc) -- Z_F is the union of two smooth quadrics meeting in a line in direction z
twoquadricsxz = scrollxz+sub(minors(2,matrix{{b_1,b_2},{b_3,b_4}}),Rabc) -- Z_F is the union of two smooth quadrics meeting in a line in direction y
twoquadricsyz = scrollyz+sub(minors(2,matrix{{a_1,a_2},{a_3,a_4}}),Rabc) -- Z_F is the union of two smooth quadrics meeting in a line in direction x
decsingLinexy == {linezquadricxy,twoquadricsxy}

time decsingLinexConicyz = decompose ideal singularLocus(LinexConicyz) -- three components: linezquadricxy,lineyquadricxz,scrollyz
decsingLinexConicyz == {linezquadricxy,lineyquadricxz,scrollyz}


-- First, the components "singular conic" all pairwise intersect on the 5-codim components "space connected cubic, union of three non concurrent lines"
Linexy+Linexz == LinexMeetingLineyz
Linexy+Lineyz == LineyMeetingLinexz
Linexz+Lineyz == LinezMeetingLinexy

-- Also the components "union of a non-singular conic and a line" all pairwise intersect on the 5-codim components "space connected cubic, union of three non concurrent lines"
LinexConicyz+LineyConicxz == LinezMeetingLinexy 
LinexConicyz+LinezConicxy == LineyMeetingLinexz 
LineyConicxz+LinezConicxy == LinexMeetingLineyz 

-- Finally, also the mixed intersections between a component "singular conic" and a component "union of a non-singular conic and a line" are equal to a component "space connected cubic, union of three non concurrent lines"
LinexConicyz+Linexy == LineyMeetingLinexz
LinexConicyz+Linexz == LinezMeetingLinexy

LineyConicxz+Linexy == LinexMeetingLineyz
LineyConicxz+Lineyz == LinezMeetingLinexy

LinezConicxy+Linexz == LinexMeetingLineyz
LinezConicxy+Lineyz == LineyMeetingLinexz

decompose(LinexConicyz+Lineyz) -- 2 components
(oo#0 == LineyMeetingLinexz, oo#1 == LinezMeetingLinexy)

decompose(LineyConicxz+Linexz) -- 2 components
(oo#1 == LinexMeetingLineyz, oo#0 == LinezMeetingLinexy)

decompose(LinezConicxy+Linexy) -- 2 components
(oo#1 == LinexMeetingLineyz, oo#0 == LineyMeetingLinexz)

trim(scrollxy+LinexMeetingLineyz) == twoquadricsxy
trim(scrollxy+LineyMeetingLinexz) == twoquadricsxy
trim(scrollxy+LinezMeetingLinexy) == twoquadricsxy

codim(linexquadricyz) -- 7
codim(linexquadricyz+LinexMeetingLineyz) -- 7
codim(linexquadricyz+LineyMeetingLinexz) -- 7
codim(linexquadricyz+LinezMeetingLinexy) -- 7
-- hence linexquadricyz,lineyquadricxz,linezquadricxy are all contained in LinexMeetingLineyz,LineyMeetingLinexz,LinezMeetingLinexy

-- Summing up, Sing^3(Δ(E)) consists of six components:
-- three components of codim 5 and degree 10: LinexMeetingLineyz, LineyMeetingLinexz, LinezMeetingLinexy
-- three components of codim 8: scrollxy, scrollxz, scrollyz




-- Now we consider Sing^4(Δ(E))
-- The command "ideal singularLocus(LinexMeetingLineyz)" is computationally heavy, therefore we use instead these lines: 
S = KK[L11,L12,L21,L22,M1,M2,N1,N2];
phi = {M1*N1,M1*N2,M2*N1,M2*N2,L11*N1,L11*N2,L12*N1,L12*N2,L21*M1,L21*M2,L22*M1,L22*M2};
jacphi = diff(basis(1,S),transpose matrix{phi});
rank jacphi -- 7, indeed the variety LinexMeetingLineyz has codimension 5 in CC^12
time singphi = minors(7,jacphi);
time decsingphi = decompose singphi; -- = {ideal (N2, N1), ideal (M2, M1)}. This shows that the singular locus of LinexMeetingLineyz is the union of twoquadricsxy and twoquadricsxz

-- The components "space connected cubic, union of three non concurrent lines" all intersect in the same locus:
LinexMeetingLineyz+LineyMeetingLinexz == LinexMeetingLineyz+LinezMeetingLinexy
LinexMeetingLineyz+LineyMeetingLinexz == LineyMeetingLinexz+LinezMeetingLinexy

ThreeLinesMeeting = LinexMeetingLineyz+LineyMeetingLinexz -- Z_F is space connected cubic, union of three non concurrent lines
codim(ThreeLinesMeeting), degree(ThreeLinesMeeting) -- (6,14)
gensThreeLinesMeeting = transpose mingens ThreeLinesMeeting -- 15 minimal generators




-- Now we consider Sing^5(Δ(E))
-- The command "ideal singularLocus ThreeLinesMeeting" is computationally heavy, therefore we use instead these lines: 
S2 = KK[L1,L2,M1,M2,N1,N2]
phi2 = {M1*N1,M1*N2,M2*N1,M2*N2,L1*N1,L1*N2,L2*N1,L2*N2,L1*M1,L1*M2,L2*M1,L2*M2}
jacphi2 = diff(basis(1,S2),transpose matrix{phi2})
rank jacphi2 -- 6, indeed the variety ThreeLinesMeeting has codimension 6 in CC^12
time sing3LinesMeeting = minors(6,jacphi2);
time decsing3LinesMeeting = decompose sing3LinesMeeting; -- = {ideal (N2, N1), ideal (M2, M1), ideal (L2, L1)}. This shows that the singular locus of ThreeLinesMeeting is the union of twoquadricsxy, twoquadricsxz, and twoquadricsyz

-- This concludes the study of the singular strata of Δ(E)
