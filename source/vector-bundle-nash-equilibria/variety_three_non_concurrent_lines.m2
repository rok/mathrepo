-- case of three non concurrent lines
restart
needsPackage "ToricInvariants"
needsPackage "Polyhedra"

R = QQ[t_1..t_8,x_1..x_12]

Itorus = ideal(product(8,i->t_(i+1))) -- ideal torus
A = matrix{{1,0,1,0,0,0,0,0},
           {1,0,0,1,0,0,0,0},
	   {0,1,1,0,0,0,0,0},
	   {0,1,0,1,0,0,0,0},
	   {0,0,1,0,1,0,0,0},
	   {0,0,1,0,0,1,0,0},
	   {0,0,0,1,1,0,0,0},
	   {0,0,0,1,0,1,0,0},
	   {0,0,0,0,1,0,1,0},
	   {0,0,0,0,1,0,0,1},
	   {0,0,0,0,0,1,1,0},
	   {0,0,0,0,0,1,0,1}}
A = transpose A

phi = matrix{apply(12, i-> product(8, j-> t_(j+1)^(A_(j,i))))} -- monomial map

X = eliminate(toList(t_1..t_8), saturate(ideal(phi-matrix{{x_1..x_12}}), Itorus)) -- toric ideal
11-codim X
degree X

cmVolumes A
P = convexHull A
volume P
