-- numerical computation of Nash equilibria, specific example of format (3,3,3) for the paper
restart
KK = QQ;
d = {3,3,3}; -- game format
R = KK[flatten apply(#d, i-> apply(d#i, j-> p_(i+1,j+1)))];
for i in 1..#d do vecp_i = matrix{apply(d#(i-1), j-> p_(i,j+1))};

-- list of multi-indices
I = toList((#d:1)..toSequence(d))

-- payoff tensors;
X1 = {matrix {{-20, -4, -12}, {8, 20, 20}, {-76, 4, -4}}, matrix {{-16, 8, 4}, {-20, -20, 16}, {8, 4, -4}}, matrix {{12, 8, -4}, {12, -4, -23}, {12, -4, 8}}}
X2 = {matrix {{6, -2, 4}, {-2, 0, -10}, {2, -10, 4}}, matrix {{-8, 4, -6}, {-2, 6, 4}, {4, -8, -2}}, matrix {{-10, -2, 0}, {10, 6, -2}, {-2, -5, 2}}}
X3 = {matrix {{-8, -4, 6}, {-6, -10, 6}, {4, 10, -2}}, matrix {{-8, -4, 0}, {0, -10, 2}, {-4, 0, 14}}, matrix {{-10, 10, 10}, {2, 4, -10}, {0, -6, 3}}}

-- the lines below are for a 3-player game with payoff tensors written as above
T1 = sum(I, j-> (X1#(j#2-1))_(j#0-1,j#1-1)*p_(1,j#0)*p_(2,j#1)*p_(3,j#2))
T2 = sum(I, j-> (X2#(j#2-1))_(j#0-1,j#1-1)*p_(1,j#0)*p_(2,j#1)*p_(3,j#2))
T3 = sum(I, j-> (X3#(j#2-1))_(j#0-1,j#1-1)*p_(1,j#0)*p_(2,j#1)*p_(3,j#2))

A1 = contract(vecp_1,T1) || matrix{toList(d#0:1)}
A2 = contract(vecp_2,T2) || matrix{toList(d#1:1)}
A3 = contract(vecp_3,T3) || matrix{toList(d#2:1)}

J1 = minors(2, A1) + ideal(-1 + sum first entries vecp_1);
J2 = minors(2, A2) + ideal(-1 + sum first entries vecp_2);
J3 = minors(2, A3) + ideal(-1 + sum first entries vecp_3);

J = trim(J1+J2+J3);
codim J
degree J

decJ = decompose J;

-- in our specific example, J has degree ten and two components over QQ.
decJ#0
for i in 1..#d do for j in 1..d#(i-1) do print concatenate{"p_(",toString(i),",",toString(j),") = ",toString((p_(i,j)%(decJ#0)))}

loadPackage("RealRoots")
-- The following is a rational univariate representation of the zero-dimensional ideal J (see the reference).
-- This is a triple (f, ch, ph) where f is a linear form that is separating
-- for V(J), ch is the characteristic polynomial of the operator m_f (multiplication by f) which retains the multiplicities of points of V(J),
-- if not their scheme structure, and ph is a rational map ph:KK->KK^(# variables) that restricts to a bijection between the roots of ch and the points of V(J).
(f, ch, ph) = rationalUnivariateRepresentation(J);

SturmCount(ch) -- ch has 4 real roots

-- An application of Sturm’s Theorem is to give isolating intervals, which are disjoint intervals
-- each containing exactly one root of ch. The following command gives a list of pairs {p, q} such
-- that the interval (p, q] contains a unique root of ch and q-p is less than a user-provided tolerance.
-- The numbers p, q are dyadic, lying in Z[1/2], as they are found in a binary search.
tol = 1/(10^8)
L = realRootIsolation(ch, tol);
S = ring(ch)

-- the following line is just to visualize better the intervals (p,q]
for l in L do print apply(ph, i-> sort({sub(sub(i, Z => l#0),RR),sub(sub(i, Z => l#1),RR)}))

-- we map the extrema of the intervals (p,q] via ph and check if the images are positive
for l in L do print apply(ph, i-> min({sub(i, Z => l#0),sub(i, Z => l#1)})>0)
-- with tolerance tol, this tells us that the other points in V(J) have positive coordinates,
-- hence they correspond to totally mixed Nash equilibria
