-- We consider the vector bundle E = O(0,1,1)+O(1,0,1)+2*O(1,1,0) on X = P^1 x P^1 x P^2
restart
KK = ZZ/101
Rx = KK[x_1,x_2]
Ry = KK[y_1,y_2]
Rz = KK[z_1,z_2,z_3]
Ra = KK[a_1..a_6]
Rb = KK[b_1..b_6]
Rc = KK[c_(1,1)..c_(2,4)]
Rabc = Ra**Rb**Rc
R = Rx**Ry**Rz**Ra**Rb**Rc
vx = matrix{{x_1,x_2}}
vy = matrix{{y_1,y_2}}
vz = matrix{{z_1..z_3}}
F1 = (vy**vz)*transpose matrix{{a_1..a_6}}
F2 = (vx**vz)*transpose matrix{{b_1..b_6}}
F3 = (vx**vy)*transpose matrix{{c_(1,1)..c_(1,4)}}
F4 = (vx**vy)*transpose matrix{{c_(2,1)..c_(2,4)}}

-- F = (F1,F2,F3,F4) defines a global section of E
I = ideal (F1,F2,F3,F4)

-- We compute the discriminant locus Δ(E) of E
aI = sub(I,{x_2=>1,y_2=>1,z_3=>1})
diffjac = transpose diff(transpose matrix{{x_1,y_1,z_1,z_2}},gens aI)
dI = det(diffjac)
facdI = factor dI
J1 = aI+ideal(facdI#0#0)
J2 = aI+ideal(facdI#1#0)
D1 = sub(eliminate({x_1,y_1,z_1,z_2},J1),Rabc);
D2 = sub(eliminate({x_1,y_1,z_1,z_2},J2),Rabc);

codim D1, degree D1
codim D2, degree D2

time decD2 = decompose D2; -- 806 seconds over KK == QQ
#decD2 --1 , so irreducible over QQ

-- D1 has one generator of degree 4
-- D2 has 11 generators

-- now we compute the singular locus of Δ(E)
time singD1 = ideal singularLocus D1;
time decsingD1 = decompose singD1;
#decsingD1

time singD2 = ideal singularLocus D2;
#decsingD2

