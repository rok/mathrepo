-- case of three concurrent lines
restart
needsPackage "ToricInvariants"
needsPackage "Polyhedra"

R = QQ[t_1..t_6,x_1..x_12]

Itorus = ideal(product(6,i->t_(i+1))) -- ideal torus
A = matrix{{1,0,1,0,0,0},
           {1,0,0,1,0,0},
	   {0,1,1,0,0,0},
	   {0,1,0,1,0,0},
	   {1,0,0,0,1,0},
	   {1,0,0,0,0,1},
	   {0,1,0,0,1,0},
	   {0,1,0,0,0,1},
	   {0,0,1,0,1,0},
	   {0,0,1,0,0,1},
	   {0,0,0,1,1,0},
	   {0,0,0,1,0,1}}
A = transpose A

phi = matrix{apply(12, i-> product(6, j-> t_(j+1)^(A_(j,i))))} -- monomial map

X = eliminate(toList(t_1..t_6), saturate(ideal(phi-matrix{{x_1..x_12}}), Itorus)) -- toric ideal
11-codim X
degree X


cmVolumes A
reverse oo
P = convexHull A
volume P
