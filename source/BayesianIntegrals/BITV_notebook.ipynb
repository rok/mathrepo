{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Bayesian Integrals on Toric Varieties\n",
    "\n",
    "This notebook accompanies the paper \"Bayesian Integrals on Toric Varieties\" by Michael Borinsky, Anna-Laura Sattelberger, Bernd Sturmfels, and Simon Telen. The code runs in Julia v1.7.1 and uses the packages\n",
    "\n",
    "- Polymake (v0.7.1)\n",
    "- DynamicPolynomials (v0.3.21)\n",
    "- HomotopyContinuation (v2.6.3)\n",
    "- IterTools (v1.4.0)\n",
    "- HCubature (v1.5.0)\n",
    "\n",
    "The file BITV.jl contains our main functions. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "polymake version 4.6\n",
      "Copyright (c) 1997-2021\n",
      "Ewgenij Gawrilow, Michael Joswig, and the polymake team\n",
      "Technische Universität Berlin, Germany\n",
      "https://polymake.org\n",
      "\n",
      "This is free software licensed under GPL; see the source for copying conditions.\n",
      "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n",
      "\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "getM2 (generic function with 1 method)"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "include(\"BITV.jl\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Below, the examples are numbered as in the paper. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Example 4.5\n",
    "\n",
    "We define a list of numerator and denominator factors. In this example, the numerator is $f$ and the denominator is $g$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "@polyvar x[1:5]\n",
    "numers = [2*x[1]^2*x[2]^2*x[3]^3*x[4]*x[5]^3 + 3*x[1]^2*x[2]*x[3]^2*x[4]^2*x[5]^4 + 5*x[1]*x[2]^2*x[3]^5*x[4]*x[5]^2];\n",
    "denoms = [7*x[1]^3*x[2]^3*x[3]^2*x[5]^3+11*x[1]^3*x[2]*x[4]^2*x[5]^5+13*x[1]*x[3]^3*x[4]^3*x[5]^4+17*x[2]^2*x[3]^7*x[4]*x[5]];"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We display the numerator and denominator below"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$ 2x_{1}^{2}x_{2}^{2}x_{3}^{3}x_{4}x_{5}^{3} + 3x_{1}^{2}x_{2}x_{3}^{2}x_{4}^{2}x_{5}^{4} + 5x_{1}x_{2}^{2}x_{3}^{5}x_{4}x_{5}^{2} $$"
      ],
      "text/plain": [
       "2x₁²x₂²x₃³x₄x₅³ + 3x₁²x₂x₃²x₄²x₅⁴ + 5x₁x₂²x₃⁵x₄x₅²"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "numers[1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$ 7x_{1}^{3}x_{2}^{3}x_{3}^{2}x_{5}^{3} + 11x_{1}^{3}x_{2}x_{4}^{2}x_{5}^{5} + 13x_{1}x_{3}^{3}x_{4}^{3}x_{5}^{4} + 17x_{2}^{2}x_{3}^{7}x_{4}x_{5} $$"
      ],
      "text/plain": [
       "7x₁³x₂³x₃²x₅³ + 11x₁³x₂x₄²x₅⁵ + 13x₁x₃³x₄³x₅⁴ + 17x₂²x₃⁷x₄x₅"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "denoms[1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now define the lists of exponents $\\mu$ and $\\nu$ for the numerator and denominator factors respectively. In this example, we integrate $f^1/g^1$. We also define the matrix $V$ of ray generators and we fix a number of samples to use. The main function is ${\\tt integrate\\_tropical}$. The option \"verbatim = true\" makes sure that some intermediate output is printed. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1. Sum the Newton polytopes\n",
      "----------------------------------------------\n",
      "2. Compute the normal fan of the sum\n",
      "Σ has 6 rays and 6 maximal cones\n",
      "----------------------------------------------\n",
      "3. For each maximal cone, compute the winning exponent\n",
      "[[-1, -1, 1, 1, 0], [-1, 0, 2, 0, -1], [1, 1, -1, -1, 0], [-2, -1, 3, 1, -1], [0, 2, 2, -2, -2], [1, 0, -2, 0, 1]]\n",
      "----------------------------------------------\n",
      "4. Compute the sector integral for all 6 maximal cones\n",
      "     <<<<<<< Itr = 37//4 >>>>>>\n",
      "    I_σ^tr = 1//1\n",
      "    Number of samples for this cone: 10811\n",
      "    Computed the value for sector 1: 0.47833986740530376\n",
      "    I_σ^tr = 2//1\n",
      "    Number of samples for this cone: 21622\n",
      "    Computed the value for sector 2: 0.5061742517431539\n",
      "    I_σ^tr = 3//2\n",
      "    Number of samples for this cone: 16216\n",
      "    Computed the value for sector 3: 0.36310802859909797\n",
      "    I_σ^tr = 1//1\n",
      "    Number of samples for this cone: 10811\n",
      "    Computed the value for sector 4: 0.43697192157606796\n",
      "    I_σ^tr = 1//4\n",
      "    Number of samples for this cone: 2703\n",
      "    Computed the value for sector 5: 0.07664476251139477\n",
      "    I_σ^tr = 7//2\n",
      "    Number of samples for this cone: 37838\n",
      "    Computed the value for sector 6: 1.0127646409048066\n",
      "----------------------------------------------\n",
      "5. The integral is 2.874003472739825\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "2.874003472739825"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "μ = [1] # exponents of the factors in the numerator \n",
    "ν = [1] # exponents of the factors in the denominator\n",
    "V = [1 1 -1 -1 0; 0 -1 -1 1 1] # matrix of ray generators\n",
    "nsamples = 100000 # number of samples\n",
    "I = integrate_tropical(V,numers,denoms,x,μ,ν,nsamples; verbatim = true)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We compute a reference value for the integral using numerical cubature procedures implemented in the package HCubature.jl. The optional input parameter \"inttol\" specifies the relative accuracy with which the sector integrals are computed. Its default value is set to $10^{-3}$. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2.872660369526546"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "I = integrate_cubature(V,numers,denoms,x,μ,ν; verbatim = false, inttol = 1e-6)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now use Algorithm 4.3 to draw samples from the posterior distribution. We use 100000 candidate samples."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "21705"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "nsamples = 100000\n",
    "samples, densities = sample_rejection(V,numers,denoms,x,μ,ν,nsamples; verbatim = false);\n",
    "length(samples)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We see that about 21000 samples are accepted, and 79000 were rejected. The bound from Proposition 4.4 predicts acceptance of about $(M_1/M_2)\\cdot 100000$ samples. We compute this estimate below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2916.666666666666666666666666666666666666666666666666666666666666666666666666678"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "M1 = getM1(numers, denoms, μ, ν) \n",
    "M2 = getM2(numers, denoms, μ, ν) \n",
    "M1/M2*100000"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We see that the bound is pessimistic. The actual expected acceptance rate is \n",
    "$$ \\frac{1}{M_2} \\frac{I}{I^{\\rm tr}}, $$\n",
    "which is close to the empirical acceptance rate."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "21739.051445065753667320776101503823254559491131756756756756756756756756756757"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Itr = 37//4\n",
    "(1/M2)*I/Itr*100000"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Example 5.3\n",
    "\n",
    "Example 5.3 computes the area of our pentagon. This requires integrating the toric Hessian determinant $\\det H$ against the canonical form. We computed $\\det H$ using Maple and make use of the fact that the denominator can be written as $q^3$. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "@polyvar x[1:5]\n",
    "numers = [4*x[1]^4*x[2]^4*x[3]^2*x[4]^2*x[5]^3+x[1]^3*x[2]^5*x[3]^5*x[4]*x[5]+4*x[1]^3*x[2]^4*x[3]^4*x[4]^2*x[5]^2+9*x[1]^3*x[2]^3*x[3]^3*x[4]^3*x[5]^3+4*x[1]^3*x[2]^2*x[3]^2*x[4]^4*x[5]^4+8*x[1]^2*x[2]^3*x[3]^5*x[4]^3*x[5]^2+4*x[1]^2*x[2]^2*x[3]^4*x[4]^4*x[5]^3+x[1]*x[2]^3*x[3]^7*x[4]^3*x[5]+x[1]*x[2]*x[3]^5*x[4]^5*x[5]^3];\n",
    "denoms = [x[1]^2*x[2]^2*x[5]+x[1]*x[2]^2*x[3]^2+x[1]*x[4]^2*x[5]^2+x[2]*x[3]^3*x[4]+x[3]^2*x[4]^2*x[5]];"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$ 4x_{1}^{4}x_{2}^{4}x_{3}^{2}x_{4}^{2}x_{5}^{3} + x_{1}^{3}x_{2}^{5}x_{3}^{5}x_{4}x_{5} + 4x_{1}^{3}x_{2}^{4}x_{3}^{4}x_{4}^{2}x_{5}^{2} + 9x_{1}^{3}x_{2}^{3}x_{3}^{3}x_{4}^{3}x_{5}^{3} + 4x_{1}^{3}x_{2}^{2}x_{3}^{2}x_{4}^{4}x_{5}^{4} + 8x_{1}^{2}x_{2}^{3}x_{3}^{5}x_{4}^{3}x_{5}^{2} + 4x_{1}^{2}x_{2}^{2}x_{3}^{4}x_{4}^{4}x_{5}^{3} + x_{1}x_{2}^{3}x_{3}^{7}x_{4}^{3}x_{5} + x_{1}x_{2}x_{3}^{5}x_{4}^{5}x_{5}^{3} $$"
      ],
      "text/plain": [
       "4x₁⁴x₂⁴x₃²x₄²x₅³ + x₁³x₂⁵x₃⁵x₄x₅ + 4x₁³x₂⁴x₃⁴x₄²x₅² + 9x₁³x₂³x₃³x₄³x₅³ + 4x₁³x₂²x₃²x₄⁴x₅⁴ + 8x₁²x₂³x₃⁵x₄³x₅² + 4x₁²x₂²x₃⁴x₄⁴x₅³ + x₁x₂³x₃⁷x₄³x₅ + x₁x₂x₃⁵x₄⁵x₅³"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "nn = numers[1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$ x_{1}^{2}x_{2}^{2}x_{5} + x_{1}x_{2}^{2}x_{3}^{2} + x_{1}x_{4}^{2}x_{5}^{2} + x_{2}x_{3}^{3}x_{4} + x_{3}^{2}x_{4}^{2}x_{5} $$"
      ],
      "text/plain": [
       "x₁²x₂²x₅ + x₁x₂²x₃² + x₁x₄²x₅² + x₂x₃³x₄ + x₃²x₄²x₅"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "dd = denoms[1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2.5005096864871557"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "μ = [1] # exponents of the factors in the numerator \n",
    "ν = [3] # exponents of the factors in the denominator\n",
    "V = [1 1 -1 -1 0; 0 -1 -1 1 1] # matrix of ray generators\n",
    "nsamples = 100000 # number of samples\n",
    "I = integrate_tropical(V,numers,denoms,x,μ,ν,nsamples; verbatim = false)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Example 6.2\n",
    "\n",
    "Example 6.2 considers a conditional independence model (coin model). In particular, we consider rank two tensors. Here is how to define the input:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [],
   "source": [
    "@polyvar x[1:6] # These represent [x[0],x[1],s[0],s[1],t[0],t[1]] from the paper. \n",
    "d = 2\n",
    "u = [2,1,2]\n",
    "numers = [binomial(d,ℓ)*(x[1]*x[3]^ℓ*x[4]^(d-ℓ)*(x[5]+x[6])^d + x[2]*x[5]^ℓ*x[6]^(d-ℓ)*(x[3]+x[4])^d) for ℓ = 0:d]\n",
    "numers = [numers;prod(x)]\n",
    "denoms = [x[1]+x[2];x[3]+x[4];x[5]+x[6]]\n",
    "μ = [u;1]\n",
    "ν = sum(u)*[1;ones(Int64,d)*d] + 2*ones(Int64,d+1)\n",
    "V = [1 -1 0 0 0 0; 0 0 1 -1 0 0; 0 0 0 0 1 -1]\n",
    "nsamples = 100000;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Integrating gives approximately 0.00145. To see the values $I_{\\sigma}^{\\rm tr}$, choose \"verbatim = true\". "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.00142410937323444"
      ]
     },
     "execution_count": 15,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "I = integrate_tropical(V,numers,denoms,x,μ,ν,nsamples; verbatim = false)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We check this using HCubature.jl: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.0014538991546372338"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "I = integrate_cubature(V,numers,denoms,x,μ,ν; verbatim = false, inttol = 1e-4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Example 6.3\n",
    "\n",
    "This example considers the linear model and the Wachspress model associated to our pentagon.  \n",
    "\n",
    "The linear model has 5 numerator factors coming from the edges of the pentagon, and one from the toric Hessian. We have $w = (0,1,3,1,0)$ and set $\\gamma = (5,5,5,5,5)$, but we ignore the $\\gamma$'s at first. They will be taken into account later."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "6-element Vector{Polynomial{true, Int64}}:\n",
       " 2x₁²x₂²x₅ + x₁x₂²x₃² + x₁x₄²x₅²\n",
       " 2x₁²x₂²x₅ + 2x₁x₂²x₃² + x₂x₃³x₄\n",
       " 2x₁x₂²x₃² + 3x₂x₃³x₄ + 2x₃²x₄²x₅\n",
       " 2x₁x₄²x₅² + x₂x₃³x₄ + 2x₃²x₄²x₅\n",
       " x₁²x₂²x₅ + 2x₁x₄²x₅² + x₃²x₄²x₅\n",
       " 4x₁⁴x₂⁴x₃²x₄²x₅³ + x₁³x₂⁵x₃⁵x₄x₅ + 4x₁³x₂⁴x₃⁴x₄²x₅² + 9x₁³x₂³x₃³x₄³x₅³ + 4x₁³x₂²x₃²x₄⁴x₅⁴ + 8x₁²x₂³x₃⁵x₄³x₅² + 4x₁²x₂²x₃⁴x₄⁴x₅³ + x₁x₂³x₃⁷x₄³x₅ + x₁x₂x₃⁵x₄⁵x₅³"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "@polyvar x[1:5]\n",
    "numers = [(2*x[1]*x[2]^2*x[5]+x[2]^2*x[3]^2+x[4]^2*x[5]^2)*x[1];\n",
    "        (2*x[1]^2*x[2]*x[5] + 2*x[1]*x[2]*x[3]^2 + x[3]^3*x[4])*x[2];\n",
    "        (2*x[2]^2*x[1]+3*x[2]*x[3]*x[4]+2*x[4]^2*x[5])*x[3]^2;\n",
    "        (2*x[1]*x[4]*x[5]^2+x[2]*x[3]^3+2*x[3]^2*x[4]*x[5])*x[4];\n",
    "        (x[1]^2*x[2]^2+2*x[1]*x[4]^2*x[5]+x[3]^2*x[4]^2)*x[5];\n",
    "        4*x[1]^4*x[2]^4*x[3]^2*x[4]^2*x[5]^3+x[1]^3*x[2]^5*x[3]^5*x[4]*x[5]+4*x[1]^3*x[2]^4*x[3]^4*x[4]^2*x[5]^2+9*x[1]^3*x[2]^3*x[3]^3*x[4]^3*x[5]^3+4*x[1]^3*x[2]^2*x[3]^2*x[4]^4*x[5]^4+8*x[1]^2*x[2]^3*x[3]^5*x[4]^3*x[5]^2+4*x[1]^2*x[2]^2*x[3]^4*x[4]^4*x[5]^3+x[1]*x[2]^3*x[3]^7*x[4]^3*x[5]+x[1]*x[2]*x[3]^5*x[4]^5*x[5]^3]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Each of the $p_i$ has a pole along $q = 0$, where $q$ is  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1-element Vector{Polynomial{true, Int64}}:\n",
       " x₁²x₂²x₅ + x₁x₂²x₃² + x₁x₄²x₅² + x₂x₃³x₄ + x₃²x₄²x₅"
      ]
     },
     "execution_count": 18,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "denoms = [x[1]^2*x[2]^2*x[5]+x[1]*x[2]^2*x[3]^2+x[1]*x[4]^2*x[5]^2+x[2]*x[3]^3*x[4]+x[3]^2*x[4]^2*x[5]]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here are the data used in Example 6.3."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1.2478259257919542"
      ]
     },
     "execution_count": 19,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "μ = [20,16,10,15,23,1]\n",
    "ν = [sum(μ)+2];\n",
    "V = [1 1 -1 -1 0; 0 -1 -1 1 1] \n",
    "I = integrate_cubature(V,numers,denoms,x,μ,ν; verbatim = false, inttol = 1e-4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To compute $I_u$, we should scale by $5^{-84} \\cdot (2/5)$. The first factor comes from $\\gamma$, the second divides by the area of the pentagon. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "9.654585472476343e-60"
      ]
     },
     "execution_count": 20,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "I_u = I*5^(-84)*(2/5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now turn to the Wachspress model. This involves the adjoint polynomial \n",
    "$$ A(y) = 7 + 2(y_1 + y_2) - (y_1-y_2)^2.$$\n",
    "$A(y)$ pulls back to the degree zero rational function with numerator"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$ 8x_{1}^{4}x_{2}^{4}x_{5}^{2} + 12x_{1}^{3}x_{2}^{4}x_{3}^{2}x_{5} + 20x_{1}^{3}x_{2}^{2}x_{4}^{2}x_{5}^{3} + 4x_{1}^{2}x_{2}^{4}x_{3}^{4} + 12x_{1}^{2}x_{2}^{3}x_{3}^{3}x_{4}x_{5} + 32x_{1}^{2}x_{2}^{2}x_{3}^{2}x_{4}^{2}x_{5}^{2} + 8x_{1}^{2}x_{4}^{4}x_{5}^{4} + 8x_{1}x_{2}^{3}x_{3}^{5}x_{4} + 12x_{1}x_{2}^{2}x_{3}^{4}x_{4}^{2}x_{5} + 12x_{1}x_{2}x_{3}^{3}x_{4}^{3}x_{5}^{2} + 12x_{1}x_{3}^{2}x_{4}^{4}x_{5}^{3} + 3x_{2}^{2}x_{3}^{6}x_{4}^{2} + 8x_{2}x_{3}^{5}x_{4}^{3}x_{5} + 4x_{3}^{4}x_{4}^{4}x_{5}^{2} $$"
      ],
      "text/plain": [
       "8x₁⁴x₂⁴x₅² + 12x₁³x₂⁴x₃²x₅ + 20x₁³x₂²x₄²x₅³ + 4x₁²x₂⁴x₃⁴ + 12x₁²x₂³x₃³x₄x₅ + 32x₁²x₂²x₃²x₄²x₅² + 8x₁²x₄⁴x₅⁴ + 8x₁x₂³x₃⁵x₄ + 12x₁x₂²x₃⁴x₄²x₅ + 12x₁x₂x₃³x₄³x₅² + 12x₁x₃²x₄⁴x₅³ + 3x₂²x₃⁶x₄² + 8x₂x₃⁵x₄³x₅ + 4x₃⁴x₄⁴x₅²"
      ]
     },
     "execution_count": 21,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "@polyvar x[1:5]\n",
    "numer_A = 8*x[1]^4*x[2]^4*x[5]^2+12*x[1]^3*x[2]^4*x[3]^2*x[5]+20*x[1]^3*x[2]^2*x[4]^2*x[5]^3+4*x[1]^2*x[2]^4*x[3]^4+12*x[1]^2*x[2]^3*x[3]^3*x[4]*x[5]+32*x[1]^2*x[2]^2*x[3]^2*x[4]^2*x[5]^2+8*x[1]^2*x[4]^4*x[5]^4+8*x[1]*x[2]^3*x[3]^5*x[4]+12*x[1]*x[2]^2*x[3]^4*x[4]^2*x[5]+12*x[1]*x[2]*x[3]^3*x[4]^3*x[5]^2+12*x[1]*x[3]^2*x[4]^4*x[5]^3+3*x[2]^2*x[3]^6*x[4]^2+8*x[2]*x[3]^5*x[4]^3*x[5]+4*x[3]^4*x[4]^4*x[5]^2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "and denominator $q^2$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "7-element Vector{Polynomial{true, Int64}}:\n",
       " 2x₁²x₂²x₅ + x₁x₂²x₃² + x₁x₄²x₅²\n",
       " 2x₁²x₂²x₅ + 2x₁x₂²x₃² + x₂x₃³x₄\n",
       " 2x₁x₂²x₃² + 3x₂x₃³x₄ + 2x₃²x₄²x₅\n",
       " 2x₁x₄²x₅² + x₂x₃³x₄ + 2x₃²x₄²x₅\n",
       " x₁²x₂²x₅ + 2x₁x₄²x₅² + x₃²x₄²x₅\n",
       " 4x₁⁴x₂⁴x₃²x₄²x₅³ + x₁³x₂⁵x₃⁵x₄x₅ + 4x₁³x₂⁴x₃⁴x₄²x₅² + 9x₁³x₂³x₃³x₄³x₅³ + 4x₁³x₂²x₃²x₄⁴x₅⁴ + 8x₁²x₂³x₃⁵x₄³x₅² + 4x₁²x₂²x₃⁴x₄⁴x₅³ + x₁x₂³x₃⁷x₄³x₅ + x₁x₂x₃⁵x₄⁵x₅³\n",
       " x₁²x₂²x₅ + x₁x₂²x₃² + x₁x₄²x₅² + x₂x₃³x₄ + x₃²x₄²x₅"
      ]
     },
     "execution_count": 22,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "numers = [(2*x[1]*x[2]^2*x[5]+x[2]^2*x[3]^2+x[4]^2*x[5]^2)*x[1];\n",
    "        (2*x[1]^2*x[2]*x[5] + 2*x[1]*x[2]*x[3]^2 + x[3]^3*x[4])*x[2];\n",
    "        (2*x[2]^2*x[1]+3*x[2]*x[3]*x[4]+2*x[4]^2*x[5])*x[3]^2;\n",
    "        (2*x[1]*x[4]*x[5]^2+x[2]*x[3]^3+2*x[3]^2*x[4]*x[5])*x[4];\n",
    "        (x[1]^2*x[2]^2+2*x[1]*x[4]^2*x[5]+x[3]^2*x[4]^2)*x[5];\n",
    "        4*x[1]^4*x[2]^4*x[3]^2*x[4]^2*x[5]^3+x[1]^3*x[2]^5*x[3]^5*x[4]*x[5]+4*x[1]^3*x[2]^4*x[3]^4*x[4]^2*x[5]^2+9*x[1]^3*x[2]^3*x[3]^3*x[4]^3*x[5]^3+4*x[1]^3*x[2]^2*x[3]^2*x[4]^4*x[5]^4+8*x[1]^2*x[2]^3*x[3]^5*x[4]^3*x[5]^2+4*x[1]^2*x[2]^2*x[3]^4*x[4]^4*x[5]^3+x[1]*x[2]^3*x[3]^7*x[4]^3*x[5]+x[1]*x[2]*x[3]^5*x[4]^5*x[5]^3;\n",
    "x[1]^2*x[2]^2*x[5]+x[1]*x[2]^2*x[3]^2+x[1]*x[4]^2*x[5]^2+x[2]*x[3]^3*x[4]+x[3]^2*x[4]^2*x[5]]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1-element Vector{Polynomial{true, Int64}}:\n",
       " 8x₁⁴x₂⁴x₅² + 12x₁³x₂⁴x₃²x₅ + 20x₁³x₂²x₄²x₅³ + 4x₁²x₂⁴x₃⁴ + 12x₁²x₂³x₃³x₄x₅ + 32x₁²x₂²x₃²x₄²x₅² + 8x₁²x₄⁴x₅⁴ + 8x₁x₂³x₃⁵x₄ + 12x₁x₂²x₃⁴x₄²x₅ + 12x₁x₂x₃³x₄³x₅² + 12x₁x₃²x₄⁴x₅³ + 3x₂²x₃⁶x₄² + 8x₂x₃⁵x₄³x₅ + 4x₃⁴x₄⁴x₅²"
      ]
     },
     "execution_count": 23,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "denoms = [numer_A]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now insert the data from Example 6.3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {},
   "outputs": [],
   "source": [
    "u = [20,16,10,15,23]\n",
    "μ = [u...,1,2*84-sum(u)-3]\n",
    "ν = [84];\n",
    "V = [1 1 -1 -1 0; 0 -1 -1 1 1];"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We compute the integral and scale by $2^{13} \\cdot (2/5)$ to obtain $I_u$:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1.2185066463399776e-66"
      ]
     },
     "execution_count": 25,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "R = integrate_cubature(V,numers,denoms,x,μ,ν; verbatim = false, inttol = 1e-4)\n",
    "I_u = R*2^(13)*2/5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Example 6.4\n",
    "\n",
    "We now use our code for Bayesian model decision. As in Example 6.4, we compute a Bayesian factor from two different toric models.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {},
   "outputs": [],
   "source": [
    "@polyvar x[1:5]\n",
    "c1 = [2,3,5,7,11,13]\n",
    "numers1 = [c1[1]*x[2]*x[3]^3*x[4]; c1[2]*x[1]*x[2]^2*x[3]^2; c1[3]*x[3]^2*x[4]^2*x[5];\n",
    "     c1[4]*prod(x); c1[5]*x[1]^2*x[2]^2*x[5]; c1[6]*x[1]*x[4]^2*x[5]^2;\n",
    "      4*x[1]^4*x[2]^4*x[3]^2*x[4]^2*x[5]^3+x[1]^3*x[2]^5*x[3]^5*x[4]*x[5]+4*x[1]^3*x[2]^4*x[3]^4*x[4]^2*x[5]^2+9*x[1]^3*x[2]^3*x[3]^3*x[4]^3*x[5]^3+4*x[1]^3*x[2]^2*x[3]^2*x[4]^4*x[5]^4+8*x[1]^2*x[2]^3*x[3]^5*x[4]^3*x[5]^2+4*x[1]^2*x[2]^2*x[3]^4*x[4]^4*x[5]^3+x[1]*x[2]^3*x[3]^7*x[4]^3*x[5]+x[1]*x[2]*x[3]^5*x[4]^5*x[5]^3];\n",
    "denoms1 = [sum(numers1[1:6]);x[1]^2*x[2]^2*x[5]+x[1]*x[2]^2*x[3]^2+x[1]*x[4]^2*x[5]^2+x[2]*x[3]^3*x[4]+x[3]^2*x[4]^2*x[5]]\n",
    "\n",
    "c2 = [32,16,8,4,2,1]\n",
    "numers2 = [c2[1]*x[2]*x[3]^3*x[4]; c2[2]*x[1]*x[2]^2*x[3]^2; c2[3]*x[3]^2*x[4]^2*x[5];\n",
    "     c2[4]*prod(x); c2[5]*x[1]^2*x[2]^2*x[5]; c2[6]*x[1]*x[4]^2*x[5]^2;\n",
    "      4*x[1]^4*x[2]^4*x[3]^2*x[4]^2*x[5]^3+x[1]^3*x[2]^5*x[3]^5*x[4]*x[5]+4*x[1]^3*x[2]^4*x[3]^4*x[4]^2*x[5]^2+9*x[1]^3*x[2]^3*x[3]^3*x[4]^3*x[5]^3+4*x[1]^3*x[2]^2*x[3]^2*x[4]^4*x[5]^4+8*x[1]^2*x[2]^3*x[3]^5*x[4]^3*x[5]^2+4*x[1]^2*x[2]^2*x[3]^4*x[4]^4*x[5]^3+x[1]*x[2]^3*x[3]^7*x[4]^3*x[5]+x[1]*x[2]*x[3]^5*x[4]^5*x[5]^3];\n",
    "denoms2 = [sum(numers2[1:6]);x[1]^2*x[2]^2*x[5]+x[1]*x[2]^2*x[3]^2+x[1]*x[4]^2*x[5]^2+x[2]*x[3]^3*x[4]+x[3]^2*x[4]^2*x[5]];"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here are the data used in Example 6.4:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "u = [1,2,4,8,16,32]\n",
    "μ = [u...,1];\n",
    "ν = [sum(u),3];\n",
    "V = [1 1 -1 -1 0; 0 -1 -1 1 1]; \n",
    "nsamples = 100000 \n",
    "I1 = (2/5)*integrate_cubature(V,numers1,denoms1,x,μ,ν; verbatim=false, inttol = 1e-5)\n",
    "I2 = (2/5)*integrate_cubature(V,numers2,denoms2,x,μ,ν; verbatim=false, inttol = 1e-5)\n",
    "[I1 I2]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The Bayes factor is the ratio of these marginal likelihood integrals. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "I1/I2"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.7.1",
   "language": "julia",
   "name": "julia-1.7"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.7.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
