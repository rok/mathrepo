=====================================
Bayesian Integrals on Toric Varieties
=====================================

| This page contains auxiliary files to the paper:
| Michael Borinsky, Anna-Laura Sattelberger, Bernd Sturmfels and Simon Telen: Bayesian integrals on toric varieties
| In: SIAM journal on applied algebra and geometry, 7 (2023) 1, p. 77-103
| DOI: `10.1137/22M1490569 <https://dx.doi.org/10.1137/22M1490569>`_ ARXIV: https://arxiv.org/abs/2204.06414 CODE: https://mathrepo.mis.mpg.de/BayesianIntegrals

ABSTRACT: We explore the positive geometry of statistical models in the setting
of toric varieties. Our focus lies on models for discrete data that are
parameterized in terms of Cox coordinates. We develop a geometric theory
for computations in Bayesian statistics, such as evaluating marginal
likelihood integrals and  sampling from posterior distributions.
These are based on a tropical sampling method for evaluating
Feynman integrals in physics. We  here extend that method from 
projective spaces to arbitrary toric varieties.


As an illustration, the following figure shows the sector decomposition of a pentagon as seen in Example 4.5. 

.. image:: pentagon.png
  :width: 700

The paper also features a polytope arising from a three-dimensional conditional independence model. Its Schlegel diagram is shown below. 

.. image:: schlegel.png
  :width: 700

Our algorithms are implemented `julia <https://julialang.org/>`_. The code can be downloaded here :download:`julia_code.zip <julia_code.zip>`. It is illustrated in the following notebook:  

.. toctree::
	:maxdepth: 1
	:glob:

	BITV_notebook

Project page created: 15/04/2022

Project contributors: Michael Borinsky, Anna-Laura Sattelberger, Bernd Sturmfels, Simon Telen

.. Jupyter notebook written by: Jane Doe, dd/mm/yy (this should be a comment and not displayed on the webpage)

Corresponding author of this page: Simon Telen, Simon.Telen@mis.mpg.de

Software used: Julia (Version 1.7.1)
