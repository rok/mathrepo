=======================================
Mukai lifting of self-dual points in ℙ⁶
=======================================

| This page contains auxiliary code and examples to the paper:
| Barbara Betti and Leonie Kayser: *Mukai lifting of self-dual points in* :math:`\mathbb{P}^6`
| **ARXIV:** [2406.02734](https://arxiv.org/abs/2406.02734)

.. contents:: Table of Contents
  :local:

Overview
--------

**Abstract.** A set of 2n points in :math:`\mathbb{P}^{n-1}` is self-dual if it is invariant under the Gale transform. Motivated by Mukai's work on canonical curves, Petrakiev showed that a general self-dual set of 14 points in :math:`\mathbb{P}^6` arises as the intersection of the Grassmannian :math:`\mathrm{Gr}(2,6)` in its Plücker embedding in :math:`\mathbb{P}^{14}` with a linear space of dimension 6. In this paper we focus on the inverse problem of recovering such a linear space associated to a general self-dual set of points. We use numerical homotopy continuation to approach the problem and implement an algorithm in Julia to solve it. Along the way we also implement the forward problem of slicing Grassmannians and use it to experimentally study the real solutions to this problem.

On this page you can find an implementation of the slicing and lifting problem. The extended example from section 5 is provided in a notebook.

* Julia source code :download:`MukaiLiftSource.zip <MukaiLiftSource.zip>`
* Julia example notebook :download:`example.ipynb <example.ipynb>`

Slicing using homotopy continuation
-----------------------------------

The computational approach to slicing the Grassmannian :math:`{\rm Gr}(2,6) \subseteq \mathbb{P}^{14}` is detailed in section 3 of the paper.
The implementation can be found in the file ``MukaiLiftP6/src/Slicing.jl`` in the source code :download:`MukaiLiftSource.zip <MukaiLiftSource.zip>`.

Lifting using homotopy continuation
-----------------------------------

The computational approach to the Mukai lifting problem is detailed in section 4 of the paper.
The implementation can be found in the file ``MukaiLiftP6/src/Lifting.jl`` in the source code :download:`MukaiLiftSource.zip <MukaiLiftSource.zip>`, there is also an instructive notebook :download:`example.ipynb <example.ipynb>`. You can view the notebook in your browser on the following subpage:


.. toctree::
    :maxdepth: 1
    :glob:

    example.ipynb


The folder ``MukaiLiftP6/src/Monodromy`` in the source code :download:`MukaiLiftSource.zip <MukaiLiftSource.zip>` contains the file ``Monodromy.jl`` that computes two different liftings of a configuration of self-dual points using the approach described in section 4.1 of the paper. The parameters used and the corresponding solutions are saved in text files in the same folder. 
------------------------------------------------------------------------------------------------------------------------------


| Project page created: 13/6/2024

| Project contributors: Barbara Betti, Leonie Kayser

| Corresponding author of this page: Barbara Betti, betti@mis.mpg.de

| Software used: Julia 1.9.3, OSCAR 1.0.3, HomotopyContinuation 2.9.3

| System used: MPI MiS compute server with 512 GB RAM using 2x 12-Core Intel Xeon E5-2680 v3 processor at 2.50 GHz

| License for code of this project page: MIT License (https://spdx.org/licenses/MIT.html)

| License for all other content of this project page (text, images, …): CC BY 4.0 (https://creativecommons.org/licenses/by/4.0/)

| Last updated 6/3/2025
