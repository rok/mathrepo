
# pkg> activate KhovanskiiSolving 
using KhovanskiiSolving 
using Oscar
using LinearAlgebra


######################################################### 
######################## Row q=0 ########################

#QQ
varstring = vcat(["x$i" for i=0:3],["u$i$j" for i=0:1 for j=0:3 ]);
K = QQ
S, xu = PolynomialRing(K, varstring);
mons_deg1_x=collect(Oscar.monomials(sum(xu[1:4])^1));
mons_deg2_x=collect(Oscar.monomials(sum(xu[1:4])^2));
L1=[ transpose(xu[1:4])*xu[5:8], transpose(xu[1:4])*xu[9:12] ]
F=[]
# 4x Degree 2 curves defined by 1 quadric + 1 linear form
@time for i=1:4
		f=transpose(rand(-10:10,length(mons_deg1_x)))*mons_deg1_x;
		g=transpose(rand(-10:10,length(mons_deg2_x)))*mons_deg2_x;
		C= ideal([f,g]);
		J1= C+ideal(L1);
		J1_sat=saturation(J1, ideal(mons_deg1_x));
		J1_sat_elim= eliminate(J1_sat, xu[1:4] );
		push!(F,gens(J1_sat_elim));
	  end
F=vcat(F...);
length(F) 
exps_F=[ [collect(exponents(F[i]))[j][5:12] for j=1:length(collect(exponents(F[i])))] for i=1:length(F) ];
coeff_F=[ collect(coefficients(F[i])) for i=1:length(F)];
terms_F=[ collect(terms(F[i])) for i=1:length(F)];
# need a new ring with only uij variables
T, u = PolynomialRing(K, ["u$i$j" for i=0:1 for j=0:3 ]);
MS = MatrixSpace(T,2,4);
M = MS(transpose(reshape(u,4,2)));
ϕ = minors(M,2)
w = [-4,-3,-2,-1,-1,-2,-3,-4 ];
F=[ transpose(coeff_F[i])*([prod(u.^(exps_F[i][j])) for j=1:length(exps_F[i])]) for i=1:length(F)];
leadexps = [leadexp(hh,w) for hh in ϕ];
degs_F = [2 for i=1:length(F)];
dreg = sum(degs_F)-3+1 
HF_Gr(2,4,dreg) 
 
@time Mul = get_commuting_matrices(F,dreg,degs_F,ϕ,K,u,leadexps);
size(Mul[1])
 

# GF
varstring = vcat(["x$i" for i=0:3],["u$i$j" for i=0:1 for j=0:3 ]);
K= GF(9716633)
S, xu = PolynomialRing(K, varstring);
mons_deg1_x=collect(Oscar.monomials(sum(xu[1:4])^1));
mons_deg2_x=collect(Oscar.monomials(sum(xu[1:4])^2));
L1=[ transpose(xu[1:4])*xu[5:8], transpose(xu[1:4])*xu[9:12] ]

F=[]
# 4x Degree 2 curves defined by 1 quadric + 1 linear form
@time for i=1:4
		f=transpose(rand(-10:10,length(mons_deg1_x)))*mons_deg1_x;
		g=transpose(rand(-10:10,length(mons_deg2_x)))*mons_deg2_x;
		C= ideal([f,g]);
		J1= C+ideal(L1);
		J1_sat=saturation(J1, ideal(mons_deg1_x));
		J1_sat_elim= eliminate(J1_sat, xu[1:4] );
		push!(F,gens(J1_sat_elim));
	  end
F=vcat(F...);
length(F)
exps_F=[ [collect(exponents(F[i]))[j][5:12] for j=1:length(collect(exponents(F[i])))] for i=1:length(F) ];
coeff_F=[ collect(coefficients(F[i])) for i=1:length(F)];
terms_F=[ collect(terms(F[i])) for i=1:length(F)];

T, u = PolynomialRing(K, ["u$i$j" for i=0:1 for j=0:3 ]);
MS = MatrixSpace(T,2,4);
M = MS(transpose(reshape(u,4,2)));
ϕ = minors(M,2)
w = [-4,-3,-2,-1,-1,-2,-3,-4 ];
F=[ transpose(coeff_F[i])*([prod(u.^(exps_F[i][j])) for j=1:length(exps_F[i])]) for i=1:length(F)];
leadexps = [leadexp(hh,w) for hh in ϕ];
degs_F = [2 for i=1:length(F)];
dreg = sum(degs_F)-3+1
HF_Gr(2,4,dreg)

@time Mul = get_commuting_matrices(F,dreg,degs_F,ϕ,K,u,leadexps);

size(Mul[1])


######################################################### 
######################## Row q=1 ########################


# QQ 
varstring = vcat(["x$i" for i=0:3],["u$i$j" for i=0:1 for j=0:3 ]);
K = QQ
S, xu = PolynomialRing(K, varstring);
mons_deg1_x=collect(Oscar.monomials(sum(xu[1:4])^1));
mons_deg2_x=collect(Oscar.monomials(sum(xu[1:4])^2));
L1=[ transpose(xu[1:4])*xu[5:8], transpose(xu[1:4])*xu[9:12] ]
F=[]
# 1x Degree 4 curves defined by 2 quadrics
@time for i=1:1
		f=transpose(rand(-10:10,length(mons_deg2_x)))*mons_deg2_x;
		g=transpose(rand(-10:10,length(mons_deg2_x)))*mons_deg2_x;
		C= ideal([f,g]);
		J1= C+ideal(L1);
		J1_sat=saturation(J1, ideal(mons_deg1_x));
		J1_sat_elim= eliminate(J1_sat, xu[1:4] );
		push!(F,gens(J1_sat_elim))
	  end

# 3x Degree 2 curves defined by 1 quadric + 1 linear form
@time for i=1:3
		f=transpose(rand(-10:10,length(mons_deg1_x)))*mons_deg1_x;
		g=transpose(rand(-10:10,length(mons_deg2_x)))*mons_deg2_x;
		C= ideal([f,g]);
		J1= C+ideal(L1);
		J1_sat=saturation(J1, ideal(mons_deg1_x));
		J1_sat_elim= eliminate(J1_sat, xu[1:4] );
		push!(F,gens(J1_sat_elim));
	  end
F=vcat(F...);
length(F)
exps_F=[ [collect(exponents(F[i]))[j][5:12] for j=1:length(collect(exponents(F[i])))] for i=1:length(F) ];
coeff_F=[ collect(coefficients(F[i])) for i=1:length(F)];
terms_F=[ collect(terms(F[i])) for i=1:length(F)];

# need a new ring with only uij variables
T, u = PolynomialRing(K, ["u$i$j" for i=0:1 for j=0:3 ]);
MS = MatrixSpace(T,2,4);
M = MS(transpose(reshape(u,4,2)));
ϕ = minors(M,2);
w = [-4,-3,-2,-1,-1,-2,-3,-4 ];
F=[ transpose(coeff_F[i])*([prod(u.^(exps_F[i][j])) for j=1:length(exps_F[i])]) for i=1:length(F)];
leadexps = [leadexp(hh,w) for hh in ϕ];
degs_F = [4;2;2;2];
dreg = sum(degs_F)-3+1 
HF_Gr(2,4,dreg) 

@time Mul = get_commuting_matrices(F,dreg,degs_F,ϕ,K,u,leadexps);



# GF
varstring = vcat(["x$i" for i=0:3],["u$i$j" for i=0:1 for j=0:3 ]);
K= GF(9716633)
S, xu = PolynomialRing(K, varstring);
mons_deg1_x=collect(Oscar.monomials(sum(xu[1:4])^1));
mons_deg2_x=collect(Oscar.monomials(sum(xu[1:4])^2));
L1=[ transpose(xu[1:4])*xu[5:8], transpose(xu[1:4])*xu[9:12] ]
F=[]
# 1x Degree 4 curves defined by 2 quadrics
@time for i=1:1
		f=transpose(rand(-10:10,length(mons_deg2_x)))*mons_deg2_x;
		g=transpose(rand(-10:10,length(mons_deg2_x)))*mons_deg2_x;
		C= ideal([f,g]);
		J1= C+ideal(L1);
		J1_sat=saturation(J1, ideal(mons_deg1_x));
		J1_sat_elim= eliminate(J1_sat, xu[1:4] );
		push!(F,gens(J1_sat_elim))
	  end

# 3x Degree 2 curves defined by 1 quadric + 1 linear form
@time for i=1:3
		f=transpose(rand(-10:10,length(mons_deg1_x)))*mons_deg1_x;
		g=transpose(rand(-10:10,length(mons_deg2_x)))*mons_deg2_x;
		C= ideal([f,g]);
		J1= C+ideal(L1);
		J1_sat=saturation(J1, ideal(mons_deg1_x));
		J1_sat_elim= eliminate(J1_sat, xu[1:4] );
		push!(F,gens(J1_sat_elim));
	  end
F=vcat(F...);
length(F)
exps_F=[ [collect(exponents(F[i]))[j][5:12] for j=1:length(collect(exponents(F[i])))] for i=1:length(F) ];
coeff_F=[ collect(coefficients(F[i])) for i=1:length(F)];
terms_F=[ collect(terms(F[i])) for i=1:length(F)];

# need a new ring with only uij variables
T, u = PolynomialRing(K, ["u$i$j" for i=0:1 for j=0:3 ])
MS = MatrixSpace(T,2,4);
M = MS(transpose(reshape(u,4,2)));
ϕ = minors(M,2)
w = [-4,-3,-2,-1,-1,-2,-3,-4 ];
F=[ transpose(coeff_F[i])*([prod(u.^(exps_F[i][j])) for j=1:length(exps_F[i])]) for i=1:length(F)];
leadexps = [leadexp(hh,w) for hh in ϕ];
degs_F = [4;2;2;2];
dreg = sum(degs_F)-3+1
HF_Gr(2,4,dreg)

@time Mul = get_commuting_matrices(F,dreg,degs_F,ϕ,K,u,leadexps);



######################################################### 
######################## Row q=2 ########################


# QQ 
q=2
varstring = vcat(["x$i" for i=0:3],["u$i$j" for i=0:1 for j=0:3 ]);
K = QQ
S, xu = PolynomialRing(K, varstring);
mons_deg1_x=collect(Oscar.monomials(sum(xu[1:4])^1));
mons_deg2_x=collect(Oscar.monomials(sum(xu[1:4])^2));
L1=[ transpose(xu[1:4])*xu[5:8], transpose(xu[1:4])*xu[9:12] ]
F=[]
# 2x Degree 4 curves defined by 2 quadrics
@time for i=1:2
		f=transpose(rand(-10:10,length(mons_deg2_x)))*mons_deg2_x;
		g=transpose(rand(-10:10,length(mons_deg2_x)))*mons_deg2_x;
		C= ideal([f,g]);
		J1= C+ideal(L1);
		J1_sat=saturation(J1, ideal(mons_deg1_x));
		J1_sat_elim= eliminate(J1_sat, xu[1:4] );
		push!(F,gens(J1_sat_elim))
	  end

# 2x Degree 2 curves defined by 1 quadric + 1 linear form
@time for i=1:2
		f=transpose(rand(-10:10,length(mons_deg1_x)))*mons_deg1_x;
		g=transpose(rand(-10:10,length(mons_deg2_x)))*mons_deg2_x;
		C= ideal([f,g]);
		J1= C+ideal(L1);
		J1_sat=saturation(J1, ideal(mons_deg1_x));
		J1_sat_elim= eliminate(J1_sat, xu[1:4] );
		push!(F,gens(J1_sat_elim));
	  end
F=vcat(F...);
length(F)
exps_F=[ [collect(exponents(F[i]))[j][5:12] for j=1:length(collect(exponents(F[i])))] for i=1:length(F) ];
coeff_F=[ collect(coefficients(F[i])) for i=1:length(F)];
terms_F=[ collect(terms(F[i])) for i=1:length(F)];

# need a new ring with only uij variables
T, u = PolynomialRing(K, ["u$i$j" for i=0:1 for j=0:3 ]);
MS = MatrixSpace(T,2,4);
M = MS(transpose(reshape(u,4,2)));
ϕ = minors(M,2)
w = [-4,-3,-2,-1,-1,-2,-3,-4 ];
F=[ transpose(coeff_F[i])*([prod(u.^(exps_F[i][j])) for j=1:length(exps_F[i])]) for i=1:length(F)];
leadexps = [leadexp(hh,w) for hh in ϕ];
degs_F = [4;4;2;2];
dreg = sum(degs_F)-3+1 
HF_Gr(2,4,dreg)

@time Mul = get_commuting_matrices(F,dreg,degs_F,ϕ,K,u,leadexps);



# GF
q=2
varstring = vcat(["x$i" for i=0:3],["u$i$j" for i=0:1 for j=0:3 ]);
K= GF(9716633)
S, xu = PolynomialRing(K, varstring);
mons_deg1_x=collect(Oscar.monomials(sum(xu[1:4])^1));
mons_deg2_x=collect(Oscar.monomials(sum(xu[1:4])^2));
L1=[ transpose(xu[1:4])*xu[5:8], transpose(xu[1:4])*xu[9:12] ]
F=[]
# 2x Degree 4 curves defined by 2 quadrics
@time for i=1:2
		f=transpose(rand(-10:10,length(mons_deg2_x)))*mons_deg2_x;
		g=transpose(rand(-10:10,length(mons_deg2_x)))*mons_deg2_x;
		C= ideal([f,g]);
		J1= C+ideal(L1);
		J1_sat=saturation(J1, ideal(mons_deg1_x));
		J1_sat_elim= eliminate(J1_sat, xu[1:4] );
		push!(F,gens(J1_sat_elim))
	  end

# 2x Degree 2 curves defined by 1 quadric + 1 linear form
@time for i=1:2
		f=transpose(rand(-10:10,length(mons_deg1_x)))*mons_deg1_x;
		g=transpose(rand(-10:10,length(mons_deg2_x)))*mons_deg2_x;
		C= ideal([f,g]);
		J1= C+ideal(L1);
		J1_sat=saturation(J1, ideal(mons_deg1_x));
		J1_sat_elim= eliminate(J1_sat, xu[1:4] );
		push!(F,gens(J1_sat_elim));
	  end
F=vcat(F...);
length(F)
exps_F=[ [collect(exponents(F[i]))[j][5:12] for j=1:length(collect(exponents(F[i])))] for i=1:length(F) ];
coeff_F=[ collect(coefficients(F[i])) for i=1:length(F)];
terms_F=[ collect(terms(F[i])) for i=1:length(F)];

# need a new ring with only uij variables
T, u = PolynomialRing(K, ["u$i$j" for i=0:1 for j=0:3 ])
MS = MatrixSpace(T,2,4)
M = MS(transpose(reshape(u,4,2)))
ϕ = minors(M,2)
w = [-4,-3,-2,-1,-1,-2,-3,-4 ];
F=[ transpose(coeff_F[i])*([prod(u.^(exps_F[i][j])) for j=1:length(exps_F[i])]) for i=1:length(F)];
leadexps = [leadexp(hh,w) for hh in ϕ];
degs_F = [4;4;2;2];
dreg = sum(degs_F)-3+1
HF_Gr(2,4,dreg)

@time Mul = get_commuting_matrices(F,dreg,degs_F,ϕ,K,u,leadexps);



######################################################### 
######################## Row q=3 ########################


# QQ 
q=3
varstring = vcat(["x$i" for i=0:3],["u$i$j" for i=0:1 for j=0:3 ]);
K = QQ
S, xu = PolynomialRing(K, varstring);
mons_deg1_x=collect(Oscar.monomials(sum(xu[1:4])^1));
mons_deg2_x=collect(Oscar.monomials(sum(xu[1:4])^2));
L1=[ transpose(xu[1:4])*xu[5:8], transpose(xu[1:4])*xu[9:12] ]
F=[]
# 3x Degree 4 curves defined by 2 quadrics
@time for i=1:3
		f=transpose(rand(-10:10,length(mons_deg2_x)))*mons_deg2_x;
		g=transpose(rand(-10:10,length(mons_deg2_x)))*mons_deg2_x;
		C= ideal([f,g]);
		J1= C+ideal(L1);
		J1_sat=saturation(J1, ideal(mons_deg1_x));
		J1_sat_elim= eliminate(J1_sat, xu[1:4] );
		push!(F,gens(J1_sat_elim))
	  end

# 1x Degree 2 curves defined by 1 quadric + 1 linear form
@time for i=1:1
		f=transpose(rand(-10:10,length(mons_deg1_x)))*mons_deg1_x;
		g=transpose(rand(-10:10,length(mons_deg2_x)))*mons_deg2_x;
		C= ideal([f,g]);
		J1= C+ideal(L1);
		J1_sat=saturation(J1, ideal(mons_deg1_x));
		J1_sat_elim= eliminate(J1_sat, xu[1:4] );
		push!(F,gens(J1_sat_elim));
	  end
F=vcat(F...);
length(F)
exps_F=[ [collect(exponents(F[i]))[j][5:12] for j=1:length(collect(exponents(F[i])))] for i=1:length(F) ];
coeff_F=[ collect(coefficients(F[i])) for i=1:length(F)];
terms_F=[ collect(terms(F[i])) for i=1:length(F)];

# need a new ring with only uij variables
T, u = PolynomialRing(K, ["u$i$j" for i=0:1 for j=0:3 ])
MS = MatrixSpace(T,2,4)
M = MS(transpose(reshape(u,4,2)))
ϕ = minors(M,2)
w = [-4,-3,-2,-1,-1,-2,-3,-4 ];
F=[ transpose(coeff_F[i])*([prod(u.^(exps_F[i][j])) for j=1:length(exps_F[i])]) for i=1:length(F)];
leadexps = [leadexp(hh,w) for hh in ϕ];
degs_F = [4;4;4;2];
dreg = sum(degs_F)-3+1 
HF_Gr(2,4,dreg) 

@time Mul = get_commuting_matrices(F,dreg,degs_F,ϕ,K,u,leadexps);
size(Mul[1])


# GF 
q=3
varstring = vcat(["x$i" for i=0:3],["u$i$j" for i=0:1 for j=0:3 ]);
K= GF(9716633)
S, xu = PolynomialRing(K, varstring);
mons_deg1_x=collect(Oscar.monomials(sum(xu[1:4])^1));
mons_deg2_x=collect(Oscar.monomials(sum(xu[1:4])^2));
L1=[ transpose(xu[1:4])*xu[5:8], transpose(xu[1:4])*xu[9:12] ]
F=[]
# 3x Degree 4 curves defined by 2 quadrics
@time for i=1:3
		f=transpose(rand(-10:10,length(mons_deg2_x)))*mons_deg2_x;
		g=transpose(rand(-10:10,length(mons_deg2_x)))*mons_deg2_x;
		C= ideal([f,g]);
		J1= C+ideal(L1);
		J1_sat=saturation(J1, ideal(mons_deg1_x));
		J1_sat_elim= eliminate(J1_sat, xu[1:4] );
		push!(F,gens(J1_sat_elim))
	  end

# 1x Degree 2 curves defined by 1 quadric + 1 linear form
@time for i=1:1
		f=transpose(rand(-10:10,length(mons_deg1_x)))*mons_deg1_x;
		g=transpose(rand(-10:10,length(mons_deg2_x)))*mons_deg2_x;
		C= ideal([f,g]);
		J1= C+ideal(L1);
		J1_sat=saturation(J1, ideal(mons_deg1_x));
		J1_sat_elim= eliminate(J1_sat, xu[1:4] );
		push!(F,gens(J1_sat_elim));
	  end
F=vcat(F...);
length(F)
exps_F=[ [collect(exponents(F[i]))[j][5:12] for j=1:length(collect(exponents(F[i])))] for i=1:length(F) ];
coeff_F=[ collect(coefficients(F[i])) for i=1:length(F)];
terms_F=[ collect(terms(F[i])) for i=1:length(F)];

# need a new ring with only uij variables
T, u = PolynomialRing(K, ["u$i$j" for i=0:1 for j=0:3 ]);
MS = MatrixSpace(T,2,4);
M = MS(transpose(reshape(u,4,2)));
ϕ = minors(M,2);
w = [-4,-3,-2,-1,-1,-2,-3,-4 ];
F=[ transpose(coeff_F[i])*([prod(u.^(exps_F[i][j])) for j=1:length(exps_F[i])]) for i=1:length(F)];
leadexps = [leadexp(hh,w) for hh in ϕ];
degs_F = [4;4;4;2];
dreg = sum(degs_F)-3+1 
HF_Gr(2,4,dreg) 

@time Mul = get_commuting_matrices(F,dreg,degs_F,ϕ,K,u,leadexps);
size(Mul[1]) 


######################################################### 
######################## Row q=4 ########################


# QQ 
q=4
varstring = vcat(["x$i" for i=0:3],["u$i$j" for i=0:1 for j=0:3 ]);
K = QQ
S, xu = PolynomialRing(K, varstring);
mons_deg1_x=collect(Oscar.monomials(sum(xu[1:4])^1));
mons_deg2_x=collect(Oscar.monomials(sum(xu[1:4])^2));
L1=[ transpose(xu[1:4])*xu[5:8], transpose(xu[1:4])*xu[9:12] ]
F=[]
# 4x Degree 4 curves defined by 2 quadrics
@time for i=1:4
		f=transpose(rand(-10:10,length(mons_deg2_x)))*mons_deg2_x;
		g=transpose(rand(-10:10,length(mons_deg2_x)))*mons_deg2_x;
		C= ideal([f,g]);
		J1= C+ideal(L1);
		J1_sat=saturation(J1, ideal(mons_deg1_x));
		J1_sat_elim= eliminate(J1_sat, xu[1:4] );
		push!(F,gens(J1_sat_elim))
	  end
F=vcat(F...);
length(F) 
exps_F=[ [collect(exponents(F[i]))[j][5:12] for j=1:length(collect(exponents(F[i])))] for i=1:length(F) ];
coeff_F=[ collect(coefficients(F[i])) for i=1:length(F)];
terms_F=[ collect(terms(F[i])) for i=1:length(F)];

# need a new ring with only uij variables
T, u = PolynomialRing(K, ["u$i$j" for i=0:1 for j=0:3 ]);
MS = MatrixSpace(T,2,4);
M = MS(transpose(reshape(u,4,2)));
ϕ = minors(M,2);
w = [-4,-3,-2,-1,-1,-2,-3,-4 ];
F=[ transpose(coeff_F[i])*([prod(u.^(exps_F[i][j])) for j=1:length(exps_F[i])]) for i=1:length(F)];
leadexps = [leadexp(hh,w) for hh in ϕ];
degs_F = [4;4;4;4];
dreg = sum(degs_F)-3+1
HF_Gr(2,4,dreg) 

@time Mul = get_commuting_matrices(F,dreg,degs_F,ϕ,K,u,leadexps);



# GF 
q=4
varstring = vcat(["x$i" for i=0:3],["u$i$j" for i=0:1 for j=0:3 ]);
K= GF(9716633)
S, xu = PolynomialRing(K, varstring);
mons_deg1_x=collect(Oscar.monomials(sum(xu[1:4])^1));
mons_deg2_x=collect(Oscar.monomials(sum(xu[1:4])^2));
L1=[ transpose(xu[1:4])*xu[5:8], transpose(xu[1:4])*xu[9:12] ]
F=[]
# 4x Degree 4 curves defined by 2 quadrics
@time for i=1:4
		f=transpose(rand(-10:10,length(mons_deg2_x)))*mons_deg2_x;
		g=transpose(rand(-10:10,length(mons_deg2_x)))*mons_deg2_x;
		C= ideal([f,g]);
		J1= C+ideal(L1);
		J1_sat=saturation(J1, ideal(mons_deg1_x));
		J1_sat_elim= eliminate(J1_sat, xu[1:4] );
		push!(F,gens(J1_sat_elim))
	  end
F=vcat(F...);
length(F)
exps_F=[ [collect(exponents(F[i]))[j][5:12] for j=1:length(collect(exponents(F[i])))] for i=1:length(F) ];
coeff_F=[ collect(coefficients(F[i])) for i=1:length(F)];
terms_F=[ collect(terms(F[i])) for i=1:length(F)];

# need a new ring with only uij variables
T, u = PolynomialRing(K, ["u$i$j" for i=0:1 for j=0:3 ])
MS = MatrixSpace(T,2,4)
M = MS(transpose(reshape(u,4,2)))
ϕ = minors(M,2)
w = [-4,-3,-2,-1,-1,-2,-3,-4 ];
F=[ transpose(coeff_F[i])*([prod(u.^(exps_F[i][j])) for j=1:length(exps_F[i])]) for i=1:length(F)];
leadexps = [leadexp(hh,w) for hh in ϕ];
degs_F = [4;4;4;4];
dreg = sum(degs_F)-3+1
HF_Gr(2,4,dreg)
@time Mul = get_commuting_matrices(F,dreg,degs_F,ϕ,K,u,leadexps);









