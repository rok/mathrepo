========================================
Solving Equations Using Khovanskii Bases
========================================

| This page contains auxiliary files to the paper: 
| Barbara Betti, Marta Panizzut and Simon Telen: Solving equations using Khovanskii bases
| In: Journal of symbolic computation, 126 (2025), 102340
| DOI: `10.1016/j.jsc.2024.102340 <https://doi.org/10.1016/j.jsc.2024.102340>`_ ARXIV: `<https://arxiv.org/abs/2306.14871>`_
| CODE: `<https://mathrepo.mis.mpg.de/KhovanskiiSolving>`_

ABSTRACT: We develop a new eigenvalue method for solving structured polynomial equations over any field. The equations are defined on a projective algebraic variety which admits a rational parameterization by a Khovanskii basis, e.g., a Grassmannian in its Plücker embedding. This generalizes established algorithms for toric varieties, and introduces the effective use of Khovanskii bases in computer algebra. We investigate regularity questions and discuss several applications. 

The picture below represents the initial algebra of the Del Pezzo surface in Example 2.6 in the paper.

.. image:: 2purplepictures.png
  :width: 700


Our eigenvalue method is implemented in `julia <https://julialang.org/>`_. The package is called KhovanskiiSolving.jl. It makes use of `Oscar.jl <https://www.oscar-system.org/>`_ (v0.12.1). 

Our Julia code can be downloaded here :download:`KhovanskiiSolving_Julia_code.zip <KhovanskiiSolving_Julia_code.zip>`.

All the examples computed and discussed in the paper can be reproduced using this code, and they can be downloaded here :download:`Examples_KhovanskiiSolving.zip <Examples_KhovanskiiSolving.zip>`.

The following tables report the computations performed over the Grassmannians Gr(3,6) and Gr(2,4).

.. image:: Table1.png
  :width: 700

.. image:: Table2.png
  :width: 700

The code in `julia <https://julialang.org/>`_ used for creating Table 1 and Table 2 can be downloaded here :download:`Table1_Gr36_code.jl <Table1_Gr36_code.jl>`, :download:`Table2_Gr24_code.jl <Table2_Gr24_code.jl>` . 

Project page created: 23/06/2023

Project contributors: Barbara Betti, Marta Panizzut, Simon Telen

Corresponding author of this page: Barbara Betti, email betti@mis.mpg.de

Software used: Julia (Version 1.9).

License for code of this project page: MIT License `https://spdx.org/licenses/MIT.html <https://spdx.org/licenses/MIT.html>`_.
