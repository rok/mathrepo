=========================================
Moment Varieties for Mixtures of Products
=========================================

| This page contains auxiliary files to the paper:
| Yulia Alexandr, Joe Kileel, and Bernd Sturmfels: Moment varieties for mixtures of products
| In: ISSAC '23 proceedings of the international symposium on symbolic and algebraic computation ; Tromso Norway ; July 24-27, 2023 / Alicia Dickenstein ... (eds.)
| New York : ACM, 2023 . - P. 53-60 
| DOI: `10.1145/3597066.3597084 <https://dx.doi.org/10.1145/3597066.3597084>`_ ARXIV: https://arxiv.org/abs/2301.09068 CODE: https://mathrepo.mis.mpg.de/MomentVarietiesForMixturesOfProducts



ABSTRACT: The setting of this article is nonparametric algebraic statistics. We study moment varieties of conditionally independent mixture distributions on :math:`\mathbb{R}^n`. These are the secant varieties of toric varieties that express independence in terms of univariate moments. Our results revolve around the dimensions and defining polynomials of these varieties.

Summary
----------
Consider :math:`n` independent random variables :math:`X_1,X_2,\ldots,X_n` on :math:`\mathbb{R}`. We make no assumptions about the :math:`X_k` other than that their moments :math:`\mu_{ki} = \mathbb{E}(X_k^i)` exist.  We write :math:`m_{i_1i_2\ldots i_n}` for the moments of the random vector :math:`X=(X_1,X_2\ldots,X_n)`. By independence, we have

.. math:: \begin{align*}
      m_{i_1 i_2 \cdots i_n}=
         \mathbb{E}(X_1^{i_1} X_2^{i_2} \cdots X_n^{i_n}) =
      \mathbb{E}(X_1^{i_1} ) \mathbb{E}(X_2^{i_2})  \cdots 
      \mathbb{E}(X_n^{i_n}) =
      \mu_{1 i_1} \mu_{2 i_2} \cdots \mu_{n i_n} .
   \end{align*} 

We consider the squarefree parametrization for the moments with :math:`i_1 + i_2 + \cdots + i_n = d`, whose image is the toric variety :math:`\mathcal{M}_{n,d}`. Furthermore, we study mixtures of :math:`r` independent distributions and their images under certain coordinate projections. The associated moment varieties are the secant varieties :math:`\sigma_r(\mathcal{M}_{n,d})` and :math:`\sigma_r(\mathcal{M}_{n,\lambda})`, respectively.

Code
----------

Our code is written in :math:`\verb|Macaulay2|`, :math:`\verb|Julia|`, and :math:`\verb|Maple|`. We use :math:`\verb|Macaulay2|` to compute dimensions of the secant varieties we study. We also compute the degrees of several secant varieties, both numerically and symbolically.

.. toctree::
   :maxdepth: 2

   ComputingDimensions.rst
   ComputingDegrees.rst


We also include the :download:`code <code-for-Proposition-34.mpl>` used to prove Proposition 34, implemented in :math:`\verb|Maple|`, the :download:`code <mask-matrices.m2>` to form a masked Hankel flattening matrix, the :download:`code <computing-toric-ideals.m2>` to compute toric ideals of the varieties when :math:`r=1`, and the :download:`code <finiteness-testing.m2>` relevant for the finiteness results in Section 3.

.. role:: raw-html(raw)
    :format: html

Project page created: 10/05/2023. :raw-html:`<br />`
Project contributors: Yulia Alexandr, Joe Kileel, and Bernd Sturmfels. :raw-html:`<br />`

Software used: Macaulay2 (version 1.17), Julia (version 1.8), Maple. :raw-html:`<br />`
System setup used: MacBook Pro with macOS BigSur 11.1, Chip Apple M1, Memory 8 GB. :raw-html:`<br />`

Corresponding author of this page: Yulia Alexandr, yulia@math.berkeley.edu
