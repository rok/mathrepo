needsPackage("Graphs");

-- Function that builds the ideal of the graph curve of the input graph G,
-- and the ideals of its lines and its nodes
GraphCurve = G ->
(
  -- Construct cycle matrix Cyc of the graph G
  A = {};
  for e in edges(G) do
  {
    column = {};

    for i from 0 to (#vertices(G) - 1) do
    {
      if (i == e#0) then {column = append(column, 1);}
      else {if (i == e#1) then {column = append(column, -1);}
            else {column = append(column, 0);}}
    };

    A = append(A, column);
  };
  A = transpose(matrix(A));


  K = kernel(A);
  Cyc = {};

  for i from 0 to (g - 1) do
  {
    currentRow = {};

    for j from 0 to (#edges(G) - 1) do
    {
      currentRow = append(currentRow, (K_i)_j);
    };

    Cyc = append(Cyc, currentRow);
  };

  Cyc = matrix(Cyc);


  -- Construct the ideals of the lines and the nodes in the graph curve
  IdealsOfVertices = {};
  IdealsOfEdges = {};

  columnVars = transpose(matrix({gens S}));

  for i from 0 to (#vertices(G) - 1) do
  {
    adjEdges = {};
    for j from 0 to (#edges(G) - 1) do
    {
      if member(i, (edges(G))#j) then adjEdges = append(adjEdges, j);
    };

    -- L is ideal of line corresponding to vertex i
    L = minors(3, Cyc_adjEdges | columnVars);
    IdealsOfVertices = append(IdealsOfVertices, radical(L));
  };


  for e in Edges do
  {
    -- vertces adjacent to edge i
    v0 = e_0;
    v1 = e_1;

    -- N is ideal of node corresponding to edge i
    N = IdealsOfVertices_v0 + IdealsOfVertices_v1;
    IdealsOfEdges = append(IdealsOfEdges, radical(N));
  };

  -- I is the ideal of the graph curve, and GenList contains its generators
  I = intersect(IdealsOfVertices);

  GenList = (entries(mingens(I)))_0;

  return (I, GenList, IdealsOfVertices, IdealsOfEdges);
);


-- Function returns the ideal of the linear span of input variety
LinearSpanOfVariety = VarietyIdeal ->
(
  polys = (entries(mingens(VarietyIdeal)))_0;
  PlaneIdeal = ideal(0);
  for poly in polys do
  (
    if degree(poly) == {1} then PlaneIdeal = PlaneIdeal + ideal(poly);
  );
  PlaneIdeal = trim(PlaneIdeal);
  return PlaneIdeal;
);


-- Function returns the plane spanned by the two lines of the
-- graph curve meeting at the node corresponding to EdgeNumber
PlaneOfAnEdge = EdgeNumber ->
(
  v0 = (Edges_EdgeNumber)_0;
  v1 = (Edges_EdgeNumber)_1;
  PlaneIdeal0 = radical((IdealsOfVertices_v0) * (IdealsOfVertices_v1));
  return sub(LinearSpanOfVariety(PlaneIdeal0), S);
);


-- Function returns a list of edges that neighbor the given EdgeNumber
EdgeNeighborEdges = EdgeNumber ->
(
  NeighborList = {};

  for i from 0 to (#Edges - 1) do
  {
    if #(set(Edges_i) * set(Edges_EdgeNumber)) == 1 then
      NeighborList = append(NeighborList, i);
  };

  return NeighborList;
);


-- Function splits the 4 edges neighboring the given EdgeNumber into two pairs,
-- according to the edge pairing inout by the user
EdgeNeighborsPaired = EdgeNumber ->
(
  Unpaired = EdgeNeighborEdges(EdgeNumber);
  Pair1 = {};
  Pair2 = {};

  for e in Unpaired do
  (
    if member(e, EdgePairing_EdgeNumber) then Pair1 = append(Pair1, e) else Pair2 = append(Pair2, e);
  );

  return {Pair1, Pair2};
);



FindGenNotInOtherIdeal = (SmallIdeal,BigIdeal) ->
(
  polys = (entries(mingens(BigIdeal)))_0;
  for poly in polys do
  (
    if not((poly % SmallIdeal) == 0) then GenNotInOther = poly;
  );
  return GenNotInOther;
);



FindLinesAccordingToPairing = EdgeNumber ->
(
  PairOfPairs = EdgeNeighborsPaired(EdgeNumber);
  Ind0 = (PairOfPairs_0)_0;
  Ind1 = (PairOfPairs_0)_1;
  Line1 = radical((IdealsOfEdges_Ind0) * (IdealsOfEdges_Ind1));
  Line1 = LinearSpanOfVariety(Line1);

  Ind0 = (PairOfPairs_1)_0;
  Ind1 = (PairOfPairs_1)_1;
  Line2 = radical((IdealsOfEdges_Ind0) * (IdealsOfEdges_Ind1));
  Line2 = LinearSpanOfVariety(Line2);

  EPlane = PlaneOfAnEdge(EdgeNumber);
  Linearform1 = FindGenNotInOtherIdeal(EPlane, Line1);
  Linearform2 = FindGenNotInOtherIdeal(EPlane, Line2);

  return {Linearform1, Linearform2};
);


HesseMatrix = Quad ->
(
  Quad = sub(Quad,S);
  use S;
  Hesse = mutableMatrix(0 * id_(S^(n+1)));
  for i from 0 to n do
  (
    for j from 0 to n do
    (
      Hesse_(i,j) = diff(x_i, diff(x_j, Quad));
    );
  );
  return matrix(Hesse);
);


FindQuadraticGenerator = LinesIdeal ->
(
  polys = (entries(mingens(LinesIdeal)))_0;
  for poly in polys do
  (
    if degree(poly) == {2} then Quadgen = poly;
  );
  return Quadgen;
);


-- Function that computes the right sign of a tangent deformation
GoodSign = (Quad1,Quad2) ->
(
  Ri = QQ[t];
  H1 = HesseMatrix(Quad1);
  H2 = HesseMatrix(Quad2);
  H1 = sub(H1,Ri);
  H2 = sub(H2,Ri);
  Resultant = minors(3, H1 + t * H2);
  Resultant = saturate(Resultant, ideal(t));
  minzer = (t % Resultant);
  minzer = sub(minzer, QQ);

  return (minzer < 0);
);


-- Function computes the tangent direction that smooths out the node
-- corresponding to given EdgeNumber
DeformEdge = EdgeNumber ->
(
  e = Edges_EdgeNumber;
  v0 = e_0;
  v1 = e_1;

  LineIdeal = intersect(IdealsOfVertices_v0, IdealsOfVertices_v1);
  QuadGen = FindQuadraticGenerator(LineIdeal);
  EdgePlane = PlaneOfAnEdge(EdgeNumber);

  QuadGen = QuadGen % EdgePlane;

  NewLines = FindLinesAccordingToPairing(EdgeNumber);
  ProdNewLines = (NewLines_0) * (NewLines_1);
  ProdNewLines = ProdNewLines % EdgePlane;

  -- Compute the correct sign of the tangent direction, so that
  -- the deformation agrees with the EdgePairing input
  if GoodSign(QuadGen,ProdNewLines) then ProdNewLines = ProdNewLines else ProdNewLines = -ProdNewLines;

  D = QQ[eps]/ideal(eps^2);

  S2 = D[x_0 .. x_n];
  QuadGen = sub(QuadGen, S2);


  QuadGenE = QuadGen + eps * sub(ProdNewLines,S2);

  Hyperbel = sub(PlaneOfAnEdge(EdgeNumber),S2) + ideal(QuadGenE);

  DefId = Hyperbel;
  for i from 0 to (2*g-3) do
  (
    if member(i, Edges_EdgeNumber) then 1+1 else DefId = intersect(DefId, sub(IdealsOfVertices_i, S2));
  );

  DefId = saturate(DefId, ideal(x_0 .. x_n));
  return DefId;
);


-- !!! INPUT edges of graph G
-- !!! G is a graph with genus g and 2*g-2 vertices labeled 0...(2*g-3)

Edges = {{0, 1}, {1, 2}, {2, 0}, {3, 4}, {4, 5}, {5, 3}, {0, 3}, {1, 4}, {2, 5}};


G = digraph(Edges, EntryMode => "edges");


-- compute genus g of graph G and ambient dimension n
g = (#vertices(G) + 2) // 2;
n = g - 1;


S = QQ[x_0 .. x_n];


-- Compute ideal of graph curve, list of its generators,
-- the ideals of the lines and the nodes in the graph curve
(I, GenList, IdealsOfVertices, IdealsOfEdges) = GraphCurve(G);



--  !!! INPUT edge pairing:

EdgePairing = {{1,2}, {0,2}, {0,1}, {4,5}, {3,5}, {3,4}, {2,5}, {1,4}, {1,4}};



Tang = matrix{GenList};

for j from 0 to (3*g-4) do
(
  Jeps = DeformEdge(j);
  M = (matrix{{-eps}} | mingens(Jeps));
  Tang0 = sub(matrix{GenList}, S2) // M;
  Tang0 = submatrix(Tang0, {0}, );
  Tang0 = sub(Tang0, S);
  Tang0 = Tang0 % I;
  Tang = Tang || Tang0;
);

Tang = submatrix'(Tang, {0}, );

allOnes = matrix{toList(1..(3*g-3))} - matrix{toList(0..(3*g-4))};
MMVector = allOnes * Tang;


print("Generators of graph curve ideal:");
print(toString(GenList));
<< endl;

print("Tangent vectors:");
print(toString(Tang));
<< endl;

print("Sum of tangent vectors:");
print(toString((entries(MMVector))_0));

