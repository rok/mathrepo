restart
needsPackage "Divisor"
needsPackage "Polyhedra"

Ft = matrix{{-1,0},{-1,-6},{1,0},{1,2}}
(D,P,Q) = smithNormalForm(Ft)
k = rank target Ft
clX = ZZ^k/(image D)

J = {4}
compJ = apply(k,i->i+1)
for i from 0 to length J - 1 do compJ = delete(J_i,compJ)
iden = diagonalMatrix(apply(k,i->1))
f = map(clX,ZZ^k,iden)
PJ = submatrix(P,apply(length compJ, i-> compJ_i - 1))
M = gens ker (f*PJ)
(D2,P2,Q2) = smithNormalForm(M)
sJ = 1
for i from 0 to (rank source D2)-1 do sJ = sJ*D2_(i,i)

n= rank source Ft
Pdoubleprime = transpose submatrix(transpose P, {n..k-1})
Pdoubleprime = submatrix(Pdoubleprime,apply(length compJ, i->compJ_i-1))
Z = transpose matrix{apply(k-n+1,i->0^i)}
V = Z | (matrix{apply(k-length J,i->1)} || Pdoubleprime)
volOrbitPol = (k-n)! * (volume convexHull V)
(D3,P3,Q3) = smithNormalForm(Pdoubleprime)
latticeIndex = 1
for i from 0 to min(rank target D3,rank source D3)-1 do latticeIndex = latticeIndex*D3_(i,i)
orbitdegree = sJ*volOrbitPol/latticeIndex