{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "d551a9b5",
   "metadata": {},
   "source": [
    "## 5. Idempotents ##\n",
    "We want to compute the idempotent $E_{\\lambda} \\in \\mathbb{C}[\\mathfrak{S}_k]$ associated to the summand $W_{\\lambda}(V)$ in the decomposition\n",
    "$$\n",
    "V^{\\otimes k} = \\bigoplus_{\\lambda \\vdash k}{W_{\\lambda}(V)}.\n",
    "$$\n",
    "An explicit combinatorial formula for these idempotents was proven by Garsia and Reutenauer [GR89, Theorem 3.2]. It has been implemented in SageMath, see <a href=\"https://doc.sagemath.org/html/en/reference/algebras/sage/combinat/descent_algebra.html#sage.combinat.descent_algebra.DescentAlgebra.I\">here</a>:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "42701362",
   "metadata": {},
   "outputs": [],
   "source": [
    "def higher_lie_idempotent(la):\n",
    "    return DescentAlgebra(QQ, la.size()).I().idempotent(la).to_symmetric_group_algebra()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2fae54be",
   "metadata": {},
   "source": [
    "Here are the idempotents for the case $k=3$, see Example 5.1:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "cd4a5461",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[3]\n",
      "1/3*[1, 2, 3] - 1/6*[1, 3, 2] - 1/6*[2, 1, 3] - 1/6*[2, 3, 1] - 1/6*[3, 1, 2] + 1/3*[3, 2, 1]\n",
      "[2, 1]\n",
      "1/2*[1, 2, 3] - 1/2*[3, 2, 1]\n",
      "[1, 1, 1]\n",
      "1/6*[1, 2, 3] + 1/6*[1, 3, 2] + 1/6*[2, 1, 3] + 1/6*[2, 3, 1] + 1/6*[3, 1, 2] + 1/6*[3, 2, 1]\n"
     ]
    }
   ],
   "source": [
    "k=3\n",
    "for la in Partitions(k):\n",
    "    print(la)\n",
    "    print(higher_lie_idempotent(la))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5340b764",
   "metadata": {},
   "source": [
    "We now compare the Thrall decomposition with the decomposition of $V^{\\otimes k}$ into isotypic components.\n",
    "\n",
    "The function $\\texttt{common_refinement}$ computes the idempotents associated to the coarsest common refinement of the isotypic decomposition and the Thrall decomposition, i.e. to the modules $W_{\\lambda}(V)\\cap \\mathbb{S}_{\\mu}(V)^{\\oplus m_{\\mu}}$. See Example 5.1."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "abcd9f00",
   "metadata": {},
   "outputs": [],
   "source": [
    "def isotypic_idempotent(mu):\n",
    "    return SymmetricGroupAlgebra(QQ,mu.size()).central_orthogonal_idempotent(mu)\n",
    "def common_refinement(k,verbose=True):\n",
    "    ans={}\n",
    "    s=SymmetricFunctions(QQ).schur()\n",
    "    for la in Partitions(k):\n",
    "        thrallCoefficients=s.gessel_reutenauer(la).monomial_coefficients()     \n",
    "        E=higher_lie_idempotent(la)\n",
    "        if verbose:\n",
    "            print(\"The Thrall module \" + str(la)+\":\")\n",
    "        for mu in thrallCoefficients:\n",
    "            f=E*isotypic_idempotent(mu)\n",
    "            ans[(la,mu)]=f\n",
    "            if verbose:\n",
    "                print(\"Contains \" + str(thrallCoefficients[mu]) + \" copies of the Schur module \"+str(mu)+\". The idempotent is given by\")\n",
    "                print(f)\n",
    "        if verbose:\n",
    "            print()\n",
    "    return ans"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "9d722b7a",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The Thrall module [3]:\n",
      "Contains 1 copies of the Schur module [2, 1]. The idempotent is given by\n",
      "1/3*[1, 2, 3] - 1/6*[1, 3, 2] - 1/6*[2, 1, 3] - 1/6*[2, 3, 1] - 1/6*[3, 1, 2] + 1/3*[3, 2, 1]\n",
      "\n",
      "The Thrall module [2, 1]:\n",
      "Contains 1 copies of the Schur module [1, 1, 1]. The idempotent is given by\n",
      "1/6*[1, 2, 3] - 1/6*[1, 3, 2] - 1/6*[2, 1, 3] + 1/6*[2, 3, 1] + 1/6*[3, 1, 2] - 1/6*[3, 2, 1]\n",
      "Contains 1 copies of the Schur module [2, 1]. The idempotent is given by\n",
      "1/3*[1, 2, 3] + 1/6*[1, 3, 2] + 1/6*[2, 1, 3] - 1/6*[2, 3, 1] - 1/6*[3, 1, 2] - 1/3*[3, 2, 1]\n",
      "\n",
      "The Thrall module [1, 1, 1]:\n",
      "Contains 1 copies of the Schur module [3]. The idempotent is given by\n",
      "1/6*[1, 2, 3] + 1/6*[1, 3, 2] + 1/6*[2, 1, 3] + 1/6*[2, 3, 1] + 1/6*[3, 1, 2] + 1/6*[3, 2, 1]\n",
      "\n"
     ]
    }
   ],
   "source": [
    "common_refinement(3);"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "41f39d23",
   "metadata": {},
   "source": [
    "Finally, we compare the Thrall decomposition with the (noncanonical) decomposition of $V^{\\otimes k}$ given by the Young symmetrizers $c_\\tau \\in \\mathbb{C}[\\mathfrak{S}_k]$, and with the decomposition given by the \n",
    "<q>twisted Young symmetrizers</q> $\\tilde{c}_\\tau \\in \\mathbb{C}[\\mathfrak{S}_k]$ introduced in Example 5.1."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "7336b467",
   "metadata": {},
   "outputs": [],
   "source": [
    "from sage.combinat.symmetric_group_algebra import a,b, kappa\n",
    "from sage.combinat.tableau import from_shape_and_word\n",
    "def young_symmetrizer(T):\n",
    "    return a(T)*b(T)/kappa(T.shape())\n",
    "def twisted_young_symmetrizer(T):\n",
    "    return b(T)*a(T)/kappa(T.shape())\n",
    "def compare_with_Young_symmetrizers(k,verbose=False):\n",
    "    commonRefinement=common_refinement(k,verbose=verbose)\n",
    "    for (la,mu) in commonRefinement:\n",
    "        print(\"The Thrall module \" + str(la) + \" contains a copy of the Schur module \" + str(mu) +\",\")\n",
    "        E=commonRefinement[(la,mu)]\n",
    "        for p in Permutations(k):\n",
    "            found=False\n",
    "            T=from_shape_and_word(mu,p,convention=\"English\")\n",
    "            symmetrizer=young_symmetrizer(T)\n",
    "            if E*symmetrizer==symmetrizer:\n",
    "                print(\"given by the Young symmetrizer \" + str(T) + \".\")\n",
    "                found = True\n",
    "                break\n",
    "            twistedSymmetrizer=twisted_young_symmetrizer(T)\n",
    "            if E*twistedSymmetrizer==twistedSymmetrizer:\n",
    "                print(\"given by the twisted Young symmetrizer \" + str(T) + \".\")\n",
    "                found = True\n",
    "                break\n",
    "        if not found:\n",
    "            print(\"not given by any Young symmetrizer.\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "20021a17",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The Thrall module [3] contains a copy of the Schur module [2, 1],\n",
      "given by the Young symmetrizer [[1, 3], [2]].\n",
      "The Thrall module [2, 1] contains a copy of the Schur module [1, 1, 1],\n",
      "given by the Young symmetrizer [[1], [2], [3]].\n",
      "The Thrall module [2, 1] contains a copy of the Schur module [2, 1],\n",
      "given by the twisted Young symmetrizer [[1, 2], [3]].\n",
      "The Thrall module [1, 1, 1] contains a copy of the Schur module [3],\n",
      "given by the Young symmetrizer [[1, 2, 3]].\n"
     ]
    }
   ],
   "source": [
    "compare_with_Young_symmetrizers(3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "2ec8ea50",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The Thrall module [4] contains a copy of the Schur module [2, 1, 1],\n",
      "not given by any Young symmetrizer.\n",
      "The Thrall module [4] contains a copy of the Schur module [3, 1],\n",
      "not given by any Young symmetrizer.\n",
      "The Thrall module [3, 1] contains a copy of the Schur module [2, 1, 1],\n",
      "not given by any Young symmetrizer.\n",
      "The Thrall module [3, 1] contains a copy of the Schur module [3, 1],\n",
      "not given by any Young symmetrizer.\n",
      "The Thrall module [3, 1] contains a copy of the Schur module [2, 2],\n",
      "given by the Young symmetrizer [[1, 3], [2, 4]].\n",
      "The Thrall module [2, 2] contains a copy of the Schur module [1, 1, 1, 1],\n",
      "given by the Young symmetrizer [[1], [2], [3], [4]].\n",
      "The Thrall module [2, 2] contains a copy of the Schur module [2, 2],\n",
      "given by the twisted Young symmetrizer [[1, 3], [2, 4]].\n",
      "The Thrall module [2, 1, 1] contains a copy of the Schur module [2, 1, 1],\n",
      "not given by any Young symmetrizer.\n",
      "The Thrall module [2, 1, 1] contains a copy of the Schur module [3, 1],\n",
      "given by the twisted Young symmetrizer [[1, 2, 3], [4]].\n",
      "The Thrall module [1, 1, 1, 1] contains a copy of the Schur module [4],\n",
      "given by the Young symmetrizer [[1, 2, 3, 4]].\n"
     ]
    }
   ],
   "source": [
    "compare_with_Young_symmetrizers(4)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4374d0fd",
   "metadata": {},
   "source": [
    "<h2>Bibliography</h2>\n",
    "<ul>\n",
    "<li> [GR89] A. M. Garsia C. Reutenauer. <cite>A decomposition of Solomon’s descent algebra</cite>, Adv.\n",
    "Math., 77(2):189–262, 1989. </li>\n",
    "</ul>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "SageMath 9.5",
   "language": "sage",
   "name": "sagemath"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
