=============================================
Decomposing Tensor Spaces via Path Signatures
=============================================

| This notebook contains code accompanying the paper:
| Carlos Améndola, Francesco Galuppi, Ángel David Ríos Ortiz, Pierpaola Santarsiero, Tim Seynnaeve. Decomposing tensor spaces via path signatures
| In: Journal of pure and applied algebra, 229 (2025) 1, 107807
| DOI: `10.1016/j.jpaa.2024.107807 <https://dx.doi.org/10.1016/j.jpaa.2024.107807>`_ ARXIV: https://arxiv.org/abs/2308.11571 CODE: https://mathrepo.mis.mpg.de/TensorSpacesViaSignatures/

ABSTRACT: The signature of a path is a sequence of tensors whose entries are iterated integrals, playing a key role in stochastic analysis and applications. The set of all signature tensors at a particular level gives rise to the universal signature variety. We show that the parametrization of this variety induces a natural decomposition of tensor spaces via representation theory, and connect this to the study of path invariants. We also examine the question of determining what is the tensor rank of a signature tensor.

.. toctree::
	:maxdepth: 1
        :glob:
        	
        section_4
        section_5
        section_6
        section_7.rst
  
The first three pages are based on Jupyter notebooks in SageMath which can be downloaded here:

* :download:`section_4.ipynb <section_4.ipynb>`

* :download:`section_5.ipynb <section_5.ipynb>`

* :download:`section_6.ipynb <section_6.ipynb>`

------------------------------------------------------------------------------------------------------------------------------

Project page created: 22/08/2023

Project contributors: Carlos Améndola, Francesco Galuppi, Ángel David Ríos Ortiz, Pierpaola Santarsiero, Tim Seynnaeve

Corresponding author of this page: Tim Seynnaeve, tim.seynnaeve@kuleuven.be

Software used: SageMath (Version 9.5), Macaulay2 (Version 1.21)

License for code of this project page: MIT License (https://spdx.org/licenses/MIT.html)

License for all other content of this project page: CC BY 4.0 (https://creativecommons.org/licenses/by/4.0/)


Last updated 23/08/2023.
