##################################
Some examples
##################################

This site contains some illustrative examples. We first present *Macaulay2* sessions reproducing the most relevant computations in the paper. Next we provide the code for several examples, which should be executed after loading the functions from :download:`noetherianOperatorsCode.m2 <noetherianOperatorsCode.m2>`.

We present the running example given in the Introduction.

.. code-block:: macaulay2

		load "noetherianOperatorsCode.m2"
		R=QQ[x_1,x_2,x_3,x_4];
		Q = ideal(3*x_1^2*x_2^2-x_2^3*x_3-x_1^3*x_4-3*x_1*x_2*x_3*x_4+2*x_3^2*x_4^2,3*x_1^3*x_2*x_4
		-3*x_1*x_2^2*x_3*x_4-3*x_1^2*x_3*x_4^2+3*x_2*x_3^2*x_4^2+2*x_2^3-2*x_3*x_4^2,3*x_2^4*x_3-6*
		x_1*x_2^2*x_3*x_4+3*x_1^2*x_3*x_4^2+x_2^3-x_3*x_4^2,4*x_1*x_2^3*x_3+x_1^4*x_4-6*x_1^2*x_2*x
		_3*x_4-3*x_2^2*x_3^2*x_4+4*x_1*x_3^2*x_4^2,x_2^5-x_1*x_2^3*x_4-x_2^2*x_3*x_4^2+x_1*x_3*x_4^
		3,x_1*x_2^4-x_2^3*x_3*x_4-x_1*x_2*x_3*x_4^2+x_3^2*x_4^3,x_1^4*x_2-x_2^3*x_3^2-2*x_1^3*x_3*x
		_4+2*x_1*x_2*x_3^2*x_4,x_1^5-4*x_1^3*x_2*x_3+3*x_1*x_2^2*x_3^2+2*x_1^2*x_3^2*x_4-2*x_2*x_3^
		3*x_4,3*x_1^4*x_3*x_4-6*x_1^2*x_2*x_3^2*x_4+3*x_2^2*x_3^3*x_4+2*x_1^3*x_2+6*x_1*x_2^2*x_3-6
		*x_1^2*x_3*x_4-2*x_2*x_3^2*x_4,4*x_2^3*x_3^3+4*x_1^3*x_3^2*x_4-12*x_1*x_2*x_3^3*x_4+4*x_3^4
		*x_4^2-x_1^4+6*x_1^2*x_2*x_3+3*x_2^2*x_3^2-8*x_1*x_3^2*x_4);
		L = getNoetherianOperatorsHilb(Q)
		Q' = getIdealFromNoetherianOperators(L, radical Q);
		Q == Q'


.. image:: Ex_Intro.png

Below we have the Example 7.8 regarding the join construction. This example has the interesting particularity that we can describe the same primary ideal with two different sets of Noetherian operators.

.. code-block:: macaulay2

		R=QQ[x_1,x_2,x_3,x_4];
		MM = matrix {{x_3,x_1,x_2},{x_1,x_2,x_4}};
		P = minors(2,MM);
		M=ideal{x_1^2,x_2^2,x_3^2,x_4^2};
		Q=joinIdeals(P,M);
		
		-- compute the Noetherian operators of Q
		L1 = time getNoetherianOperatorsHilb(Q)
		Q1 = getIdealFromNoetherianOperators(L1, radical Q); 
		Q == Q1

		-- compute the Noetherian operators of M 
		L2 = time getNoetherianOperatorsHilb(M) 
		Q2 = getIdealFromNoetherianOperators(L2, radical Q); 
		Q == Q2


.. image:: Ex_Join.png


Computations in Example 8.5 are described below.

.. code-block:: macaulay2

		R = QQ[x1,x2,x3,x4];
		Q = ideal(x1^2,x1*x2,x1*x3,x1*x4-x3^2+x1,x3^2*x4-x2^2,x3^2*x4-x3^2-x2*x3+2*x1);
		L = getNoetherianOperatorsHilb(Q)
		Q2 = getIdealFromNoetherianOperators(L, radical Q);
		Q == Q2 


.. image:: Ex_8.5.png


Next we show computations in Example 8.6 (continuation of Example 3.8) for k=3.

.. code-block:: macaulay2

		R = QQ[x1,x2,x3,x4];
		k=3;
		J = ideal((x1^2-x2*x3)^k,(x1*x2-x3*x4)^k,(x2^2-x1*x4)^k);
		Q = saturate(J,ideal(x1*x2*x3*x4))		
		L = getNoetherianOperatorsHilb(Q)


.. image:: Ex_8.6.png


		
Finally we give the complete list of examples you can also download in :download:`Macaulay2 Examples <noetherianOperatorsExamples.m2>`.


.. code-block:: macaulay2


		----------------------------------------------------
		-- SEVERAL EXAMPLES -----------------------------------
		----------------------------------------------------



		----------------------------------------------------
		----------------------------------------------------
		-- Example 1: Running example throughout the paper
		-- We compute the ideal as explained in the introduction.
		R=QQ[x_1,x_2,x_3,x_4]
		Q = ideal(3*x_1^2*x_2^2-x_2^3*x_3-x_1^3*x_4-3*x_1*x_2*x_3*x_4+2*x_3^2*x_4^2,3*x_1^3*x_2*x_4-3*x
		_1*x_2^2*x_3*x_4-3*x_1^2*x_3*x_4^2+3*x_2*x_3^2*x_4^2+2*x_2^3-2*x_3*x_4^2,3*x_2^4*x_3-6*x_1*
		x_2^2*x_3*x_4+3*x_1^2*x_3*x_4^2+x_2^3-x_3*x_4^2,4*x_1*x_2^3*x_3+x_1^4*x_4-6*x_1^2*x_2*x_3*x
		_4-3*x_2^2*x_3^2*x_4+4*x_1*x_3^2*x_4^2,x_2^5-x_1*x_2^3*x_4-x_2^2*x_3*x_4^2+x_1*x_3*x_4^3,x_
		1*x_2^4-x_2^3*x_3*x_4-x_1*x_2*x_3*x_4^2+x_3^2*x_4^3,x_1^4*x_2-x_2^3*x_3^2-2*x_1^3*x_3*x_4+2
		*x_1*x_2*x_3^2*x_4,x_1^5-4*x_1^3*x_2*x_3+3*x_1*x_2^2*x_3^2+2*x_1^2*x_3^2*x_4-2*x_2*x_3^3*x_
		4,3*x_1^4*x_3*x_4-6*x_1^2*x_2*x_3^2*x_4+3*x_2^2*x_3^3*x_4+2*x_1^3*x_2+6*x_1*x_2^2*x_3-6*x_1
		^2*x_3*x_4-2*x_2*x_3^2*x_4,4*x_2^3*x_3^3+4*x_1^3*x_3^2*x_4-12*x_1*x_2*x_3^3*x_4+4*x_3^4*x_4
		^2-x_1^4+6*x_1^2*x_2*x_3+3*x_2^2*x_3^2-8*x_1*x_3^2*x_4)
		L = time getNoetherianOperatorsHilb(Q)
		Q' = getIdealFromNoetherianOperators(L, radical Q)
		Q == Q'


		diffVars = apply(gens R, i -> value("symbol d" | toString(i)) )
		D = R(monoid[diffVars])
		L = {dx_3^5}
		Q2 = getIdealFromNoetherianOperators(L, radical Q)
		L3 = getNoetherianOperatorsHilb Q2
		Q3 = getIdealFromNoetherianOperators(L3, radical Q)
		Q3 == Q2
		----------------------------------------------------
		----------------------------------------------------

		----------------------------------------------------
		----------------------------------------------------
		-- Example 2 : This the Example 7.8 regarding the join construction.
		-- This example has the interesting property that we 
		--   describe the same primary ideal with two different sets of Noetherian operators
		R=QQ[x_1,x_2,x_3,x_4]
		MM = matrix {{x_3,x_1,x_2},{x_1,x_2,x_4}}
		P = minors(2,MM)
		M=ideal{x_1^2,x_2^2,x_3^2,x_4^2}

		Q=joinIdeals(P,M)
		L1 = time getNoetherianOperatorsHilb(Q) -- compute the Noetherian operators of Q
		Q1 = getIdealFromNoetherianOperators(L1, radical Q) 
		Q == Q1

		L2 = time getNoetherianOperatorsHilb(M) -- compute the Noetherian operators of M (to apply later Theorem 7.7)
		Q2 = getIdealFromNoetherianOperators(L2, radical Q) 
		Q == Q2
		----------------------------------------------------
		----------------------------------------------------

		----------------------------------------------------
		----------------------------------------------------
		-- Example 3: Palamodov's example
		---------------------------------------------------
		R = QQ[x_1, x_2, x_3]
		Q = ideal(x_1^2, x_2^2, x_1-x_2*x_3)
		L = time getNoetherianOperatorsHilb(Q)
		Q' = getIdealFromNoetherianOperators(L, radical Q)
		Q == Q'
		----------------------------------------------------
		----------------------------------------------------

		----------------------------------------------------
		----------------------------------------------------
		-- Example 4: taken from page 143 of "Solving Systems of Polynomial Equations"
		---------------------------------------------------
		R = QQ[x_1, x_2, x_3, x_4]
		Q = ideal(x_1^3*x_4^2-x_2^5, x_1^2*x_4^3-x_3^5, x_1*x_3^2-x_2^3, x_2^2*x_4 - x_3^3)
		Q1 = ideal(x_1*x_4-x_2*x_3, x_1*x_3^2-x_2^3, x_2^2*x_4-x_3^3)
		Q2 = ideal(x_1^2, x_2^2, x_3^2)
		Q3 = ideal(x_2^2, x_3^2, x_4^2)
		Q4 = ideal(x_1^3, x_2^3, x_3^3, x_4^3, x_1*x_3^2, x_2^2*x_4)
		assert(Q == intersect(Q1, Q2, Q3, Q4)) 
		---- the Noetherian operators of Q1
		isPrime Q1
		-- since it is prime we can choose 1 as the Noetherian operator
		L = time getNoetherianOperatorsHilb(Q1)
		Q1' = getIdealFromNoetherianOperators(L, radical Q1)
		Q1 == Q1'
		---- the Noetherian operators of Q2 
		isPrime Q2
		P2 = radical Q2 -- it is equal to (x_1, x_2, x_3)
		L = time getNoetherianOperatorsHilb(Q2)
		Q2' = getIdealFromNoetherianOperators(L, radical Q2)
		Q2 == Q2'
		---- the Noetherian operators of Q3
		isPrime Q3
		P3 = radical Q3 -- it is equal to (x2, x3, x4)
		L = time getNoetherianOperatorsHilb(Q3)
		Q3' = getIdealFromNoetherianOperators(L, radical Q3)
		Q3 == Q3'
		---- the Noetherian operators of Q4
		isPrime Q4
		P4 = radical Q4 -- it is equal to (x1, x2, x3, x4)
		L = time getNoetherianOperatorsHilb(Q4)
		Q4' = getIdealFromNoetherianOperators(L, radical Q4)
		Q4 == Q4'
		----------------------------------------------------
		----------------------------------------------------

		----------------------------------------------------
		----------------------------------------------------
		-- Example 5: some random primary ideal
		---------------------------------------------------
		R = QQ[x_1,x_2,x_3]
		Q = ideal(random(3, R), random(2, R), random(2, R), random(4, R))
		assert(dim Q == 0)
		L = time getNoetherianOperatorsHilb(Q)
		Q' = getIdealFromNoetherianOperators(L, radical Q)
		Q == Q'
		----------------------------------------------------
		----------------------------------------------------

		----------------------------------------------------
		----------------------------------------------------
		-- Example 6 : a small example 
		---------------------------------------------------
		R = QQ[x_1,x_2,x_3]
		Q = ideal(x_1^2, x_2^2, x_3^2, x_1*x_2 + x_1*x_3 +x_2*x_3)
		L = time getNoetherianOperatorsHilb(Q)
		Q' = getIdealFromNoetherianOperators(L, radical Q)
		Q == Q'
		----------------------------------------------------
		----------------------------------------------------

		----------------------------------------------------
		----------------------------------------------------
		-- Example 7:
		---------------------------------------------------
		R = QQ[x_1,x_2,x_3,x_4]
		J = ideal(x_1^4 + x_2*x_3*x_4, x_2^4 + x_1*x_3*x_4, x_3^4 + x_1*x_2*x_4)
		dim J
		primDec = primaryDecomposition J
		-- here we will only take care of the first primary component...
		Q = primDec_0
		L = time getNoetherianOperatorsHilb(Q)
		Q' = getIdealFromNoetherianOperators(L, radical Q)
		Q == Q'
		----------------------------------------------------
		----------------------------------------------------

		----------------------------------------------------
		----------------------------------------------------
		-- Example 8: powers of the maximal irrelevant ideal 
		---------------------------------------------------
		R = QQ[x_1,x_2,x_3]
		mm= ideal vars R
		n=4
		Q=mm^n
		L = time getNoetherianOperatorsHilb(Q)
		Q' = getIdealFromNoetherianOperators(L, radical Q)
		Q == Q'
		----------------------------------------------------
		----------------------------------------------------

		----------------------------------------------------
		----------------------------------------------------
		-- Example 9:
		---------------------------------------------------
		R = QQ[x_1,x_2,x_3]
		Q = ideal(x_1^2,x_2^2,x_3^2)
		L = time getNoetherianOperatorsHilb(Q)
		Q' = getIdealFromNoetherianOperators(L, radical Q)
		Q == Q'
		----------------------------------------------------
		----------------------------------------------------

		----------------------------------------------------
		----------------------------------------------------
		-- Example 10:
		---------------------------------------------------
		R = QQ[x_1,x_2,x_3]
		P = ideal(x_1,x_2,x_3)
		diffVars = apply(gens R, i -> value("symbol d" | toString(i)) )
		D = R(monoid[diffVars])
		L = {dx_1, dx_2, dx_3^5}
		Q = getIdealFromNoetherianOperators(L, P)
		time getNoetherianOperatorsHilb(Q)
		isPrimary Q
		----------------------------------------------------
		----------------------------------------------------



		----------------------------------------------------
		-- Example 11:
		---------------------------------------------------
		R = QQ[x_1..x_9]
		MM = genericMatrix(R, 3, 3)
		P = minors(2, MM)
		M = ideal(x_1^2, x_5^2, x_9^2, x_2, x_3, x_4, x_6, x_7, x_8)
		codim M
		degree M
		Q = joinIdeals(P, M)
		betti mingens Q
		degree Q
		codim Q
		L = time getNoetherianOperatorsHilb(Q) -- compute the Noetherian operators of Q
		Q' = getIdealFromNoetherianOperators(L, radical Q) 
		Q == Q'
		betti mingens Q'
		degree Q'
		codim Q'
		----------------------------------------------------
		----------------------------------------------------


		----------------------------------------------------
		-- Example 12:
		---------------------------------------------------
		R = QQ[x_1..x_9]
		MM = genericMatrix(R, 3, 3)
		P = minors(2, MM)
		independentSets P
		M = ideal(x_1^2, x_2^2, x_3^2, x_4, x_5, x_6, x_7, x_8, x_9)
		Q = joinIdeals(P, M)
		betti mingens Q
		codim Q
		degree Q
		L = time getNoetherianOperatorsHilb(Q) -- compute the Noetherian operators of Q
		Q' = getIdealFromNoetherianOperators(L, radical Q) 
		Q==Q'
		betti Q'
		degree Q'
		codim Q'

		L2 = time getNoetherianOperatorsHilb(M)
		Q2= getIdealFromNoetherianOperators(L2, radical Q) 
		Q2==Q
		----------------------------------------------------
		----------------------------------------------------

		-------------------------------------------------------------------------------------------
		-- Example 13: Example 3.1. Not every set of Noetherian operators defines a P-primary ideal
		-------------------------------------------------------------------------------------------
		restart
		R=QQ[x_1,x_2]
		D = R[dx_1,dx_2]
		L1={1_D,dx_1}
		P=ideal{x_1,x_2}
		Q1 = getIdealFromNoetherianOperators(L1,P) 

		L2={dx_1,dx_2}
		Q2 = getIdealFromNoetherianOperators(L2,P) 

		restart
		R=QQ[x_1,x_2]
		Q=ideal{x_1^2,x_2}
		L=getNoetherianOperatorsHilb(Q)
		Q'=getIdealFromNoetherianOperators(L,P)
		Q==Q'



		-------------------------------------------------------------------------------------------
		-- Example 14: Example 8.5
		-------------------------------------------------------------------------------------------
		R = QQ[x1,x2,x3,x4];
		Q = ideal(x1^2,x1*x2,x1*x3,x1*x4-x3^2+x1,x3^2*x4-x2^2,x3^2*x4-x3^2-x2*x3+2*x1)
		L = time getNoetherianOperatorsHilb(Q)
		Q2 = time getIdealFromNoetherianOperators(L, radical Q)
		Q == Q2 -- check that both ideals are equal
		----------------------------------------------------
		----------------------------------------------------



		-------------------------------------------------------------------------------------------
		-- Example 15: Running times of the Example 8.6
		-------------------------------------------------------------------------------------------

		k = 12

		R = QQ[x1,x2,x3,x4];

		-- This for-loop prints the table of the example
		for k from 1 to 12 do (
		J = ideal((x1^2-x2*x3)^k,(x1*x2-x3*x4)^k,(x2^2-x1*x4)^k);
		Q = saturate(J,ideal(x1*x2*x3*x4));
		s = cpuTime();
		L = getNoetherianOperatorsHilb(Q);
		t = cpuTime();
		<< k << "  &  " << (numcols mingens Q) << "  &  " <<  (degree Q)/3 << "  &  " << toString(t-s) << "  \\\\"  << endl;
		<< "\\hline" << endl;
		assert( (degree Q)/3 == length L);
		)


		J = ideal((x1^2-x2*x3)^k,(x1*x2-x3*x4)^k,(x2^2-x1*x4)^k);
		Q = saturate(J,ideal(x1*x2*x3*x4));

		k,(degree Q)/3, numcols mingens Q
		s = cpuTime();
		L = time getNoetherianOperatorsHilb(Q);
		t = cpuTime();
		t-s
		Q2 = time getIdealFromNoetherianOperators(L, radical Q)
		Q == Q2
		(degree I) / 3 
		----------------------------------------------------
		----------------------------------------------------

