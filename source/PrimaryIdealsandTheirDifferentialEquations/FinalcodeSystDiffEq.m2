restart

-- commputes the coefficient given as multi-factorial number of the exponents
getCoef = f -> 
(
    L = flatten exponents f;
    c := 1;
    for n in L do c = c * (n!);
    1 / c
)

--- Computes the join of two ideals
joinIdeals = (J, K) -> 
(
    v := symbol v; 
    w := symbol w;
    R := ring J;
    n := numgens R;
    T := (coefficientRing R)[v_1..v_n, w_1..w_n];
    Q := ((map(T, R, toList(v_1..v_n))) J) + ((map(T, R, toList(w_1..w_n))) K);
    S := T / Q;
    F := map(S, R, apply(n, j -> v_(j+1) + w_(j+1)));
    ker F     
) 



--- This function returns the ring we shall use to parametrize the punctual Hilbert scheme
--- We will need the polynomial ring S=(frac(R/P))[y_1..y_c] to specify where do we want 
--- the "points" of the punctual Hilbert scheme.
getHilb = P -> 
(
    R := ring P;
    c := codim P;
    assert(isPrime(P));	   -- check P is prime
    y := symbol y;
    S := (frac(R/P))[y_1..y_c];
    S
)


-- This map receives an ideal Q in R=QQ[x_1..x_n] primary to a maximal ideal P
-- and it returns an ideal I in S=(frac(R/P))[y_1..y_c] which is primary with respect to (y_1..y_c).
-- According to Theorem 4.4 this is the corresponding point in punctual Hilbert scheme.
-- Input: Q a primary ideal
-- Input: S=(frac(R/P))[y_1..y_c] the chosen polynomial ring to represent the punctual Hilbert scheme.
-- Output: returns the corresponding point in the punctual Hilbert scheme.
mapRtoHilb = (Q, S) ->
(
    R := ring Q;
    n := numgens R;
    c := codim Q;
    assQ := ass Q;
    assert(length(assQ) == 1); 	  -- check primary ideal
    P := radical Q;
    assert(assQ_0 == P);      	  -- check I is P-primary     
        
    m := 0; -- compute the exponent that determines the order of the diff ops
    while (Q : P^m) != ideal(1_R) do m = m + 1;   
    
    -- map from R into the "base changed" module of principal parts
    diag := ideal apply(c, j -> S_j);
    L := join(apply(c, j -> S_j + R_j), apply(c..(n-1), j -> R_j));
    mapRtoS := map(S, R, L);
    ideal mingens ((mapRtoS Q) + diag^m)
)
 
 
 
-- This function returns a set of Noetherian operators given the ideal I in the punctual Hilbert scheme
-- that parametrizes the primary ideal Q.
-- Input: An ideal I in the the punctual Hilbert scheme (S=(frac(R/P))[y1..yc]).
-- Output: A set of Noetherian operators for the primary ideal Q.    
getNoetherianOpsfromHilb = I -> 
(
    S := ring I;
    R := ambient ring numerator 1_(coefficientRing S);
    assert(dim I == 0); -- we should have dim I = 0
    mm := ideal vars S; -- maximal irrelevant ideal of R
    assert((mm : I) == ideal(1_S)); -- we should have that I is mm-primary
    
    m := 0; -- compute the exponent that determines the order of the diff ops
    while (I : mm^m) != ideal(1_S) do m = m + 1;   

    allMons := {}; 
    for i from 0 to m-1 do allMons = join(allMons, flatten entries basis(i, S));
    
    X := S/I;
    basisX := flatten entries basis X; -- a basis of the finite dim vector space X
    LinX := apply(allMons, v -> sub(v, X)); -- substitute elements in X
    
    FF := coefficientRing S;
    W := FF[dx_1..dx_(numgens S)];
    mapTtoW := map(W, S, {dx_1..dx_(numgens S)});
   
    -- the dual of each vector in the basis of X can be lifted as a diff op
    noethOps:= {};  
    for f in basisX do (	
	valFunct := apply(LinX, v -> sub(v // f, FF)); -- obtain the value of the "functional"
	op := 0_W;
	for i from 0 to (length allMons)-1 do (
	    c := getCoef(allMons_i); 
	    op = op + c * valFunct_i * mapTtoW(allMons_i);
	);     
	noethOps = append(noethOps, op);
     );
    
    -- the Noetherian operators
    noethOps
)


-- This function can compute the Noetherian operators of a primary ideal Q.
-- We always assume that the ideal Q is given in a polynomial ring R with variables x_1..x_n for some positive integer n.
-- This function can be used after determining a Noetherian normalization.
-- We assume that k[x_(c+1)..x_n] \hookrightarrow R/P is an integral extension.  
-- Input: Q is primary ideal that is Noether Normal Position with respect to x_1..x_c where c = codim(Q)
-- Output: A set of Noetherian operators.
getNoetherianAfterNoetherNormalization = Q -> 
(
    assert(isPrimary Q);
    P := radical Q;
    S := getHilb P;
    I := mapRtoHilb(Q, S);
    noethOps := getNoetherianOpsfromHilb(I);
    noethOps
)    

 
 
 
 
 
 
 
 ----------------------------------------------------
-- SOME EXAMPLES -----------------------------------
----------------------------------------------------
-- In these examples we make the Noether normalization by hand


----------------------------------------------------
----------------------------------------------------
-- Example 0: Running example throughout the paper
-- We compute the ideal as explained in the introduction.
U= QQ[x_1,x_2,x_3,x_4,u_1,u_2,u_3,u_4,y_1,y_2];
A = matrix {{u_3,u_1,u_2},{u_1,u_2,u_4}};
PP = minors(2,A);
JJ=ideal{PP,x_1-u_1-y_1,x_2-u_2-y_2,x_3-u_3,x_4-u_4,y_1^3,y_2+x_2*y_1^2};
J=ideal{eliminate(JJ,{u_1,u_2,u_3,u_4,y_1,y_2})};

R=QQ[x_1,x_2,x_3,x_4];
F=map(R,U);
Q=F(J);

assert( Q == ideal(3*x_1^2*x_2^2-x_2^3*x_3-x_1^3*x_4-3*x_1*x_2*x_3*x_4+2*x_3^2*x_4^2,3*x_1^3*x_2*x_4-3*x
      _1*x_2^2*x_3*x_4-3*x_1^2*x_3*x_4^2+3*x_2*x_3^2*x_4^2+2*x_2^3-2*x_3*x_4^2,3*x_2^4*x_3-6*x_1*
      x_2^2*x_3*x_4+3*x_1^2*x_3*x_4^2+x_2^3-x_3*x_4^2,4*x_1*x_2^3*x_3+x_1^4*x_4-6*x_1^2*x_2*x_3*x
      _4-3*x_2^2*x_3^2*x_4+4*x_1*x_3^2*x_4^2,x_2^5-x_1*x_2^3*x_4-x_2^2*x_3*x_4^2+x_1*x_3*x_4^3,x_
      1*x_2^4-x_2^3*x_3*x_4-x_1*x_2*x_3*x_4^2+x_3^2*x_4^3,x_1^4*x_2-x_2^3*x_3^2-2*x_1^3*x_3*x_4+2
      *x_1*x_2*x_3^2*x_4,x_1^5-4*x_1^3*x_2*x_3+3*x_1*x_2^2*x_3^2+2*x_1^2*x_3^2*x_4-2*x_2*x_3^3*x_
      4,3*x_1^4*x_3*x_4-6*x_1^2*x_2*x_3^2*x_4+3*x_2^2*x_3^3*x_4+2*x_1^3*x_2+6*x_1*x_2^2*x_3-6*x_1
      ^2*x_3*x_4-2*x_2*x_3^2*x_4,4*x_2^3*x_3^3+4*x_1^3*x_3^2*x_4-12*x_1*x_2*x_3^3*x_4+4*x_3^4*x_4
      ^2-x_1^4+6*x_1^2*x_2*x_3+3*x_2^2*x_3^2-8*x_1*x_3^2*x_4) );

getNoetherianAfterNoetherNormalization(Q)
----------------------------------------------------
----------------------------------------------------


----------------------------------------------------
----------------------------------------------------
-- Example 1 : Contains the computations in Example 3.10
R=QQ[x_1,x_2,x_3,x_4];
Q=ideal{x_1^2,x_1*x_2,x_1*x_3,x_1*x_4-x_3^2+x_1,x_3^2*x_4-x_2^2,x_3^2*x_4-x_3^2-x_2*x_3+2*x_1};
getNoetherianAfterNoetherNormalization(Q)
----------------------------------------------------
----------------------------------------------------


----------------------------------------------------
----------------------------------------------------
-- Example 2 : This the Example 7.8 regarding the join construction.
R=QQ[x_1,x_2,x_3,x_4];
MM = matrix {{x_3,x_1,x_2},{x_1,x_2,x_4}};
P = minors(2,MM);
M=ideal{x_1^2,x_2^2,x_3^2,x_4^2};
Q=joinIdeals(P,M)

getNoetherianAfterNoetherNormalization(Q)
----------------------------------------------------
----------------------------------------------------


----------------------------------------------------
----------------------------------------------------
-- Example 3: Palamodov's example
---------------------------------------------------
R = QQ[x_1, x_2, x_3]
Q = ideal(x_1^2, x_2^2, x_1-x_2*x_3)
getNoetherianAfterNoetherNormalization Q
----------------------------------------------------
----------------------------------------------------



----------------------------------------------------
----------------------------------------------------
-- Example 4: taken from page 143 of "Solving Systems of Polynomial Equations"
---------------------------------------------------
R = QQ[x_1, x_2, x_3, x_4]
Q = ideal(x_1^3*x_4^2-x_2^5, x_1^2*x_4^3-x_3^5, x_1*x_3^2-x_2^3, x_2^2*x_4 - x_3^3)
Q1 = ideal(x_1*x_4-x_2*x_3, x_1*x_3^2-x_2^3, x_2^2*x_4-x_3^3)
Q2 = ideal(x_1^2, x_2^2, x_3^2)
Q3 = ideal(x_2^2, x_3^2, x_4^2)
Q4 = ideal(x_1^3, x_2^3, x_3^3, x_4^3, x_1*x_3^2, x_2^2*x_4)
assert(Q == intersect(Q1, Q2, Q3, Q4)) -- check that we copied correctly

---- the Noetherian operators of Q1
isPrime Q1
-- since it is prime we can choose 1 as the Noetherian operator

---- the Noetherian operators of Q2 
isPrime Q2
P2 = radical Q2 -- it is equal to (x_1, x_2, x_3)
getNoetherianAfterNoetherNormalization Q2


---- the Noetherian operators of Q3
isPrime Q3
P3 = radical Q3 -- it is equal to (x2, x3, x4)
F = map(R, R, {x_4, x_1, x_2, x_3}) -- change of variables to get Noether normal position
Q3' = F Q3
getNoetherianAfterNoetherNormalization Q3'


---- the Noetherian operators of Q4
isPrime Q4
P4 = radical Q4 -- it is equal to (x1, x2, x3, x4)
                -- no need to "take-out" anyone
getNoetherianAfterNoetherNormalization Q4
----------------------------------------------------
----------------------------------------------------




----------------------------------------------------
----------------------------------------------------
-- Example 5: some random primary ideal
---------------------------------------------------
R = QQ[x_1,x_2,x_3]
Q = ideal(random(3, R), random(2, R), random(2, R), random(4, R))
assert(dim Q == 0)
getNoetherianAfterNoetherNormalization Q
----------------------------------------------------
----------------------------------------------------



----------------------------------------------------
----------------------------------------------------
-- Example 6 : a small example 
---------------------------------------------------
R = QQ[x_1,x_2,x_3]
Q = ideal(x_1^2, x_2^2, x_3^2, x_1*x_2 + x_1*x_3 +x_2*x_3)
getNoetherianAfterNoetherNormalization Q
----------------------------------------------------
----------------------------------------------------




----------------------------------------------------
----------------------------------------------------
-- Example 7:
---------------------------------------------------
R = QQ[x_1,x_2,x_3,x_4]
J = ideal(x_1^4 + x_2*x_3*x_4, x_2^4 + x_1*x_3*x_4, x_3^4 + x_1*x_2*x_4)
dim J
primDec = primaryDecomposition J
-- here we will only take care of the first primary component...

Q = primDec_0
getNoetherianAfterNoetherNormalization(Q)
----------------------------------------------------
----------------------------------------------------




----------------------------------------------------
----------------------------------------------------
-- Example 8: powers of the maximal irrelevant ideal 
---------------------------------------------------
R = QQ[x_1,x_2,x_3]
mm= ideal vars R
n=4
Q=mm^n
getNoetherianAfterNoetherNormalization(Q)
----------------------------------------------------
----------------------------------------------------



----------------------------------------------------
----------------------------------------------------
-- Example 9:
---------------------------------------------------
R = QQ[x_1,x_2,x_3]
Q = ideal(x_1^2,x_2^2,x_3^2)

getNoetherianAfterNoetherNormalization(Q)
----------------------------------------------------
----------------------------------------------------



