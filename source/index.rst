..  image:: Banner_mathrepo1.png

.. raw:: html

  <style>
    h1 {
      display: none;
    }
  </style>



##########################
Mathematical Research Data
##########################

.. toctree::
   :maxdepth: 1
   :hidden:

   termsofuse.rst
   years.rst
   software.rst
   authors.rst
   events.rst
   AZ.rst
   
This is a repository of the `Max Planck Institute for Mathematics in the Sciences <https://www.mis.mpg.de>`_ in Leipzig,
dedicated to mathematical research data. `Research data
<https://www.forschungsdaten.info/themen/informieren-und-planen/was-sind-forschungsdaten/>`_ are all digital objects
that arise during the process of doing research or are a result thereof. In particular, the purpose of this repository
is to collect scripts and code, to explain applications of mathematical software, to showcase additional examples to
paper publications, and more generally to host supplementary material developed for research projects or discussed in
workshops.

This website is currently maintained by Tabea Bacher, Alessio Borzì, Ben Hollering, and Maximilian Wiesmann. It was set up and curated by Carlos Améndola, Claudia Fevola, Christiane Görgen, Leonie Kayser, Lukas Kühne, Verena Morys, Yue Ren and Mahsa Sayyary Namin from 2017 on. You can contact us at `mathrepo@mis.mpg.de <mathrepo@mis.mpg.de>`_.

MathRepo is in the process of transformation. We aim to restructure the repository so that its content meets the `FAIR
Principles <https://www.nature.com/articles/sdata201618>`_ for sustainable research. In the future, MathRepo will follow
the guidelines developed by the Mathematical Research Data Initiative `MaRDI <https://www.mardi4nfdi.de/>`_.

=================
How to contribute
=================
MathRepo is an institutional repository and publications are only possible for scientists of the MPI MiS and their collaborators.
The current standards and requirements for contribution are outlined in the `Terms of Use <https://mathrepo.mis.mpg.de/termsofuse.html>`_ page.
If you fulfill the aforementioned requirements, please ask `edv@mis.mpg.de <edv@mis.mpg.de>`_  for a login invitation and get started by following the instructions in the `Readme <https://gitlab.mis.mpg.de/rok/mathrepo/-/blob/develop/README.md>`_ on the `MPI MiS GitLab <https://gitlab.mis.mpg.de/rok/mathrepo/>`_ page. 
Finally, on the GitLab page, please press the button "Request access" to gain access as a developer or email us at `mathrepo@mis.mpg.de <mathrepo@mis.mpg.de>`_ to ask for developer access.
