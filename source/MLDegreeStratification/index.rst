===========================================================
Matroid Stratification of ML Degrees of Independence Models
===========================================================

| This page contains auxiliary files to the paper
| Oliver Clarke, Serkan Hoşten, Nataliia Kushnerchuk, Janike Oldekop: Matroid stratification of ML degrees of independence models
| In: Algebraic statistics, 15 (2024) 2, p. 199-223
| DOI: `10.2140/astat.2024.15.199 <https://dx.doi.org/10.2140/astat.2024.15.199>`_ ARXIV: https://arxiv.org/abs/2312.10010 CODE: https://mathrepo.mis.mpg.de/MLDegreeStratification


ABSTRACT:  We study the maximum likelihood (ML) degree of discrete exponential independence models and models defined by the second hypersimplex. For models with two independent variables, we show that the ML degree is an invariant of a matroid associated to the model. We use this description to explore ML degrees via hyperplane arrangements. For independence models with more variables, we investigate the connection between the vanishing of factors of its principal :math:`A`-determinant and its ML degree. Similarly, for models defined by the second hypersimplex, we determine its principal :math:`A`-determinant and give computational evidence towards a conjectured lower bound of its ML degree.





Preliminaries
~~~~~~~~~~~~~

Discrete exponential models can be described as toric varieties given by parameterizations of the form 

.. math::
    \theta = (\theta_1, \ldots, \theta_d) \mapsto (w_1 \theta^{a_1}, \ldots, w_n \theta^{a_n}),

where :math:`a_1, \ldots, a_n \in \mathbb{Z}^d` can be considered as the lattice points of a polytope. The ML degree of such a model is at most the degree of the corresponding toric variety. For generic scalings :math:`w_1, \ldots, w_n \in \mathbb{C}^*`, the ML degree achieves this upper bound. The locus of nongeneric scalings that gives rise to toric models with lower ML degree is determined by the principal :math:`A`-determinant.






Independence Models
~~~~~~~~~~~~~~~~~~~

Required Software: `Macaulay2 <https://macaulay2.com>`_

We consider scaled Segre embeddings of :math:`\mathbb{P}^{m-1} \times \mathbb{P}^{n-1}` with scaling matrix :math:`w \in (\mathbb{C}^*)^{m \times n}`. The juxtaposition of the identity matrix with :math:`w` is

.. math::
    \hat w = [I_m \mid w] \in \mathbb{C}^{m \times (m+n)}.

Linear Matroids :math:`M_w` that arise as the columns of :math:`\hat w` are called special. The ML degree of the scaled Segre embedding with scaling matrix :math:`w` is equal to the beta invariant of :math:`M_w`. In Example 2.22 and 2.23 in the paper, we computed the beta invariants of all special matroids of rank :math:`m=4` for :math:`n=4` and :math:`5`, shown in Table 1 and 2. These computations are based on a database of matroids that is available `here <https://www-imai.is.s.u-tokyo.ac.jp/~ymatsu/matroid/>`_.

.. list-table::  Table 1: The beta invariants of all 568 special matroids of rank 4 for n=4.
   :widths: 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5
   :stub-columns: 1

   * - Beta invariant
     - 1
     - 2
     - 3
     - 4
     - 5
     - 6
     - 7
     - 8
     - 9
     - 10
     - 11
     - 12
     - 13
     - 14
     - 15
     - 16
     - 17
     - 18
     - 19
     - 20   
   * - No. matroids
     - 1
     - 6
     - 10
     - 16
     - 17
     - 26
     - 27
     - 33
     - 29
     - 47
     - 59
     - 74
     - 84
     - 67
     - 40
     - 20
     - 7
     - 3
     - 1
     - 1


.. list-table::  Table 2: The beta invariants of all 185 253 special matroids of rank 4 for n=5.
   :widths: 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5
   :stub-columns: 1

   * - Beta invariant
     - 1
     - 2
     - 3
     - 4
     - 5
     - 6
     - 7
     - 8
     - 9
     - 10
     - 11
     - 12
     - 13
     - 14
     - 15
     - 16
     - 17
     - 18
     - 19
     - 20   
     - 21
     - 22
     - 23
     - 24
     - 25
     - 26
     - 27
     - 28
     - 29
     - 30
     - 31
     - 32
     - 33
     - 34
     - 35
   * - No. matroids
     - 1
     - 9
     - 20
     - 34
     - 48
     - 75
     - 93
     - 133 
     - 168 
     - 265
     - 361
     - 486
     - 636
     - 760
     - 845
     - 1180
     - 1827
     - 2881
     - 4767
     - 7807
     - 11600
     - 17153
     - 25328
     - 33480
     - 33963
     - 24293
     - 11856
     - 3961
     - 967
     - 199
     - 42
     - 10
     - 3
     - 1
     - 1

Our code and the necessary files from the database are available for download here:

****
    | 
    | 
    | :download:`mat4n.m2 <mat4n.m2>`
    | :download:`allr4n08.txt <allr4n08.txt>`
    | :download:`allr4n09.txt <allr4n09.txt>`





Second Hypersimplex
~~~~~~~~~~~~~~~~~~~

Required Software: `Mathematica <https://www.wolfram.com/mathematica/>`_ | `julia <https://julialang.org/>`_ | `HomotopyContinuation.jl <https://www.juliahomotopycontinuation.org/>`_

We consider the second hypersimplex :math:`\Delta_{2,d}`. The principal :math:`A`-determinant of scaled models defined by this polytope is a product of principal minors of a symmetric matrix. For :math:`d = 4,\ldots, 8`, the following table, corresponding to Table 5 in the paper, shows the ML degrees for scalings such that all factors of the principal :math:`A`-determinant vanish. A proof for :math:`d=4` is given in the paper. For :math:`d = 5,\ldots, 8`, we used `Mathematica <https://www.wolfram.com/mathematica/>`_ to compute these scalings. The associated ML degrees were computed with `HomotopyContinuation.jl <https://www.juliahomotopycontinuation.org/>`_.

.. list-table::  Table 3: Data of the uniform rank two matroid on up to eight elements.
   :widths: 5 20 20 20 20 20
   :header-rows: 1

   * - :math:`d`
     - Degree
     - ML Degree
     - ML Degree Drop 
     - # Principal 4-minors
     - # Principal 6-minors
   * - 4
     - 4
     - 3
     - 1
     - 1
     -
   * - 5
     - 11
     - 6
     - 5
     - 5
     -
   * - 6
     - 26
     - 10
     - 16
     - 15
     - 1
   * - 7
     - 57
     - 15
     - 42
     - 35
     - 7
   * - 8
     - 120
     - 21
     - 99
     - 70
     - 28

The code is available for download here:

****
    | 
    | 
    | :download:`Table5.nb <Table5.nb>`
    | :download:`Table5.jl <Table5.jl>`

Table 6 in the paper shows the ML degree stratification of models defined by :math:`\Delta_{2,5}` for different scalings. Again, the scalings and ML degrees were computed using `Mathematica <https://www.wolfram.com/mathematica/>`_ and `HomotopyContinuation.jl <https://www.juliahomotopycontinuation.org/>`_, respectively. The code is available for download here:

****
    | 
    | 
    | :download:`Table6.nb <Table6.nb>`
    | :download:`Table6.jl <Table6.jl>`





------------------------------------------------------------------------------------------------------------------------------

Project page created: 18/12/2023

Project contributors: Oliver Clarke, Serkan Hoşten, Nataliia Kushnerchuk, Janike Oldekop

Corresponding author of this page: Janike Oldekop, oldekop@math.tu-berlin.de

Software used: Julia (version 1.8.5), Macaulay2 (Version 1.20), Mathematica (version 12.3.1)

System setup used: MacBook Pro with macOS Venture 13.3.1, Processor 2.3 GHz Dual Core Intel Core i5, Memory 8 GB

License for code of this project page: MIT License (https://spdx.org/licenses/MIT.html)

License for all other content of this project page: CC BY 4.0 (https://creativecommons.org/licenses/by/4.0/)


Last updated 18/12/2023.



