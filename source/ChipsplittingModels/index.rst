==============================================================================
Classifying one-dimensional discrete models with maximum likelihood degree one
==============================================================================

| This page contains auxiliary files to the paper:
| Arthur Bik and Orlando Marigliano: Classifying one-dimensional discrete models with maximum likelihood degree one
| ARXIV: https://arxiv.org/abs/2205.09547 CODE: https://mathrepo.mis.mpg.de/ChipsplittingModels

In the paper, we prove for :math:`n=2,3,4` that a one-dimensional model in :math:`\Delta_n` with a rational maximum likelihood estimator is parametrized by scalar multiples of monomials in :math:`t,1-t` of degree :math:`\leq 2(n+1)-3`. We prove the cases :math:`n=2,3,4` in Sections :math:`5,6,7` respectively. In Section 8, we generate a list of all fundamental models in :math:`\Delta_n` parametrized by monomials of degree :math:`\leq9` for :math:`n\leq 5`. 

In Section 5, we introduce the Invertibility Criterion and use it prove that a one-dimensional model in :math:`\Delta_n` with a rational MLE are parametrized by scalar multiples of monomials of degree :math:`\leq 2(n+1)-3` for :math:`n=2`. As the number of possible configurations of :math:`3` points in a grid is small, we can do this by hand. In Sections 6 and 7, we introduce the Hyperfield and Hexagon Criteria and prove the result for :math:`n=3,4`. As the number of possible configurations is now to big to handle by hand, we let a computer go through them all. The same holds for the computations in Section 8. The code can be viewed below.

.. toctree::
   :maxdepth: 1
   :glob:
   
   Section6
   Section7
   Section8

The source code can be downloaded here: 

:download:`Section6.ipynb <Section6.ipynb>`

:download:`Section7.ipynb <Section7.ipynb>`

:download:`Section8.ipynb <Section8.ipynb>`

Project page created: 20/05/2022

Project contributors: Arthur Bik, Orlando Marigliano
 
Code written by: Arthur Bik

Software used: SageMath (Version 9.2)

Corresponding author of this page: Arthur Bik, arthur.bik@mis.mpg.de
