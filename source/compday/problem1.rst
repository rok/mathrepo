#########
Problem 1
#########

.. compday_problem1:
   
Write a loop that expresses :math:`x^n+y^n+z^n` in terms of elementary symmetric
polynomials, for as many positive integers :math:`n` as you can.

Solution
--------

Following you can find a built in function in *Mathematica*:

.. code-block:: mathematica

                xyzDecomp[n_] := SymmetricReduction[x^n + y^n + z^n, {x, y, z}][[1]]

an example where :math:`n=5`:

.. code-block:: mathematica

                xyzDecomp[5]

and a plot where the :math:`x` axis is :math:`n` and the :math:`y` axis is how long the computation takes in
seconds:
                  
.. image:: 1.png

