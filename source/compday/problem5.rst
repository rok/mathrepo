#########
Problem 5
#########

.. compday_problem5:
   
The symmetric group :math:`S_5` has an irreducible representation of dimension 6. Construct explicit
:math:`6 \times 6`-matrices such that the matrix group they generate in :math:`{\rm GL}(6,\mathbb{C})` is
isomorphic to that representation.


Solution
========

GAP
---

.. code-block:: gap
   
   g:=SymmetricGroup(5);
   r:=IrreducibleRepresentations(g);

GAP returns a list of all 7 irreducible representations of :math:`S_5`; the 6-dimensional one looks as
follows:

.. code-block:: gap
   
   GroupHomomorphismByImages( SymmetricGroup( [ 1 .. 5 ] ), Group(
    [ [ [ 0, 0, 0, 0, -1, 0 ], [ 0, 0, 1, 1, 1, 0 ], [ 0, 0, 0, 0, 0, 1 ], 
          [ 0, 0, -1, 0, 0, 0 ], [ -1, 0, 1, 0, 0, -1 ], 
          [ 0, -1, 0, 0, 1, 1 ] ], 
      [ [ 0, -1, 0, 0, 0, 0 ], [ -1, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, -1, 0 ], 
          [ 0, 0, 0, -1, 0, 0 ], [ 0, 0, -1, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 1 ] 
         ] ]), [ (1,2,3,4,5), (1,2) ], 
    [ [ [ 0, 0, 0, 0, -1, 0 ], [ 0, 0, 1, 1, 1, 0 ], [ 0, 0, 0, 0, 0, 1 ], 
          [ 0, 0, -1, 0, 0, 0 ], [ -1, 0, 1, 0, 0, -1 ], 
          [ 0, -1, 0, 0, 1, 1 ] ], 
      [ [ 0, -1, 0, 0, 0, 0 ], [ -1, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, -1, 0 ], 
          [ 0, 0, 0, -1, 0, 0 ], [ 0, 0, -1, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 1 ] 
         ] ] )


So the wanted matrices are

.. math::

   \begin{pmatrix}
   0 & 0 & 0 & 0 & -1 & 0 \\
   0 & 0 & 1 & 1 & 1 & 0 \\
   0 & 0 & 0 & 0 & 0 & 1 \\
   0 & 0 & -1 & 0 & 0 & 0 \\
   -1 & 0 & 1 & 0 & 0 & -1 \\
   0 & -1 & 0 & 0 & 1 & 1
   \end{pmatrix}
         
and 

.. math::
   
   \begin{pmatrix}
   0 & -1 & 0 & 0 & 0 & 0 \\
   -1 & 0 & 0 & 0 & 0 & 0 \\
   0 & 0 & 0 & 0 & -1 & 0 \\
   0 & 0 & 0 & -1 & 0 & 0 \\
   0 & 0 & -1 & 0 & 0 & 0 \\
   0 & 0 & 0 & 0 & 0 & 1
   \end{pmatrix}

   
