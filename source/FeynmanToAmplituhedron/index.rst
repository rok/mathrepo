======================================
From Feynman Diagrams to the Amplituhedron: A Gentle Review
======================================

| This page contains auxiliary files to the paper:
| Shounak De, Dmitrii Pavlov, Marcus Spradlin and Anastasia Volovich: From Feynman Rules to the Amplituhedron: A Gentle Review
| ARXIV: `2410.11757 <https://arxiv.org/abs/2410.11757>`_

| ABSTRACT: In these notes we review, for a mathematical audience, the computation of (tree-level) scattering amplitudes in Yang-Mills theory in detail. In particular we demonstrate explicitly how the same formulas for six-particle NMHV helicity amplitudes are obtained from summing Feynman diagrams and from computing the canonical form of the n=6, k=1, m=4 amplituhedron.

We provide a Mathematica package for computing NMHV amplitudes in Yang-Mills theory. This includes computing the amplitude from Feynman diagrams, and extracting individual NMHV amplitudes from the superamplitude. Our code can be downloaded here: :download:`FeynmanToAmplituhedron.m <FeynmanToAmplituhedron.m>`. Detailed description of the code is contained in the comments in source code file.

Project page created: 14/10/2024.

Project contributors: Shounak De, Dmitrii Pavlov, Marcus Spradlin and Anastasia Volovich.

Code written by: Marcus Spradlin.

Corresponding author of this page: Dmitrii Pavlov, `pavlov@mis.mpg.de <mailto:pavlov@mis.mpg.de>`_.
 
Software used: Wolfram Mathematica (Version 13.3.1).

System setup used: MacBook Pro with macOS Monterey 12.6, Processor 2,8 GHz Intel Core i7, Memory 16 GB 2133 MHz LPDDR3, Graphics Intel HD Graphics 630 1536 MB.

License for code of this project page: MIT License (https://spdx.org/licenses/MIT.html).

License for all other content of this project page (text, images, …): CC BY 4.0 (https://creativecommons.org/licenses/by/4.0/).

Last updated 04/03/25.


