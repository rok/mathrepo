========================
The Hessian Discriminant
========================

| This page contains supplementary code to the following paper:
| Rodica Dinu and Tim Seynnaeve: The Hessian discriminant
| In: Le Matematiche, 75 (2020) 2, p. 595-610
| MIS-Preprint: `88/2019 <https://www.mis.mpg.de/publications/preprints/2019/prepr2019-88.html>`_ DOI: `10.4418/2020.75.2.12 <https://dx.doi.org/10.4418/2020.75.2.12>`_ ARXIV: https://arxiv.org/abs/1909.06681 CODE: https://mathrepo.mis.mpg.de/HessianDiscriminant

All code is in :math:`\verb|Macaulay2|`.

.. toctree::
   :maxdepth: 2

   Sections12.rst
   Section3.rst

Project contributors: Rodica Dinu and Tim Seynnaeve

Software used: Macaulay2