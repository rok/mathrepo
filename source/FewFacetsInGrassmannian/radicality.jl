using Oscar 
using Combinatorics

R, vr = graded_polynomial_ring(QQ, ["p12","p13", "p23", "p14", "p24", "p34"])
p12 = vr[1]
p13 = vr[2]
p23 = vr[3]
p14 = vr[4]
p24 = vr[5]
p34 = vr[6]

# generate 8 combinatorial types
#Start from 8 non-isomorphic Gale transforms 
G1 = [-2//1 -2 -1 0 1 2 2 2;-1 0 2 2 2 2 -1 -2]

G2 = [-2//1 -2 -2 -1 0 1 2 2; -3 -2 -1 2 2 2 0 -1]

G3 = [-1//1 -1 -1 0 1 2 2 1; -1 0 3 1 3 3 1 -1]

G4 = [-1//1 -1 -3 -1 0 1 3 1; -1 0 1 3 1 3 1 -1]

G5 = [-1//1 -1 -2 -1 0 1 3 2; -2 -1 1 3 1 3 1 -3]

G6 = [-1//1 -3 -3 0 1 3 1 2; -1 1 2 1 2 1 0 -3]

G7 = [-1//1 -1 -2 -1 0 2 1 1; -2 -1 1 2 1 1 -1 -2]

G8 = [-1//1 -2 -2 0 1 1 1 1; -2 -1 1 1 3 1 0 -2]

# generate 8 coefficient matrices for facet hyperplanes
pol1 = nullspace(matrix_space(QQ,size(G1)...)(G1))[2]
pol2 = nullspace(matrix_space(QQ,size(G2)...)(G2))[2]
pol3 = nullspace(matrix_space(QQ,size(G3)...)(G3))[2]
pol4 = nullspace(matrix_space(QQ,size(G4)...)(G4))[2]
pol5 = nullspace(matrix_space(QQ,size(G5)...)(G5))[2]
pol6 = nullspace(matrix_space(QQ,size(G6)...)(G6))[2]
pol7 = nullspace(matrix_space(QQ,size(G7)...)(G7))[2]
pol8 = nullspace(matrix_space(QQ,size(G8)...)(G8))[2]

# We now vary V
# Define the set of elements
elems = collect(1:8)

# Function to generate all pairwise disjoint sets
function generate_disjoint_sets(elements)
    # Generate all combinations of two elements from the set
    two_element_combinations = collect(combinations(elements, 2))
    disjoint_triplets = []

    # Iterate over all unique triples of sets
    for (set1, set2, set3) in combinations(two_element_combinations, 3)
        # Check if the sets are pairwise disjoint
        if isempty(intersect(set1, set2)) && isempty(intersect(set1, set3)) && isempty(intersect(set2, set3))
            push!(disjoint_triplets, (set1, set2, set3))
        end
    end
    
    return disjoint_triplets
end

# Generate and print the result
prms = generate_disjoint_sets(elems)

# We now iterate over all possible combinatorial types of positive hexahedra for each combinatorial type of the associated polytope
irr = ideal([p12; p13; p23; p14; p24; p34])
hfcs = []

# Type 1
h1 = 2*p12+2*p13+2*p23+2*p14-p24-2*p34 
h2 = -5*p12-4*p13-3*p23-2*p14+4*p24+6*p34

s1 = ideal([p24; p34])
s2 = ideal([h1; h2])
s3 = ideal([p12; p13; p14; p23])
I = intersect(s1, s2, s3)
L = [p12; p13; p23; p14; p24; p34; h1; h2]
type1 = [] 
nonsat1 = [] 
gens(I)[1]

for p in prms 
    V = ideal([L[p[1][1]]*L[p[1][2]]; L[p[2][1]]*L[p[2][2]]; L[p[3][1]]*L[p[3][2]]])
    if is_radical(I+V) == false 
        push!(type1, p)
    end
    if (I+V):irr != (I+V)
        push!(nonsat1, p)
    end 
    push!(hfcs, hilbert_function(quo(R, radical(I+V))[1], 2))
end
type1
nonsat1
type1 == nonsat1 
hfcs 

# Type 2
h1 = p12+3*p13+2*p23+p14-2*p24-3*p34
h2 = -4*p12-7*p13-4*p23-p14+6*p24+8*p34 

s1 = ideal([p24; p34])
s2 = ideal([p12; h1; h2])
s3 = ideal([p13; p14; p23])
I = intersect([s1; s2; s3])
L = [p12; p13; p23; p14; p24; p34; h1; h2]
type2 = []
nonsat2 = []
gens(I)[1]

for p in prms 
    V = ideal([L[p[1][1]]*L[p[1][2]]; L[p[2][1]]*L[p[2][2]]; L[p[3][1]]*L[p[3][2]]])
    if is_radical(I+V) == false 
        push!(type2, p)
    end
    if (I+V):irr != (I+V)
        push!(nonsat2, p)
    end 
    push!(hfcs, hilbert_function(quo(R, radical(I+V))[1], 2))
end
type2
nonsat2
type2 == nonsat2

# Type 3 
h1 = 3*p12 + p13 + 3*p23 + 3*p14 + p24 - p34
h2 = -4*p12 - p13 - 2*p23 - p14 + p24 + 2*p34

s1 = ideal([h1; h2])
s2 = ideal([p24; p34])
s3 = ideal([p34; h1])
s4 = ideal([p12; p13; p14; p23; p24])
s5 = ideal([p12; p13; p14; p23; h2])
I = intersect(s1, s2, s3, s4, s5)
gens(I)[1]
L = [p12; p13; p23; p14; p24; p34; h1; h2]
type3 = []
nonsat3 = []
for p in prms 
    V = ideal([L[p[1][1]]*L[p[1][2]]; L[p[2][1]]*L[p[2][2]]; L[p[3][1]]*L[p[3][2]]])
    if is_radical(I+V) == false 
        push!(type3, p)
    end
    if (I+V):irr != (I+V)
        push!(nonsat3, p)
    end 
    push!(hfcs, hilbert_function(quo(R, radical(I+V))[1], 2))
end
type3 
nonsat3

# Type 4
h1 = p12 + 3*p13 + p23 + 3*p14 + p24 - p34
h2 = -4*p12 - 4*p13 - p23 - 2*p14 + 2*p24 + 2*p34

s1 = ideal([p24; p34])
s2 = ideal([p34; h1])
s3 = ideal([p12; h1; h2])
s4 = ideal([p13; p14; p23; p24])
s5 = ideal([p12; p13; p14; p23; h2])
I = intersect(s1, s2, s3, s4, s5);
gens(I)[1]
L = [p12; p13; p23; p14; p24; p34; h1; h2]
type4 = []
nonsat4 = []
for p in prms 
    V = ideal([L[p[1][1]]*L[p[1][2]]; L[p[2][1]]*L[p[2][2]]; L[p[3][1]]*L[p[3][2]]])
    if is_radical(I+V) == false 
        push!(type4, p)
    end
    if (I+V):irr != (I+V)
        push!(nonsat4, p)
    end 
    push!(hfcs, hilbert_function(quo(R, radical(I+V))[1], 2))
end
type4 
nonsat4 

# Type 5
h1 = 3*p12 + 4*p13 + p23 + 2*p14 - 2*p24 - 5*p34
h2 = -5*p12 - 5*p13 - p23 - p14 + 5*p24 + 7*p34 
s1 = ideal([p12; p13; p14; p23])
s2 = ideal([p13; p14; p23; p24])
s3 = ideal([p12; h1; h2])
s4 = ideal([p34; h1; h2])
s5 = ideal([p24; p34])
I = intersect(s1, s2, s3, s4, s5)
gens(I)[1]
L = [p12; p13; p23; p14; p24; p34; h1; h2]
type5 = []
nonsat5 = []
for p in prms 
    V = ideal([L[p[1][1]]*L[p[1][2]]; L[p[2][1]]*L[p[2][2]]; L[p[3][1]]*L[p[3][2]]])
    if is_radical(I+V) == false 
        push!(type5, p)
    end
    if (I+V):irr != (I+V)
        push!(nonsat5, p)
    end 
    push!(hfcs, hilbert_function(quo(R, radical(I+V))[1], 2))
end
type5
nonsat5 

# Type 6 
h1 = 3*p12 + 3*p13 + 7*p23 + 6*p14 + p24 - 7*p34
h2 = -5*p12 - p13 - p23 + 2*p14 + p24 + 5*p34

s1 = ideal([p34; h1])
s2 = ideal([p12; h1; h2])
s3 = ideal([p14; p24; p34])
s4 = ideal([p12; p13; p23; h2])
s5 = ideal([p13; p14; p23; p24])
I = intersect(s1, s2, s3, s4, s5)
gens(I)[1]
L = [p12; p13; p23; p14; p24; p34; h1; h2]
type6 = []
nonsat6 = [] 
for p in prms 
    V = ideal([L[p[1][1]]*L[p[1][2]]; L[p[2][1]]*L[p[2][2]]; L[p[3][1]]*L[p[3][2]]])
    if is_radical(I+V) == false 
        push!(type6, p)
    end
    if (I+V):irr != (I+V)
        push!(nonsat6, p)
    end 
    push!(hfcs, hilbert_function(quo(R, radical(I+V))[1], 2))
end
type6
nonsat6 

# Type 7 
h1 = 3*p12 + 3*p13 + p23 - p14 - 2*p24 - 3*p34
h2 = -5*p12 - 4*p13 - p23 + 3*p14 + 3*p24 + 4*p34

s1 = ideal([p12; p13; p23])
s2 = ideal([p12; h1; h2])
s3 = ideal([p13; p14; p23])
s4 = ideal([p14; p23; p24])
s5 = ideal([p14; p24; p34])
s6 = ideal([p24; p34; h1; h2])
I = intersect(s1, s2, s3, s4, s5, s6)
gens(I)[1]
L = [p12; p13; p23; p14; p24; p34; h1; h2]
type7 = []
nonsat7 = []
for p in prms 
    V = ideal([L[p[1][1]]*L[p[1][2]]; L[p[2][1]]*L[p[2][2]]; L[p[3][1]]*L[p[3][2]]])
    if is_radical(I+V) == false 
        push!(type7, p)
    end
    if (I+V):irr != (I+V)
        push!(nonsat7, p)
    end 
    push!(hfcs, hilbert_function(quo(R, radical(I+V))[1], 2))
end
type7
nonsat7 

# Type 8
h1 = 4*p12 + 2*p13 + 5*p23 + p14 - p24 - 5*p34 
h2 = -5*p12 - p13 - p23 + p14 + 2*p24 + 4*p34
L = [p12; p13; p23; p14; p24; p34; h1; h2]
inv([ 0//1 1 0 0 0 0; 0 0 0 1 0 0; 0 0 0 0 1 0; -5 -1 -1 1 2 4; 4 2 5 1 -1 -5; 0 0 1 0 0 0])


s1 = ideal([p12; h1; h2])
s2 = ideal([p14; p24; p34])
s3 = ideal([p24; p34; h1])
s4 = ideal([p34; h1; h2])
s5 = ideal([p12; p13; p14; p23])
s6 = ideal([p12; p13; p23; h2])
s7 = ideal([p13; p14; p23; p24])
I = intersect(s1, s2, s3, s4, s5, s6, s7)
gens(I)[1]
type8 = []
nonsat8 = []
badprms = []
badideals = []
badVs =[]

for p in prms 
    V = ideal([L[p[1][1]]*L[p[1][2]]; L[p[2][1]]*L[p[2][2]]; L[p[3][1]]*L[p[3][2]]])
    if is_radical(I+V) == false 
        push!(type8, p)
    end
    if (I+V):irr != (I+V)
        push!(nonsat8, p)
    end 
    push!(hfcs, hilbert_function(quo(R, radical(I+V))[1], 2))
    if hfcs[length(hfcs)] == 10
        push!(badprms, p)
        push!(badideals, radical(I+V))
        push!(badVs, V)
    end

end
type8
nonsat8 
