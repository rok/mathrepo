-- Examples of 8 combinatorial types of simple 5-polytopes with 8 facets and their adjoints
R = QQ[p12, p13, p23, p14, p24, p34]
V = ideal(p12*p34, p23*p14, p13*p24)

--Type 1: 2 P^3 and 1 P^1 
h1 = 2*p12+2*p13+2*p23+2*p14-p24-2*p34 
h2 = -5*p12-4*p13-3*p23-2*p14+4*p24+6*p34

s1 = ideal(p24, p34)
s2 = ideal(h1, h2)
s3 = ideal(p12, p13, p14, p23)
I = intersect(s1, s2, s3);
radical(I+V) == I+V
gens gb I

--Type 2: 1 P^3 and 2 P^2 
h1 = p12+3*p13+2*p23+p14-2*p24-3*p34
h2 = -4*p12-7*p13-4*p23-p14+6*p24+8*p34 

s1 = ideal(p24, p34)
s2 = ideal(p12, h1, h2)
s3 = ideal(p13, p14, p23)
I = intersect(s1, s2, s3);
radical(I+V) == I+V
gens gb I

--Type 3 3 P^3, 2 points 
h1 = 3*p12 + p13 + 3*p23 + 3*p14 + p24 - p34
h2 = -4*p12 - p13 - 2*p23 - p14 + p24 + 2*p34

s1 = ideal(h1, h2)
s2 = ideal(p24, p34)
s3 = ideal(p34, h1)
s4 = ideal(p12, p13, p14, p23, p24);
s5 = ideal(p12, p13, p14, p23, h2);

I = intersect(s1, s2, s3, s4, s5);
radical(I+V) == I+V
gens gb I

--Type 4 2 P^3, 1 P^2, 1 P^1, 1 point
h1 = p12 + 3*p13 + p23 + 3*p14 + p24 - p34
h2 = -4*p12 - 4*p13 - p23 - 2*p14 + 2*p24 + 2*p34

s1 = ideal(p24, p34)
s2 = ideal(p34, h1)
s3 = ideal(p12, h1, h2)
s4 = ideal(p13, p14, p23, p24)
s5 = ideal(p12, p13, p14, p23, h2)
I = intersect(s1, s2, s3, s4, s5);
radical(I+V) == I + V 
gens gb I


--Type 5 1 P^3, 2 P^2, 2 P^1
h1 = 3*p12 + 4*p13 + p23 + 2*p14 - 2*p24 - 5*p34
h2 = -5*p12 - 5*p13 - p23 - p14 + 5*p24 + 7*p34 
s1 = ideal(p12, p13, p14, p23)
s2 = ideal(p13, p14, p23, p24)
s3 = ideal(p12, h1, h2)
s4 = ideal(p34, h1, h2)
s5 = ideal(p24, p34)
I = intersect(s1, s2, s3, s4, s5);
radical(I+V) == I+V
gens gb I

--Type 6 1 P^3, 2 P^2, 2 P^1
h1 = 3*p12 + 3*p13 + 7*p23 + 6*p14 + p24 - 7*p34
h2 = -5*p12 - p13 - p23 + 2*p14 + p24 + 5*p34

s1 = ideal(p34, h1)
s2 = ideal(p12, h1, h2)
s3 = ideal(p14, p24, p34)
s4 = ideal(p12, p13, p23, h2)
s5 = ideal(p13, p14, p23, p24)
I = intersect(s1, s2, s3, s4, s5);
radical(I+V) == I+V 
gens gb I

--Type 7 5 P^2, 1 P^1 
h1 = 3*p12 + 3*p13 + p23 - p14 - 2*p24 - 3*p34
h2 = -5*p12 - 4*p13 - p23 + 3*p14 + 3*p24 + 4*p34

s1 = ideal(p12, p13, p23)
s2 = ideal(p12, h1, h2)
s3 = ideal(p13, p14, p23)
s4 = ideal(p14, p23, p24)
s5 = ideal(p14, p24, p34)
s6 = ideal(p24, p34, h1, h2)
I = intersect(s1, s2, s3, s4, s5, s6);
radical(I+V) == I+V 
saturate(I+V, ideal(p12, p13, p14, p23, p24, p34)) == I+V
gens gb radical(I+V)
gens gb I

hilbertFunction(2, I+V)
betti (I+V)
gens gb (I+V)

primaryDecomposition(radical(I+V))
primaryDecomposition(I+V)

--Type 8 4 P^2, 3 P^1
h1 = 4*p12 + 2*p13 + 5*p23 + p14 - p24 - 5*p34 
h2 = -5*p12 - p13 - p23 + p14 + 2*p24 + 4*p34

s1 = ideal(p12, h1, h2)
s2 = ideal(p14, p24, p34)
s3 = ideal(p24, p34, h1)
s4 = ideal(p34, h1, h2)
s5 = ideal(p12, p13, p14, p23)
s6 = ideal(p12, p13, p23, h2)
s7 = ideal(p13, p14, p23, p24)
I = intersect(s1, s2, s3, s4, s5, s6, s7)
radical(I+V) == I+V 
gens gb I