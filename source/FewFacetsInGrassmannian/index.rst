======================================================
Positive Polytopes with Few Facets in the Grassmannian
======================================================

| This page contains auxiliary files to the paper:
  Dmitrii Pavlov, Kristian Ranestad: Positive Polytopes with Few Facets in the Grassmannian 
| ARXIV: TBA

| ABSTRACT:  In this article we study adjoint hypersurfaces of geometric objects obtained by intersecting simple polytopes with few facets in :math:`\mathbb{P}^5` with the Grassmannian :math:`\mathrm{Gr}(2,4)`. These generalize the positive Grassmannian, which is the intersection of :math:`\mathrm{Gr}(2,4)` with the simplex. We show that if the resulting object has five facets, it is a positive geometry and the adjoint hypersurface is unique. For the case of six facets we show that the adjoint hypersurface is not necessarily unique and give an upper bound on the dimension of the family of adjoints. We illustrate our results with a range of examples. In particular, we show that even if the adjoint is not unique, a positive hexahedron can still be a positive geometry. 


Identifying residual arrangements
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

We identify the residual arrangement of a positive pentahedron (:download:`positive_hexahedra.nb <positive_hexahedra.nb>`), positive hexahedron (:download:`pos_pentahedron.nb <pos_pentahedron.nb>`) in :math:`\mathrm{Gr}(2,4)` and their associated polyopes in :math:`\mathbb{P}^5` (:download:`polytope_residuals.nb <polytope_residuals.nb>`) by using Cylindrical Algebraic Decomposition in Mathematica. This is the first step to working out all examples in the paper. 

Identifying adjoints
~~~~~~~~~~~~~~~~~~~~

We compute the ideal of the residual arrangement of a positive polytope in Macaulay2: :download:`generating_adjoints.m2 <generating_adjoints.m2>`. In particular, this gives the information about the dimension of the familty of adjoints. This is because (moudlo the Plücker relations) adjoints are the lowest-degree elements in this ideal. 

Bounding the dimension of the family of adjoints
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To prove Proposition 4.6, we iterate over 3360 combinatorial types of positive hexahedra and compute the dimension of the family of adjoints for each of them. This is done by computing the Hiblert function of the ideal of the residual arrangement. We use `Julia  <https://julialang.org>`_ with the `Oscar <https://oscar.computeralgebra.de>`_ computer algebra package. The code can be downloaded here: :download:`radicality.jl <radicality.jl>`.

Studying the topology of positive polytopes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

All of our examples in Section 5 are contractible sets. We verified this by using the package `HypersufraceRegions.jl <https://juliaalgebra.github.io/HypersurfaceRegions.jl/>`_ by Paul Breiding, Bernd Sturmfels and Kexin Wang. 

Project page created: 03/03/2025.

Project contributors: Dmitrii Pavlov, Kristian Ranestad

Corresponding author of this page: Dmitrii Pavlov, `pavlov@mis.mpg.de <mailto:pavlov@mis.mpg.de>`_.

Software used: Julia (Version 1.11), Oscar (Version. 1.0), Mathematica (Version 13.3.1), Macaulay2 (Version 1.24.11).

System setup used: MacBook Pro with macOS Ventura 13.7.4, Processor 2,8 GHz Intel Core i7, Memory 16 GB 2133 MHz LPDDR3, Graphics Intel HD Graphics 630 1536 MB.

License for code of this project page: MIT License (https://spdx.org/licenses/MIT.html).

License for all other content of this project page (text, images, …): CC BY 4.0 (https://creativecommons.org/licenses/by/4.0/).

Last updated 03/03/25.

