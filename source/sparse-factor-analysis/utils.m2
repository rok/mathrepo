loadPackage("Graphs", Reload => true);

getIdeal = (LambdaAdjMatrix) -> (
    p = numRows LambdaAdjMatrix;
    m = numColumns LambdaAdjMatrix;

    -- Collect needed variables in lists
    varlambda = {};
    l = symbol l;
    for i in 1..p do for j in 1..m do (
        if (LambdaAdjMatrix_((i-1),(j-1))==1) then
            varlambda = append(varlambda, l_(i,j));
    );
    vardiag = {};
    d = symbol d;
    for i in 1..p do (
        vardiag = append(vardiag, d_(i));
    );
    varpsi = {};
    s = symbol s;
    for i in 1..p do for j in i..p do (
        varpsi = append(varpsi, s_(i,j));
    );

    -- Generate polynomial ring with all variables
    -- Have to specify an order?
    R := QQ[varlambda, vardiag, varpsi];
    gensR = gens R;

    -- Define matrices Lambda, Sigma and Psi
    count = 0;
    Lambda = mutableMatrix(R,p,m);
    for i in 1..p do for j in 1..m do (
        if (LambdaAdjMatrix_((i-1),(j-1))==1) then (
            Lambda_((i-1),(j-1)) = gensR_(count);
            count = count + 1;
        );  
    );
    Sigma = mutableMatrix(R,p,p);
    for i in 1..p do (
        Sigma_((i-1),(i-1)) = gensR_(count);
        count=count+1;
    );
    Psi = mutableMatrix(R,p,p);
     for i in 1..p do for j in i..p do (
        Psi_((i-1),(j-1)) = gensR_(count);
        Psi_((j-1),(i-1)) = gensR_(count);
        count=count+1;
    );

    -- Compute vanishing ideal by implicitization
    M = Psi - (Sigma + Lambda * transpose(Lambda));
    gensJ = {};
    for i in 1..p do for j in i..p do gensJ = append(gensJ, M_((i-1),(j-1)));
    J := ideal(gensJ);
    I := eliminate(gensR_{0..(length(varlambda)+length(vardiag)-1)}, J);
 
    return I;
);

circularDistance = (p,i,j) -> (
    d := min(abs(i-j), p-abs(i-j));
    return d;
);

-- Block term order used for circular term order is Lex.
circularOrderRing = (p) -> (
    variablesi = flatten for i in 1..p list for j in (i+1)..p list i;
    variablesj = flatten for i in 1..p list for j in (i+1)..p list j;
    variables = flatten for i in 1..p list for j in (i+1)..p list s_(i,j); 
    maxdist = floor(p/2);
    blocks = {};
    nr_vars = length variables;
    for dist in 1..maxdist do (
        block = {};
        for k in 0..(nr_vars-1) do (
            i = variablesi_k;
            j = variablesj_k;
            d = circularDistance(p,i,j);
            if (d==dist) then (
                block = append(block,variables_k);
            );
        );
        blocks = append(blocks, block);
    );
    R = QQ[flatten(blocks), MonomialOrder => Lex];
    return R;
);

Lambda2Digraph = (LambdaAdjMatrix) -> (
    p := numRows LambdaAdjMatrix;
    m := numColumns LambdaAdjMatrix;

    -- Adjacency Matrix
    A := mutableMatrix(ZZ,(p+m),(p+m));
    for i in 1..m do for j in 1..p do (
        A_((p+i-1),(j-1)) = LambdaAdjMatrix_(j-1,i-1);
    );

    -- Nodes
    V := {};
    for i in 1..p do (
        V = append(V, i);
    );
    for i in 1..m do (
        V = append(V, h_(i));
    );

    -- Graph
    G := digraph(V,matrix(A));
    return G;
);



-- G: factor graph where the observed nodes are {1,...,p} and the latent nodes are {h_1, ..., h_m} 
-- latent nodes have at least one child

-- get all observed nodes
getObservedNodes = (G) -> (
    nodes = toList(vertexSet(G));
    observed = {};
    for v in nodes do(
        if (length(toList(children(G, v)))==0) then (
            observed = append(observed, v);
        );
    );
    return observed;
);

-- get all latent nodes
getLatentNodes = (G) -> (
    nodes = toList(vertexSet(G));
    latent = {};
    for v in nodes do(
        if ((length(toList(parents(G, v)))==0) and (length(toList(children(G, v)))>=1) ) then (
            latent = append(latent, v);
        );
    );
    return latent;
);

-- From here, the code is for sparse 2-factor analysis models.

-- get observed variables connected to first factor
getFirstFactor = (G) -> (
    nodes = toList(vertexSet(G));
    result = {};
    for v in nodes do(
        if (member(h_1, parents(G, v))) then (
            result = append(result, v);
        );
    );
    return result;
);

-- get observed variables connected to second factor
getSecondFactor = (G) -> (
    nodes = toList(vertexSet(G));
    result = {};
    for v in nodes do(
        if (member(h_2, parents(G, v))) then (
            result = append(result, v);
        );
    );
    return result;
);

-- get observed variables conneted to both factors
getOverlap = (G) -> (
    nodes = toList(vertexSet(G));
    overlap = {};
    for v in nodes do(
        if (length(toList(parents(G, v)))==2) then (
            overlap = append(overlap, v);
        );
    );
    return overlap;
);


separationIdeal = (G) -> (
    p = length(getObservedNodes(G));

    -- Create Ring
    varpsi := {};
    for i in 1..p do for j in i..p do (
        varpsi = append(varpsi, s_(i,j));
    );
    R :=  QQ[varpsi];

    -- Monomials
    gensI := {};
    for i in 1..p do for j in i..p do (
        if (length(toList(parents(G,i) * parents(G,j))) == 0) then (
            gensI = append(gensI, s_(i,j));
        );
    );

    -- Tetrads
    for u in 1..p do for v in (u+1)..p do for w in (v+1)..p do for z in (w+1)..p do (
        pau = parents(G,u);
        pav = parents(G,v);
        paw = parents(G,w);
        paz = parents(G,z);
        if (length(toList((pau + pav) * (paw + paz))) == 1) then (
            gensI = append(gensI,s_(u,w) * s_(v,z) - s_(v,w) * s_(u,z));
        );
        if (length(toList((pau + paw) * (pav + paz))) == 1) then (
            gensI = append(gensI,s_(u,v) * s_(w,z) - s_(v,w) * s_(u,z));
        );
        if (length(toList((pau + paz) * (pav + paw))) == 1) then (
            gensI = append(gensI,s_(u,v) * s_(w,z) - s_(u,w) * s_(v,z));
        );
    );

    I := ideal(gensI);
    return I;
);

getMplusS = (LambdaAdjMatrix) -> (
    p := numRows LambdaAdjMatrix;
    m := numColumns LambdaAdjMatrix;

    -- Create ring
    varpsi := {};
    for i in 1..p do for j in i..p do (
        varpsi = append(varpsi, s_(i,j));
    );
    R :=  QQ[varpsi];

    -- Graph
    G := Lambda2Digraph(LambdaAdjMatrix);

    -- Separation ideal from graph
    S := separationIdeal(G);
    S = sub(S, R);

    -- Ideal generated by (m+1)x(m+1)-minors
    Psi := matrix(for i from 1 to p list for j from 1 to p list s_(min(i,j),max(i,j)));
    M := minors(m+1,Psi);
    M = sub(M, R);

    -- Get final ideal
    J := M + S;

    return J;
);

getProposedIdeal = (LambdaAdjMatrix) -> (
    p := numRows LambdaAdjMatrix;

    -- Create ring
    varpsi := {};
    for i in 1..p do for j in i..p do (
        varpsi = append(varpsi, s_(i,j));
    );
    R :=  QQ[varpsi];

    -- Get J = M + S
    J := getMplusS(LambdaAdjMatrix);
    J = sub(J, R);
    toEliminate := {};
    gensR := gens R;
    count := 0;
    for i in 1..p do for j in i..p do (
        if (i==j) then (
            toEliminate = append(toEliminate, gensR_(count));
        );
        count = count + 1;  
    );
    I := eliminate(toEliminate, J);

    return I;
);

-- Triple (p,k,s)
-- p: total number of observed variables
-- k: number of variables connected to first factor
-- r: number of variables connected to second factor

getLambda = (p, k, r) -> (
    LambdaAdjMatrix = mutableMatrix(ZZ,p,2);
    for i in 0..(k-1) do LambdaAdjMatrix_(i,0) = 1;
    for i in (p-r)..(p-1) do LambdaAdjMatrix_(i,1) = 1; 
    return LambdaAdjMatrix;
);

