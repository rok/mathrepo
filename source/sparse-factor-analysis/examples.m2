input "utils.m2";
loadPackage "PhylogeneticTrees";
loadPackage("Graphs", Reload => true);

------------------------------------------------------------
--4.1 Polynomials in larger instersections
------------------------------------------------------------

--Overlap 3
M = transpose matrix {{1,1,1,1,1,0,0},{0,0,1,1,1,1,1}}
I = getIdeal M
R = circularOrderRing 7
J = ideal gens gb sub(I,R)


--When the overlap is 4 we find 4 new kind of polynomials in the Groebner basis
M = transpose matrix {{1,1,1,1,1,1,0,0},{0,0,1,1,1,1,1,1}}
I = getIdeal M
R = circularOrderRing 8
J = ideal gens gb sub(I,R)
--The first one has degree 5 and it is a sum of 6 monomials:
f1 =  (gens J)_110_0
--It uses 11 distinct indeterminates from the polynomial ring:
factor product terms f1
--We find the remainining 3 similarly
f2 =  (gens J)_128_0
factor product terms f2
f3 = (gens J)_101_0
factor product terms f3
f4 = (gens J)_60_0
factor product terms f4

------------------------------------------------------------
--4.2 Polynomials in the presence of more latent factors
------------------------------------------------------------

--In the presence of 4 latent variables we detect a new polynomial that has degree 5.
--It is a sum of 8 monomials and uses 13 indeterminates.
M = transpose matrix {{1,1,1,1,0,0,0,0,0,0,0,0},{0,0,1,1,1,1,1,0,0,0,0,0},{0,0,0,0,0,1,1,1,1,1,0,0},{0,0,0,0,0,0,0,0,1,1,1,1}}
I = getIdeal M
R = circularOrderRing 12
J = sub(I,R)
f = (gens JJ)_59_0
factor product terms f
