====================================================
Logarithmic Discriminants of Hyperplane Arrangements
====================================================

Overview
--------

| This page contains auxiliary files to the paper:
| Leonie Kayser, Andreas Kretschmer and Simon Telen. *Logarithmic Discriminants of Hyperplane Arrangements*
| **arXiv:** `2410.11675 <https://arxiv.org/abs/2410.11675>`_

**Abstract.** A recurring task in particle physics and statistics is to compute the complex critical points of a product of powers of affine-linear functions. The logarithmic discriminant characterizes exponents for which such a function has a degenerate critical point in the corresponding hyperplane arrangement complement. We study properties of this discriminant, exploiting its connection with the Hurwitz form of a reciprocal linear space.

On this page you can find the Macaulay2 and Julia code to compute the examples in the paper, as well as the computer proofs of **Lemma 5.3** and **Corollary 5.5**.

Computing logarithmic and Hurwitz discriminants
-----------------------------------------------

The logarithmic discriminant and the Hurwitz discriminant can be computed using variable elimination, at least in small cases. 
We implemented this in Macaulay2, the code can be downloaded here. :download:`LogDisc.m2 <logdisc.m2>`

To verify **Example 1.1** (here in the notation of **Example 8.1**) and **Example 3.3** you can run the following:

.. code-block:: macaulay2

  load "logdisc.m2"

  (M,S) = M0nArrangement 5
  elapsedTime( nabla = logDisc(M, PolyRing=>S); )
  factor (nabla_0 + 4*s_(1,3)*s_(1,4)*s_(2,3)*s_(2,4))

  bA = matrix {{1,1,1,0},{2,1,3/2,0},{1,2,3/2,0},{0,1,0,1},{0,0,1,1},{0,1,1,2}}
  elapsedTime( nabla = logDisc bA; )
  decompose nabla

The first computation terminates within a second, while the second takes about 25 seconds on our system.
We warn that this code will likely not terminate with larger numbers of hyperplanes. Already the logarithmic discriminant of the :math:`\mathcal{M}_{0,6}`-arrangement seems to be far out of reach (it is of degree :math:`30` in :math:`9` variables, see below).

Birationality of the projection for d=1
---------------------------------------

In order to prove the claim in **Lemma 5.3**, we need to show that for :math:`n+1 \geq 3` distinct points the linear system 

.. math::
    
    g_1(-,x) = g_2(-,x) = g_1(-,y) = g_2(-,y) = 0
    
has full rank. In the case of four forms :math:`\{x,x+1,x+b_2,x+b_3\}` the coefficient matrix is the following:

.. math::

    C_4 = \begin{bmatrix}
        \frac{1}{x} &  \frac{1}{x+1} &  \frac{1}{x+b_2} &  \frac{1}{x+b_3} \\
        \frac{1}{x^2} &  \frac{1}{(x+1)^2} &  \frac{1}{(x+b_2)^2} &  \frac{1}{(x+b_3)^2} \\
        \frac{1}{y} &  \frac{1}{y+1} &  \frac{1}{y+b_2} &  \frac{1}{y+b_3} \\
        \frac{1}{y^2} &  \frac{1}{(y+1)^2} &  \frac{1}{(y+b_2)^2} &  \frac{1}{(y+b_3)^2}
    \end{bmatrix}

We can compute and factorize its determinant symbolically in Macaulay2:

.. code-block:: macaulay2
  
  R = QQ[x,y,b_2..b_3]
  bb = {0,1,b_2,b_3}
  C4 = transpose matrix for i from 0 to 3 list {1/(x+bb_i), 1/(x+bb_i)^2, 1/(y+bb_i), 1/(y+bb_i)^2}
  factor det C4

.. math::
    
  \frac{\left(b_{3}\right)\left(b_{3}-1\right)\left(b_{2}\right)\left(b_{2}-1\right)\left(b_{2}-b_{3}\right)\left(x-y\right)^{4}\left(-1\right)}{\left(y\right)^{2}\left(y+1\right)^{2}\left(y+b_{3}\right)^{2}\left(y+b_{2}\right)^{2}\left(x\right)^{2}\left(x+1\right)^{2}\left(x+b_{3}\right)^{2}\left(x+b_{2}\right)^{2}}

If the numbers :math:`\mathcal{A} = \{0, -1, -b_2, -b_3\}` are distinct and :math:`x,y \in \mathbb{C}^1 \setminus \mathcal{A}` are also distinct, then :math:`C_4` has full rank as desired.
For any number of :math:`n+1` points the corresponding system contains :math:`C_4` (or is contained, in the case :math:`n+1=3`), hence has full rank.

This shows that :math:`\mathcal{B}` is indeed a trivial projective bundle over :math:`\{\,(x,y) \in X\times X \mid x\neq y\,\}` if :math:`n+1\geq 5` and empty if :math:`n+1\leq 4`.


The discriminant of four points is always singular
--------------------------------------------------

To verify the claim in **Corollary 5.5**, we can compute the discriminant (in the sense of multivariate polynomials) of the logarithmic discriminant for the generic arrangement over :math:`\mathbb{Q}(b_2,b_3)`.
We use the formula from **Corollary 5.4** to compute :math:`\Delta_{\log}` for generic points :math:`\mathcal{A} = \{0, -1, -b_2, -b_3\}`.
The discriminant of :math:`\Delta_{\log} \in \mathbb{Q}[b_2,b_3][u_0,\dots,u_3]` is a polynomial :math:`\operatorname{Disc}_u \Delta_{\log} \in \mathbb{Q}[b_2,b_3]` which vanishes for :math:`(b_2,b_3) \in \mathbb{C}^2` if any only if :math:`\nabla_{\log}` is singular. 

.. code-block:: macaulay2

  needsPackage "Resultants"
  
  R = QQ[b_2..b_3][u_0..u_3][x_0..x_1]
  bb = {0,1,b_2,b_3}
  
  g1 = sum(4, i -> u_i * product( toList(set(0..3)-set({i})), k -> x_1 + bb_k*x_0 ))
  logDisc = discriminant g1
  discriminant logDisc

The result is identically zero.

Computing the logarithmic discriminant numerically
--------------------------------------------------

.. (Should we include a computation for a nice arrangement of lines? https://en.wikipedia.org/wiki/Arrangement_of_lines#:~:text=In%20geometry%2C%20an%20arrangement%20of,the%20efficient%20construction%20of%20arrangements.)

**Conjecture 1** states that the degrees of the logarithmic discriminants of the moduli space :math:`\mathcal{M}_{0,m}`
for :math:`m = 5, 6, 7, 8` are given by :math:`m = 4, 30, 208, 1540` respectively. This can be verified numerically using 
this Julia script. :download:`NumericLocDisc.zip <NumericLocDisc.zip>`

The surface :math:`\mathcal{M}_{0,5}` can be modeled as the complement of five lines in :math:`\mathbb{C}^2`.
The Hurwitz form of the associated reciprocal linear space has degree eight. We computed this using finite 
field methods and numerical interpolation. The resulting polynomial can be downloaded here: :download:`HurwitzForm_M05.txt <HurwitzForm_M05.txt>`

-------------------------------------------------------------------------------------------------------------------------------------

| Project page created: 15/10/2024

| Project contributors: Leonie Kayser, Andreas Kretschmer, Simon Telen

| Corresponding author of this page: Leonie Kayser, kayser@mis.mpg.de

| Software used: Julia (Version 1.11.1), Macaulay2 (Version 1.24.05)

| System used: MacBook Pro with Intel Core i7 processor at 2.6 GHz, 16 GB RAM

| License for code of this project page: MIT License (https://spdx.org/licenses/MIT.html)

| License for all other content of this project page (text, images, …): CC BY 4.0 (https://creativecommons.org/licenses/by/4.0/)

| Last updated 21/10/2024