using HomotopyContinuation, DynamicPolynomials, LinearAlgebra, Polynomials, Distributions, StatsBase, Combinatorics, Random

#This code computes the number of solutions to a system of polynomial equations obtained by looking at critical points of a function L, see below. 
# It contains three functions:
# -NumCrit(d,k,n): main function 
# -Sequential evaluation of the first one
# -Polynomial interpolation of the points obtained

#bounds on values of random entries of divisors 
A=0.50;
B=1.50;

#Counts the number of critical points of L=sum_{i=1}^{d} c_i*log(det(M_i)) with c_i random real numbers and M_i is matrix obtained by stacking (I,x) with a random real (n-k,n) matrix 
#(which is Euler characteristic of Gr(k,n) minus d+1 divisors (one at infinity) evaluated at d >= 0)
function NumCrit(d,k,n)
    if d==0
        return 1 #Gr(k,n)\1SchubertDivisor = C^{k(n-k)} 
    else
        #coordinates on Gr(k,n) = variables of the system
        @var x[1:k,1:n-k];

        #element in Gr(k,n) in fixed chart: parametrisation
        C=hcat(Matrix{Float64}(I, k, k),x);

        #parameters c_i + auxiliary variables for solving start parameters if d<k(n-k)
        @var c[1:max(d,k*(n-k))];

        #build Array of length d with entries M_i: C stacked with random real (n-k,n)-matrices with values in [A,B]
        #for generic Schubert divisors

        M=vcat(C, rand(Uniform(A,B), n-k,n));
        for i in 2:max(d,k*(n-k))
            M=cat(M,vcat(C, rand(Uniform(A,B),n-k,n));dims=3);
        end
        
        #define function L
        L = sum(c[i]*log(det(M[:,:,i])) for i in 1:max(d,k*(n-k)));
        
        #define system in HomotopyContinuation given by maximizing L
        system = System(differentiate(L,vec(x)), parameters = c);

        #solve system with monodromy, guessing a starting parameters c for random x (important that |c| >= |x|)
        monodromy_result = monodromy_solve(system);
        #starting point
        c_star = parameters(monodromy_result);
        startsols = solutions(monodromy_result);
        #target parameters c : random d component + zeros 
        c_star_prime = vcat(rand(Uniform(A,B),d),zeros(max(0,k*(n-k)-d)));
        #solve via homotopy from starting point to target parameters
        cp_result = solve(system, startsols; start_parameters = c_star, target_parameters = c_star_prime,threading = true);

        #return number of non-singular solutions
        return length(solutions(cp_result; only_nonsingular = true))   
    end
end

#Computes ML degree of the complement of Gr(n-2,n) by N divisors forming a cycle of lines in P^(N-1)

function Cycle(N,n)

    k=2;
    
    #coordinates on Gr(k,n) = variables of the system
    @var x[1:n-k,1:k];

    #element in Gr(n-k,n) in fixed chart: parametrisation
    C=hcat(x,Matrix{Float64}(I, n-k, n-k));

    #parameters c_i + auxiliary variables for solving start parameters if N <= k(n-k)
    @var c[1:max(N-1,k*(n-k))];
   

    # endpoints of lines in P^(n-1) collected a scolumns of Z
    Z= hcat(vcat([1],zeros(n-1)),vcat([0,1],zeros(n-2)),rand(Uniform(A,B),n,N-2));
    # hcat(Matrix{Float64}(I, 4, 4),[-1,1,-1,1],[-4,3,-2,1],rand(Uniform(A,B),4));

    M=vcat(C, transpose(cat(Z[:,1],Z[:,N];dims=2)));
    for i in 2:(N-1)
        M=cat(M,vcat(C, transpose(cat(Z[:,i],Z[:,i+1];dims=2)));dims=3);
    end

    for i in N:(k*(n-k))
        M=cat(M,vcat(C, rand(Uniform(A,B),k,n));dims=3);
    end
    
    #define function L
    L = sum(c[i]*log(det(M[:,:,i])) for i in 1:max(N-1,k*(n-k)));
    
    #define system in HomotopyContinuation given by maximizing L  
    system = System(differentiate(L,vec(x)),parameters=c);

    #solve system with monodromy, guessing a starting parameters c for random x (important that |c| >= |x|)
    monodromy_result = monodromy_solve(system);
    #starting point
    c_star = parameters(monodromy_result);
    startsols = solutions(monodromy_result);
    #target parameters c : random d component + zeros 
    c_star_prime = vcat(rand(Uniform(A,B),N-1),zeros(max(0,k*(n-k)-N+1)));
    #solve via homotopy from starting point to target parameters
    cp_result = solve(system, startsols; start_parameters = c_star, target_parameters = c_star_prime);

    #return number of non-singular solutions
    return length(solutions(cp_result; only_nonsingular = true))   
end


#outputs minor (matrix) of A containing columns A[i] for i nonzero in p; p vectors of zeros and ones
function minor(A,p)
    k, n = size(A)
    B = zeros(k,1)
    i=1
    for j in 1:n
        if p[j]==1
            B=hcat(B,A[:,j])
            i=i+1
        end
    end
    B=B[:,2:k+1]
    return B
end

#computes the ML degree of the complement of Gr(k,n) by all Plucker hyperplanes
function Plucker(k,n)

    N=binomial(n,k);
    #coordinates on Gr(k,n) = variables of the system
    @var x[1:k,1:n-k];

    #element in Gr(k,n) in fixed chart: parametrisation
    C=hcat(Matrix{Float64}(I, k, k),x);

    #parameters c_i + auxiliary variables for solving start parameters if d<k(n-k)
    @var c[1:N];

    # P array of lenth N of vectors containing indeces of minors
    p=vcat(ones(k),zeros(n-k));
    P=unique(Combinatorics.permutations(p));

    #define function L: sum over all minors except the one associated to p (we fixed a chart where minor(C,p)=1 )
    L = c[N]*log(det(vcat(C, rand(Uniform(A,B), n-k,n))));
    for i in 2:N
        L=L+c[i-1]*log(det(minor(C,P[i])))
    end
    
    #define system in HomotopyContinuation given by maximizing L
    system = System(differentiate(L,vec(x)), parameters = c);

    #solve system with monodromy, guessing a starting parameters c for random x (important that |c| >= |x|)
    monodromy_result = monodromy_solve(system);
    #starting point
    c_star = parameters(monodromy_result);
    startsols = solutions(monodromy_result);
    #target parameters c : random d component + zeros 
    c_star_prime = vcat(rand(Uniform(A,B),N-1),zeros(1));
    #solve via homotopy from starting point to target parameters
    cp_result = solve(system, startsols; start_parameters = c_star, target_parameters = c_star_prime,threading = true);

    #return number of non-singular solutions
    return length(solutions(cp_result; only_nonsingular = true))   
end





#gives count in number of solutions for (d,k,n) on N evaluations; useful to get statistically significant results
F(d,k,n,N) = countmap([NumCrit(d,k,n) for i in 1:N])
G(N,n,L)=countmap([Cycle(N,n) for i in 1:L])
H(k,n,L)= countmap([Plucker(k,n) for i in 1:L])

#Interpolation of NumCrit(d) as a function of d with a degree k(n-k) polynomial taking values on the range 0:k(n-k)
#(It is the Euler characteristic of Gr(k,n) without d+1 Schuber divisors (normalised such that it is monic, i.e. with integer coefficients)
#note that the actual Euler characteristic is divided by the absolute value of the constant coefficient since |chi(0)|=1)
function chi(k,n) 
    ds = 0:k*(n-k);
    chis = @. NumCrit(ds,k,n);
    p = Polynomials.fit(ds, chis, k*(n-k)); map(x -> round(x,digits=8) , p);
    return map(x -> round(Int64, x) , p/p[k*(n-k)])
end


#############################################################################################################################

# chi(2,4) = 1//12*( 12 - 30*x + 23*x^2 - 6*x^3 + x^4 ) 

# x=    0       1       2       3       4       5       6       7       8       9       10
# chi=  1      0       1       4       11      26      55      106     189     316     501

############################################################################################################################

# chi(2,5) = 1//144 * (144 - 444*x + 508*x^2 - 267*x^3 + 67*x^4 - 9*x^5 + x^6) 
# interpolating [1,0,0,1,10,46,150] :  ,  7->400 , 8->931

# chi=  1     0       0       1       10         46             150              400            931
#eval.        20/20  200/200  38/40   64/100     405/1000       340/1000          
#other                                           47: 254/1000   149: 308/1000    

#F(7,2,5,1000) : 397 (159) 398 (180) 399 (182) 400 (99) /1000

#F(8,2,5,1000) : 927 => 111, 926 => 103, 928 => 97, 929 => 81, 925 => 75, 924 => 66, 923 => 63, 930 => 58, 922 => 56, 921 => 39, 918 => 26, 920 => 24, 919 => 21, 917 => 20, 931 => 14, 