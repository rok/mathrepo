using LinearAlgebra, Distributions

#bound on abs value of random numbers in matrices
M=500.00;
#number of random matrices
N=100000;

#Computes the distinct sign patterns of N points in Gr(k,n) without d divisors
function Sign(d,k,n)

    # Array of divisors in Gr(k,n)
    D=rand(Uniform(-M,M),d,k,n);
    
    #Array of random matrices in Gr(n-k,n)
    C = rand(Uniform(-M,M),N,n-k,n);
    
    #initialize Array of non-reduntant sign patterns modulo +/-1 with no 0 elements
    S=zeros(Int64,(d,1));
    S[:,1]=[Int64(sign(det(vcat(D[j,:,:],C[1,:,:])))) for j in 1:d];

    for i in 2:N
        # i-th sign pattern
        Snew = [Int64(sign(det(vcat(D[j,:,:],C[i,:,:])))) for j in 1:d];

        #continue if Snew contains a 0
        if 0 in Snew
            @goto label1
        end

        #continue if Snew is already contained in S (modulo +/-) 
        for j in 1:size(S)[2]
            if !(0 in Snew + S[:,j]) || (Snew + S[:,j])== zeros(Int64,d)
                @goto label1
            end
        end

        #add Snew to S 
        S = cat(S,Snew,dims=2);

        @label label1
    end

    return size(S)[2] 
end

#gives DxL matrix of L evaluations of Sign(d,k,n) for d in range ds, where D=length(ds)
function SignVec(ds,k,n,L)
    V = zeros(Int64,length(ds),L);
    for d in ds 
        for j in 1:L
        V[d+1-extrema(ds)[1],j]= Sign(d,k,n);
        end
    end
    return V
end

#gives a vector of length ds containing maximum number of distinct sign patterns for d divisors in Gr(k,n)
# for d in range ds and L evaluations each
function MaxSignVec(ds,k,n,L)
    A=SignVec(ds,k,n,L);
    return [findmax(A[i,:])[1] for i in 1:size(A,1)]
end

# number of regions of m-1 planes in P^(n-1)
Zaslavski(m,n)=(binomial(n,m-1) + sum(binomial(n,i) for i in 0:m))/2


#Bernd: SignVec(4:11,2,4,10) :  
#16   16   16   16   16   16   16   16   16   16
#32   32   32   32   32   32   32   32   32   32
#60   58   64   60   62   60   62   60   62   60
#110  119  119  117  122  118  117  116  119  119
#175  198  213  211  220  211  216  208  179  215
#331  328  358  346  299  322  312  351  338  339
#470  509  499  482  505  499  474  531  526  492
#681  763  720  744  652  755  753  735  739  787  

#MaxSignVec(4:11,2,4,100): 8 16 32 63 113 191 303 451
#MaxSignVec(4:13,2,5,100): 8 16 32 64 128 256 494 885 1505 2449
#MaxSignVec(4:12,2,6,30):  8 16 32 64 128 256 512 1021 1994
#MaxSignVec(4:11,3,6,10):  8 16 32 64 128 256 512 1024
