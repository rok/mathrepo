using HypersurfaceRegions
using LinearAlgebra: det
using Combinatorics: powerset

@var a b c d e f
A = [1 a b c; a 1 d e; b d 1 f; c e f 1]
pr(A,K) = det(view(A,K,K))
F = Vector{Expression}([pr(A,K) for K in powerset([1 2 3 4], 2, 4)])
R = regions(F)

signs = unique([sign(X) for X in regions(R)])
open("pr4-regions.log", "w") do file
  for s in signs
    # Note that F and hence the entries of s are ordered according to
    # the canonical ordering used in the other programs. Prepend the
    # signs of the four positive singletons and the empty set.
    p = "+++++" * join([if i > 0 "+" else "-" end for i in s])
    n = length(filter(X -> sign(X) == s, regions(R)))
    println(file, p, ": ", n)
  end
end
