   evalcontfrac(n,l) = { local(x=0, V = contfrac(n,l)); 
                        forstep(i = #V, 1, -1, x = V[i] + if(x,1/x,0)); 
                        return(x);}


  