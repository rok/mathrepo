\\ Define commands

# termL(pol) = ’t*polcoef(pol,1,t);

\\ Computes terms of degree 1 in t
L(pol) = polcoef(pol,1,t);

# termQ(pol) = polcoef(pol,0,t);

\\ Computes terms of degree 0 in t
Q(pol) = polcoef(pol,0,t);

\\ Compute the reduced cubic in P2


\\ To find the j invariant, first make sure that the two curves in P3 are defined and that they have a common rational point at [0:0:0:1]. If the curves have a different rational point, then use the algorithm in Intersection_Quadrics.nb to ensure a coordinate transformation such that the rational point is at [0:0:0:1].

CubicRed(cur1,cur2) = L(cur1)*Q(cur2) - L(cur2)*Q(cur1)

\\ Now define cur1 and cur2. Compute CubicRed. 

\\ Define the elliptic curve from the reduced cubic
E = ellfromeqn(CubicRed(cur1,cur2));


\\ Now, initialize the elliptic curve as below
ec = ellinit(E);

\\ Compute the j invariant
ec.j