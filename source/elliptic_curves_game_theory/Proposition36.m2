-----------------------------------------------
--Calculations for the proof of Proposition 3.6
-----------------------------------------------

R = QQ[a_(1,1)..a_(2,2),b_(1,1)..b_(2,2),p_(1,1),p_(1,2),p_(2,1),p_(2,2)];

--Spohn variety V:

detM1 = (a_(2,1)-a_(1,1))*p_(1,1)*p_(2,1) + (a_(2,2)-a_(1,1))*p_(1,1)*p_(2,2) + (a_(2,1)-a_(1,2))*p_(1,2)*p_(2,1) + (a_(2,2)-a_(1,2))*p_(1,2)*p_(2,2);
detM2 = (b_(1,2)-b_(1,1))*p_(1,1)*p_(1,2) + (b_(2,2)-b_(1,1))*p_(1,1)*p_(2,2) + (b_(1,2)-b_(2,1))*p_(1,2)*p_(2,1) + (b_(2,2)-b_(2,1))*p_(2,1)*p_(2,2);

I = ideal(detM1,detM2);

--Spohn cubic C:

c_1 = (a_(1,1)-a_(2,2))*(b_(1,1)-b_(1,2)); 
c_2 = (a_(1,1)-a_(2,1))*(b_(2,2)-b_(1,1)); 
c_3 = (a_(1,2)-a_(2,2))*(b_(1,1)-b_(1,2)); 
c_4 = (a_(1,1)-a_(2,1))*(b_(2,2)-b_(2,1)); 
c_5 = (a_(1,2)-a_(2,2))*(b_(2,1)-b_(1,2));
c_6 = (a_(1,2)-a_(2,1))*(b_(2,2)-b_(2,1)); 
c_7 = (a_(1,2)-a_(2,1))*(b_(2,2)-b_(1,1))+(a_(1,1)-a_(2,2))*(b_(2,1)-b_(1,2));

f = c_1*p_(1,1)^2*p_(1,2) + c_2*p_(1,1)^2*p_(2,1) + c_3*p_(1,1)*p_(1,2)^2 + c_4*p_(1,1)*p_(2,1)^2 + c_5*p_(1,2)^2*p_(2,1) + c_6*p_(1,2)*p_(2,1)^2 + c_7*p_(1,1)*p_(1,2)*p_(2,1);
J = ideal(f);

---------------------------------------------------------------
--case (1)

CompV1 = decompose(I + ideal(a_(1,1)-a_(1,2)));
CompC1 = decompose(J + ideal(a_(1,1)-a_(1,2)));

--check if irred comp of V have proj dim 1/affine dim 2
--this corresponds to codim 3 here, since dimension goes down by 1 from the assumption
apply(#CompV1, q -> codim(CompV1#q)) 

--check if the elimination ideals of min primes of V are the min primes of C
#CompV1 == #CompC1
apply(#CompV1, q -> (eliminate(p_(2,2),CompV1#q) == CompC1#q))

--------------------------------------------------------------
--case (2)

CompV2 = decompose(I + ideal(a_(1,1)-a_(2,1)));
CompC2 = decompose(J + ideal(a_(1,1)-a_(2,1)));

--check if irred comp of V have proj dim 1/affine dim 2
--this corresponds to codim 3 here, since dimension goes down by 1 from the assumption
apply(#CompV2, q -> codim(CompV2#q)) 

--check if the elimination ideals of min primes of V are the min primes of C
#CompV2 == #CompC2
apply(#CompV2, q -> (eliminate(p_(2,2),CompV2#q) == CompC2#q))


--------------------------------------------------------------
--case (3)

CompV3 = decompose(I + ideal(a_(2,1)-a_(2,2)));
CompC3 = decompose(J + ideal(a_(2,1)-a_(2,2)));

--check if irred comp of V have proj dim 1/affine dim 2
--this corresponds to codim 3 here, since dimension goes down by 1 from the assumption
apply(#CompV3, q -> codim(CompV3#q)) 

--check if the elimination ideals of min primes of V are the min primes of C
#CompV3 == #CompC3
eliminate(p_(2,2),CompV3#0) == CompC3#1
eliminate(p_(2,2),CompV3#1) == CompC3#0

--------------------------------------------------------------
--case (4)

CompV4 = decompose(I + ideal(b_(1,1)-b_(1,2)));
CompC4 = decompose(J + ideal(b_(1,1)-b_(1,2)));

--check if irred comp of V have proj dim 1/affine dim 2
--this corresponds to codim 3 here, since dimension goes down by 1 from the assumption
apply(#CompV4, q -> codim(CompV4#q)) 

--check if the elimination ideals of min primes of V are the min primes of C
#CompV4 == #CompC4
apply(#CompV4, q -> (eliminate(p_(2,2),CompV4#q) == CompC4#q))

--------------------------------------------------------------
--case (5)

CompV5 = decompose(I + ideal(b_(1,1)-b_(2,1)));
CompC5 = decompose(J + ideal(b_(1,1)-b_(2,1)));

--check if irred comp of V have proj dim 1/affine dim 2
--this corresponds to codim 3 here, since dimension goes down by 1 from the assumption
apply(#CompV5, q -> codim(CompV5#q)) 

--check if the elimination ideals of min primes of V are the min primes of C
#CompV5 == #CompC5
apply(#CompV5, q -> (eliminate(p_(2,2),CompV5#q) == CompC5#q))

--------------------------------------------------------------
--case (6)

CompV6 = decompose(I + ideal(b_(1,2)-b_(2,2)));
CompC6 = decompose(J + ideal(b_(1,2)-b_(2,2)));

--check if irred comp of V have proj dim 1/affine dim 2
--this corresponds to codim 3 here, since dimension goes down by 1 from the assumption
apply(#CompV6, q -> codim(CompV6#q)) 

--check if the elimination ideals of min primes of V are the min primes of C
#CompV6 == #CompC6
eliminate(p_(2,2),CompV6#0) == CompC6#1
eliminate(p_(2,2),CompV6#1) == CompC6#0

--------------------------------------------------------------
--case (7)

CompV7 = decompose(I + ideal(a_(1,2)-a_(2,2),b_(2,1)-b_(2,2)));
CompC7 = decompose(J + ideal(a_(1,2)-a_(2,2),b_(2,1)-b_(2,2)));

--check if irred comp of V have proj dim 1/affine dim 2
--this corresponds to codim 4 here, since dimension goes down by 2 from the assumption
apply(#CompV7, q -> codim(CompV7#q)) 

--check if the elimination ideals of min primes of V are the min primes of C
#CompV7 == #CompC7
eliminate(p_(2,2),CompV7#2) == CompC7#1
isSubset(CompC7#0, eliminate(p_(2,2),CompV7#0))
degree CompV7#0 
isSubset(CompC7#0, eliminate(p_(2,2),CompV7#1))
degree CompV8#0

--------------------------------------------------------------
--case (8)

CompV8 = decompose(I + ideal(a_(1,2)-a_(2,1),b_(1,2)-b_(2,1)));
CompC8 = decompose(J + ideal(a_(1,2)-a_(2,1),b_(1,2)-b_(2,1)));

--check if irred comp of V have proj dim 1/affine dim 2
--this corresponds to codim 4 here, since dimension goes down by 2 from the assumption
apply(#CompV8, q -> codim(CompV8#q)) 

--check if the elimination ideals of min primes of V are the min primes of C
#CompV8 == #CompC8
apply(#CompV8, q -> (eliminate(p_(2,2),CompV8#q) == CompC8#q))

--------------------------------------------------------------
--case (9)

AssumptionIdeal9 = ideal(a_(1,2)*(b_(1,2)-b_(2,2)) + a_(2,1)*(b_(2,2)-b_(2,1)) + a_(2,2)*(b_(2,1)-b_(1,2)),
                         a_(1,1)*(b_(2,2)-b_(1,2)) + a_(2,1)*(b_(1,1)-b_(2,2)) + a_(2,2)*(b_(1,2)-b_(1,1)),
                         a_(1,1)*(b_(2,2)-b_(2,1)) + a_(1,2)*(b_(1,1)-b_(2,2)) + a_(2,2)*(b_(2,1)-b_(1,1)));
codim AssumptionIdeal9

CompV9 = decompose(I + AssumptionIdeal9);
CompC9 = decompose(J + AssumptionIdeal9);

--check if irred comp of V have proj dim 1/affine dim 2
--this corresponds to codim 4 here, since dimension goes down by 2 from the assumption
apply(#CompV9, q -> codim(CompV9#q)) 

--check if the elimination ideals of min primes of V are the min primes of C
#CompV9 == #CompC9
apply(#CompV9, q -> (eliminate(p_(2,2),CompV9#q) == CompC9#q))

--------------------------------------------------------------
--case (10)

AssumptionIdeal10 = ideal(a_(1,1)*(b_(1,2)-b_(2,1)) + a_(1,2)*(b_(2,1)-b_(2,2)) + a_(2,1)*(b_(2,2)-b_(1,2)),
                          a_(1,2)*(b_(1,1)-b_(2,1)) + a_(2,1)*(b_(1,2)-b_(1,1)) + a_(2,2)*(b_(2,1)-b_(1,2)),
                          a_(1,1)*(b_(1,1)-b_(2,1)) + a_(2,1)*(b_(2,2)-b_(1,1)) + a_(2,2)*(b_(2,1)-b_(2,2)));
codim AssumptionIdeal10

CompV10 = decompose(I + AssumptionIdeal10);
CompC10 = decompose(J + AssumptionIdeal10);

--check if irred comp of V have proj dim 1/affine dim 2
--this corresponds to codim 4 here, since dimension goes down by 2 from the assumption
apply(#CompV10, q -> codim(CompV10#q)) 

--check if the elimination ideals of min primes of V are the min primes of C
#CompV10 == #CompC10
eliminate(p_(2,2),CompV10#0) == CompC10#3
eliminate(p_(2,2),CompV10#1) == CompC10#0
eliminate(p_(2,2),CompV10#2) == CompC10#1
eliminate(p_(2,2),CompV10#3) == CompC10#2

--------------------------------------------------------------
--case (11)

AssumptionIdeal11 = ideal(a_(1,2)*(b_(2,2)-b_(2,1)) + a_(2,1)*(b_(1,2)-b_(2,2)) + a_(2,2)*(b_(2,1)-b_(1,2)),
                          a_(1,1)*(b_(2,2)-b_(2,1)) + a_(2,1)*(b_(1,1)-b_(2,2)) + a_(2,2)*(b_(2,1)-b_(1,1)),
                          a_(1,1)*(b_(2,2)-b_(1,2)) + a_(1,2)*(b_(1,1)-b_(2,2)) + a_(2,2)*(b_(1,2)-b_(1,1)));
codim AssumptionIdeal11

CompV11 = decompose(I + AssumptionIdeal11);
CompC11 = decompose(J + AssumptionIdeal11);

--check if irred comp of V have proj dim 1/affine dim 2
--this corresponds to codim 4 here, since dimension goes down by 2 from the assumption
apply(#CompV11, q -> codim(CompV11#q)) 

--check if the elimination ideals of min primes of V are the min primes of C
#CompV11 == #CompC11
eliminate(p_(2,2),CompV11#0) == CompC11#2
eliminate(p_(2,2),CompV11#1) == CompC11#0
eliminate(p_(2,2),CompV11#2) == CompC11#1
eliminate(p_(2,2),CompV11#3) == CompC11#3

--------------------------------------------------------------
--case (12)

AssumptionIdeal12 = ideal(a_(1,1)*(b_(1,2)-b_(2,1)) + a_(1,2)*(b_(2,2)-b_(1,2)) + a_(2,1)*(b_(2,1)-b_(2,2)),
                          a_(1,2)*(b_(1,1)-b_(1,2)) + a_(2,1)*(b_(2,1)-b_(1,1)) + a_(2,2)*(b_(1,2)-b_(2,1)),
                          a_(1,1)*(b_(1,1)-b_(2,1)) + a_(1,2)*(b_(2,2)-b_(1,2)) + a_(2,1)*(b_(2,1)-b_(1,1)) + a_(2,2)*(b_(1,2)-b_(2,2)));
codim AssumptionIdeal12

CompV12 = decompose(I + AssumptionIdeal12);
CompC12 = decompose(J + AssumptionIdeal12);

--check if irred comp of V have proj dim 1/affine dim 2
--this corresponds to codim 4 here, since dimension goes down by 2 from the assumption
apply(#CompV12, q -> codim(CompV12#q)) 

--check if the elimination ideals of min primes of V are the min primes of C
#CompV12 == #CompC12
apply(#CompV12, q -> (eliminate(p_(2,2),CompV12#q) == CompC12#q))



