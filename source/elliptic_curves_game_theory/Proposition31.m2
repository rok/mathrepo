----------------------------------------------------
--Calculations from the proof of Proposition 3.1
----------------------------------------------------

R = QQ[a_(1,1)..a_(2,2),b_(1,1)..b_(2,2),x,y,z];

c_1 = (a_(1,1)-a_(2,2))*(b_(1,1)-b_(1,2)); 
c_2 = (a_(1,1)-a_(2,1))*(b_(2,2)-b_(1,1)); 
c_3 = (a_(1,2)-a_(2,2))*(b_(1,1)-b_(1,2)); 
c_4 = (a_(1,1)-a_(2,1))*(b_(2,2)-b_(2,1)); 
c_5 = (a_(1,2)-a_(2,2))*(b_(2,1)-b_(1,2));
c_6 = (a_(1,2)-a_(2,1))*(b_(2,2)-b_(2,1)); 
c_7 = (a_(1,2)-a_(2,1))*(b_(2,2)-b_(1,1))+(a_(1,1)-a_(2,2))*(b_(2,1)-b_(1,2));

f = c_1*x^2*y + c_2*x^2*z + + c_3*x*y^2 + c_4*x*z^2 + c_5*y^2*z + c_6*y*z^2 + c_7*x*y*z;    

--Examine the 5 lines through the intersection points of V(x),V(y),V(z) with the cubic
--to see if they are contained in it

------------------------------------------------------

f1 = sub(f, {y=>-(c_1/c_3)*x});

den1 = denominator(f1);
num1 = numerator(f1);
P1 = primaryDecomposition ideal(num1);

------------------------------------------------------

f2 = sub(f, {z=>-(c_2/c_4)*x});

den2 = denominator(f2);
num2 = numerator(f2);
P2 = primaryDecomposition ideal(num2);

-----------------------------------------------------

f3 = sub(f, {z=>-(c_5/c_6)*y});

den3 = denominator(f3);
num3 = numerator(f3);
P3 = primaryDecomposition ideal(num3);

--examine long factor:

(M3,C3) = coefficients ((first entries gens P3#(#P3-1))#0, Variables => {x,y,z});
d3 = C3_(0,0);
e3 = C3_(1,0);
J3 = ideal(d3,e3);
D3 = decompose J3;      
I9 = D3#12; 
I10 = D3#13;

--test if assumption c_6 nonzero is necessary:

--if c_6=0:
--a_(1,2)=a_(2,1):
decompose(I9 + ideal(a_(1,2)-a_(2,1))) 
decompose(I10 + ideal(a_(1,2)-a_(2,1))) 
--b_(2,2)=b_(2,1):
decompose(I9 + ideal(b_(2,2)-b_(2,1))) 
decompose(I10 + ideal(b_(2,2)-b_(2,1))) 

---------------------------------------------------

f4 = sub(f, {z=>-(c_2/c_4)*x-(c_5/c_6)*y});

den4 = denominator(f4);
num4 = numerator(f4);
P4 = primaryDecomposition ideal(num4);

--examine long factor:

(M4,C4) = coefficients ((first entries gens P4#(#P4-1))#0, Variables => {x,y,z});
d4 = C4_(0,0);
e4 = C4_(1,0);
J4 = ideal(d4,e4);
D4 = decompose J4;
I11 = D4#12;
I12 = D4#13;

--test if assumptions c_4,c_6 nonzero are necessary:

--if c_6=0: same as below for f5
--if c_4=0:
--a_(1,1)=a_(2,1) is covered by case (2)
--b_(2,2)=b_(2,1): then c_6=0. 

--------------------------------------------------

f5 = sub(f, {z=>-((c_1*c_5)/(c_3*c_6))*x-(c_5/c_6)*y});
den5 = denominator(f5);
num5 = numerator(f5);
P5 = primaryDecomposition ideal(num5);

--examine long factor:

(M5,C5) = coefficients ((first entries gens P5#(#P5-1))#0, Variables => {x,y,z});
d5 = C5_(0,0);
e5 = C5_(1,0);
J5 = ideal(d5,e5);
D5 = decompose J5; 
I11 = D5#6;
I12 = D5#7;

--test if assumptions c_3,c_6 nonzero are necessary:

--if c_3=0:
--b_(1,1)=b_(1,2) is covered by case (4)
--a_(1,2)=a_(2,2):
decompose(I11 + ideal(a_(1,2)-a_(2,2))) 
decompose(I12 + ideal(a_(1,2)-a_(2,2))) 

--if c_6=0:
--a_(1,2)=a_(2,1):
decompose(I11 + ideal(a_(1,2)-a_(2,1))) 
decompose(I12 + ideal(a_(1,2)-a_(2,1))) 
--b_(2,2)=b_(2,1):
decompose(I11 + ideal(b_(2,2)-b_(2,1))) 
decompose(I12 + ideal(b_(2,2)-b_(2,1))) 



