newPackage(
        "LyapunovModel",
        Version => "1", 
        Date => "27 October 2021",
        Authors => {
            {Name => "Roser Homs Pons", 
                Email => "roser.homs@tum.de" 
                },
            {Name => "Philipp Dettling", 
                Email => "philipp.dettling@tum.de" 
                },
            {Name => "Carlos Amendola", 
                Email => "amendola@math.tu-berlin.de" 
                },
            {Name => "Mathias Drton", 
                Email => "mathias.drton@tum.de" 
                },
            {Name => "Niels Richard Hansen", 
                Email => "niels.r.hansen@math.ku.dk" 
                }
            },
        Headline => "tools for computations regarding identifiability of Lyapunov models",
        PackageExports => {"GraphicalModels"}
        )

export {"commutationMatrix",
    "vec",
    "lyapunovData",
    "lyapunovDataPD",
    "lyapunovEquations",
    "ASigma",
    "ASigmaPD",
    "ASigmaGraph",
    "ASigmaGraphPD", 
    "principalMinors",
    "detPrincipalMinors",
    "prodPrincipalMinors",
    "extractFactors",
    "areAllFactorsPpalMinors",
    "sVarName",
    "lVarName",
    "pVarName",
    "sVarNamePD",
    "lVarNamePD",
    "pVarNamePD",
    "kernelPattern",
    "pattern"
    }



--auxiliar function to get symbols properly
toSymbol = (p) -> (
     if instance(p,Symbol) then p
     else
     if instance(p,String) then getSymbol p
     else
     error ("expected a string or symbol, but got: ", toString p))


--retrieve basic data from graph: ring, matrix M, matrix Sigma
lyapunovData = method(Dispatch=>Thing, Options=>{sVarName=>"s", lVarName=>"m", 
	  pVarName=>"m"})
   
lyapunovData Digraph :=  Sequence => opts -> (G) -> (
s:= toSymbol opts.sVarName;
m:= toSymbol opts.lVarName;
R:=gaussianRing(digraphTranspose(G),lVariableName =>m,pVariableName =>m,sVariableName=>s);
L:=directedEdgesMatrix R;
P:=bidirectedEdgesMatrix R;
M:=L+P;
S:=covarianceMatrix R;
(R,M,S)
)

--retrieve basic data from graph: ring, matrix M, matrix Sigma in variables l
lyapunovDataPD = method(Dispatch=>Thing, Options=>{sVarNamePD=>"l", lVarNamePD=>"m", 
	  pVarNamePD=>"m"})
   
lyapunovDataPD Digraph :=  Sequence => opts -> (G) -> (
l:= toSymbol opts.sVarNamePD;
m:= toSymbol opts.lVarNamePD;
R:=gaussianRing(digraphTranspose(G),lVariableName =>m,pVariableName =>m,sVariableName=>l);
L:=directedEdgesMatrix R;
P:=bidirectedEdgesMatrix R;
M:=L+P;
S:=covarianceMatrix R;
SL:=mutableMatrix S;
n:=numcols S;
for i to n-1 do for j from i+1 to n-1 do SL_(i,j)=0;
SL=matrix SL;
S=SL*transpose(SL);
(R,M,SL,S)
)

--compute Lyapunov equations
lyapunovEquations=(R,M,S,C)-> I:=ideal(M*S+S*transpose(M)+C);

------------------------------------------------------------------------
-- Matrix representation of the Lyapunov equation                     --
------------------------------------------------------------------------

--Commutation matrix
commutationMatrix=mm->(
H:=id_(QQ^mm);
sum(0..mm-1,j->sum(0..mm-1,i->H_{i}*H^{j}**H_{j}*H^{i}))
);

-- Vectorization of a matrix
vec=M-> vector flatten entries transpose M;

-- A(\Sigma)
ASigma= G->(
(R,M,S):=lyapunovData G;
RS:=coefficientRing(R)[select(gens R, v-> first baseName v == R.gaussianRingData#sVar)];
Sigma:=sub(S,RS);
mm:=numcols Sigma;
A:=Sigma**id_(RS^mm)+(id_(RS^mm)**Sigma)*commutationMatrix mm;
Cov:=mutableMatrix S;
for i to mm-1 do for j from i+1 to mm-1 do Cov_(i,j)=0;
Cov=matrix Cov;
Q:=positions(entries vec Cov,i->i!=0);
submatrix(A,Q,)
);

-- A(\Sigma) with variable l
ASigmaPD= G->(
(R,M,SL,S):=lyapunovDataPD G;
RS:=coefficientRing(R)[select(gens R, v-> first baseName v == R.gaussianRingData#sVar)];
Sigma:=sub(S,RS);
mm:=numcols Sigma;
A:=Sigma**id_(RS^mm)+(id_(RS^mm)**Sigma)*commutationMatrix mm;
--now comes test
Cov:=mutableMatrix S;
for i to mm-1 do for j from i+1 to mm-1 do Cov_(i,j)=0;
Cov=matrix Cov;
Q:=positions(entries vec Cov,i->i!=0);
submatrix(A,Q,)
);

-- A(\Sigma)_S directly from a graph
--ASigmaGraph=(G,S,M)->(
--A:=ASigma(G);
--P:=positions(entries vec M, i->i!=0);
--submatrix(A,,P)
--);

pattern= G->(
n:=length vertices G;
positions(entries vec (id_(ZZ^n)+transpose(adjacencyMatrix digraph(sort vertices G,edges G))), i->i!=0)
--positions(entries vec (id_(ZZ^n)+adjacencyMatrix digraph(sort vertices G,edges G)), i->i!=0)
);

ASigmaGraph=G->(
    A:=ASigma(G);
    A_(pattern G)
    );


ASigmaGraphPD=G->(
    A:=ASigmaPD(G);
    A_(pattern G)
    );


kernelPattern= G->(
n:=length vertices G;
positions(entries vec (id_(ZZ^n)+transpose(adjacencyMatrix G)), i->i==0)
);

-- A(\Sigma)_S directly from a graph with variables l
--ASigmaGraphPD=(G,S,M)->(
--A:=ASigmaPD(G);
--P:=positions(entries vec M, i->i!=0);
--Cov:=mutableMatrix S;
--mm:=numcols S;
--for i to mm-1 do for j from i+1 to mm-1 do Cov_(i,j)=0;
--Cov=matrix Cov;
--Q:=positions(entries vec Cov,i->i!=0);
--submatrix(A,Q,P)
--);

------------------------------------------------------------------------
-- Functions related to computations of minimal minors                --
------------------------------------------------------------------------


-- compute list of ppal minors 
principalMinors=M->(
--M=sub(M,QQ[support M]);
mm:=numcols M;
ss:=drop(subsets mm,-1);
for s in ss list submatrix'(M,s,s)
);

-- compute list of determinants of ppal minors 
detPrincipalMinors=M->(
L:=principalMinors M;
for l in L list det l
);

-- compute product of determinants of ppal minors 
prodPrincipalMinors=M->(
ideal product (detPrincipalMinors M)
);


--extract factors of an element of class Product
extractFactors=factors->(
for i from 0 to #factors-1 list (toList factors#i)_0
);

--are all factors principal minors?
areAllFactorsPpalMinors= G->(
(R,M,S):=lyapunovData G;
AS:=ASigmaGraph (G);    
RS:=coefficientRing(R)[select(gens R, v-> first baseName v == R.gaussianRingData#sVar)];
S=sub(S,RS);
P:=detPrincipalMinors S;
AS=sub(AS,RS);
F:=extractFactors(factor det AS);
for f in F do (if not (member(f,P) or member(-f,P) or (degree f)_0==0) then return f;);
print("All factors are principal minors");
);
