--- STUDY OF NON-IDENTIFIABILITY OF NON-SIMPLE GRAPHS WITH 5 NODES
-- via A(Sigma) + trek conditions
-- via kernel of A(Sigma), which do not require trek conditions

restart 
loadPackage "LyapunovModel";
loadPackage "LyapunovModel";


load "graphlist.txt";
L=value get "graphlist.txt";

--Computing trek conditions for a graph in the list
i=6; --choose graph
G=digraph({1,2,3,4,5},L_i)
(R,M,S)=lyapunovData G;
T=time trekIdeal(R,G);
betti (trim T)
netList (trim T)_*
i, length L_i  --position in the list, number of edges

(R,M,S)=lyapunovData G;
AS=ASigmaGraph(G)
T=time trekIdeal(R,G);
i, length L_i, numrows AS, numcols AS, rank AS
betti (trim T)
toString (trim T)
netList (trim T)_*

--SUMMARY OF HOW MANY TREK CONDITIONS WE HAVE DEPENDING ON
-- NUMBER OF EDGES
--10 edges --E+|p|=10+5=15
--4.3 applies <=> no trek condition
-- 4.3 not sufficient:
-- 0 trek conditions: 67,63,55,54,53,52,51,48,47,46,45,44,43,42,41,
-- 40,39,38,37,36,35,34,33,32,31,30,29,28                               --28 satisfy 4.3
-- 4.3 already tells us it's non-identifiable
-- 1 trek condition: 66,65,64,62,61,60,59,58,57,56
-- 2 trek conditions: 50
-- 3 trek conditions: 49                                                --12 don't
                                                                        -- total: 40

--9 edges --E+|p|=9+5=14
--4.3 applies <=> at most one trek condition                             
-- 4.3 not sufficient
-- 0 trek conditions: 20,19
-- 1 trek condition: 27                                                  -- 3 satisfy 4.3
-- 4.3 already tells us it's non-identifiable
-- 2 trek conditions: 26                                                 -- 1 don't
                                                                         -- + 5 don't
									 
									 -- total: 9

--8 edges --E+|p|=8+5=13
--4.3 applies <=> at most two trek condition
-- 4.3 not sufficient
-- 0 trek conditions: 6                                                  -- 1 satisfies 4.3
                                                                         -- +2 
-- 4.3 already tells us it's non-identifiable
-- 3 trek conditions: 18                                                 -- 1 don't
                                                                         -- +9
									 -- total:13 

-- 7 edges
-- 1 satisfies 4.3
-- 2 don't
-- total:3

-- 6 edges
-- 2 satisfies 4.3
-- 1 don't
-- total:3

--From a total of 68 graphs,
-- 37 satisfy the equation in 4.3
-- 31 don't

-----------------------------------------------------------------------------
-- INCORPORATION OF TREK CONDITIONS FOR FULL RANK MATRICES AS -- 22 graphs
-----------------------------------------------------------------------------

------------------------------ 6 EDGES -------------------------------------- E+|p|=6+5=11

-- i=0
-- rank sub(AS,{s_(2,5)=>0,s_(2,4)=>0,s_(1,5)=>0,s_(1,4)=>0}) --10<11    15-4=11<=11  4.3 not sufficient

-- i=1
--rank sub(AS,{s_(4,5)=>0,s_(2,5)=>0,s_(2,4)=>0,s_(1,5)=>0,s_(1,4)=>0}) --10<11  15-5=10 

-- i=2
-- rank sub(AS,{s_(2,5)=>0,s_(2,4)=>0,s_(1,5)=>0,s_(1,4)=>0}) --10<11  15-4=11<=11 4.3 not sufficient

------------------------------ 7 EDGES --------------------------------------E+|p|=7+5=12

-- i=3
-- rank sub(AS,{s_(4,5)=>0,s_(2,5)=>0,s_(1,5)=>0}) --11<12  15-3=12<=12 4.3 not sufficient

-- i=4
-- rank sub(AS,{s_(2,5)=>0,s_(2,4)=>0,s_(1,5)=>0,s_(1,4)=>0}) --11<12  15-4=11

-- i=5
-- rank sub(AS,{s_(2,5)=>0,s_(2,4)=>0,s_(1,5)=>0,s_(1,4)=>0}) --11<12  15-4=11

------------------------------ 8 EDGES --------------------------------------E+|p|=8+5=13

-- i=7
--rank sub(AS,{s_(3,5)=>0,s_(2,5)=>0,s_(1,5)=>0}) --12<13  15-3=12

--i=8
--rank sub(AS,{s_(2,5)=>0,s_(1,5)=>0}) --12<13   15-2=13<=13 4.3 not sufficient

--i=9
--rank sub(AS,{s_(3,5)=>0,s_(2,5)=>0,s_(1,5)=>0}) --12<13  15-3=12

--i=10
--rank sub(AS,{s_(2,5)=>0,s_(1,5)=>0}) --12<13  15-2=13<=13 4.3 not sufficient

--i=11
--rank sub(AS,{s_(3,5)=>0,s_(2,5)=>0,s_(1,5)=>0}) --12<13 15-3=12

--i=12
--rank sub(AS,{s_(3,5)=>0,s_(2,5)=>0,s_(1,5)=>0}) --12<13 15-3=12

--i=13
--rank sub(AS,{s_(3,5)=>0,s_(2,5)=>0,s_(1,5)=>0}) --12<13 15-3=12

--i=14
--rank sub(AS,{s_(4,5)=>0,s_(2,5)=>0,s_(1,5)=>0}) --12<13 15-3=12

--i=15
--rank sub(AS,{s_(4,5)=>0,s_(2,5)=>0,s_(1,5)=>0}) --12<13 15-3=12

--i=16
--rank sub(AS,{s_(3,5)=>0,s_(2,5)=>0,s_(1,5)=>0}) --12<13 15-3=12

--i=17
--rank sub(AS,{s_(2,5)=>0,s_(2,4)=>0,s_(1,5)=>0,s_(1,4)=>0}) --11<13   15-4=11

------------------------------ 9 EDGES -------------------------------------- E+|p|=9+5=14

--i=21
--rank sub(AS,{s_(3,5)=>0,s_(2,5)=>0,s_(1,5)=>0}) --12<14  15-3=12

--i=22
--rank sub(AS,{s_(3,5)=>0,s_(2,5)=>0,s_(1,5)=>0}) --12<14  15-3=12

--i=23
--rank sub(AS,{s_(2,5)=>0,s_(1,5)=>0}) --13<14  15-2=13

--i=24
--rank sub(AS,{s_(2,5)=>0,s_(1,5)=>0}) --13<14 15-2=13

--i=25
--rank sub(AS,{s_(2,5)=>0,s_(1,5)=>0}) --13<14 15-2=13

-- COMPUTE RESTRICTED A(SIGMA) WITHOUT RECOMPUTING GAUSSIANRING  

G=digraph({1,2,3,4,5},L_0)
A=ASigma(G);

count=0;
for k in L do (G=digraph({1,2,3,4,5},k);
aux=A_(pattern G);
print(count,length k, numrows aux, numcols aux, rank aux);
count=count+1;)

count=0;
trickyGraphs={};
for k in L do (G=digraph({1,2,3,4,5},k);
aux=A_(pattern G);
if (numcols aux==rank aux) then trickyGraphs=append(trickyGraphs,count);
count=count+1;)

trickyGraphs
-- {0, 1, 2, 3, 4, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 21, 22, 23, 24, 25}
-- 6 edges: 0,1,2
-- 7 edges: 3,4,5
-- 8 edges: 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17
-- 9 edges: 21, 22, 23, 24, 25
length trickyGraphs -- 22, all with less than 15 cols in ASigma


-- COMPUTE RESTRICTED KERNEL WITHOUT RECOMPUTING GAUSSIANRING  
restart
loadPackage "LyapunovModel";
loadPackage "LyapunovModel";


G=digraph({1,2,3,4,5},L_0)
--compute matrix A(\Sigma)
A=ASigma G
(R,M,S)=lyapunovData G
--compute kernel
kern=syz A;


count=0;
for k in L do (GG=digraph({1,2,3,4,5},k);
HG=kern^(kernelPattern GG);
print(count,length k, numcols HG, numrows HG, rank HG);
count=count+1;)

-- Print only those that require additional checking
count=0;
trickyGraphs={};
for k in L do (GG=digraph({1,2,3,4,5},k);
HG=kern^(kernelPattern GG);
if (numrows HG==rank HG) then trickyGraphs=append(trickyGraphs,count);
count=count+1;)

length trickyGraphs -- 0

