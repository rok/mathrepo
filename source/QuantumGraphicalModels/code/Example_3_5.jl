# compute ideal for QCMI variety of 3-chain graph
# this is code for Example 3.5 in "Algebraic Geometry of Quantum Graphical Models"
# by Eliana Duarte, Dmitrii Pavlov and Maximilian Wiesmann
# uses Julia version 1.9.1, Oscar version 0.12.1, HomotopyContinuation version 2.8.1

using Oscar

# instantiate variables
R, vars = PolynomialRing(QQ, vcat(["x$i" for i in 1:10], ["y$i" for i in 1:10], ["z[$i,$j]" for (i,j) in Iterators.product(1:8, 1:8) if i <= j]))
x = vars[1:10] 
y = vars[11:20]
z = vars[21:length(vars)]
zDict = Dict([t for t in tuple.(["z[$i,$j]" for (i,j) in Iterators.product(1:8, 1:8) if i <= j], z)])

id = [1 0; 0 1]

# the function converts a matrix to an element of an Oscar MatrixSpace
function matrixToOscar(m, MS)
    new_m = MS()
    for (i,j) in Iterators.product(1:(size(m)[1]), 1:(size(m)[2]))
        new_m[i,j] = m[i,j]
    end
    return new_m
end

# create matrices Λ_AB and Λ_BC from variables
MS = MatrixSpace(R, 8, 8)
M1 = [x[1] 0 x[2] 0 x[4] 0 x[7] 0; 0 x[1] 0 x[2] 0 x[4] 0 x[7]; x[2] 0 x[3] 0 x[5] 0 x[8] 0; 0 x[2] 0 x[3] 0 x[5] 0 x[8]; x[4] 0 x[5] 0 x[6] 0 x[9] 0; 0 x[4] 0 x[5] 0 x[6] 0 x[9]; x[7] 0 x[8] 0 x[9] 0 x[10] 0; 0 x[7] 0 x[8] 0 x[9] 0 x[10]]
ΛAB = matrixToOscar(M1, MS)
M2 = [y[1] y[2] y[4] y[7] 0 0 0 0; y[2] y[3] y[5] y[8] 0 0 0 0; y[4] y[5] y[6] y[9] 0 0 0 0; y[7] y[8] y[9] y[10] 0 0 0 0; 0 0 0 0 y[1] y[2] y[4] y[7]; 0 0 0 0 y[2] y[3] y[5] y[8]; 0 0 0 0 y[4] y[5] y[6] y[9]; 0 0 0 0 y[7] y[8] y[9] y[10]]
ΛBC = matrixToOscar(M2, MS)

# get equations from parametrisation of ρ
ρ = ΛAB*ΛBC
equsRho = []
for (i,j) in Iterators.product(1:8, 1:8)
    if i <= j
        push!(equsRho, get(zDict, "z[$i,$j]", 0) - ρ[i,j])
    end
end

# get equations from commutation relation between Λ's
equsComm = []
Comm = ΛAB*ΛBC - ΛBC*ΛAB
for (i,j) in Iterators.product(1:8, 1:8)
    if i <= j
        push!(equsComm, Comm[i,j])
    end
end

# create ideal and implicitise
I = ideal(vcat(equsRho, equsComm))
J = eliminate(I, vcat(x,y))

# get rid of extraneous variables
S, znew = PolynomialRing(QQ, ["z_$i" for i in 1:36])
f = hom(R, S, vcat(Int.(zeros(length(x))), Int.(zeros(length(y))), znew))
Jnew = f(J)

# compute dimension of QCMI variety
Oscar.dim(Jnew)

# write equations into file
file = open("data/QCMI_3_chain.txt", "w")
for eq in gens(Jnew)
    println(file, repr(eq))
end
close(file)

# the degree of Jnew can be easily computed in Macaulay2 using the file QCMI_3_chain.txt