# compute equations for Gibbs variety of LSSM for 3-chain graph
# this is code for Example 4.2 in "Algebraic Geometry of Quantum Graphical Models"
# by Eliana Duarte, Dmitrii Pavlov and Maximilian Wiesmann
# uses Julia version 1.9.1, Oscar version 0.12.1, HomotopyContinuation version 2.8.1

using HomotopyContinuation
using LinearAlgebra
using Oscar

# create a basis for L=S^2⊗S^2⊗Id + Id⊗S^2⊗S^2
id = [1 0; 0 1]
X = [0 1; 1 0]
Z = [1 0; 0 -1]
single_basis = [id, X, Z]
basis1 = [kron(A, kron(B, id)) for A in single_basis for B in single_basis]
basis2 = [kron(id, kron(A, B)) for A in single_basis for B in single_basis]
basis = collect(Set(vcat(basis1, basis2)))


# try to find linear equations
d = 1

# create samples on the Gibbs manifold GM(L)
samples = []
nsamples = binomial(36 - 1 + 1, 1) + 20
for i in 1:nsamples
    sampled_parameters = randn(15)
    sample_matrix = exp(sum([sampled_parameters[j] * basis[j] for j in 1:15]))
    sample_vector = [sample_matrix[i,j] for i = 1:8 for j = i:8]
    samples = push!(samples,sample_vector)
end
samples_matrix = hcat(samples...)

# instantiate variables
x_strings = ["x$i$j" for i = 1:8 for j = i:8]
R, x = PolynomialRing(QQ,x_strings)

# set up Vandermonde matrix
exp_vecs = collect(exponents(sum(x)^d))
vdm_matrix = transpose(hcat([[prod(sv.^ev) for ev in exp_vecs] for sv in samples]...))
# make the matrix numerically stable
for i = 1:size(vdm_matrix,1)
    vdm_matrix[i,:] = vdm_matrix[i,:]/norm(vdm_matrix[i,:])
end

# this function computes the kernel of a matrix via QR decomposition
function kernel_qr(R::Array{T,2}, tol::Float64) where {T <: Number}
    n,m = size(R)

    if n>=m
        index = findall(x -> abs(x) < tol, [R[i,i] for i in 1:m])
        index2 = setdiff([i for i in 1:m], index)
        R_small = R[:,index2]

        kernel = zeros(eltype(R), length(index), m)

        for i = 1:length(index)
            kernel[i,index[i]] = one(eltype(R))
            kernel[i,index2] =  (-1) .* R_small\R[:,index[i]]
        end
        return kernel
    else
        index = findall(x -> abs(x) < tol, [R[i,i] for i in 1:n])
        index2 = setdiff([i for i in 1:m], index)
        R_small = R[:,index2]

        kernel = zeros(eltype(R), length(index), m)

        for i = 1:length(index)
            kernel[i,index[i]] = one(eltype(R))
            kernel[i,index2] =  (-1) .* R_small\R[:,index[i]]
        end
        R_new = [R; LinearAlgebra.pinv(kernel) * kernel]
        s = LinearAlgebra.svd(R_new, full = true)
        rk = sum(s.S .> tol)
        return  [kernel; s.Vt[rk + 1:end,:]]
    end
end

# compute the kernel of the Vandermonde matrix
N = transpose(kernel_qr(LinearAlgebra.qr(vdm_matrix).R, 1e-7))
# there are NO linear relations


# do the same for equations of degree 2
d = 2

samples = []
nsamples = binomial(36 - 1 + 2, 2) + 100
for i in 1:nsamples
    sampled_parameters = randn(15)
    sample_matrix = exp(sum([sampled_parameters[j] * basis[j] for j in 1:15]))
    sample_vector = [sample_matrix[i,j] for i = 1:8 for j = i:8]
    samples = push!(samples,sample_vector)
end

samples_matrix = hcat(samples...)

exp_vecs = collect(exponents(sum(x)^d))
vdm_matrix = transpose(hcat([[prod(sv.^ev) for ev in exp_vecs] for sv in samples]...))
for i = 1:size(vdm_matrix,1)
    vdm_matrix[i,:] = vdm_matrix[i,:]/norm(vdm_matrix[i,:])
end

N = transpose(kernel_qr(LinearAlgebra.qr(vdm_matrix).R, 1e-7))
# there are NO quadratic equations

# in principle, one can proceed with higher degrees, however, the computation time increases rapidly