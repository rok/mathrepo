# performs computations of Example 4.4 in "Geometry of Polynomial Neural Networks"
# by Kaie Kubjas, Jiayi Li and Maximilian Wiesmann
# uses Julia version 1.9.1, Oscar version 0.12.1

using Oscar

R, vars = polynomial_ring(QQ, vcat(["x$i" for i in 1:3], ["y$i" for i in 1:3], ["z$i" for i in 1:3], ["p$i" for i in 1:20]))
x = vars[1:3]
y = vars[4:6]
z = vars[7:9]
p = vars[end-19:end]

# auxiliary function to convert Julia matrix to Oscar matrix
function matrixToOscar(m, MS)
    new_m = MS()
    for (i,j) in Iterators.product(1:(size(m)[1]), 1:(size(m)[2]))
        new_m[i,j] = m[i,j]
    end
    return new_m
end

M = [x[1]^2 2*x[1]*x[2] 2*x[1]*x[3] x[2]^2 2*x[2]*x[3] x[3]^2; y[1]^2 2*y[1]*y[2] 2*y[1]*y[3] y[2]^2 2*y[2]*y[3] y[3]^2; z[1]^2 2*z[1]*z[2] 2*z[1]*z[3] z[2]^2 2*z[2]*z[3] z[3]^2]
M = matrixToOscar(M, matrix_space(R, 3, 6))

psi = minors(M, 3)

# compute ideal of M in Plücker embedding
I = ideal([p[i] - psi[i] for i in 1:length(psi)])
I = eliminate(I, vcat(x,y))   # this may take a couple of minutes

S, p = graded_polynomial_ring(QQ, ["p$i" for i in 1:20], Int.(ones(20)))
f = hom(R, S, vcat(Int.(zeros(length(vars) - length(p))), p))
I = f(I);

# compute degree of resulting variety
A, _ = quo(S, I)
hilbert_polynomial(A)
degree(A)

# coordinate change
T, y = polynomial_ring(QQ, ["y[$i,$j]" for i in 1:3 for j in 1:6])
Y = matrix_space(T, 3, 6)(y)
new_coords = minors(Y, 3)
g = hom(S, T, new_coords)
J = g(I);
Oscar.dim(J)