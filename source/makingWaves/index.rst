===============================
Making waves with Macaulay 2
===============================

| This page contains the code described in Section 6 of the paper:
| Marc Härkönen, Jonas Hirsch, Bernd Sturmfels: Making waves
| In: La matematica, 2 (2023) 3, p. 593-615
| DOI: `10.1007/s44007-023-00056-z <https://dx.doi.org/10.1007/s44007-023-00056-z>`_ ARXIV: https://arxiv.org/abs/2111.14045 CODE: https://mathrepo.mis.mpg.de/makingWaves

.. |code| replace:: :download:`makingWaves.m2 <makingWaves.m2>`

The file |code| implements the function ``wavePairs`` in Macaulay2, which computes the *wave pair variety* of a system of linear PDE with constant coefficients.

The :math:`\ell \times k` matrix :math:`A(\partial)` with entries in :math:`\mathbb{C}[\partial_1,\dotsc,\partial_n]` represents the PDE :math:`A \bullet \phi = 0`, whose solutions are :math:`k`-tuples of distributions :math:`\phi \in \mathcal{D}'(\mathbb{R}^n, \mathbb{C}^k)`.
For :math:`r = 0,\dotsc,n-1`, the wave pair variety :math:`\mathcal{P}_A^r \subseteq \operatorname{Gr}(n-r, n) \times \mathbb{P}^{k-1}` consists of pairs :math:`(\pi, z)` such that :math:`A(y)z = 0` for all :math:`y \in \pi`.

If :math:`(\pi, z) \in \mathcal{P}_A^r` such that :math:`\dim_\mathbb{R} (\pi \cap \mathbb{R}^n) = \dim_\mathbb{C} \pi`, then there is a real :math:`n-r \times n` matrix :math:`L` whose rows span :math:`\pi`, and the wave

.. math::
   \phi(x) := \delta(Lx) \cdot z

is a solution to the PDE represented by :math:`A(\partial)` for any distribution :math:`\delta \colon \mathbb{R}^{n-r} \to \mathbb{C}^k`.

.. toctree::
  :maxdepth: 2

  implementation.rst
  example.rst

Project page created: 25/11/2021

Project contributors: Marc Härkönen, Jonas Hirsch, Bernd Sturmfels

Software used: Macaulay2 (v1.18)

System setup used: Dell XPS 13 7390 2-in-1 with Arch Linux (kernel 5.14.13), Processor 3,9 GHz Intel i7-1065G7, Memory 16 GB 4267 MHz LPDDR4, Graphics Intel Iris Plus Graphics G7.

Corresponding author of this page: Marc Härkönen, harkonen@gatech.edu
