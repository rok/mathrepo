==========================================================================================
D-Module Techniques for Solving Differential Equations in the Context of Feynman Integrals
==========================================================================================

| This page contains auxiliary files to the paper:
| Johannes Henn, Elizabeth Pratt, Anna-Laura Sattelberger, Simone Zoia: D-module techniques for solving differential equations in the context of Feynman integrals
| In: Letters in mathematical physics, 114 (2024) 3 , 87
| DOI: `10.1007/s11005-024-01835-79 <https://dx.doi.org/10.1007/s11005-024-01835-7>`_ ARXIV: https://arxiv.org/abs/2303.11105 CODE: https://mathrepo.mis.mpg.de/HessianDiscriminant

Abstract
----------

Feynman integrals are solutions to linear partial differential equations with polyno-
mial coefficients. Using a triangle integral with general exponents as a case in point,
we compare D-module methods to dedicated methods developed for solving differential
equations appearing in the context of Feynman integrals, and provide a dictionary of
the relevant concepts. In particular, we implement an algorithm due to Saito, Sturm-
fels, and Takayama to derive canonical series solutions of regular holonomic D-ideals,
and compare them to asymptotic series derived by the respective Fuchsian systems.

Code
----------
We implemented the SST algorithm for finding canonical series solutions to two-variable systems of partial differential equations.
This algorithm is a generalization of the Frobenius method for solving ODEs, and is described in Grobner Deformations of 
Hypergeometric Equations by Mutsumi Saito, Bernd Sturmfels, and Nobuki Takayama. 
The code is written in Sage and can be accessed `here <https://github.com/lizziepratt/CanonicalSeries>`_ in its most recent form, 
or downloaded directly: :download:`CanonComp.sage <CanonComp.sage>`



Project page created: 16/03/2023

Project contributors: Johannes Henn, Elizabeth Pratt, Anna-Laura Sattelberger, Simone Zoia

Software used: Python (Version 3.10.5), Sagemath (Version 9.3)

Corresponding author of this page: `Elizabeth (Lizzie) Pratt <https://lizziepratt.com>`_, epratt@berkeley.edu. 

Last updated 16/03/2023.